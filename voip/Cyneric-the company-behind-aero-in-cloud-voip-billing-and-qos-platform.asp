<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>a&euml;ro</title>
<style type="text/css">
body {
	background-color: #FFF;
	background-image: url(/css/fondo-2.jpg);
	background-repeat: no-repeat;
	background-position: center top;
	margin: 0px;
	padding: 0px;
}
</style>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body> 
<!--#include virtual="/includes/header.asp" -->
<div id="contenedortopinterior"><img src="/css/ban-aboutcyneric.png" width="961" height="147" vspace="30" /></div>
<div id="contenedor">
  <table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="654" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top"><h1>Cyneric, the company behind a&euml;ro</h1></td>
        </tr>
        <tr>
          <td align="left" valign="top"><div id="contenidostxt">
            <h2><strong> </strong>Since 1999 Cyneric has been a pioneer in the VoIP Platform space</h2>
            <p>At   <a href="http://www.cyneric.com" target="_blank"><strong>Cyneric</strong></a>, we have &nbsp;always strived to be &nbsp;leaders in our own domain.   &nbsp;Over a decade ago, we created one of the first full featured VoIP   Billing Systems. &nbsp;</p>
            <p>Since then we have evolved by building a remarkably   easy to use routing platform, &nbsp;load balancing system, QoS software and   more. &nbsp; &nbsp;</p>
            <p>Our primary goal has always been to ridge   the gap between the technology and the business user; Therefore,   &nbsp;<strong>Cyneric</strong> platform has prominently been &nbsp;one of the most user-friendly on   the market. &nbsp;</p>
            <p>In order to meet todays ever-changing demands of the voip   industry, <strong>Cyneric</strong> has delivered time and time again   a software that not only serves its core purpose but also helps telcos   make and save money. &nbsp;That&rsquo;s our speciality. &nbsp;Similarly with the   emergence of &nbsp;'' In Cloud '' business models, it only made sense for   <strong>Cyneric</strong> to go full steam a head and create an intuitive,   simple to navigate, but powerful &nbsp;Total Solution InCloud Platform &hellip;.   <strong>a&euml;ro</strong> InCloud.</p>
            <p><br />
            </p>
<h2>How is Cyneric helping a&euml;ro be the next generation InCloud VoIP   Platform?</h2>
<p>&nbsp;In   the 12 years that we have been in business we have almost seen it all.   &nbsp;Our clients, regardless if they were a startup or an established telco,   appreciate   our advice as well as our solid &nbsp;understanding of the industry. &nbsp;We   have &nbsp;applied all &nbsp;of that knowledge and experience in designing Aero   and making sure that it is a robust on-demand platform which will   delight you from day one</p>
<p align="center"><a href="/voip-platform-billing-quality-of-service/testdrive-ourr-trial-aero-in-cloud-voip-billing-and-qos-platform.asp"><img src="/css/ban-testdrive-hr.png"  border="0"    /></a></p>
          </div></td>
        </tr>
      </table></td>
      <td width="321" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" align="right" valign="top"><img src="/css/ban-download-7top-tips.png" width="146" height="82" hspace="11" /></td>
          </tr>
        <tr>
          <td align="center"><a href="/voip-platform-billing-quality-of-service/voip-billing-platform-for-voip-starups-companies.asp"><img src="/css/ban-ownbusiness-ticket.png" width="153" height="231" hspace="7" vspace="22"border="0" /></a></td>
          <td align="center"><a href="/voip-platform-billing-quality-of-service/voip-billing-platform-benefits-with-in-cloud-hosted-platform-for-retail-wholesa-voip-companies.asp"><img src="/css/ban-gotbusiness-ticket.png" width="153" height="231" hspace="7" vspace="22"border="0" /></a></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><a href="/voip-platform-billing-quality-of-service/testdrive-ourr-trial-aero-in-cloud-voip-billing-and-qos-platform.asp"></a></td>
          </tr>
      </table>
        <table border="0" align="center" cellpadding="11" cellspacing="0">
          <tr>
            <th scope="row"><a href="/base-top-features.pdf" target="_blank"><img src="/css/ban-download-top-features.png" width="153" height="231" border="0" /></a></th>
            <td><a href="/aero-general-flow.pdf" target="_blank"><img src="/css/ban-downloadgeneral-flow.png" width="153" height="231" border="0" /></a></td>
          </tr>
      </table></td>
    </tr>
  </table>  <p><img src="/css/banner-compatibilidad.png" width="977" height="139" vspace="15" /></p>
</div>
<!--#include virtual="/includes/footer.asp" -->

</body>
</html>
