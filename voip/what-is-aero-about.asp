<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>aero</title>
<style type="text/css">
body {
	background-color: #FFF;
	background-image: url(/css/fondo-2.jpg);
	background-repeat: no-repeat;
	background-position: center top;
	margin: 0px;
	padding: 0px;
}
</style>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body> 
<!--#include virtual="/includes/header.asp" -->
<div id="contenedortopinterior"><img src="/css/ban-about.png" width="961" height="147" vspace="30" /></div>
<div id="contenedor">
  <table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="654" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top"><h1>About  a&euml;ro</h1></td>
        </tr>
        <tr>
          <td align="left" valign="top"><div id="contenidostxt">
            <h2><strong>a&euml;ro</strong> is a complete carrier grade VoIP platform in the cloud</h2>
            <p>The flexibility  and robustness of a&euml;ro, allows you  to get the entire  infrastructure for your business at a very low cost. </p>
            <p>As your business grows,  you can get more ports and/or functionalities, allowing your business to be  more profitable.</p>
           <p><a href="/css/comparativa-modelos.jpg" class="fancybox"   title="Invest model Infraestructure comparative costs"><img src="/css/comparativa-modelos.jpg" width="555" border="0" /></a></p>
                  <p align="right"><a href="/css/comparativa-modelos.jpg" class="fancybox"  title="Invest model Infraestructure comparative costs">zoom comparative grid</a>&nbsp;&nbsp;</p>
            <h2>Top Features</h2>
            <ul type="disc">
              <li> Fully Integrated Carrier Grade Routing, QoS and Billing </li>
              <li> Cat 5 features *** (coming soon fully integrated multi tenant PBX) </li>
              <li> Automated Trouble Ticket System to providers and from customers </li>
              <li> LCR, LRN, NPA/NXX Routing </li>
              <li> Fully Redundant</li>
            </ul>
<p>&nbsp;</p>
<h2>Top Benefits for <strong>your</strong> <strong>business</strong>: </h2>
            <ul type="disc">
              <li> Cost effective way to run any size VoIP Telecom Business. </li>
              <li> Turn Key! &nbsp;Be up and running in matter of hours. </li>
              <li> Intuitive. &nbsp;No learning curve. </li>
              <li> Integrated dashboard (quick business overview, really)</li>
              <li> Maintenance Free, Infrastructure Free, Additional cost Free!!!            </li>
              </ul>
            <p>&nbsp;</p>
<h2>Now your VoIP <strong>operation can be enhanced </strong>and the <strong>costs</strong> of running it <strong>can  be reduced</strong>.</h2>
<p>Your <strong>staff can work efficiently</strong> and more comfortable, having the tools  they need; <br />
  <strong>a&euml;ro</strong> will bring certainty and results. That simple!</p>
<table width="100%" border="0" cellspacing="0" cellpadding="22">
  <tr>
    <td align="left" valign="top" bgcolor="#F3F2E9" scope="row"><h2>In our  valuable customers own words&hellip;</h2>
      <p>An Excellent&nbsp; <strong>turn key </strong>platform&nbsp; &hellip;Got VoIP</p><hr />
      <p>a&euml;ro helped us <strong>reduce</strong> our <strong>maintenance costs by  30%</strong>.&nbsp; The integrated<strong> QoS alerts</strong> saved us from <strong>loosing  traffic</strong> on numerous occasions!&nbsp; &hellip;USA  Teleport,</p><hr />
      <p>With the a&euml;roplatform we  have grown over 200% while <strong>increasing our infrastructure cost 0%</strong>.&nbsp; Thank You aero &hellip;ABP Telecom<br />
        <hr />
        Our telecom <strong>needs are unpredictable</strong>.&nbsp;  Aero helps us dynamically adjust the usage without costly infrastructure  investments &hellip;NTel<hr />
        The interface is <strong>user friendly</strong>.  We are a small operation and do  not have time nor resources for a long learning curve.&nbsp; Everything is is<strong> pretty much self  explanatory</strong>. &nbsp;&hellip;BBC Tel </p><hr />
      <p>        <strong>We change routing and providers often</strong>.&nbsp;  Aero lets us configure suppliers and test routes seamlessly.&nbsp; <strong>Love the support for LRN dipping</strong>!&nbsp; &hellip;Safesoft<br /><hr />
        We literally run our <strong>whole operation with aero and two people in the whole  company</strong>.&nbsp; Its that easy &hellip; that  intuitive&nbsp; &hellip;.Tel Telecom<hr />
        <strong>Loved</strong> the all in one <strong>dashboard</strong>.&nbsp; <strong>Integrated</strong> Routing, Billing, QoS, and invoicing  simplifies our life!&nbsp; Not to mention  makes it a lot more cost effective and <strong>maintenance free.&nbsp; Thanks Guys!!!</strong>&nbsp; &hellip; WeirdTel<hr />
        <strong>Love the QoS metrics</strong>, and real time call  troubleshooting.&nbsp; <strong>Intuitive</strong>, informative  and <strong>helpful</strong>.&nbsp; I haven&rsquo;t seen anything  like it yet!&nbsp;  Thanks aero !!! &hellip; Bob Telecom</p></td>
  </tr>
</table>
<br />
<br />
<h2>Learn about  benefits and solutions provided by  a&euml;ro, the complete voip business platform</h2>
<ul>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-Account-management-customers-providers-and-origin-calls.asp" class="fancybox fancybox.iframe">Account management, customers, providers and origin calls, more&hellip;</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-Billing-modules-features.asp"  class="fancybox fancybox.iframe" >Billing modules features, more...</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-Price-List-management.asp"  class="fancybox fancybox.iframe" >Price List management, more...</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/Top-features-Callrouting-management-for-voip-business.asp"  class="fancybox fancybox.iframe" >Callrouting management, more...</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-Control-Quality-of-service-from-your-suppliers-and-to-your-customers.asp" class="fancybox fancybox.iframe">Control, Quality of service from your suppliers and to your customers, more&hellip;</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-24-7-service-monitoring-module-protocols-and-procedures.asp"  class="fancybox fancybox.iframe">24/7 service monitoring module, protocols and procedures, more...</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-Customer-loyalty-customer-web-portal-on-line-product-offering-collection-services-and-more-using-our-multiple-payment-gateway-PayPal-2CheckOut-Credit-Card.asp" class="fancybox fancybox.iframe" >Customer loyalty, customer web portal, on-line product offering, collection services and more, using our multiple payment gateway, PayPal, 2CheckOut, Credit Card, etc., more&hellip;</a></li>
  <li><a href="/voip-platform-billing-quality-of-service/top-features-Large-number-of-reports-to-facilitate-decision-making-process.asp"  class="fancybox fancybox.iframe">Large number of reports, to facilitate decision-making process, more&hellip;</a></li>
</ul>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle" scope="row"><h2>Try our <strong>a&euml;ro platform  free for 15 days</strong>, <br />
      get <strong>24/7 support</strong>&nbsp;</h2>      <a href="/voip-platform-billing-quality-of-service/testdrive-ourr-trial-aero-in-cloud-voip-billing-and-qos-platform.asp"><img src="/css/ban-testdrive-hr.png" width="328" vspace="22" border="0" /><br />
      </a></td>
  </tr>
</table>
<h2>&nbsp;</h2>
<p></p>
          </div></td>
        </tr>
      </table></td>
      <td width="321" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" align="right" valign="top"><img src="/css/ban-download-7top-tips.png" width="146" height="82" hspace="11" /></td>
          </tr>
        <tr>
          <td align="center"><a href="/voip-platform-billing-quality-of-service/voip-billing-platform-for-voip-starups-companies.asp"><img src="/css/ban-ownbusiness-ticket.png" width="153" height="231" vspace="22" border="0" /></a></td>
          <td align="center"><a href="/voip-platform-billing-quality-of-service/voip-billing-platform-benefits-with-in-cloud-hosted-platform-for-retail-wholesa-voip-companies.asp"><img src="/css/ban-gotbusiness-ticket.png" width="153" height="231" vspace="22" border="0" /></a></td>
        </tr>
        <tr>
          <td colspan="2" align="center">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>  <p><img src="/css/banner-compatibilidad.png" width="977" height="139" vspace="15" /></p>
</div>
<!--#include virtual="/includes/footer.asp" -->

</body>
</html>
