<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Prestaciones y soluciones para Gesti&oacute;n de Cuentas, Usuarios Proveedores de Origen y Terminacion para negocios de telfonia VOIP</title>
<link href="css/estilos.css" rel="stylesheet" type="textcss" />

<style>
.semit1
{opacity: 0.7;
background-color: black;}

</style>

</head>

<body style="background-attachment: fixed; height:480px; overflow:auto; ; text-align:center;">
<div id="contenedortopinterior" style="color:white;background-color:#344277; opacity:0.8; padding-top:10px; text-align:center;"></div>
    <img src="../images/IMG/TP_05.png" vspace="0" style=" height:180px; margin-left:50px;"/>
    <img src="../images/IMG/TPC_01.png" vspace="0" style=" height:180px; margin-left:50px;"/>

<div id="contenedor" style="color:white;background-color:#344277; opacity:0.8; text-align:center;">
<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Account management, customers, providers and origin calls</h2>
<table width="90%" align="center" cellpadding="0" cellspacing="0" style=" text-align:center;">
  <col width="14" />
  <col width="79" />
  <col width="428" />
  <col width="15" />
  <col width="26" />
  <tr>
    <td width="33">&nbsp;</td>
    <td width="70%">&nbsp;</td>
    <td width="100" align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Multi Level (Reseller - Agent)</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Carrier Definition</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Information</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Pre-Paid Accounts</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Post-Paid Account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Wholesale Account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Retail Account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Pre-Paid expiration    funds</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rate list per account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Minutes Plan per    account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account activation /    deactivation</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Automatic Invoice</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>CDR send by email</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>No Payment Advices</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>No Payment Automatic    Account disconnection</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Email advice when low    credit</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Post-Paid credit    limit</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Post-Paid time use    limit</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Call rounding per    account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Limit ports per    account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Routing definition    per account</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Access type by Origin    IP</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Access Type by prefix</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Access Type by ANI</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Access Type by DID</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Access Type by PIN</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
</body>
</html>
