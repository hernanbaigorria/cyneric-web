<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
</head>


 		<link rel="stylesheet" type="text/css" href="/incl    udes/css/ui-lightness/jquery-ui-1.8.2.custom.css" />  
    <style type="text/css">
	
	
			#demoNavigation {
				margin-top : 0.5em;
				margin-right : 1em;
				text-align: right;
			}
			
	

				margin-bottom: 0.2em;
				font-weight: bold;
				font-size: 0.8em;
			}

			label.error {
				color: red;
				font-size: 0.8em;
				margin-left : 0.5em;
			}

			.step span {
				float: right;
				font-weight: bold;
				padding-right: 0.8em;
			}

			.navigation_button {
				width : 70px;
			}
			
			#data {
					overflow : auto;
			}
		</style>



<body>

<div id="demoWrapper">
  <form id="demoForm" method="post" action="normal_submit.html" class="bbq">
<div id="fieldWrapper">
			  <span class="step" id="first">
      <span class="font_normal_07em_black">First step - Name</span><br />
					<label for="firstname">First name</label><br />
					<input class="input_field_12em" name="firstname" id="firstname" /><br />
					<label for="surname">Surname</label><br />
					<input class="input_field_12em" name="surname" id="surname" /><br />
				</span>
				<span id="finland" class="step">
					<span class="font_normal_07em_black">Step 2 - Personal information</span><br />
					<label for="day_fi">Social Security Number</label><br />
					<input class="input_field_25em" name="day" id="day_fi" value="DD" />
					<input class="input_field_25em" name="month" id="month_fi" value="MM" />
					<input class="input_field_3em" name="year" id="year_fi" value="YYYY" /> - 
					<input class="input_field_3em" name="lastFour" id="lastFour_fi" value="XXXX" /><br />
					<label for="countryPrefix_fi">Phone number</label><br />
					<input class="input_field_35em" name="countryPrefix" id="countryPrefix_fi" value="+358" /> - 
					<input class="input_field_3em" name="areaCode" id="areaCode_fi" /> - 
					<input class="input_field_12em" name="phoneNumber" id="phoneNumber_fi" /><br />
					<label for="email">*Email</label><br />
					<input class="input_field_12em email required" name="myemail" id="myemail" /><br />	 						
				</span>
				<span id="confirmation" class="step">
					<span class="font_normal_07em_black">Last step - Username</span><br />
					<label for="username">User name</label><br />
					<input class="input_field_12em" name="username" id="username" /><br />
					<label for="password">Password</label><br />
					<input class="input_field_12em" name="password" id="password" type="password" /><br />
					<label for="retypePassword">Retype password</label><br />
					<input class="input_field_12em" name="retypePassword" id="retypePassword" type="password" /><br />
				</span>
				</div>
				<div id="demoNavigation"> 							
					<input class="navigation_button" id="back" value="Back" type="reset" />
					<input class="navigation_button" id="next" value="Next" type="submit" />
				</div>
			</form>
			<hr />
			
			<p id="data"></p>
		</div>

   <script type="text/javascript" src="/includes/js/jquery-1.4.2.min.js"></script>		
    <script type="text/javascript" src="/includes/js/jquery.form.js"></script>
    <script type="text/javascript" src="/includes/js/jquery.validate.js"></script>
    <script type="text/javascript" src="/includes/js/bbq.js"></script>
    <script type="text/javascript" src="/includes/js/jquery-ui-1.8.5.custom.min.js"></script>
    <script type="text/javascript" src="/includes/js/jquery.form.wizard.js"></script>
    
    <script type="text/javascript">
			$(function(){
				$("#demoForm").formwizard({ 
				 	validationEnabled: true,
				 	focusFirstInput : true,
				 }
				);
  		});
    </script>


</body>
</html>
