<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>aero</title>
<style type="textcss">
body {
	background-color: #FFF;
	background-image: url(css/fondo-2.jpg);
	background-repeat: no-repeat;
	background-position: center top;
	margin: 0px;
	padding: 0px;
}
</style>
<link href="css/estilos.css" rel="stylesheet" type="textcss" />
</head>

<body> 

<div id="contenedortopinterior" style="color:white;background-color:#344277; opacity:0.8"><img src="css/ban-about.png" width="100%" height="147" vspace="0"  /></div>
<div id="contenedor" style="color:white;background-color:#344277; opacity:0.8">
  <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="654" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top"><h1>Top  Cyneric Platform features</h1></td>
        </tr>
        <tr>
          <td align="left" valign="top"><div id="contenidostxt">
            <h2>What are Cyneric Platform's Top <strong>10 </strong>Benefits according to our clients?</h2>
            <ol start="1" type="1">
                <li> Extremely intuitive, world of difference from what we used before ..&nbsp;</li>
                <li> Employing&nbsp;<strong>Cyneric Platform </strong>&nbsp;we were able to&nbsp;<strong>reduce our support and&nbsp;maintenance&nbsp;cost.</strong>&nbsp;We&nbsp;literally&nbsp;saved&nbsp;thousands&nbsp;of   dollars per month once we switched to aero.. </li>
                <li> &nbsp;<strong>Cyneric Platform </strong>&nbsp;platform   enabled us to offer a   greater range of services. &nbsp;We have increased our bottom line by 15% in   just 2 months after implementing a simple but powerful aero QoS tool we   previously didn't have. &nbsp;<strong>Cyneric Platform </strong>basically pays for it self. &nbsp;</li>
                <li> Our traffic demands are dynamic. Some months we need 300 ports while other months we need 500. <br />
                  <strong>Cyneric Platform </strong>platform enables for scalable cost&nbsp;adjustment&nbsp;in our infrastructure.   &nbsp;This feature alone saved us close to a $1000 per month and increased   our margin by 10%&nbsp;&nbsp;</li>
                <li> &nbsp;Aero   has a user friendly interface. From creating a new customer, &nbsp;to   troubleshooting a route, the interface is really intuitive. &nbsp;Anyone can   do it.</li>
                <li> We do several routing changes per day; <strong>&nbsp;<strong>Cyneric Platform </strong></strong>&nbsp;helps us expedite the process while minimizing the human error. &nbsp;Last month   we saved $10,000 USD from preventing a mistake that aero platform caught in advance. &nbsp;</li>
                <li><strong><strong>Cyneric Platform </strong></strong>automated&nbsp;Client   Management Portal helped us save a bundle on customer care support.   &nbsp;We&nbsp;use to&nbsp; outsource our customer care to a call center in another   country. &nbsp;Aero basically made   that completely&nbsp;unnecessary. &nbsp;The&nbsp;clients&nbsp;are now able to self manage   their own accounts. &nbsp;We are still getting a few calls in-house but   nothing like before. &nbsp;&nbsp;</li>
                <li> Our termination partners required us to have <strong>LRN capabilities</strong>. &nbsp;Aaro came through and we are not saving on costs and meeting our carriers requirements.&nbsp;</li>
                <li><strong>Cyneric Platform </strong>was great help when it came to auto-provisioning our&nbsp;&nbsp;Ip-Pbx business clients. &nbsp;Replicating configuration and   easy to manage aero tools simplified our life&nbsp;immensely.</li>
                <li><strong>Cyneric Platform </strong>helped us eliminate the need for infrastructure&nbsp;investment. &nbsp;We saved $80,000.00 and invested that money in   our marketing efforts. &nbsp;Thanks to&nbsp;<strong>Cyneric Platform </strong>our business   has&nbsp;tripled&nbsp;in the first 2 quarters of Year 1. &nbsp;We did not expect such   volume until the end of Year 2. &nbsp;Thanks <strong>Cyneric Platform </strong>!!!&nbsp;&nbsp;</li>
              </ol>
            <p>&nbsp;</p>
            <table border="0" align="center" cellpadding="11" cellspacing="0">
              <tr>
                <th scope="row"><a href="/base-top-features.pdf" target="_blank"><img src="css/ban-download-top-features.png" width="153" height="231" border="0" /></a></th>
                <td><a href="/aero-call-flow.pdf" target="_blank"><img src="css/ban-download-call-flow.png" width="153" height="231" border="0" /></a></td>
                <td><a href="/aero-general-flow.pdf" target="_blank"><img src="css/ban-downloadgeneral-flow.png" width="153" height="231" border="0" /></a></td>
              </tr>
            </table>
            <p>&nbsp;</p>

<p>&nbsp;</p>
            <p>&nbsp;</p>
          </div></td>
        </tr>
      </table></td>
    </tr>
  </table>  
</div>


</body>
</html>
