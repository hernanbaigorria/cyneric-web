<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body style="background-attachment: fixed; height:480px; overflow:auto;">
<h2>&nbsp;</h2>
<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Prestaciones y soluciones para Area T&eacute;cnica de su empresa</h2>
<table width="80%" align="center" cellpadding="0" cellspacing="0">
  <col width="14" />
  <col width="79" />
  <col width="428" />
  <col width="105" />
  <col width="26" />
  <tr>
    <td width="79">Routing</td>
    <td width="428">&nbsp;</td>
    <td width="66" align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Routing Dial-Plan per account</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Routing Manager</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route Hunt-Stop</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Route Allowed    Breakouts</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route Bloked    Breakouts</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Route Provider Order</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route breakout import    from Excel</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Route Activation /    deactivation</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route Provider port    limits</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Route Provider jump    when provider disabled</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route Intra/Inter    State Rates</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Route LRN</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route Static Priority</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Route LCR</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Route Client    Migration</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Limit ports per    account</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Troubleshoot Active    calls</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Routing Ease to    manage [1 to 10]</td>
    <td align="center">---</td>
  </tr>
  <tr>
    <td>QoS</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Stats per provider</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Stats per    Customer</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS ASR</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS ACD</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS ASR last 24h</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS ACD last 24h</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Graphs Actual</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Graphs last 24h</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Alerts</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Alerts to Email</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Alerts to SMS</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Alerts tracking</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS CDR Reports</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Information    Provided [1 to 10]</td>
    <td align="center">---</td>
  </tr>
  <tr>
    <td colspan="2">Monitoring</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Switch Monitoring</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring Active    Calls</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Monitoring CAPS</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring Sessions</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Monitoring CPU</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring MEM</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Monitoring graphs</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring last 24h</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Switch Alerts</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Troubleshoot Active    calls</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Alerts by email</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring    information provided[1 to 10]</td>
    <td align="center">---</td>
  </tr>
  <tr>
    <td colspan="2">Customer Web Portal /    Customer Care</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Payment gateway integration</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer Trouble    tickets by web</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Customer trouble    tickets by Client web portal</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Trouble Ticket    follow-up</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rouble ticket by    email</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Web Portal for Client</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Auto reload account</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer CDR Report</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Customer balance    report</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer Account    Information</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Customer Web Portal    ease to use[1 to 10]</td>
    <td align="center">---</td>
  </tr>
  <tr>
    <td>Reports</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report export to PDF</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Report export to    Excel</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report Export to Word</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Report Export to    Image</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report Export to HTML</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Report Export to XML</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report by email</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Automatic Report send    by email</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Reports -    Access by User</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Account Reports - CDR    Detailed</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage Per Destination</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage Per Destination per Provider</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage per Destination per IP</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage per Destination per Calling</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Total Per Account&nbsp;</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit per Day</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit Per Provider</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Services asignation&nbsp;</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profiles List</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Referral Plans asignation</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Free Minutes Plans asignation</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Referral Plans list</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Free Minutes Plans list</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Total Per Account inbound / Outbound</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Total CDR</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Intra / Inter State Traffic</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Destination)</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Destination/Customer)</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Destination/Termin)</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Detailed)</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Client)</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice Log</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice &amp; Payments</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice Brief</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Accounts not invoiced</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Payments</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Collection</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Account Receivables</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice print</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring Reports -    Active Calls</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Monitoring Reports -    CPU, CAPS, LOAD</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring Reports -    Services / Server Status</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Report - ASR /    Status</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Report - Tickets    with Providers</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Report - Alerts</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Report - Graphs    Export</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Report - LRN DIP</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Report - Carrier    ASR/ACD</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Breakout</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Price Lists</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Rates</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Price Lists    Call Units</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Price List    Promotions</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Resellers    Price List</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Price List    Assignments</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Rate    Amendment</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Routing -    Dial-plan&nbsp; / Accounts report</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Routing - Dial-plan /    provider</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Technical Reports -    DID Stock</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Technical Reports -    DID Usage&nbsp;</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Technical Reports -    Usage QoS</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Technical Reports -    Usage per Hour</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Technical Reports -    Non Processed Calls</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer Care -    History Claims</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Customer Care -    Representative follow up</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer Care -    Tickets Statistics</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Active calls    list&nbsp; / Filter</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr>
    <td colspan="2">Platform Help</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Help on-line</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Help Videos</td>
    <td align="center"><img src="/css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Help Information    Provided [1 to 10]</td>
    <td align="center">---</td>
  </tr>
  <tr>
    <td colspan="2">Platform Sign-up Process</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Sign-up on-line</td>
    <td align="center">NO</td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Time for account    creation</td>
    <td align="center">24</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Easy account creation    [1 to 10]</td>
    <td align="center">NO</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Assessment value</td>
    <td align="center">243</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
