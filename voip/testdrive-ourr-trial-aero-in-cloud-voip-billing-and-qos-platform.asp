<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>aero</title>
<style type="text/css">
body {
	background-color: #FFF;
	background-image: url(/css/fondo-2.jpg);
	background-repeat: no-repeat;
	background-position: center top;
	margin: 0px;
	padding: 0px;
}
</style>
<link href="/css/estilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--#include virtual="/includes/header.asp" -->
<div id="contenedortopinterior"><img src="/css/ban-testdrive.png" width="961" height="147" vspace="30" /></div>
<div id="contenedor">
  <table width="950" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="654" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
          <tr>
            <td align="left" valign="top"><h1>Testdrive  a&euml;ro</h1></td>
          </tr>
          <tr>
            <td align="left" valign="top"><div id="contenidostxt">
                <h2>
                  <% If request("enviado") = 1 Then %>
                </h2>
                <h2>&nbsp;</h2>
                <h2>Your suscription for FREE 15 days Testdrive was received.</h2>
                <p>A representative will contact you to adjust the final details and give you access to your preconfigured a&euml;ro  platform.</p>
                <p>See you soon, have a nice week</p>
                <% Else %>
                <h2>FREE test-drive a&euml;ro </h2>
                <p>You can test a&euml;ro in cloud, with all the functionalities the platform  offers.<br />
                Start today. </p>
                <p>Get all the benefits provided by a&euml;ro incloud!</p>
                <p>&nbsp;</p>
                <h2>                  Fill the form and start now:</h2>
<h2 style="color:#666" >&nbsp;</h2>
                <p>

                  <style type="text/css">
	
	
			#demoNavigation {
				margin-top : 0.5em;
				margin-right : 1em;
				text-align: right;
			}
			
			label.error {
				color: red;
				font-size: 0.8em;
				margin-left : 0.5em;
			}

			.step span {
	float: right;
	font-weight: normal;
	padding-right: 0.8em;
	color: #069;
	font-size:21px;
	width: 100%;
	background-color:#FFC;
	text-align:right;
	
			}

			.navigation_button {
				width : 70px;
			}
			
			#data {
					overflow : auto;
			}
		form  p {
	margin: 0px;
	padding: 0px;
}
                  </style>
                </p>
                <form id="demoForm" method="post" action="testdrive-cyneric.asp" class="bbq">
  <div id="fieldWrapper">
			  <span class="step" id="first">
      <span class="font_normal_07em_black">First step - Name &amp; Contact Information</span><br />
					<label for="firstname">First name</label>
					*<br />
					<strong><font face="Trebuchet MS, MS Sans Serif, Verdana" size="2">
					<input name="name" type="text" class="input_field_12em required" id="name" size="33" maxlength="50" />
					</font></strong><br />
  <br />
					<label for="surname">Last name</label>
					*<br />
					<strong><font face="Trebuchet MS, MS Sans Serif, Verdana" size="2">
					<input name="last_name" type="text" class="input_field_12em required" id="last_name" size="33" maxlength="50" />
			  </font></strong><br />
<br />
              					<label for="countryPrefix_fi">Phone number</label>
              					*<br />
   					<input name="telephone" class="input_field_12em required" id="phoneNumber_fi" size="19" />
					<br />
  <br />
					<label for="email">E-mail</label>
					*<br />
					<strong><font face="Trebuchet MS, MS Sans Serif, Verdana" size="2">
					<input name="email" type="text" class="input_field_12em email required" id="email" size="30" maxlength="50" />
					</font></strong><br />	
				</span>
				<span id="finland" class="step">
			<span class="font_normal_07em_black">Step 2 - Personal information</span><br />
 				<table width="554" border="0" cellpadding="2" cellspacing="2">
				  <tr valign="middle">
				    <td width="546" align="right" valign="top"><hr /></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top">Company</font> *</td>
			      </tr>
				  <tr valign="middle">
				    <td height="50" align="left" valign="top"><strong><strong>
				      <input type="text" name="CompanyName" class="cyneric  required" maxlength="50" size="35" />
			        </strong></font></strong></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top">Company&acute;s Web site</td>
			      </tr>
				  <tr valign="middle">
				    <td height="50" align="left" valign="top"><strong>
				      <input type="text" name="website" class="cyneric" value=" http://" maxlength="50" size="34" />
			        </font></strong></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top">City</td>
			      </tr>
				  <tr valign="middle">
				    <td height="50" align="left" valign="top"><strong>
				      <input name="city" type="text" class="cyneric" id="city" size="34" maxlength="50" />
			        </font></strong></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top">Country</td>
			      </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><div class="select_wrap">
				      <select name="Country" class="select_field"  >
				        <option>Seleccione</option>
				        <option value="USA">United States of America </option>
				        <option value="CAN">Canada </option>
				        <option value="DEU">Germany </option>
				        <option value="FRA">France </option>
				        <option value="GBR">United Kingdom </option>
				        <option value="IND">India </option>
				        <option value="AFG">Afghanistan </option>
				        <option value="ALB">Albania </option>
				        <option value="DZA">Algeria </option>
				        <option value="ASM">American Samoa </option>
				        <option value="AND">Andorra </option>
				        <option value="AGO">Angola </option>
				        <option value="AIA">Anguilla </option>
				        <option value="ATA">Antarctica </option>
				        <option value="ATG">Antigua and Barbuda </option>
				        <option value="ARG">Argentina </option>
				        <option value="ARM">Armenia </option>
				        <option value="ABW">Aruba </option>
				        <option value="AUS">Australia </option>
				        <option value="AUT">Austria </option>
				        <option value="AZE">Azerbaijan </option>
				        <option value="BHS">Bahamas </option>
				        <option value="BHR">Bahrain </option>
				        <option value="BGD">Bangladesh </option>
				        <option value="BRB">Barbados </option>
				        <option value="BLR">Belarus </option>
				        <option value="BEL">Belgium </option>
				        <option value="BLZ">Belize </option>
				        <option value="BEN">Benin </option>
				        <option value="BMU">Bermuda </option>
				        <option value="BTN">Bhutan </option>
				        <option value="BOL">Bolivia </option>
				        <option value="BIH">Bosnia and Herzegowina </option>
				        <option value="BWA">Botswana </option>
				        <option value="BVT">Bouvet Island </option>
				        <option value="BRA">Brazil </option>
				        <option value="IOT">British Indian Ocean Territory </option>
				        <option value="BRN">Brunei Darussalam </option>
				        <option value="BGR">Bulgaria </option>
				        <option value="BFA">Burkina Faso </option>
				        <option value="BDI">Burundi </option>
				        <option value="KHM">Cambodia </option>
				        <option value="CMR">Cameroon </option>
				        <option value="CPV">Cape Verde </option>
				        <option value="CYM">Cayman Islands </option>
				        <option value="CAF">Central African Republic </option>
				        <option value="TCD">Chad </option>
				        <option value="CHL">Chile </option>
				        <option value="CHN">China </option>
				        <option value="CXR">Christmas Island </option>
				        <option value="CCK">Cocoa (Keeling) Islands </option>
				        <option value="COL">Colombia </option>
				        <option value="COM">Comoros </option>
				        <option value="COG">Congo </option>
				        <option value="COK">Cook Islands </option>
				        <option value="CRI">Costa Rica </option>
				        <option value="CIV">Cote Divoire </option>
				        <option value="HRV">Croatia (Hrvatska) </option>
				        <option value="CUB">Cuba </option>
				        <option value="CYP">Cyprus </option>
				        <option value="CZE">Czech Republic </option>
				        <option value="DNK">Denmark </option>
				        <option value="DJI">Djibouti </option>
				        <option value="DMA">Dominica </option>
				        <option value="DOM">Dominican Republic </option>
				        <option value="TMP">East Timor </option>
				        <option value="ECU">Ecuador </option>
				        <option value="EGY">Egypt </option>
				        <option value="SLV">El Salvador </option>
				        <option value="GNQ">Equatorial Guinea </option>
				        <option value="ERI">Eritrea </option>
				        <option value="EST">Estonia </option>
				        <option value="ETH">Ethiopia </option>
				        <option value="FLK">Falkland Islands (Malvinas) </option>
				        <option value="FRO">Faroe Islands </option>
				        <option value="FJI">Fiji </option>
				        <option value="FIN">Finland </option>
				        <option value="FXX">France, Metropolitan </option>
				        <option value="GUF">French Guiana </option>
				        <option value="PYF">French Polynesia </option>
				        <option value="ATF">French Southern Territories </option>
				        <option value="GAB">Gabon </option>
				        <option value="GMB">Gambia </option>
				        <option value="GEO">Georgia </option>
				        <option value="GHA">Ghana </option>
				        <option value="GIB">Gibraltar </option>
				        <option value="GRC">Greece </option>
				        <option value="GRL">Greenland </option>
				        <option value="GRD">Grenada </option>
				        <option value="GLP">&gt;Guadeloupe </option>
				        <option value="GUM">Guam </option>
				        <option value="GTM">Guatemala </option>
				        <option value="GIN">Guinea </option>
				        <option value="GNB">Guinea-Bissau </option>
				        <option value="GUY">Guyana </option>
				        <option value="HTI">Haiti </option>
				        <option value="HMD">Heard Mc Donald Islands </option>
				        <option value="HND">Honduras </option>
				        <option value="HKG">Hong Kong </option>
				        <option value="HUN">Hungary </option>
				        <option value="ISL">Iceland </option>
				        <option value="IDN">Indonesia </option>
				        <option value="IRN">Iran (Islamic Republic of) </option>
				        <option value="IRQ">Iraq </option>
				        <option value="IRL">Ireland </option>
				        <option value="ISR">Israel </option>
				        <option value="ITA">Italy </option>
				        <option value="JAM">Jamaica </option>
				        <option value="JPN">Japan </option>
				        <option value="JOR">Jordan </option>
				        <option value="KAZ">Kazakhstan </option>
				        <option value="KEN">Kenya </option>
				        <option value="KIR">Kiribati </option>
				        <option value="PRK">Korea, Democratic</option>
				        <option value="KOR">Korea, Republic of </option>
				        <option value="KWT">Kuwait </option>
				        <option value="KGZ">Kyrgyzstan </option>
				        <option value="LAO">Lao Peoples </option>
				        <option value="LVA">Latvia </option>
				        <option value="LBN">Lebanon </option>
				        <option value="LSO">Lesotho </option>
				        <option value="LBR">Liberia </option>
				        <option value="LBY">Libyan Arab Jamahiriya </option>
				        <option value="LIE">Liechtenstein </option>
				        <option value="LTU">Lithuania </option>
				        <option value="LUX">Luxembourg </option>
				        <option value="MAC">Macau </option>
				        <option value="MKD">Macedonia</option>
				        <option value="MDG">Madagascar </option>
				        <option value="MWI">Malawi </option>
				        <option value="MYS">Malaysia </option>
				        <option value="MDV">Maldives </option>
				        <option value="MLI">Mali </option>
				        <option value="MLT">Malta </option>
				        <option value="MHL">Marshall Islands </option>
				        <option value="MTQ">Martinique </option>
				        <option value="MRT">Mauritania </option>
				        <option value="MVS">Mauritius </option>
				        <option value="MYT">Mayotte </option>
				        <option value="MEX">Mexico </option>
				        <option value="FSM">Micronesia</option>
				        <option value="MDA">Moldova, Republic of </option>
				        <option value="MCO">Monaco </option>
				        <option value="MNG">Mongolia </option>
				        <option value="MSR">Montserrat </option>
				        <option value="MAR">Morocco </option>
				        <option value="MOZ">Mozambique </option>
				        <option value="MMR">Myanmar </option>
				        <option value="NAM">Namibia </option>
				        <option value="NRU">Nauru </option>
				        <option value="NPL">Nepal </option>
				        <option value="NLD">Netherlands </option>
				        <option value="ANT">Netherlands Antilles </option>
				        <option value="NCL">New Caledonia </option>
				        <option value="NZL">New Zealand </option>
				        <option value="NIC">Nicaragua </option>
				        <option value="NER">Niger </option>
				        <option value="NGA">Nigeria </option>
				        <option value="NIU">Niue </option>
				        <option value="NFK">Norfolk Island </option>
				        <option value="MNP">Northern Mariana Islands </option>
				        <option value="MOR">Norway </option>
				        <option value="OMN">Oman </option>
				        <option value="PAK">Pakistan </option>
				        <option value="PLW">Palau </option>
				        <option value="PAN">Panama </option>
				        <option value="PNG">Papua New Guinea </option>
				        <option value="PRY">Paraguay </option>
				        <option value="PER">Peru </option>
				        <option value="PHL">Philippines </option>
				        <option value="PCN">Pitcairn </option>
				        <option value="POL">Poland </option>
				        <option value="PRT">Portugal </option>
				        <option value="PRI">Puerto Rico </option>
				        <option value="QAT">Qatar </option>
				        <option value="REU">Reunion </option>
				        <option value="ROM">Romania </option>
				        <option value="RUS">Russian Federation </option>
				        <option value="RWA">Rwanda </option>
				        <option value="KNA">Saint Kitts and Nevis </option>
				        <option value="LCA">Saint Lucia </option>
				        <option value="VCT">Saint Vincent</option>
				        <option value="WSM">Samoa </option>
				        <option value="SMR">San Marino </option>
				        <option value="STP">Sao Tome and Principe </option>
				        <option value="SAU">Saudi Arabia </option>
				        <option value="SEN">Senegal </option>
				        <option value="SYC">Seychelles </option>
				        <option value="SLE">Sierra Leone </option>
				        <option value="SGP">Singapore </option>
				        <option value="SVK">Slovakia </option>
				        <option value="SVN">Slovenia </option>
				        <option value="SLB">Solomon Islands </option>
				        <option value="SOM">Somalia </option>
				        <option value="ZAF">South Africa </option>
				        <option value="ESP">Spain </option>
				        <option value="LKA">Sri Lanka </option>
				        <option value="SHN">St. Helena </option>
				        <option value="SPM">St. Pierre and Miquelon </option>
				        <option value="SDN">Sudan </option>
				        <option value="SUR">Suriname </option>
				        <option value="SJM">Svalbard</option>
				        <option value="SWZ">Swaziland </option>
				        <option value="SWE">Sweden </option>
				        <option value="CHE">Switzerland </option>
				        <option value="SYR">Syrian Arab Republic </option>
				        <option value="TWN">Taiwan </option>
				        <option value="TJK">Tajikistan </option>
				        <option value="TZA">Tanzania</option>
				        <option value="THA">Thailand </option>
				        <option value="TGO">Togo </option>
				        <option value="TKL">Tokelau </option>
				        <option value="TON">Tonga </option>
				        <option value="TTO">Trinidad and Tobago </option>
				        <option value="TUN">Tunisia </option>
				        <option value="TUR">Turkey </option>
				        <option value="TKM">Turkmenistan </option>
				        <option value="TCA">Turks and Caicos</option>
				        <option value="TUV">Tuvalu </option>
				        <option value="UGA">Uganda </option>
				        <option value="UKR">Ukraine </option>
				        <option value="ARE">United Arab Emirates </option>
				        <option value="UMI">United States M.O.I.</option>
				        <option value="URY">Uruguay </option>
				        <option value="UZB">Uzbekistan </option>
				        <option value="VUT">Vanuatu </option>
				        <option value="VAT">Vatican City State</option>
				        <option value="VEN">Venezuela </option>
				        <option value="VNM">Viet Nam </option>
				        <option value="VGB">Virgin Islands (British) </option>
				        <option value="VIR">Virgin Islands (U.S.) </option>
				        <option value="WLF">Wallisw and Futuna Islands </option>
				        <option value="ESH">Western Sahara </option>
				        <option value="YEM">Yeman </option>
				        <option value="YUG">Yugoslavia </option>
				        <option value="ZAR">Zaire </option>
				        <option value="ZMB">Zambia </option>
				        <option value="ZWE">Zimbabwe </option>
				        <option value="UNK">Not Listed </option>
				        </select>
			        </div></td>
				    </tr>
				  <tr>
				    <td align="right" valign="top"><hr /></td>
				    </tr>
				  </table>		
				</span>
				<span id="billing" class="step">
					<span class="font_normal_07em_black">Step 3 - Your Operation  Billing &amp; Market Issues</span><br />
				    <table width="554" border="0" cellpadding="2" cellspacing="0">
				  <tr valign="middle">
				    <td width="550" align="right" valign="top"><hr /></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>Are you a start-up or have been in telecom business for some time?</em></td>
			      </tr>
				  <tr valign="middle">
				    <td height="50" align="left" valign="top"><p>
				      <label>
				        <input type="radio" name="newbieornot" value="Start-up VoIP Business" id="newbieornot_0" />
				        Start-up VoIP Business</label>
				      <br />
				      <label>
				        <input type="radio" name="newbieornot" value="Existing VoIP operation" id="newbieornot_1" />
				        Existing VoIP operation</label>
				      <br />
			        </p></td>
			      </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>What is your current Billing Platform if any?</em></td>
				    </tr>
				  <tr valign="middle">
				    <td height="66" align="left" valign="top"><label for="currentbillingplatform"></label>
			        <textarea name="currentbillingplatform" cols="55" rows="2" id="currentbillingplatform"></textarea></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>What is your primary reason to change billing, or any aspects of your current system?</em></td>
			      </tr>
				  <tr valign="middle">
				    <td height="6" align="left" valign="top"><textarea name="reazontochange" id="reazontochange" cols="55" rows="2"></textarea></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>Is your majority of business wholesale, retail or combination </em></td>
				    </tr>
				  <tr valign="middle">
				    <td height="100" align="left" valign="top"><p>
				      <label>
				        <input type="radio" name="busmodeltype" value="Wholesale" id="busmodeltype_0" />
				        Wholesale</label>
				      <br />
				      <label>
				        <input type="radio" name="busmodeltype" value="Retail" id="busmodeltype_1" />
				        Retail</label>
				      <br />
				      <input type="radio" name="busmodeltype" value="ippbx" id="busmodeltype_3" />
				      IP-PBX				      <br />
				      <label>
				        <input type="radio" name="busmodeltype" value="Combination of above" id="busmodeltype_3" /></label>
				      Combination of above<br />
			        </p></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>What   would you say is a profile of your typical client (i.e. An ISP, a   small business, an individual, carrier etc.) <br />
			        Further help us   understand your feature   needs.</em></td>
				    </tr>
				  <tr valign="middle">
				    <td height="66" align="left" valign="top"><textarea name="typicalclient" id="typicalclient" cols="55" rows="2"></textarea></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>Are there any elements in your current system that prevent you from growing your business, and if so what are they</em></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><textarea name="grow" id="grow" cols="55" rows="2"></textarea></td>
				    </tr>
				  <tr>
				    <td align="right" valign="top"><hr /></td>
				    </tr>
				  </table>
				</span>
                  
                  
                  				<span id="techincal" class="step">
					<span class="font_normal_07em_black">Step 4 - Your Operation Technical Profile &amp; Issues</span><br />
				    <table width="554" border="0" cellpadding="2" cellspacing="0">
				  <tr valign="middle">
				    <td width="550" align="right" valign="top"><hr /></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>What Switching Equipment are you using if any?</em></td>
				    </tr>
				  <tr valign="middle">
				    <td height="66" align="left" valign="top"><textarea name="switchs" id="switchs" cols="55" rows="2"></textarea></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>How   many concurrent calls are you passing on average? &nbsp;What is your peak   time concurrent call volume. &nbsp;If you don&rsquo;t know please estimate your   monthly minutes volume</em></td>
				    </tr>
				  <tr valign="middle">
				    <td height="66" align="left" valign="top"><textarea name="concurrentcalls" id="concurrentcalls" cols="55" rows="2"></textarea></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="bottom"><em>How many individuals are in your NOC / Technical Department --&gt; helps us understand your support needs
                    </em></td>
				    </tr>
				  <tr valign="middle">
				    <td height="100" align="left" valign="top"><p>
				      <label>
				        <input type="radio" name="tecnicos" value="1 a 5" id="tecnicos_0" />
				        1</label>
				    to 5 persons<br />
			          <label>
			            <input type="radio" name="tecnicos" value="2" id="tecnicos_1" />
			            5 to 20 persons
			          </label>
			           <br />
			          <label>
			            <input type="radio" name="tecnicos" value="3" id="tecnicos_2" />
			            + 20 persons
			          </label>
			          <br />
		            </p></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><em>What is your timeline for this upgrade, and ideally when would you like to have the new system up and running?</em></td>
				    </tr>
				  <tr valign="middle">
				    <td align="left" valign="top"><p>
				      <label>
				        <input type="radio" name="timeline" value="As soon as possible" id="timeline_2" />
				        As soon as possible</label>
				      <br />
				      <label>
				        <input type="radio" name="timeline" value="30 days" id="timeline_4" />
				        30 days</label>
				      <br />
				      <label>
				        <input type="radio" name="timeline" value="60 days" id="timeline_5" />
				        60 days</label>
				      <br />
				      <label>
				        <input type="radio" name="timeline" value="4 moths" id="timeline_6" />
				        4 moths</label>
				      <br />
				      <label>
				        <input type="radio" name="timeline" value="6 months" id="timeline_7" />
				        6 months</label>
				      <br />
				      <label>
				        <input type="radio" name="timeline" value="Not sure" id="RadioGroup1_7" />
				        Not sure</label>
				      <br />
				    </p></td>
				    </tr>
				  <tr>
				    <td align="right" valign="top"><hr /></td>
				    </tr>
				  </table></span>
                  
</div>
				<div id="demoNavigation"><strong><font face="Trebuchet MS, MS Sans Serif, Verdana" size="2">
				  <input type="hidden" name="aquien" value="support@cyneric.com" />
				</font></strong>
				  <input type="hidden" name="Source_Website" id="Source_Website" value="aeroincloud.com-testdrive-register" /> 							
				  <input class="navigation_button" id="back" value="Back" type="reset" />
					<input class="navigation_button" id="next" value="Next" type="submit" />
				</div>
			</form>
			
                <p>
                  <script type="text/javascript" src="/includes/js/jquery-1.4.2.min.js"></script>		
                  <script type="text/javascript" src="/includes/js/jquery.form.js"></script>
                  <script type="text/javascript" src="/includes/js/jquery.validate.js"></script>
                  <script type="text/javascript" src="/includes/js/bbq.js"></script>
                  <script type="text/javascript" src="/includes/js/jquery-ui-1.8.5.custom.min.js"></script>
                  <script type="text/javascript" src="/includes/js/jquery.form.wizard.js"></script>
                  <script type="text/javascript">
			$(function(){
				$("#demoForm").formwizard({ 
				 	validationEnabled: true,
				 	focusFirstInput : true,
				 }
				);
  		});
                  </script>
                  
                  
                  
                  
                <% End If %>
</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <hr />
                <p>Remember <strong>we offer 24/7 </strong>email <strong>Support</strong> to our &ldquo;test-drive&rdquo; customers and  we also provide 5 technical support<strong> hours for free, valued at US$ 2.500</strong>. Use  them!</p>
                <p><br />
                  If you have any technical or commercial question, you can contact our  support department for Europe and USA at :</p>
<div align="center" >                <h2>                  <strong>0855-SET-AERO</strong></h2>
  <h3>&nbsp;</h3>
  <h3>You can also e-mail us at</h3>
  <h2><a href="mailto:support-usa@aeroincloud.com">support-usa@aeroincloud.com</a> <br />
    <a href="mailto:support-europe@aeroincloud.com">support-europe@aeroincloud.com</a><br />
  </h2> 
</div>
                <p>&nbsp;</p>
<p>&nbsp;</p>
              </div></td>
          </tr>
          <tr>
            <td align="left" valign="top">&nbsp;</td>
          </tr>
        </table></td>
      <td width="321" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" align="right" valign="top"><img src="/css/ban-download-7top-tips.png" width="146" height="82" hspace="11" /></td>
          </tr>
          <tr>
            <td align="center"><a href="/voip-platform-billing-quality-of-service/voip-billing-platform-for-voip-starups-companies.asp"><img src="/css/ban-ownbusiness-ticket.png" width="153" height="231" hspace="7" vspace="22"border="0" /></a></td>
            <td align="center"><a href="/voip-platform-billing-quality-of-service/voip-billing-platform-benefits-with-in-cloud-hosted-platform-for-retail-wholesa-voip-companies.asp"><img src="/css/ban-gotbusiness-ticket.png" width="153" height="231" hspace="7" vspace="22"border="0" /></a></td>
          </tr>
          <tr>
            <td colspan="2" align="center">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
  </table>
  <p><img src="/css/banner-compatibilidad.png" width="977" height="139" vspace="15" /></p>
</div>
<!--#include virtual="/includes/footer.asp" -->

</body>
</html>
