<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t�tulo</title>
<link href="css/estilos.css" rel="stylesheet" type="textcss" />
<style>
.semit1
{opacity: 0.7;
background-color: black;}

</style>
</head>

<body style="background-attachment: fixed; height:480px; overflow:auto; ; text-align:center;">
<div id="contenedortopinterior" style="color:white;background-color:#344277; opacity:0.8; padding-top:10px; text-align:center;"></div>
    <img src="../images/IMG/TP_01.png" vspace="0" style=" height:180px; width:380px; margin-left:50px;"/>
    <img src="../images/IMG/TP_08.png" vspace="0" style=" height:180px; width:380px;margin-left:50px;"/>
<div id="contenedor" style="color:white;background-color:#344277; opacity:0.8; text-align:center;">
<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Large number of reports, to facilitate decision-making process</h2>
<table width="90%" align="center" cellpadding="0" cellspacing="0" style=" text-align:center;">
  <col width="14" />
  <col width="79" />
  <col width="428" />
  <col width="15" />
  <col width="26" />
  <tr>
    <td width="79">&nbsp;</td>
    <td width="428">&nbsp;</td>
    <td width="66" align="center">&nbsp;</td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report export to PDF</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Report export to    Excel</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report Export to Word</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Report Export to    Image</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report Export to HTML</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Report Export to XML</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Report by email</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Automatic Report send    by email</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Reports -    Access by User</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Account Reports - CDR    Detailed</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage Per Destination</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage Per Destination per Provider</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage per Destination per IP</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Account Reports -    Usage per Destination per Calling</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Total Per Account&nbsp;</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit per Day</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit Per Provider</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Services asignation&nbsp;</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profiles List</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Referral Plans asignation</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Free Minutes Plans asignation</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Referral Plans list</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Free Minutes Plans list</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Total Per Account inbound / Outbound</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Total CDR</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Intra / Inter State Traffic</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Destination)</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Destination/Customer)</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Destination/Termin)</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Detailed)</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Commercial Reports -    Profit &amp; Loss Report (Client)</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice Log</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice &amp; Payments</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice Brief</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Accounts not invoiced</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Payments</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Collection</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Account Receivables</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Invoice &amp;    Payments - Invoice print</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring Reports -    Active Calls</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Monitoring Reports -    CPU, CAPS, LOAD</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Monitoring Reports -    Services / Server Status</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Report - ASR /    Status</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Report - Tickets    with Providers</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Report - Alerts</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Report - Graphs    Export</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>QoS Report - LRN DIP</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>QoS Report - Carrier    ASR/ACD</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Breakout</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Price Lists</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Rates</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Price Lists    Call Units</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Price List    Promotions</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Resellers    Price List</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Rates - Price List    Assignments</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Rates - Rate    Amendment</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Routing -    Dial-plan&nbsp; / Accounts report</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Routing - Dial-plan /    provider</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Technical Reports -    DID Stock</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Technical Reports -    DID Usage&nbsp;</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Technical Reports -    Usage QoS</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Technical Reports -    Usage per Hour</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Technical Reports -    Non Processed Calls</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer Care -    History Claims</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Customer Care -    Representative follow up</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit2">
    <td>&nbsp;</td>
    <td>Customer Care -    Tickets Statistics</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
  <tr class="semit1">
    <td>&nbsp;</td>
    <td>Active calls    list&nbsp; / Filter</td>
    <td align="center"><img src="css/ok.png" /></td>
  </tr>
</table>
<p>&nbsp;</p>
</div>
</body>
</html>
