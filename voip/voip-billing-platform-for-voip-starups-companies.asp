<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>aero</title>
<style type="textcss">
body {
	background-color: #FFF;
	background-image: url(css/fondo-2.jpg);
	background-repeat: no-repeat;
	background-position: center top;
	margin: 0px;
	padding: 0px;
}
</style>
<link href="css/estilos.css" rel="stylesheet" type="textcss" />
</head>

<body>

<div id="contenedortopinterior" style="color:white;background-color:#344277; opacity:0.8"><img src="css/ban-starting-own-business.png" width="100%" height="147" vspace="0" /></div>
<div id="contenedor" style="color:white;background-color:#344277; opacity:0.8">
  <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="654" rowspan="2" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
          <tr>
            <td align="left" valign="top"><h1>Starting-up your VoIP business?<br />
                You are not alone </h1></td>
          </tr>
          <tr>
            <td align="left" valign="top"><div id="contenidostxt">
                <div id="contenidostxt2">
                  <p><strong>Cyneric Platform</strong> On-Demand, delivers real-time telecom services with cloud scalability.   <br />
                  Pay-as-you-grow pricing helps you avoid costly initial infrastructure investments.  Focus on your company's growth, and we will focus on your network and platform needs.</p>
                  <ul>
                    <li> <strong>Cyneric Platform Session Border Controller</strong>
                      <ul>
                        <li>                          Access  a full suite of best of breed business related tools</li>
                        <li>                          Dynamic Routing, Testing, LCR, LRN </li>
                        <li> QoS reporting and alerting system </li>
                        <li> Load Balancing with designed switch redundancy<br />
                          <br />
                        </li>
                      </ul>
                    </li>
                    <li><strong>Cyneric Platform  Billing Platform</strong>
<ul>
                        <li>                      Next generation billing system that is easy to use</li>
                        <li>                          Trouble-Ticket System, Minute Plans, Additional Service, Dynamic Invoicing, Low Credit Cap alerting and more</li>
                        <li>                          Full set of reports</li>
                        <li>                          Redundant set up for zero data loss</li>
                      </ul>
                    </li>
                  </ul>
                  <h2><br />
                  Why do other companies use  Cyneric Platform?</h2>
                  <p> How <strong>Cyneric Platform</strong> brings value if you are starting a VoIP business? Below 5  important reasons from Cyneric Platform users: </p>
                  <ol start="1" type="1">
                    <li> Short time to market. Our clients are &nbsp;able to start selling their product in matter of days.</li>
                      <li> Zero start up &nbsp;investments. The initial &nbsp;capital can be used for growth not infrastructure.</li>
                      <li> Low support cost. &nbsp;No need to hire an engineering team, when you have Cyneric Platform.</li>
                      <li> <strong>Cyneric Platform</strong>'s<strong>&nbsp;</strong>best of breed tools together with our intuitive interface minimizes the learning curve</li>
                      <li> Scalability, Scalability, Scalability! Pay as you GROW!</li>
                  </ol>
                  <p>&nbsp;</p>
                  <h2>Which technological aspects of Cyneric Platform  have made this platform the first choice, for niche of new and medium VoIP  Companies?  </h2>
                  <ul type="disc">
                    <li>
                      <p>Ability to define different routes for       clients, hunt-stop options, failover, LCR, LRN and more </p>
                    </li>
                    <li>
                      <p>Friendly Interfaces for administrators,       technicians, resellers and customers. Each entity has a tool to perform the       job efficiently. </p>
                    </li>
                    <li>
                      <p><strong>Cyneric Platform</strong> can generate real-time statistics, allowing       being proactive and not reactive on quality issues. </p>
                    </li>
                    <li>
                      <p>The platform has of a solid cluster of       switches on the back end, allowing users to quickly increase ports in Cyneric Platform. </p>
                    </li>
                    <li>
                      <p>Integration of information is fundamental.       Define a supplier, assign a price list to manage costs, then define a route       with this provider and assign it to a client. A complex task that in <strong>Cyneric Platform</strong> does not take more than 5 minutes </p>
                    </li>
                    <li>
                      <p><strong>Cyneric Platform</strong> handles high volume processes, like       invoicing, in a fast and effective manner. Through this back-end, the       platform performs customer billing, sends alerts and reports. </p>
                    </li>
                    <li>
                      <p><strong>Cyneric Platform</strong> is the result of 10 years of research,       development and tests in different scenarios. We know exactly what the       challenges are in VoIP business and we offer the right tools to overcome       them. </p>
                    </li>
                  </ul>
                  <p>&nbsp;</p>
                  
                  
                </div>
              </div></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      
    </tr>
  </table>
  
</div>
</body>
</html>
