<style type="text/css">

/* Begin CSS Popout Menu */


#menuh-container
	{
	position:  relative;
	width: 770px;
	top: 0px;
	height: 27px;
	padding: 0px;
	}

#menuh
	{
	font-size: 10px;
	font-family: "Trebuchet MS", Tahoma, Arial;
	width:100%;
	float:left;
	padding: 0px;
	position: relative;
	left: 0px;
	top: 0px;
	}
		
#menuh a
	{
	text-align: left;
	display:block;
	white-space:nowrap;
	height: 20px;
	padding-right: 5px;
	padding-left: 8px;
	padding-top: 1px;
	border-left: 2px solid #D8F0FE;
	padding-bottom: 0px;
	}
	
#menuh a, #menuh a:visited	/* menu at rest */
	{
	color: #000000;
	text-decoration:none;
	}
	
#menuh a:hover	/* menu at mouse-over  */
	{
	color: #000000;
	}	
			
#menuh a.top_parent, #menuh a.top_parent:hover  /* attaches down-arrow to all top-parents */
	{
	background: url(/includes/navdown_blue.gif) no-repeat left center;
	}
	
#menuh a.parent, #menuh a.parent:hover 	/* attaches side-arrow to all parents */
	{
	background-image: url(/includes/nav_blue.gif);
	background-position: right center;
	background-repeat: no-repeat;
	}

#menuh ul
	{
	list-style:none;
	margin:0;
	padding:0;
	float:left;
	width:13em;
	}

#menuh li
	{
	position:relative;
	border-right: none;
	border-left: none;
	padding: 0;
	}

#menuh ul ul
	{
	position:absolute;
	z-index:500;
	display:none;
	padding: 0px 2px;
	margin:1px 0 0 1px;
	background-color: #F2F2F2;
	height: 16px;
	border-style: none;
	}

#menuh ul ul ul
	{
	top:0;
	left:100%;
	border-top: 1px solid #FFFFFF;
	border-bottom: 1px solid #CCCCCC;
	}

div#menuh li:hover
	{
	cursor:pointer;
	z-index:100;
	}

div#menuh li:hover ul ul,
div#menuh li li:hover ul ul,
div#menuh li li li:hover ul ul,
div#menuh li li li li:hover ul ul
{display:none;}

div#menuh li:hover ul,
div#menuh li li:hover ul,
div#menuh li li li:hover ul,
div#menuh li li li li:hover ul
{display:block;}

/* End CSS Popout Menu */
#menuh li li {
	position:inherit;
	border-right: none;
	border-left: none;
	padding: 0;
	border-top: 1px solid #FFFFFF;
	border-bottom: 1px solid #CCCCCC;
	
}
#menuh li li a:hover {
	background-color: #EAF9FD;
}
</style>
<table width="777" height="66" border="0" cellpadding="0" cellspacing="0" background="../imagenes/barra-enlace-home.gif" id="menutop">
  <tr>
    <td width="34" align="left" valign="top">&nbsp;</td>
    <td valign="top">
	
	<div id="menuh-container">	
	
			<div id="menuh">
						<ul>
					<li><a href="/principal.aspx" target="contenido" onfocus="if(this.blur)this.blur()" >Calls List </a>					</li>
				</ul>
			
				<ul>
					<li><a href="#" class="top_parent" onfocus="if(this.blur)this.blur()">Alerts</a>
					  <ul>
						<li><a href="listar_alertas.aspx" target="contenido" onfocus="if(this.blur)this.blur()">Alerts Manager</a></li>
						<li><a href="listar_alertas.aspx?destino=Alerts.aspx" target="contenido" onfocus="if(this.blur)this.blur()">New Alert</a></li>
						<li><a href="/Alertas/listar_alertas_ejecutadas.aspx" target="contenido" onfocus="if(this.blur)this.blur()">Alerts Tracking</a></li>
					</ul>
					</li>
				</ul>
			
				<ul>
					<li><a href="#" class="top_parent" onfocus="if(this.blur)this.blur()">Users</a>
					  <ul>
						<li><a href="../usuarios/lista_usuarios.aspx" target="contenido" onfocus="if(this.blur)this.blur()" >Users Manager</a>
						</li>
						<li><a href="../usuarios/lista_usuarios.aspx?destino=usuarios.aspx" target="contenido" onfocus="if(this.blur)this.blur()" >Create User</a>						</li>
					</ul>
				  </li>
				</ul>
				
				<ul>
					<li><a href="/ticket/lista_tickets.aspx" target="contenido" onfocus="if(this.blur)this.blur()">Tickets</a>
				  </li>
				</ul>
				<ul>
					<li><a href="../cerrar.aspx" target="_parent" onfocus="if(this.blur)this.blur()">Close session </a>
				</ul>
			</div>
	  </div>
	
	
	
	</td>
    <td width="120" align="right" valign="top"><a href="http://www.cyneric.com" target="_blank" onfocus="if(this.blur)this.blur()"></a></td>
  </tr>
</table>

