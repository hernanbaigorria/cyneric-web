<table width="214" border="0" align="center" cellspacing="0">
      <tr>
        <td width="212" scope="col"><h1><span style="margin-bottom:12px">Whitepapers Library</span></h1>
      <div style="visibility:hidden; display:none"><a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Expand All</a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Contact All</a></div>

<ul id="treemenu1" class="treeview">
<li><a href="/documentation/sip.asp">SIP</a></li>
<li><a href="/documentation/asterisk-osp.asp">Asterisk OSP</a></li>
<li>VoIP Telephony
	<ul>
	<li><a href="/documentation/3comsolutions.asp">Strategies for successful IP </a></li>
	<li><a href="/documentation/IP_telephony_deployments.asp">The Five Considerations </a></li>
	<li><a href="/documentation/increasing_business_value_with_voip.asp">Increasing business </a></li>
	</ul>
</li>
<li><a href="/documentation/VoIP_addressing_QoS.asp">VoIP networking</a></li>

	<li><a href="/documentation/asterisk.asp">Asterisk Open Source</a></li>
	<li><a href="/documentation/quintum.asp">Life on the Edge</a></li>
		<li><a href="/documentation/Open_Source_Telephony.asp">Open Source Telephony</a></li>
			<li><a href="/documentation/Strategies_for_Migrating_Corporate.asp">Risks and Rewards</a></li>
	<li><a href="/documentation/A_Proactive_Approach_to_VoIP_Security.asp">Approach to VoIP Security</a></li>
	<li><a href="/documentation/Secure_Multi-lateral_Peering.asp">Secure, Multi-lateral Peering</a></li>
		
	<li><a href="/documentation/Security_Wiretapping_and_the_Internet.asp">Security, Wiretapping</a></li>


<li><a href="/documentation/Cisco_2007_AnnualSecurity_Report.asp">Cisco, Anual Security Report</a></li>
		  </ul>
		  
		  
<h4>
  <script type="text/javascript">

//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

ddtreemenu.createTree("treemenu1", true)
ddtreemenu.createTree("treemenu2", false)

  </script>
</h4></td>
      </tr>
    </table>