<%
'
'	Copyright (c) InterAKT Online 2000-2005
'
Class KT_Image
	Private userErrorMessage		'array		-	error message to be displayed as User Error
	Private develErrorMessage		'array		-	error message to be displayed as Developer Error
	
	Private available_DotNET
	Private available_IMKShell
	
	Private URLToDotNETImageComponent		' will keep the url to the aspx page
	Private arrCommands  					' array with the commands to be executed
	Private imageMagickPath					' the cached successfull path to image magick
	
	Private IMKShell				' shell object used to call Image Magick
	Private HTTPObj					' used to comunicate with aspx page

	Private initialized_DotNET
	Private initialized_IMKShell
			
	Private InitErrors_DotNET
	Private InitErrors_IMKShell
	
	Private KT_Image__KT_prefered_image_lib

	Private orderLib
	Private PathToSecurityFile
	Private UseSecurityCheck
	Private Sub Class_Initialize()
		orderLib = Array(".net", "imagemagick")
		PathToSecurityFile = ""
		UseSecurityCheck = True
		
		arrCommands = Array()
		imageMagickPath = ""

		userErrorMessage	= Array()
		develErrorMessage	= Array()
		Set IMKShell = nothing
		Set HTTPObj = nothing
		
		available_DotNET = false
		available_IMKShell = false
		
		initialized_DotNET = false
		initialized_IMKShell = false
		
		KT_Image__KT_prefered_image_lib = ""
	End Sub

	Private Sub Class_terminate()
		Set ShellObj = nothing
		Set IMKObj = nothing
		Set HTTPObj = nothing
	End Sub
	
	Public Sub addCommand(path__param)
		path = trim(path__param)
		If path <> "" Then
			path = replace(path, "/", "\")
			If right(path, 1) <> "\" Then
				path = path & "\"
			End If
			
			Redim Preserve arrCommands(Ubound(arrCommands) + 1)
			For i = Ubound(arrCommands) -1 To 0 Step -1
				arrCommands(i+1) = arrCommands(i)
			Next
			arrCommands(0) = path
		End If
	End Sub

	Public Sub setPreferedLib(lib)
		If Not KT_in_array(LCase(lib), orderLib, True) Then
			Exit Sub
		End If
		lib = LCase(lib)
		KT_Image__KT_prefered_image_lib = lib
		newOrder = array()
		Dim i
		newOrder = KT_array_push(newOrder, lib)
		For i=0 to ubound(orderLib)
			If orderLib(i) <> lib Then
				newOrder = KT_array_push(newOrder, orderLib(i))
			End If	
		Next
		orderLib = newOrder
	End Sub


	' ====== IMAGE SIZE =====	
	Public Function ImageSize (sourceFilePath) 
		ImageSize = Array (-1,-1)

		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableDotNET() Then
					GenerateAndWriteSecurityFile sourceFilePath
					post = "command=imagesize"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					output = ExecCommand_DotNET (post)
					DeleteSecurityFile
					If Not hasError Then
						If instr(output, "###") <> 0 Then
							arrSize = Split(output, "###")
							On Error resume Next
							ImageSize = Array (Cint(arrSize(0)), Cint(arrSize(1)))	
							On Error GoTo 0
						End If	
					End If
					Exit Function							
				End If
				End If
				
			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					output = ExecCommand_IMKShell ("identify.exe", Array("-format ""%w###%h""", """" & sourceFilePath & """"))
					If Not hasError Then
						If instr(output, "###") <> 0 Then
							arrSize = Split(output, "###")
							On Error resume Next
							ImageSize = Array (Cint(arrSize(0)), Cint(arrSize(1)))	
							On Error GoTo 0
						End If	
					End If
					Exit Function
				End If
				End If
			End Select
		Next
		
		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Function
		End If			
	End Function


	' ====== RESIZE =====	
	Public Sub Resize (sourceFilePath, destinationFolder, destinationFileName, width, height, keepProportion) 
		If destinationFolder <> "" Then
			Dim fld: Set fld = new KT_Folder
			fld.createFolder destinationFolder
			
			If fld.hasError Then
				errors = fld.getError
				setError "%s", Array(errors(0)), Array(errors(1))
				Set fld = nothing	
				Exit Sub
			End If
			Set fld = nothing
		
			If right(destinationFolder,1) <> "\" Then
				destinationFolder = destinationFolder & "\"
			End If
		End If
		outputPath = destinationFolder  & destinationFileName	

		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If isAvailableDotNET() Then
					GenerateAndWriteSecurityFile outputPath
					post = "command=resize"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					post = post & "&destination=" & Server.URLEncode(outputPath)
					post = post & "&width=" & Server.URLEncode(width)
					post = post & "&height=" & Server.URLEncode(height)
					post = post & "&keepproportion=" & Server.URLEncode(keepProportion)
					ExecCommand_DotNET (post)
					DeleteSecurityFile
					Exit Sub
				End If
				End If

			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					On Error Resume Next
					width = Clng(width)
					If err.Number <> 0 Then
						width = 0
						err.Clear
					End If
					height = Clng(height)
					If err.Number <> 0 Then
						height = 0
						err.Clear
					End If
					On Error GoTo 0
					
					propWidth = width
					If width = 0 Then
						propWidth =""
					End If
					propHeight = height
					If height = 0 Then
						propHeight =""
					End If
					If keepProportion Then
						propSign = ">"
					Else
						propSign = "!"
					End If
					cmdArg = propWidth & "x" & propHeight & propSign
					If keepProportion And width <> 0 And height <> 0 Then
						sizeArr = ImageSize (sourceFilePath)
						If hasError Then
							Exit Sub
						End If
						srcWidth = sizeArr(0)
						srcHeight = sizeArr(1)				
						doNotKeepProportion = false
						ratioWidth = CDbl(srcWidth) / CDbl(width)
						ratioHeight = CDbl(srcHeight) / CDbl(height)
						If ratioWidth < ratioHeight Then
							destWidth = CInt(srcWidth / ratioHeight)
							destHeight = height
						Else
							destWidth = width
							destHeight = CInt(srcHeight / ratioWidth)
						End If
						If destWidth < 1 Then
							destWidth = 1
							doNotKeepProportion = true
						End If
						If destHeight < 1 Then
							destHeight = 1
							doNotKeepProportion = true
						End If
						If doNotKeepProportion Then
							cmdArg = destWidth & "x" & destHeight & "!"
						End If
					End If
					ExecCommand_IMKShell "convert.exe", Array("-resize", cmdArg,   """" & sourceFilePath & """",  """" & outputPath & """")
					Exit Sub
				End If
				End If
			End Select
		Next

		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Sub
		End If		
	End Sub

	' ====== THUMBNAIL =====	
	Public Sub Thumbnail (sourceFilePath, destinationFolder, destinationFileName, width, height, keepProportion) 
		If destinationFolder <> "" Then
			Dim fld: Set fld = new KT_Folder
			fld.createFolder destinationFolder
			
			If fld.hasError Then
				errors = fld.getError
				setError "%s", Array(errors(0)), Array(errors(1))
				Set fld = nothing	
				Exit Sub
			End If
			Set fld = nothing
		
			If right(destinationFolder,1) <> "\" Then
				destinationFolder = destinationFolder & "\"
			End If
		End If
		outputPath = destinationFolder  & destinationFileName	

		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If isAvailableDotNET() Then
					GenerateAndWriteSecurityFile outputPath
					post = "command=thumbnail"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					post = post & "&destination=" & Server.URLEncode(outputPath)
					post = post & "&width=" & Server.URLEncode(width)
					post = post & "&height=" & Server.URLEncode(height)
					post = post & "&keepproportion=" & Server.URLEncode(keepProportion)
					ExecCommand_DotNET (post)
					DeleteSecurityFile
					Exit Sub
				End If
				End If

			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					On Error Resume Next
					width = Clng(width)
					If err.Number <> 0 Then
						width = 0
						err.Clear
					End If
					height = Clng(height)
					If err.Number <> 0 Then
						height = 0
						err.Clear
					End If
					On Error GoTo 0
					
					propWidth = width
					If width = 0 Then
						propWidth =""
					End If
					propHeight = height
					If height = 0 Then
						propHeight =""
					End If
					If keepProportion Then
						propSign = ">"
					Else
						propSign = "!"
					End If
					cmdArg = propWidth & "x" & propHeight & propSign
					If keepProportion And width <> 0 And height <> 0 Then
						sizeArr = ImageSize (sourceFilePath)
						If hasError Then
							Exit Sub
						End If
						srcWidth = sizeArr(0)
						srcHeight = sizeArr(1)				
						doNotKeepProportion = false
						ratioWidth = CDbl(srcWidth) / CDbl(width)
						ratioHeight = CDbl(srcHeight) / CDbl(height)
						If ratioWidth < ratioHeight Then
							destWidth = CInt(srcWidth / ratioHeight)
							destHeight = height
						Else
							destWidth = width
							destHeight = CInt(srcHeight / ratioWidth)
						End If
						If destWidth < 1 Then
							destWidth = 1
							doNotKeepProportion = true
						End If
						If destHeight < 1 Then
							destHeight = 1
							doNotKeepProportion = true
						End If
						If doNotKeepProportion Then
							cmdArg = destWidth & "x" & destHeight & "!"
						End If
					End If
					ExecCommand_IMKShell "convert.exe", Array("-thumbnail", cmdArg,  """" & sourceFilePath & """",  """" & outputPath & """")
					Exit Sub
				End If
				End If
			End Select
		Next
		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Sub
		End If		
	End Sub


	' ====== CROP =====	
	Public Sub Crop (sourceFilePath, x, y,  width, height) 
		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If isAvailableDotNET() Then
					GenerateAndWriteSecurityFile sourceFilePath
					post = "command=crop"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					post = post & "&destination=" & Server.URLEncode(sourceFilePath)
					post = post & "&x=" & Server.URLEncode(x)
					post = post & "&y=" & Server.URLEncode(y)
					post = post & "&width=" & Server.URLEncode(width)
					post = post & "&height=" & Server.URLEncode(height)
					ExecCommand_DotNET (post)
					DeleteSecurityFile
					Exit Sub
				End If
				End If

			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					ExecCommand_IMKShell "convert.exe", Array("-crop", width & "x" & height & "+" & x & "+" & y, """" & sourceFilePath & """",  """" & sourceFilePath & """")
					Exit Sub
				End If
				End If
			End Select
		Next
		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Sub
		End If		
	End Sub


	' ====== ADJUST QUALITY =====	
	Public Sub AdjustQuality (sourceFilePath, quality) 
		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If isAvailableDotNET() Then
					GenerateAndWriteSecurityFile sourceFilePath
					post = "command=adjustquality"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					post = post & "&destination=" & Server.URLEncode(sourceFilePath)
					post = post & "&quality=" & Server.URLEncode(quality)
					ExecCommand_DotNET (post)
					DeleteSecurityFile
					Exit Sub
				End If	
				End If		
			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					ExecCommand_IMKShell "convert.exe", Array("-quality", quality , """" & sourceFilePath & """",  """" & sourceFilePath & """")
					Exit Sub
				End If
				End If
			End Select
		Next
		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Sub
		End If		
	End Sub


	' ====== ROTATE =====	
	Public Sub Rotate (sourceFilePath, degree) 
		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If isAvailableDotNET() Then
					GenerateAndWriteSecurityFile sourceFilePath
					post = "command=rotate"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					post = post & "&destination=" & Server.URLEncode(sourceFilePath)
					post = post & "&degree=" & Server.URLEncode(degree)
					ExecCommand_DotNET (post)
					DeleteSecurityFile
					Exit Sub
				End If
				End If
			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					ExecCommand_IMKShell "convert.exe", Array("-rotate", degree , """" & sourceFilePath & """",  """" & sourceFilePath & """")
					Exit Sub
				End If
				End If
			End Select
		Next
		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Sub
		End If		
	End Sub


	' ====== FLIP =====	
	Public Sub Flip (sourceFilePath, direction) 
		Dim i
		For i=0 to ubound(orderLib)
			lib = orderLib(i)

			Select Case lib
			Case ".net"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If isAvailableDotNET() Then
					GenerateAndWriteSecurityFile sourceFilePath
					post = "command=flip"
					post = post & "&PathToSecurityFile=" & Server.URLEncode(PathToSecurityFile)
					post = post & "&source=" & Server.URLEncode(sourceFilePath)
					post = post & "&destination=" & Server.URLEncode(sourceFilePath)
					post = post & "&direction=" & Server.URLEncode(direction)
					ExecCommand_DotNET (post)
					DeleteSecurityFile
					Exit Sub
				End If
				End If

			Case "imagemagick"
				If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = lib Then
				If IsAvailableIMKShell() Then
					If lcase(direction) = "vertical" Then
						ExecCommand_IMKShell "convert.exe", Array("-flip", """" & sourceFilePath & """",  """" & sourceFilePath & """")
					Else
						ExecCommand_IMKShell "convert.exe", Array("-flop", """" & sourceFilePath & """",  """" & sourceFilePath & """")					
					End If
					Exit Sub
				End If
				End If
			End Select
		Next
		If Not available_DotNET and Not available_IMKShell Then
			clearError
			setError "ASP_IMAGE_COMPONENTS_NOT_AVAILABLE", Array(), Array(InitErrors_DotNET & "<br />" & InitErrors_IMKShell)
			Exit Sub
		End If		
	End Sub


	' ====== Sharpen =====	
	Public Sub Sharpen (sourceFilePath, intensity)
		If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = "imagemagick" Then
			If Not IsAvailableIMKShell() Then
				If hasError() Then
					develErrorMessage = KT_array_push(develErrorMessage, KT_getResource("ASP_IMAGE_SHARPEN_NOT_AVAILABLE_D", "Image", array(InitErrors_IMKShell)))
				Else
					setError "ASP_IMAGE_SHARPEN_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
				End If	
				Exit Sub
			End If
			If isnull(intensity)Then
				intensity = 5
			End If
			ExecCommand_IMKShell "convert.exe", Array("-sharpen", "3x" & intensity, """" & sourceFilePath & """",  """" & sourceFilePath & """")
		Else
			setError "ASP_IMAGE_SHARPEN_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
		End If
	End Sub

	' ====== Blur =====	
	Public Sub Blur (sourceFilePath, intensity)
		If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = "imagemagick" Then
			If Not IsAvailableIMKShell() Then
				If hasError() Then
					develErrorMessage = KT_array_push(develErrorMessage, KT_getResource("ASP_IMAGE_BLUR_NOT_AVAILABLE_D", "Image", array(InitErrors_IMKShell)))
				Else
					setError "ASP_IMAGE_BLUR_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
				End If	
				Exit Sub
			End If
			If isnull(intensity)Then
				intensity = 1
			End If
			ExecCommand_IMKShell "convert.exe", Array("-blur", Cstr(intensity), """" & sourceFilePath & """",  """" & sourceFilePath & """")
		Else
			setError "ASP_IMAGE_BLUR_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
		End If
	End Sub

	' ====== Contrast =====	
	Public Sub Contrast (sourceFilePath, direction) 
		If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = "imagemagick" Then
			If Not IsAvailableIMKShell() Then
				If hasError() Then
					develErrorMessage = KT_array_push(develErrorMessage, KT_getResource("ASP_IMAGE_CONTRAST_NOT_AVAILABLE_D", "Image", array(InitErrors_IMKShell)))
				Else
					setError "ASP_IMAGE_CONTRAST_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
				End If	
				Exit Sub
			End If
			If LCase(direction) = "decrease" Then
				cmd = "+contrast"
			Else
				cmd = "-contrast"
			End If
			ExecCommand_IMKShell "convert.exe", Array(cmd, """" & sourceFilePath & """",  """" & sourceFilePath & """")
		Else
			setError "ASP_IMAGE_BLUR_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
		End If	
	End Sub
	
	' ====== Brightness =====	
	Public Sub Brightness (sourceFilePath, direction)
		If KT_Image__KT_prefered_image_lib = "" Or KT_Image__KT_prefered_image_lib = "imagemagick" Then
			If Not IsAvailableIMKShell() Then
				If hasError() Then
					develErrorMessage = KT_array_push(develErrorMessage, KT_getResource("ASP_IMAGE_BRIGHTNESS_NOT_AVAILABLE_D", "Image", array(InitErrors_IMKShell)))
				Else
					setError "ASP_IMAGE_BRIGHTNESS_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
				End If	
				Exit Sub
			End If
			If LCase(direction) = "decrease" Then
				cmd = "80"
			Else
				cmd = "120"
			End If
			ExecCommand_IMKShell "convert.exe", Array("-modulate", cmd, """" & sourceFilePath & """",  """" & sourceFilePath & """")
		Else
			setError "ASP_IMAGE_BLUR_NOT_AVAILABLE", array(), Array(InitErrors_IMKShell & "<br />")
		End If
	End Sub	
	
	Private Sub GenerateAndWriteSecurityFile(path)
		If UseSecurityCheck Then
			Dim fso: Set fso = Server.CreateObject("Scripting.FileSystemObject")
			PathToSecurityFile = path & "_" & Session.SessionID & "_" & fso.GetTempName & ".sec"
			On Error Resume Next
			Dim textStreamObject: Set textStreamObject = fso.CreateTextFile(PathToSecurityFile ,true) 
			textStreamObject.WriteLine(Cstr(len(PathToSecurityFile))) 
			textStreamObject.Close
			Set textStreamObject = Nothing 
			If err.number <> 0 Then
				error = err.Description
				setError "ASP_IMAGE_DOTNET_ERR_WRITE_SEC_FILE", array() , Array(PathToSecurityFile, error)
			End If
			On Error GoTo 0
			Set fso = Nothing 
		End If	
	End Sub		

	Private Sub DeleteSecurityFile
		If UseSecurityCheck Then
			Dim fso: Set fso = CreateObject("Scripting.FileSystemObject") 
			On Error Resume Next
			If fso.FileExists(PathToSecurityFile) Then
				fso.DeleteFile PathToSecurityFile
			End If
			If err.number <> 0 Then
				error = err.Description
				setError "ASP_IMAGE_DOTNET_ERR_DELETE_SEC_FILE", array() , Array(PathToSecurityFile, error)
			End If
			On Error GoTo 0
			Set fso = Nothing 
		End If	
	End Sub

	'---  generic command execution using .NET framework ---
	Private Function ExecCommand_DotNET (PostData)
		ExecCommand_DotNET = ""
		On Error Resume Next
		HTTPObj.open "POST", URLToDotNETImageComponent , false
		HTTPObj.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"

		HTTPObj.send PostData			
		If err.number <> 0  Then
			error = err.Description
			setError "ASP_IMAGE_DOTNET_EXEC_ERROR", Array(), Array(URLToDotNETImageComponent, error)
			userErrorMessage = Array(KT_getResource("ASP_IMAGE_CMD_GENERIC_ERROR", "Image", array()))			
			On Error GoTo 0
			Exit Function
		End If
		On Error GoTo 0

		strRetText = HTTPObj.ResponseText
		intRetStatus = HTTPObj.Status
		If intRetStatus = 200 Then
			If instr(strRetText, "ERROR") <> 0 Then
				errorparams = split(strRetText, "###")
				errorArr = array()
				errorKey = errorparams(0)
				If ubound(errorparams) = 1 Then
					errorArr = split(errorparams(1), "##")
				End If
				setError errorKey, Array(), errorArr
				userErrorMessage = Array(KT_getResource("ASP_IMAGE_CMD_GENERIC_ERROR", "Image", array()))
			Else
				ExecCommand_DotNET = strRetText		 
			End If			
		Else
			setError "ASP_IMAGE_DOTNET_EXEC_NON200STATUS", Array(), Array(intRetStatus, URLToDotNETImageComponent)		
			userErrorMessage = Array(KT_getResource("ASP_IMAGE_CMD_GENERIC_ERROR", "Image", array()))
		End If
	End Function
	
	'---  generic command execution Image Magick from Shell ---
	Private Function ExecCommand_IMKShell(cmd, ByRef params)
		' build the arr commands to be used
		
		If imageMagickPath <> "" Then
			commands = Array(imageMagickPath & cmd)
		Else
			commands = arrCommands
			Dim i
			For i = 0 To Ubound(commands)
				commands(i) = commands(i) & cmd
			Next
		End If
		IMKShell.clearError
		output = IMKShell.execute (commands, params)
		If IMKShell.hasError Then
			'errors  = IMKShell.getError
            'missingRightsError = KT_getResource("ASP_IMAGE_IMK_MISSING_RIGHTS_D", "Image", Array(left(join(commands, ";"), len(join(commands, ";")) -1)))
			'setError "%s", array(errors(0)), array(errors(1) & "<br />" & missingRightsError)
			'userErrorMessage = Array(KT_getResource("ASP_IMAGE_CMD_GENERIC_ERROR", "Image", array()))

			errors =  IMKShell.getError()
			setError "%s", Array(KT_getResource("ASP_IMAGE_CMD_GENERIC_ERROR", "Image", array())), Array(KT_getResource("ASP_IMAGE_CMD_GENERIC_ERROR", "Image", array()) & ": <br />" & errors(1))
		End If
		ExecCommand_IMKShell = output
	End Function


	Public Function IsAvailableIMKShell()
		IsAvailableIMKShell	= false
		If Not initialized_IMKShell Then
			initialized_IMKShell = true
			Set	IMKShell = new KT_Shell

			commands = arrCommands
			Dim i
			For i = 0 To Ubound(commands)
				commands(i) = commands(i) & "convert.exe"
			Next
			
			output = IMKShell.execute (commands, Array("-version"))
			If IMKShell.hasError Then
				available_IMKShell = false
				errors  = IMKShell.getError
				InitErrors_IMKShell = KT_getResource("ASP_IMAGE_IMK_INIT_ERROR_D", "Image", Array( join(commands, ";") ))
			Else
				If instr(1,output,"ImageMagick",1) <> 0 Then
					imageMagickPath = IMKShell.getExecutedCommand()
					If imageMagickPath <> "" Then
						If InStrRev(imageMagickPath, "\") <> 0 Then
							imageMagickPath = left(imageMagickPath, InStrRev(imageMagickPath, "\"))
						Else
							imageMagickPath = ""
						End If	
					End If
					available_IMKShell = true
					IsAvailableIMKShell = true
				End If	
			End If
		Else
			IsAvailableIMKShell = available_IMKShell	
		End If
	End Function		
	
	Public Function getImageMagickPath()
		getImageMagickPath = imageMagickPath
	End Function

	
	Public Function IsAvailableDotNET()
		IsAvailableDotNET = false
		If Not initialized_DotNET Then
			initialized_DotNET = true
			' must init URLToImageComponent
			URLToDotNETImageComponent = KT_GetURLToResource("includes/common/lib/image/KT_ImageUtil.aspx")
			
			On Error resume Next
			Set HTTPObj = Server.CreateObject("MSXML2.ServerXMLHTTP")
			If err.number <> 0 Then
				Set HTTPObj = Server.CreateObject("Microsoft.XMLHTTP")
				err.Clear
			End If

			HTTPObj.open "GET", URLToDotNETImageComponent & "?command=TestImageComponent", false
			HTTPObj.send 
			' GET the response
			If err.number <> 0  Then
				' Something bad happend
				error  = err.Description
				InitErrors_DotNET = KT_getResource("ASP_IMAGE_DOTNET_INIT_ERROR_D", "Image", Array(URLToDotNETImageComponent, error))
				availableDotNET = false
				On Error GoTo 0
				Exit Function
			End If
			On Error GoTo 0

			strRetText = HTTPObj.ResponseText
			intRetStatus = HTTPObj.Status
			If intRetStatus = 200 Then
				If instr(strRetText, "TestImageComponent") <> 0 Then
					available_DotNET = true
					IsAvailableDotNET = true
					Exit Function
				End If
			End If
			
			If trim(strRetText) <> "" and instr(1,strRetText, "@ page language=""VB""",1) <> 0 Then
				InitErrors_DotNET = KT_getResource("ASP_IMAGE_DOTNET_NOT_INSTALLED_D", "Image", Array())
				available_DotNET = false
				Exit Function
			Else
				InitErrors_DotNET = KT_getResource("ASP_IMAGE_DOTNET_INIT_NON200STATUS_D", "Image", Array(intRetStatus, URLToDotNETImageComponent))
				available_DotNET = false
				Exit Function
			End If
		Else
			IsAvailableDotNET = available_DotNET
		End If	
	End Function




	Private Sub setError (errorCode, arrArgsUsr, arrArgsDev)
		errorCodeDev = errorCode
		If Not KT_in_array(errorCodeDev, array("", "%s"), false) Then
			errorCodeDev = errorCodeDev & "_D"
		End If
		If errorCode <> "" Then
			userErrorMessage = KT_array_push (userErrorMessage, KT_getResource(errorCode, "Image", arrArgsUsr))
		Else
			userErrorMessage = array()
		End If
		
		If errorCodeDev <> "" Then
			develErrorMessage = KT_array_push (develErrorMessage, KT_getResource(errorCodeDev, "Image", arrArgsDev))
		Else
			develErrorMessage = array()
		End If			
	End Sub

	Public Function hasError
		If ubound(userErrorMessage) > -1 Then
			hasError = True
		Else
			hasError = False	
		End If
	End Function
	

	Public Function getError
		getError = Array ( join(userErrorMessage,"<br />"), join (develErrorMessage, "<br />"))
	End Function

	Public Function clearError
		userErrorMessage = Array()
		develErrorMessage = Array()
	End Function
End Class
%>
