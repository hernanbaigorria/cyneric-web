<%
Response.Buffer = True
'For browsers (set expiration date some time well in the past)
Response.Expires = -1000
'For HTTP/1.1 based proxy servers
Response.CacheControl = "no-cache"
'For HTTP/1.0 based proxy servers
Response.AddHeader "Pragma", "no-cache"
%>


<%
Dim Tip(10)

Tip(1)  = "<div id='1'><h1>Calling Card Platform Administration<br />Fully integrated efficient & flexible solution </h1><p>Part of our family of solutions, calling card application is plug-and-play. Ideal business for variety of consumer segments, especially popular among the mobile phone users. We are here every step of the way to help you deploy and execute.</p><p align='right'><a href='../a1/VoIP-Billing-Platform-Calling-Card-Administration.asp'>read more about Calling Card Platform solution </a></p></div>"

Tip(2)  = "<div id='1'><h1>Residential VoIP Telephony<br />Efficient &amp; flexible solution </h1> <p>Carrier-grade VoIP billing and CRM platform that enables VoIP telephony providers to launch, price and provision their services.  Supports Caller-ID (ANI) based billing, with both prepaid and postpaid billing models, as well as prepaid calling cards and account/PIN systems with Interactive Voice Response (IVR).<p align='right'><a href='/a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp'>read more about this VoIP Telephony solution</a></p></div>"

Tip(3)  = "<div id='1'>  <h1>Callshop business </h1>  <p> Part of our VoIP family of solutions. <strong><a href='VoIP-Billing-Platform.asp'>Utilizing the  Cyneric platform</a></strong><a href='VoIP-Billing-Platform.asp'>,</a> the callshop business reaches an unprecedented level of ease, immediacy, control and management.</p>  <p align='right'><a href='/a1/VoIP-Billing-Platform-Call-Shop.asp'>read more about Cyneric�s Callshop business solutions</a></p></div>"

Tip(4)  = "<div id='1'><h1>Wholesale Termination &amp; Origination<br />  Features &amp; Benefits </h1> <p>Enjoy the benefits of our family of <a href='VoIP-Billing-Platform.asp'> solutions for commercial VoIP operations</a>.We offer a world-class VoIP billing solution tailored to each business model. </p>  <p align='right'><a href='../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp'>read more about Wholesale Termination solutions</a></p></div>"

Tip(5)  = "<div id='1'><h1>Billing platform for VoIP telephony</h1>  <p>We provide a total  VoIP solution . Cyneric offers a complete <a href='/a1/VoIP-Billing-Platform.asp'><strong>VoIP billing solution</strong></a> customized to your business model.<br />  Quality of service monitoring, Full NOC support, VoIP software that meets all your technical and commercial needs.</p>  <p align='right'><a href='/a1/VoIP-Billing-Platform.asp'>read more about this solution </a></p></div>"

Tip(6)  = "<div id='1'>  <h1>VoIP Networks, Quality of service</h1> <p>On of the basic issues of VoIP networks is Quality of service maintenance. For this purpose, we have developed different tools and services oriented towards maximizing your network profits as well as considerably reducing maintenance costs. <p align='right'><a href='../a1/VoIP-Network-platform.asp'>read more about this solution </a></p></div>"

Tip(7)  = "<div id='1'><h1>Cyner CR2, real time traffic monitoring <br />Quality, the foundation of your business </h1> <p> <a href='/a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp'>Part of our VoIP Network Monitoring family of solutions</a>. Answers the critical issue of staying on top of termination carrier's quality. Using multiple variables, it provides and alerts you to critical QoS fluctuations.</p> <p align='right'><a href='/a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp'>read more about this  real time traffic monitoring solution </a></p></div>"

Tip(8)  = "<div id='1'><h1>Real time traffic monitoring</h1> <p>Quality, the foundation of your business. <a href='/a1/VoIP-Network-platform.asp'>Part of our VoIP Network Monitoring family of solutions</a>. Answers the critical issue of staying on top of termination carrier's quality. Using multiple variables, it provides and alerts you to critical QoS fluctuations. <p align='right'><a href='../a1/VoIP-Network-Real-Time-alert-monitoring.asp'>read more about this solution </a></p></div>"

Tip(9)  = "<div id='1'><h1>Cisco Systems, real-time monitor &amp; alerts</h1> <p> <a href='VoIP-Network-platform.asp'>Part of our VoIP network monitoring family of solutions.</a>   It is a  simple, effective and essential  tool for your Cisco based IP network.<br />You need to be informed and you should know when problems arise. We have the tool that simplifies your business' technical operations. </p> <p align='right'><a href='/a1/VoIP-Network-Real-Time-Cisco-monitoring.asp'>read more about Cyneric�s Cisco solutions</a></p></div>"

Tip(10) = "<div id='1'><h1>Specialized technical support for Wholesale and Residential Telephony Networks</h1> <p><a href='/a1/VoIP-Billing-Platform.asp'>Part of NOC monitoring and servicing family of solutions.</a> One of the basic issues in VoIP network management is ensuring continuous operational flow.  Our resources and knowledge base in cross sections of industry equipment and models insures exactly that.  Reduced interruptions and increased productivity. <p align='right'><a href='/a1/VoIP-Network-Specialized-Tech-Support-for-Retail-Wholesale-Networks.asp'>read more about this solution </a></p></div>"

Function RandomNumber(intHighestNumber)
	Randomize
	RandomNumber = Int(intHighestNumber * Rnd) + 1
End Function

Dim strRandomTip
strRandomTip = Tip(RandomNumber(10))
%>




<!-- titulos caja 1 Voip illing Solutions  ////////////////////////////////////////////////////////////// -->
<%
Dim Tiphomecaja1(3)
Tiphomecaja1(1)  = "Voip Billing Platform"
Tiphomecaja1(2)  = "Voip Billing Software"
Tiphomecaja1(3)  = "Voip Billing Solutions"

Function RandomNumber(intHighestNumber)
	Randomize
	RandomNumber = Int(intHighestNumber * Rnd) + 1
End Function
Dim Tcaja1
Tcaja1 = Tiphomecaja1(RandomNumber(3))
%>
<!-- fin de titulos  /////////////////////////////////////////////////////////////////////////////////// -->

<!-- titulos caja 2 network quality of service ///////////////////////////////////////////////////////// -->
<%
Dim Tiphomecaja2(3)
Tiphomecaja2(1)  = "Network quality of service"
Tiphomecaja2(2)  = "Improve business Technical QoS"
Tiphomecaja2(3)  = "Realtime service control"

Function RandomNumber(intHighestNumber)
	Randomize
	RandomNumber = Int(intHighestNumber * Rnd) + 1
End Function
Dim Tcaja2
Tcaja2 = Tiphomecaja2(RandomNumber(3))
%>
<!-- fin de titulos caja 2 network quality of service ////////////////////////////////////////////////// -->


