<%
'
'	Copyright (c) InterAKT Online 2000-2006. All rights reserved.
'

Class tNG_TidyContent 
	
	Private tNG
	Public columns
	Public errorMsg
	Public tNG_param
	Public allowedTagsList
	Public deniedTagsList
	Public tidiedValues
	Public outEncoding
	Public folderName
	
	Private Sub Class_Initialize()
	End Sub

	Private Sub Class_terminate()
	End Sub

	Public Sub Init(ByRef tNG_param)
		Set tNG = tNG_param
		columns = array()
		outEncoding = "ascii"
		deniedTagsList = ""
		allowedTagsList = ""
				Set tidiedValues = Server.CreateObject("Scripting.Dictionary")
	End Sub

	Public Sub addColumn(name_param)
		If name_param = "" OR name_param = " " Then
			Exit Sub
		Else
			KT_array_push columns, name_param
		End If
	End Sub

	Private Sub setOutEncoding(outEncoding_param)
		If outEncoding_param = "" Then
			outEncoding_param = "ascii"
		End If
		outEncoding = outEncoding_param
	End Sub

	Public Sub setAllowedTags(allowedTags_param)
		allowedTagsList = replace (allowedTags_param, ">", "")
		allowedTagsList = replace (allowedTags_param, "<", "")
		allowedTagsList = replace (allowedTags_param, " ", "")
		allowedTagsList = replace (allowedTags_param, "\", "")
	End Sub
	
	Public Sub setDeniedTags(deniedTags_param)
		deniedTagsList = replace (deniedTags_param, ">", "")
		deniedTagsList = replace (deniedTags_param, "<", "")
		deniedTagsList = replace (deniedTags_param, " ", "")
		deniedTagsList = replace (deniedTags_param, "\", "")
	End Sub
	
	Public Sub setErrorMsg(errMsg1, errMsg2)
		If tNG_debug_mode = "DEVELOPMENT" Then
		Response.Write(errMsg2)
		Response.End()
			errorMsg =  KT_DynamicData(errMsg2, tNG, null, null, null, null)
		Else
			errorMsg =  KT_DynamicData(errMsg1, tNG, null, null, null, null)		
		End If
	End Sub

	Public Function execute()
		Dim ret: Set ret = Nothing
		Dim cols: Set cols = Server.CreateObject("Scripting.Dictionary")
		Dim arr: Set arr = Server.CreateObject("Scripting.Dictionary")
		Dim colDetails: Set colDetails = Server.CreateObject("Scripting.Dictionary")
		Dim colName
		Dim colValue
		Dim returnValue
		Dim error: error = False
		Dim itemsArray
		Dim fieldWithErrors: fieldWithErrors = array()
		
		Set	arr = tNG.columns
		For each colName in arr
			Set colDetails = arr.item(colName)
			If KT_in_array(colName, columns, true) AND colDetails("type") = "STRING_TYPE" Then
				cols.add colName, colDetails("value")
			End If
		Next 
		
		If cols.count = 0 Then
			Exit Function
		End If		
		
		For Each colName in cols
			colValue = cols.item(colName)
			returnValue = execTidy(colName, colValue)
			If returnValue = False Then
				error = true
				KT_array_push fieldWithErrors, colName
			Else
				cols(colName) = tidiedValues.item(colName)
			End If
		Next 			
		
		For Each colName in cols
			tNG.setColumnValue colName, tidiedValues(colName)
		Next
		If (error = True) Then
			If (errorMsg = "") Then
				Set ret = new tNG_error
				ret.init "ERROR_TIDY_CONTENT",array(),array()
			Else
				Set ret = new tNG_error
				ret.init "%s",array(errorMsg),array()
			End If
		End If
		Set execute = ret
	End Function
	
	Public Function execTidy(colName_param, colValue_param)
		Dim arg_test: arg_test = Array("--version")		
		
		If Session("TidyContent_ExecPath") <> "" Then
			new_XHTMLLocations = Array(Session("TidyContent_ExecPat"))
		Else
			pathToExe = KT_addSuffixToPath(KT_prefered_tidy_path, "\")
			new_XHTMLLocations = Array(pathToExe & "tidy.exe", pathToExe & "tidy")
			Redim Preserve new_XHTMLLocations(Ubound(TidyContent_TidyLocations) + 2)
			For i=0 To ubound(TidyContent_TidyLocations)
				new_XHTMLLocations(i+2) = TidyContent_TidyLocations(i)
			Next	
		End If		
		
		
		Set shell = new KT_Shell
		shell.Execute new_XHTMLLocations, arg_test
		If shell.hasError() Then
			errors = shell.getError()
			setErrorMsg errors(0), errors(1)
			execTidy = false
			Exit Function
		End If
		If Session("TidyContent_ExecPat") = "" Then
			Session("TidyContent_ExecPat") = shell.getExecutedCommand()
		End If		
		
		tidyEncoding = "raw"
		If Lcase(trim(encoding)) = "iso-8859-1" Then
			tidyEncoding = "ascii"
		End If
		If Lcase(trim(encoding)) = "utf-8" Then
			tidyEncoding = "utf8"
		End If	
	
		colValue_param = replace(colValue_param, "&amp;nbsp;", "&amp;amp;nbsp;")
		colValue_param = replace(colValue_param, "&nbsp;", "&amp;nbsp;")		
		Dim folderName: folderName = KT_normalizeAbsolutePath(Server.MapPath(".") & "\" & TidyContentnam_TidyTempPath)
		Set fso = Server.CreateObject("Scripting.FileSystemObject")
		If Not fso.FolderExists(folderName) Then
			Set folder = new KT_folder
			folder.createFolder folderName
			If folder.hasError() Then
				errors = folder.getError()
				setErrorMsg errors(0), errors(1)
				execTidy = false
				Exit Function				
			End If
		End If
		
		fileName = folderName & fso.GetTempName
		fileNameOut = fileName & ".out"
		
		Set file = new KT_file
		file.setEncoding outEncoding
		file.writeFile fileName, "truncate", colValue_param
		If file.hasError() Then
			errors = file.getError()
			setErrorMsg errors(0), errors(1)
			execTidy = false
			Exit Function
		End If
		Dim path: path = TidyContent_TidyConfigPath
		args = Array ("-config", _ 
						"""" & path & """", _
						"-" & tidyEncoding, _
						"-o", _
						"""" & fileNameOut & """", _
						"""" & fileName & """")
		
		shell.clearError
		shell.Execute new_XHTMLLocations, args
		If shell.hasError() And Not fso.FileExists(fileNameOut) Then
			errors = shell.getError()
			setErrorMsg errors(0), errors(1)
			On Error Resume Next
			fso.DeleteFile fileName	
			fso.DeleteFile fileNameOut	
			On Error GoTo 0
			execTidy = false
			Exit Function
		End If		
		
		file.clearError
		content = file.readFile(fileNameOut)
		If file.hasError() Then
			errors = file.getError()
			setErrorMsg errors(0), errors(1)
			On Error Resume Next
			fso.DeleteFile fileName	
			fso.DeleteFile fileNameOut	
			On Error GoTo 0			
			execTidy = false
			Exit Function
		End If
		On Error Resume Next
		content = replace(content, "&amp;nbsp;", "&nbsp;")
		content = replace(content, "&amp;&amp;nbsp;", "&amp;nbsp;")		
		fso.DeleteFile fileName	
		fso.DeleteFile fileNameOut	
		On Error GoTo 0	
		ret = cleanContent(content)
		tidiedValues.item(colName_param) = ret
		execTidy = True
	End Function
	
	Public Function cleanContent(content_param)
		ret = KT_cleanContent(content_param, allowedTagsList, deniedTagsList)
		cleanContent = ret
	End Function
	

End class

%>