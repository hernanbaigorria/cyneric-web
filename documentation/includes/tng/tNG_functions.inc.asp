<% 
'
'	Copyright (c) InterAKT Online 2000-2005
'

Function tNG_fieldHasChanged(fieldName, fieldValue)
	
	ExecStr  = "" & _ 
	"tNG_fieldHasChanged_returnedVal = false" & vbNewLine & _
	"If Not IsObject(tNG_fieldHasChanged_values) Then" & vbNewLine & _ 
	"	Set tNG_fieldHasChanged_values = Server.CreateObject(""Scripting.Dictionary"")" & vbNewLine & _
	"End If" & vbNewLine & _
	"If tNG_fieldHasChanged_values.Exists(""" & fieldName & """) Then"  & vbNewLine & _
	"	If tNG_fieldHasChanged_values(""" & fieldName & """) <> """ & fieldValue & """ Then"  & vbNewLine & _
	"		tNG_fieldHasChanged_returnedVal = true"  & vbNewLine & _
	"	End If" & vbNewLine & _
	"Else" & vbNewLine & _
	"	tNG_fieldHasChanged_returnedVal = true"  & vbNewLine & _
	"End If" & vbNewLine & _
	"tNG_fieldHasChanged_values(""" & fieldName & """) = """ & fieldValue & """"
	
	ExecuteGlobal ExecStr
	'Response.write vbNewLine & vbNewLine &  ExecStr & vbNewLine &  vbNewLine
	tNG_fieldHasChanged = tNG_fieldHasChanged_returnedVal
End Function

Function tNG_fileExists(dynamicFolder, dynamicFileName)
	ret = false
	If dynamicFileName <> "" Then
		folder = KT_DynamicData(dynamicFolder, nothing, null, null, null, null)
		fileName = KT_DynamicData(dynamicFileName, nothing, null, null, null, null)
		If fileName <> "" Then
			folder = replace(folder, "\", "/")
			If right(folder, 1) <> "/" Then
				folder = folder & "/"
			End If
			folder = KT_makeIncludedPath(folder)
			relPath = folder & fileName
			Dim fso: Set fso = Server.CreateObject("Scripting.FileSystemObject")
			On Error Resume Next
			If fso.FileExists(KT_Server_MapPath(relPath)) Then
				ret = True
			End If
			If Err.Number <> 0 Then
				ret = False
			End If
			On Error GoTo 0
			Set fso = nothing
		End If
	End If	
	tNG_fileExists = ret
End Function	

Function tNG_downloadDynamicFile(siteRootPath, dynamicFolder, dynamicFileName) 
	ret = ""
	If dynamicFileName <> "" Then
		folder = KT_DynamicData(dynamicFolder, nothing, null, null, null, null)
		fileName = KT_DynamicData(dynamicFileName, nothing, null, null, null, null)
		folder = KT_makeIncludedPath(folder)
		folder = KT_Server_MapPath(folder) & "\"
		absPath = folder & fileName

		nowdt = Timer()
		If IsObject(Session("tNG_download"))Then
			Set downloadInfo = Server.CreateObject("Scripting.Dictionary")
			Set arr = Session("tNG_download")
			For each tmpId in arr
				Set details = arr(tmpId)
				If details("time") > (nowdt - 5 * 60) Then
					Set downloadInfo(tmpId) = details
				End If
			Next
			Set Session("tNG_download") = downloadInfo
		Else
			Set Session("tNG_download") = Server.CreateObject("Scripting.Dictionary")
		End If

		uniqueId = KT_md5(absPath)
		If Not Session("tNG_download").Exists(uniqueId) Then
			Set downloadInfo = Server.CreateObject("Scripting.Dictionary")
			downloadInfo("realPath") = absPath
			downloadInfo("fileName") = fileName
			downloadInfo("time") = nowdt
			Set Session("tNG_download")(uniqueId) = downloadInfo
		End If
		If siteRootPath <> "" Then
			siteRootPath = replace(siteRootPath, "\", "/")
			If right(siteRootPath, 1) <> "/" Then
				siteRootPath = siteRootPath & "/"
			End If		
		End If
		ret = siteRootPath & "includes/tng/pub/tNG_download.asp?id=" & Server.URLEncode(uniqueId)
	End If
	tNG_downloadDynamicFile	 = ret
End Function


Function tNG_showDynamicImage(siteRootPath, dynamicFolder, dynamicFileName)
	If dynamicFileName <> "" Then
		folder = KT_DynamicData(dynamicFolder, nothing, null, null, null, null)
		fileName = KT_DynamicData(dynamicFileName, nothing, null, null, null, null)
		If fileName <> "" Then
			folder = replace(folder, "\", "/")
			If right(folder, 1) <> "/" Then
				folder = folder & "/"
			End If
			relPath = folder & fileName
			Dim fso: Set fso = Server.CreateObject("Scripting.FileSystemObject")
			On Error Resume Next
			If Not fso.FileExists(KT_Server_MapPath(KT_makeIncludedPath(folder) & fileName)) Then
				relPath = siteRootPath & "includes/tng/styles/img_not_found.gif"
			End If
			If Err.Number <> 0 Then
				relPath = siteRootPath & "includes/tng/styles/img_not_found.gif"
			End If
			On Error GoTo 0			
			Set fso = nothing
		Else 
			relPath = siteRootPath & "includes/tng/styles/img_not_found.gif"
		End If	
	Else
		relPath = siteRootPath & "includes/tng/styles/img_not_found.gif"
	End If	
	tNG_showDynamicImage = relPath
End Function



Function tNG_showDynamicThumbnail(siteRootPath, dynamicFolder, dynamicFileName, width, height, proportional)
	relPath = ""
	If dynamicFileName <> "" Then
		folder = KT_DynamicData(dynamicFolder, nothing,  null, null, null, null)
		fileName = KT_DynamicData(dynamicFileName, nothing, null, null, null, null)
		folder = replace(folder, "\", "/")
		If right(folder, 1) <> "/" Then
			folder = folder & "/"
		End If
		
		relPath = folder & fileName
		Set path_info = KT_pathinfo(fileName)
		thumbnailFolder = folder  &  "thumbnails/"
		thumbnailName = path_info("filename") & "_" &  Cint(width) & "x" & Cint(height) & path_info("dot") & path_info("extension")
		relPath = thumbnailFolder & thumbnailName

		Dim fso: Set fso = Server.CreateObject("Scripting.FileSystemObject")
		
		fileExists = True
		On Error Resume Next
		fileExists = fso.FileExists(KT_Server_MapPath(KT_makeIncludedPath(folder) & fileName))
		If Err.Number <> 0 Then
			relPath = siteRootPath & "includes/tng/styles/img_not_found.gif"
		End If
		On Error GoTo 0
		
		If fileExists Then
			' check if thumbnail doesn't exist and create it
			If Not fso.FileExists(KT_Server_MapPath(KT_makeIncludedPath(relPath))) Then
				Set image = new KT_image
				image.setPreferedLib tNG_prefered_image_lib
				image.addCommand tNG_prefered_imagemagick_path
				image.thumbnail KT_Server_MapPath(KT_makeIncludedPath(folder) & fileName), KT_Server_MapPath(KT_makeIncludedPath(thumbnailFolder)), thumbnailName, width, height, proportional
				If image.hasError() Then
					errorArr = image.getError()
					If tNG_debug_mode = "DEVELOPMENT" Then
						errMsg = errorArr(1)
						relPath = siteRootPath & "includes/tng/styles/cannot_thumbnail.gif"" />" & errMsg & "<img src=""" & siteRootPath & "includes/tng/styles/cannot_thumbnail.gif"
					Else
						relPath = siteRootPath & "includes/tng/styles/cannot_thumbnail.gif"
					End If
				End If
			End If
		Else
			relPath = siteRootPath & "includes/tng/styles/img_not_found.gif"
		End If
		Set fso = nothing
	End If
	tNG_showDynamicThumbnail = relPath
End Function




Sub tNG_prepareValues(ByRef colDetails)
	Dim type2alt: Set type2alt = Server.CreateObject("Scripting.Dictionary")
	type2alt("CHECKBOX_1_0_TYPE")	=	 "1"
	type2alt("CHECKBOX_-1_0_TYPE")	=	 "-1"
	type2alt("CHECKBOX_YN_TYPE")	=	 "Y"
	type2alt("CHECKBOX_TF_TYPE")	=	 "t"
	If colDetails.Exists("method") And colDetails.Exists("reference") And colDetails.Exists("type") Then
		colValue = KT_getRealValue(colDetails("method"), colDetails("reference"))
		colType = colDetails("type")

		If colDetails("method") = "VALUE" Then
			colValue = KT_DynamicData(colValue, nothing, null, null, null, null)
			If colDetails.Exists("default") Then
				colDetails("default") = colValue
			End If
		ElseIf colDetails.Exists("default") Then
			colDetails("default") = KT_DynamicData(colDetails("default"), nothing, null, null, null, null)	
		End If
		
		If colType = "CHECKBOX_YN_TYPE" OR colType = "CHECKBOX_1_0_TYPE" OR colType = "CHECKBOX_-1_0_TYPE" OR colType = "CHECKBOX_TF_TYPE" Then
			If KT_isSet(colValue) Then
				colValue = type2alt(colType)
			Else
				colValue = ""
			End If
		End If
		
		If colType = "DATE_TYPE" Or colType = "DATE_ACCESS_TYPE" Then
			colValue = KT_formatDate2DB(colValue)
			If colDetails.Exists("default") Then
				If colDetails("default") <> "" Then
					colDetails("default") = KT_formatDate2DB(colDetails("default"))
				End If	
			End If
		End If
	Else
		colValue = ""
	End If
	colDetails("value") = colValue
End Sub


' LOGIN functions

' retrieves the url path to the root site (eg /sites/MYSITE )
Function tNG_getRememberMePath()
	If isEmpty(Session("KT_SitePath")) Then
		KT_SetPathSessions
	End If
	tNG_getRememberMePath = Session("KT_SitePath") & "/"
End Function

Function tNG_generateRandomString(length)
	Randomize Timer
	newstring = KT_md5(CStr(Rnd * 999999999999))
	If length > 0 Then
		tNG_generateRandomString = Mid(newstring, 1, length)
	Else
		tNG_generateRandomString = newstring
	End If
End Function


Function tNG_encryptString (plain_string) 
	tNG_encryptString = KT_md5(plain_string)
End Function

Function tNG_activationLogin(connection)
	If Request.QueryString("kt_login_id") <> "" AND Request.QueryString("kt_login_random") <> "" Then
		' make an instance of the transaction object
		Set loginTransaction_activation = new tNG_login
		loginTransaction_activation.Init connection
		' register triggers
		' automatically start the transaction
		loginTransaction_activation.registerTrigger Array("STARTER", "Trigger_Default_Starter", 1, "VALUE", "1")
		' add columns
		loginTransaction_activation.setLoginType "activation"
		loginTransaction_activation.addColumn "kt_login_id", tNG_login_config("pk_type"), "GET", "kt_login_id", ""
		loginTransaction_activation.addColumn "kt_login_random", "STRING_TYPE", "GET", "kt_login_random", ""
		loginTransaction_activation.executeTransaction
		Set cols = loginTransaction_activation.columns
		If cols.Exists("kt_login_redirect") Then
			' return already computed redirect page
			tNG_activationLogin = loginTransaction_activation.getColumnValue("kt_login_redirect")
			Exit Function
		End If
	End If
	tNG_activationLogin = ""
End Function


Function tNG_cookieLogin(connection)
	If Session("kt_login_user") <> "" Then	
		Exit Function
	End If

	If Request.Cookies("kt_login_id") <> "" AND Request.Cookies("kt_login_test") <> "" Then
		' make an instance of the transaction object
		Set loginTransaction_cookie = new tNG_login
		loginTransaction_cookie.Init connection
		' register triggers
		' automatically start the transaction
		loginTransaction_cookie.registerTrigger Array("STARTER", "Trigger_Default_Starter", 1, "VALUE", "1")
		' add columns
		loginTransaction_cookie.setLoginType "cookie"
		loginTransaction_cookie.addColumn "kt_login_id", tNG_login_config("pk_type"), "COOKIE", "kt_login_id", ""
		loginTransaction_cookie.addColumn "kt_login_test", "STRING_TYPE", "COOKIE", "kt_login_test", ""
		loginTransaction_cookie.executeTransaction
	End If
End Function

' Used by addFields trigger
Sub tNG_addColumn (ByRef tNG, colName__param, type__param, method__param, reference__param) 
	If KT_in_array(tNG.getTransactionType(), Array("_insert", "_multipleInsert"), True) Then
		tNG.addColumn colName__param, type__param, method__param, reference__param, ""
	Else
		tNG.addColumn colName__param, type__param, method__param, reference__param
	End If	
End Sub

'wrapper for KT_getResource(resourceName, "NXT")
Function NXT_getResource(resourceName)
	NXT_getResource = KT_getResource(resourceName, "NXT", null)
End Function

%>