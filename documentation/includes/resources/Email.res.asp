<%
Dim res: Set res = Server.CreateObject("Scripting.Dictionary")
res("EMAIL_ERROR_SENDING") = "Error sending e-mail."
res("ASP_EMAIL_MISSING_OBJECT") = "Error sending e-mail."
res("ASP_EMAIL_ERROR_NOCONFIG") = "Error sending e-mail."
res("EMAIL_ERROR_SENDING_D") = "E-mail couldn't be sent. Error returned: %s."
res("ASP_EMAIL_MISSING_OBJECT_D") = "Cannot send e-mails on this server because Collaborative Data Object used for sending e-mails was not found."
res("ASP_EMAIL_ERROR_NOCONFIG_D") = "Please use InterAKT Control Panel to configure the E-mail Settings."
%>