<%
Dim res: Set res = Server.CreateObject("Scripting.Dictionary")
res("ASP_FOLDER_NO_PATH") = "No folder path supplied."
res("ASP_FOLDER_CREATE_ERR") = "Error creating folder."
res("ASP_FOLDER_INVALID_PATH") = "Invalid folder path supplied."
res("ASP_FOLDER_MISSING") = "Folder doesn't exist."
res("ASP_FOLDER_DELETE_ERR") = "Error deleting folder."
res("ASP_FOLDER_NO_PATH_D") = "No folder path supplied."
res("ASP_FOLDER_CREATE_ERR_D") = "Error creating folder '%s'.<br />Error description: %s<br />"
res("ASP_FOLDER_INVALID_PATH_D") = "Invalid folder path supplied '%s.'"
res("ASP_FOLDER_MISSING_D") = "Folder '%s' doesn't exist."
res("ASP_FOLDER_DELETE_ERR_D") = "Error deleting folder '%s'.<br />Error description: %s<br />"
res("ASP_FOLDER_RENAME_NO_FILE") = "Folder Error. Internal Error."
res("ASP_FOLDER_RENAME_NO_FILE_D") = "Error renaming folder. Source folder '%s' missing."
res("ASP_FOLDER_RENAME_EXISTS") = "Folder Error. Internal Error."
res("ASP_FOLDER_RENAME_EXISTS_D") = "Error renaming folder. Destination folder '%s' already exists."
res("ASP_FOLDER_RENAME") = "Folder Error. Internal Error."
res("ASP_FOLDER_RENAME_D") = "Folder Error. Error renaming folder '%s' to '%s'. <br />%s."
%>