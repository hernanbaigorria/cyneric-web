<%
Dim res: Set res = Server.CreateObject("Scripting.Dictionary")
res("the_mask_is") = "The mask is:"
res("the_format_is") = "Please insert only numbers"
res("also_floats") = ", including floats"
res("also_negatives") = ", including negatives"
res("max_character_number") = "Maximum number of characters reached!"
res("the_date_format_is") = "The date format is:"
res("calendar_button") = "..."
res("rte_maximum_reached") = "Maximum number of characters reached!"
res("dyn_add_label_text") = "Add this"
res("dyn_are_you_sure_text") = "Are you sure you want to add the '%s' element to the database?"
res("dyn_submit_text") = "Some data you entered is not in the database. Shall we submit it first?"
res("dyn_default_option_text") = "Please select"
%>