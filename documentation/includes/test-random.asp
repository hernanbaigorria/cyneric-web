<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%

'For browsers (set expiration date some time well in the past)
Response.Expires = -1000

'For HTTP/1.1 based proxy servers
Response.CacheControl = "no-cache"

'For HTTP/1.0 based proxy servers
Response.AddHeader "Pragma", "no-cache"

Dim Tip(10)

Tip(1)  = "This is tip 1"
Tip(2)  = "This is tip 2"
Tip(3)  = "This is tip 3"
Tip(4)  = "This is tip 4"
Tip(5)  = "This is tip 5"
Tip(6)  = "This is tip 6"
Tip(7)  = "This is tip 7"
Tip(8)  = "This is tip 8"
Tip(9)  = "This is tip 9"
Tip(10) = "This is tip 10"

Function RandomNumber(intHighestNumber)
	Randomize
	RandomNumber = Int(intHighestNumber * Rnd) + 1
End Function

Dim strRandomTip
strRandomTip = Tip(RandomNumber(10))
%>
<html>
<head>
  <title>Random Tip Example</title>
</head>
<body>
  <div style="border:thin solid black;margin:1em;
    padding:1em;height:100px;width:100%">

    <h1>Tip of the Day</h1>
    <p><%= strRandomTip %><p>

  </div>
  <br />

  <form name="frmNewTip">
  <input type="button" name="cmdNewTip" value="Another Tip"
    onClick="window.open(document.location.href,'_top');" />
  </form>
</body>
</html>
<% Response.End %>

