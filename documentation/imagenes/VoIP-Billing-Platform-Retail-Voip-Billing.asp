<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><%= Session("nombre")%></title>
<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../includes/chromejs/chrome.js">
</script>
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />

</head>
<body onselectstart="return false">
<table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif"><h1>VoIP Billing Platform&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
           <!-- <li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
            <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Customer Area </a></li>
            <li><a href="casos-de-exito.asp">Success Histories</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;The best partner for networks and Voip Invoicing
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a> </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="productos-CRA.asp" onfocus="if(this.blur)this.blur()">Cyner CRA</a>  <a href="productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="productos-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a> 
			
			<a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			<a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome">
  <tr>
    <td width="507" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top"><h2>Residential VoIP Telephony</h2>
            <div id="divfrase">
              <script type="text/javascript" src="../includes/marquee.js"></script>
              <a id="fade_link"></a></div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h1>We provide an efficient and flexible solution </h1>
            <p>Part of our family of solutions for commercial VoIP Operations. With our platform you can define pre and post-paid customers, even those who fall into the credit category, which decreases operational risks. Independently of the type of customer or the access type used, the platform records each operation thus making the business's daily commercial and technical management easier.</p>
            <p>&nbsp;</p>
            <h1>The power of having the information </h1>
            <p>For a successful operation it is necessary to keep up the level of service, the back-end agile and have cash-flow and profit information per destination. We provide you with all that.  </p>
            <p>&nbsp;</p>
            <h1>Business model features and platform performance </h1>
            <p>&nbsp;</p>
            <div id="Accordion1" class="Accordion" tabindex="0">
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Managing a large number of customers</div>
                <div class="AccordionPanelContent">
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Our system allows you to limitlessly define customers. 
We have customers that process 40,000 invoices per month using our platform.
 </p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"> <img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />We support the following retail models</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>
                    <ul>
                      <li>	Local access number, indirect dialing</li>
                      <li>ATA service registered with SIP servers</li>
                      <li>DID (remote numbers) services</li>
                      <li>Calling cards platform access</li>
                      <li>Collect calls</li>
                    </ul>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Price list definition</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Our platform supports different price lists, and works with them in Excel format.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Services and taxes definition</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>With our platform you will be able to define monthly services that will show up on the customer's invoice. This way you can increase your billing, offer flat rates per month, and charge for Technical Service, Voice-mail, Fax-to-mail services, etc.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Promotions definition</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/> Our platform allows you to define promotions according to dates, day of the week and time of day. This way, if the customer makes calls during this period, promotional rates will be applied. </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Multiple possibilities of printing invoices </div>
                <div class="AccordionPanelContent">
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Paper, email or file exported invoices. Our software allows you to invoice clients, automatically print out or e-mail each customer, as well as export them to pdf format for future administrative or commercial use.</p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Alerts and disconnection</span></div>
                <div class="AccordionPanelContent"><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>The platform allows you to send automatic alerts to clients who have overdue invoices, as well as disconnect post-paid  customers who have overdue invoices.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Pre-paid models</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>You can define customers with  credit and the system will authenticate each call discounting  the amount from the balance in real-time.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Customer web access</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/> Your clients can see invoices, calls, make payments, file complaints, and purchase credit through our website for retail customers.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Define vendors, distribute profits and pay commissions</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Define vendors to determine profits, which destinations are more profitable, which destination needs high quality and compare traffic with your vendors' invoices. </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Reports, decision-making facilitator </div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Profit, traffic, overdue invoices, complaints, quality by destination, vendor traffic, daily usage and potential fraud reports, among others.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Numbers/DIDs: Stock control</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Local number stock management that allows auto provisioning and control.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Compatibility, integration and portability </div>
                <div class="AccordionPanelContent">
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Real-time integration with most of the equipment available on the market.</p>
                </div>
              </div>
            </div>
            <p>
              <script type="text/javascript">
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
             </script>
            </p>
            <p>&nbsp;</p>
            <h1>We have even more to show you </h1>
            <p>With our platform is easy define retail business models to keep existing customers, generate new ones and attract new business by marketing an excellent service, truly decreasing operational costs, and keeping daily issues under control, as well as the most delicate details of your business. <a href="../a1/contact-cyneric.asp?motivo=Nos interesa evaluar su plataforma" target="_self">Please request a free demostration</a>. </p>
            <p>&nbsp;</p>
            <hr />
            <p>
              <script type="text/javascript">
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
             </script>
            </p>
            <h1><img src="../imagenes/ico-mas.gif" width="22" height="23" hspace="3" align="absmiddle" />More information, products and services </h1>
            <strong>What is the business model of your corporation? </strong>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator, Originator</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP Telephony</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Cabin Booth Centers</a></li>
              </ul>
          </td>
        </tr>
      </table></td>
    <td width="270" align="center" valign="top"><p>&nbsp;</p>
      <table width="240" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-business.gif); background-repeat:no-repeat">
        <tr>
          <td width="227" height="182" align="left" valign="top"><h1>VoIP Platform Billing </h1>
            <p>What is the business model of your corporation?  </p>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator, Originator</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP Telephony</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Cabin Booth Centers</a></li>
              </ul></td>
        </tr>
      </table>
      <table width="240" height="182" border="0" cellpadding="0" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-sreenshots.gif); background-repeat:no-repeat">
        <tr>
          <td width="240" height="182" align="left" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','240','height','182','hspace','0','vspace','0','src','../imagenes/s-wholesale','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','wmode','transparent','movie','../imagenes/s-wholesale' ); //end AC code
      </script>
              <noscript>
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="240" height="182" hspace="0" vspace="0">
                <param name="movie" value="../imagenes/s-wholesale.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <embed src="../imagenes/s-wholesale.swf" width="240" height="182" hspace="0" vspace="0" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
              </object>
            </noscript></td>
        </tr>
      </table>
      <!--#include file="../includes/es-ban-demo.asp" -->
<table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat; background-position:bottom"><h1>We provide complete support with our products, services and implementations. </h1>
              <p>&nbsp;</p></td>
        </tr>
      </table>
            <iframe src="../includes/soporte.html" name="soporte" width="250" marginwidth="0" height="116" marginheight="0" align="left" scrolling="No" frameborder="0" hspace="0" vspace="0" id="lasnews" allowtransparency="true" application="true" style="margin-left:7px; margin-top:4px"></iframe></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><!--#include file="../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
