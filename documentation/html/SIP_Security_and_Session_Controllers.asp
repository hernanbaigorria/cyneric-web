<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
SIP Security and the IMS Core<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">

<b>Executive Summary</b><br />
Rolling out a public SIP service brings with it several security issues. Both users and Service Providers must understand these issues, but the burden is with the Service Provider to offer a secure and reliable service to the user. This means they must show that the service does not compromise existing security and that the user's public presence is protected and managed. The Service Providers must also protect their own <a href="../../a1/VoIP-Network-platform.asp">network</a>s from outside attacks and service abuse.<br />
<br />
This White Paper examines the security issues faced by users and looks at how the Service Provider can overcome these through the deployment of session border controllers in the access <a href="../a1/VoIP-Network-platform.asp">network</a> and in the core.<br />
<br />
<b>What problems do users face?</b><br />
<br />
<b>The Firewall - a Brick Wall</b><br />
When you fire up your browser and surf the net from your desk at work, have you ever wondered what is going on in the <a href="../a1/VoIP-Network-platform.asp">network</a>? You take it for granted that you have the freedom to access web sites, yet at the same time you expect to be kept safe from malicious attacks on your machine. The important fact that helps to keep you safe is that you requested the information from the Web. This means that the devices keeping your <a href="../a1/VoIP-Network-platform.asp">network</a> safe will allow your outgoing connection to the Web server and accept the reply returning from it. This enables the Firewall to reject incoming messages that it was not expecting.<br />
<br />
Making a call over IP means setting up two separate connections. One connection is for signalling messages, the other for the media. Naturally, these two connections are related. The clients making and receiving the call use information in the signalling connection to learn how to make the media connection. This works well when the clients are in the same <a href="../a1/VoIP-Network-platform.asp">network</a>. However, the devices such as NATs and Firewalls that separate <a href="../a1/VoIP-Network-platform.asp">networks</a> are unaware that these connections are related. This means an invitation in the signalling connection to send voice to a particular address will be invisible to a Firewall. The Firewall will therefore reject the incoming voice.<br />
<br />
<img src="images/The_Firewall.jpg" /><br />
<br />
Now think of making a making a phone call to Bill who is outside your <a href="../a1/VoIP-Network-platform.asp">network</a>. First, your phone sends out an invitation to Bill. This goes out through the Firewall and finds Bill's IP phone. When Bill answers the call, his phone sends an acknowledgement back to your phone, which will reach you as the Firewall accepts the reply on the same port. However, when the phones send media, any Firewalls in the path will probably pass the outgoing media, but reject the incoming media. The result is the call appears to connect, but the voice path is broken.<br />
<br />
<br />
Now think of receiving a phone call. You don't know you are going to receive the call and you don't know where it is coming from. The Firewall sees an unexpected incoming message from an unknown source so it blocks it. So, the call fails to reach you.<br />
<br />



</p>




<p class="virtualpage3">
<b>How do I make myself visible?</b><br /><br />
<b>A Public Address - A Public Liability</b><br />
Firstly, you need a public IP address so you can be called; this must be advertised so that you can be found. This takes care of the signalling. Secondly, you need a second IP address to exchange media. This is the address you will invite callers to send their media to.<br />
<br />
Because your client is sitting behind a Firewall, these addresses have to be on the public side of the Firewall. The Firewall must link these addresses back to your client on the inside. This means leaving two holes in the Firewall permanently linked to your client. Now, far from being an anonymous Web user, you have advertised your presence to the world and invited them in. Unfortunately, this is like advertising your real address and leaving the front door open.<br />
<br />
<b>Make your Firewall Work</b><br />
The Firewall is there to protect you and your <a href="../a1/VoIP-Network-platform.asp">network</a> so you should make the best use of it. You can achieve this by making your IP phone call work more like your browser. That means ensuring all signalling and media connections are started outwards - even incoming calls. This may sound impossible, but that is what session border controllers do.<br />
<br />
<b>How can a Session Border Controller Help?</b><br />
The session border controller sits within the public <a href="../a1/VoIP-Network-platform.asp">network</a> and is the point to which you send your signalling. When you start your client, it registers with a server in the public <a href="../a1/VoIP-Network-platform.asp">network</a>. This registration message is sent via the session border controller, which modifies the message and registers one of its own addresses with the server. Your public address is now on the session border controller. So, this is like having a PO box number; you can be reached but your real address is only known to the post office.<br />
<br />
Now you can change your Firewall to allow the signalling to be started as an outgoing connection. You can also restrict the destination of this connection to be only the session border controller.<br />
<br />
<img src="images/Firewall_Working.jpg" /><br />
<br />
<b>Session Border Controller Benefits</b><br />
A session border controller in the public <a href="../a1/VoIP-Network-platform.asp">network</a> allows you to create stricter Firewall rules:<br />
<br />
- All signalling and media connections can be dynamically opened<br />
- All signalling and media connections are started as outbound connections<br />
- The Firewall can restrict connections to just the session border controller<br />
- The session border controller improves security by:<br />
- Hiding your real address<br />
- Dynamically allocating media ports<br />
- Policing signalling connection<br />
- Policing media connection<br />
<br />

</p>

<p class="virtualpage3">
<b>How can Service Providers Help?</b><br />
If you are a Service Provider, you are probably well aware of these problems which are faced by customers who need a public SIP service. The need to connect and to create a public presence has to be weighed against the security implications. The Service Provider is ideally placed to address these issues. Deploying a carrier-class session border controller in the access <a href="../a1/VoIP-Network-platform.asp">network</a> overcomes a number of issues:<br />
<br />
It solves the traversal problem for all NATs from customer to core<br />
It provides a secure connection to the user<br />
It works with existing Customer Premise Equipment<br />
It controls which customer uses which service<br />
<br />
NATs are often used in access <a href="../../a1/VoIP-Network-platform.asp">network</a>s to create more IP addresses. This means that solving NAT traversal just for the customer premise is not a complete answer. The Service Provider must solve the traversal problem for multiple NATs. Placing a session border controller between the access and core <a href="../../a1/VoIP-Network-platform.asp">network</a>s achieves this.<br />
<br />
With a session border controller in place, the Service Provider can offer services to any customer. The customer does not have to replace any of their equipment. The session border controller offers a secure, managed public presence for each user. The customer's Firewall can limit outgoing connections to the session border controller.<br />
<br />
Offering a secure public presence to the customer enhances any service offering. The Service Provider is minimising the visibility and hence exposure of the customer's <a href="../a1/VoIP-Network-platform.asp">network</a>. The service becomes a security enhancement rather than a security problem.<br />
<br />
<img src="images/Session_Border.jpg" /><br />
<br />
The Newport Networks' 1460 session border controller is designed for just such duty. All signalling and media passing through the 1460 is policed. This further enhances the security of the connection:<br />
<br />
- Packets from unauthorised sources are blocked<br />
- Packets carrying invalid protocols can be blocked<br />
- Media streams can be rate limited<br />
- QoS settings can be policed<br />
<br />
This means that the 1460 is effective in limiting the effects of port scanning. Denial of Service attacks against clients can be blocked or restricted. This protects both the access <a href="../a1/VoIP-Network-platform.asp">network</a> from overload, as well as the customer. The 1460 session border controller can offer Service Providers a secure, reliable way of connecting to all customers, even those behind Firewalls, without compromising security.<br />
<br />

</p>


<p class="virtualpage3">
<b>Security between <a href="../../a1/VoIP-Network-platform.asp">Network</a>s</b><br />
Securing the customer connection is not the only precaution that a Service Provider must take. Connections at peering points must also be secured. A recent Yankee Group report cited '<a href="../a1/VoIP-Network-platform.asp">network</a> topology hiding' as one of the key drivers behind deploying session border controllers. We refer to a session border controller sited at a peering point as a Core Session Border Controller. A Core Session Border Controller performs several duties:<br />
<br />
- It hides the real addresses of your customers from peer <a href="../../a1/VoIP-Network-platform.asp">networks</a><br />
- It hides the details of your internal <a href="../a1/VoIP-Network-platform.asp">network</a> from peer <a href="../../a1/VoIP-Network-platform.asp">network</a>s<br />
- It polices the connection to other Service Providers<br />
- It can remark QoS settings between Service Providers<br />
- It provides detailed call information<br />
<br />
<b>Protect your Customers</b><br />
A Core Session Border Controller acts as a proxy for all users in a <a href="../a1/VoIP-Network-platform.asp">network</a>. The home <a href="../a1/VoIP-Network-platform.asp">network</a>'s DNS ensures that all off-<a href="../a1/VoIP-Network-platform.asp">network</a> calls are routed to the Core Session Border Controller. It does this by giving the address of the Core Session Border Controller as the address of any remote Call Agent. The session border controller creates new signalling and media addresses that are sent to the remote <a href="../a1/VoIP-Network-platform.asp">network</a>. The called party in the remote <a href="../a1/VoIP-Network-platform.asp">network</a> sees the session border controller as the source of the call. All signalling and media will be returned via the session border controller. In this way, the called party has no visibility of the user's real address.<br />
<br />
Incoming calls are also routed via the home network's Core Session Border Controller. The remote network's DNS supplies the address of the Core Session Border Controller as the home network's Call Agent. Therefore, the Core Session Border Controller receives all calls coming into the home <a href="../a1/VoIP-Network-platform.asp">network</a>. It presents its own addresses in the reply for both signalling and media.<br />
<br />
This architecture prevents visibility of the user's real <a href="../a1/VoIP-Network-platform.asp">network</a> address in the remote <a href="../a1/VoIP-Network-platform.asp">network</a>. The Core Session Border Controller can prevent scanning and DOS attack at the peering point. At Newport N<a href="../../a1/VoIP-Network-platform.asp">etworks</a>, we believe that the carrier-class 1460 session border controller is ideal for deployment in these demanding locations. Designed for high availability, it offers Service Providers a reliable method of securely interconnecting multimedia <a href="../../a1/VoIP-Network-platform.asp">network</a>s.<br />
<br />
<img src="images/Core_Session.jpg" /><br />
<br />
<b>Protect your <a href="../../a1/VoIP-Network-platform.asp">Network</a></b><br />
In addition to hiding the address of the user, the Core Session Border Controller hides the internal <a href="../../a1/VoIP-Network-platform.asp">network</a> details. The Core Session Border Controller acts as an end-point for the two legs of the SIP call: one to the home <a href="../a1/VoIP-Network-platform.asp">network</a> and one to the remote <a href="../a1/VoIP-Network-platform.asp">network</a>. This means that details of routing in one leg are not passed to the other. There is a clean separation between the networks. Therefore, the only information visible in the remote <a href="../a1/VoIP-Network-platform.asp">network</a> is that of its own <a href="../a1/VoIP-Network-platform.asp">network</a>.</p>

<p class="virtualpage3">
<b>Police the Border</b><br />
A Core Session Border Controller connects all inter-<a href="../a1/VoIP-Network-platform.asp">network</a> multimedia <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. The Newport Networks' 1460 session border controller polices <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> flow-by-flow as it enters and leaves the <a href="../a1/VoIP-Network-platform.asp">network</a>. Calls established using SIP carry an identifier of the media type. The 1460 measures the actual flow against expected flow for the requested media type. This can prevent service theft, i.e. requesting a low bandwidth connection and using high bandwidth media. If excessive data rates are seen, corrective action is taken. For example, it can dump excess <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, it can generate an alarm or it can create punitive charging records.<br />
<br />

The 1460 session border controller can also check and, if necessary, remark QoS bits. This can be done generically for each <a href="../a1/VoIP-Network-platform.asp">network</a>, or specifically for each session. This prevents users from manipulating the <a href="../a1/VoIP-Network-platform.asp">quality</a> settings of their call to get a better service than they are paying for. This also enables carriers to enforce IP-IP interconnect agreements to deliver 'end-to-end' SLAs<br />
<br />
<b>Conclusion</b><br />
Session border controllers enhance the security of multimedia networks both in the access <a href="../a1/VoIP-Network-platform.asp">network</a> and in the core. In the access <a href="../a1/VoIP-Network-platform.asp">network</a>, they hide a user's real address and provide a managed public address. This public address can be policed, minimising the opportunities for scanning and DOS attacks. Session border controllers permit access to clients behind Firewalls whilst maintaining the Firewalls effectiveness. In the core, session border controllers protect both the users and the <a href="../a1/VoIP-Network-platform.asp">network</a>. They hide <a href="../a1/VoIP-Network-platform.asp">network</a> topology and users' real addresses. They can also police bandwidth and QoS abuse.<br />
<br />
The Newport Networks' 1460 session border controller is a carrier-class solution suitable for these applications. Built to provide 'five 9s' availability, it is designed for demanding deployments at both <a href="../a1/VoIP-Network-platform.asp">network</a> peering points and in the access <a href="../a1/VoIP-Network-platform.asp">network</a>. Service Providers who plan to roll out voice and multimedia services over IP must consider security as an integral part of the service. Deploying the right infrastructure lays the foundation stones upon which all successful future services will be built.
</p>
</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/SIP_Security_and_Session_Controllers.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../doc/SIP_Security_and_Session_Controllers.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
