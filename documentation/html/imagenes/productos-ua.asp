<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><%= Session("nombre")%></title>
<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../includes/chromejs/chrome.js">
</script>
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
	
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
</head>
<body>
<table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif"><h1>Products &amp; Services  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
            <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Customer Area </a></li>
            <li><a href="casos-de-exito.asp">Success Histories</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;The best partner for networks and Voip Invoicing
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a> </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="productos-CRA.asp" onfocus="if(this.blur)this.blur()">Cyner CRA</a>  <a href="productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="productos-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a> 
			<a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			
			<a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" id="menuhome">
  <tr>
    <td width="507" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top" background="VoIP-Billing-Platform.asp"><h2>Products: Cyner User Admin </h2>
            <div id="divfrase">
              <script type="text/javascript" src="../includes/marquee.js"></script>
              <a id="fade_link"></a></div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h1>Global scenario</h1>
            <h3>Our platform's management interface</h3>
            <p>Part of our family of solutions for commercial VoIP Operations.<b> Cyner User Admin</b> was designed to centralize all commercial operations. It is the interface we use to establish real-time communication and management of your business.</p>
            <p>It supports the following customer models:</p>
            <p><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Pre-paid </p>
            <p><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Post-paid  </p>
            <p><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Post-paid with credit caps</p>
            <p>Recognize multiple type of users, giving each different levels of access and restrictions, thus accomplish with security standards needed for an adequate operational management. </p>
            <p>&nbsp; </p>
            <h3>CRM, Customer management</h3>
            <p> Creation and definition of customers under different methods</p>
            <p> Commercial and technical reports</p>
            <p> Price lists creations, promotion creation and assignment and more.  </p>
            <p>&nbsp;</p>
            <h3>Multiple User levels</h3>
            <p> <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Administrators<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Resellers<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Agents<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Customers<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Carriers</p>
            <p>&nbsp;</p>
            <h3>Provisioning</h3>
            <p>Access types.</p>
            <p> ANI (Access Number Identification)</p>
            <p> Destination number</p>
            <p> PIN access</p>
            <p> DID-Ring number</p>
            <p> Prefix number</p>
            <p> Interface</p>
            <p> Origin IP/Destination IP</p>
            <p> Origin Peer/Destination Peer</p>
            <p>&nbsp;</p>
            <h3>Billing and collection  services  </h3>
            <p>Price lists and services assignment  </p>
            <p>Discounts, credits and more </p>
            <p>Payment registration.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h3>Price list module</h3>
            <p>Create, change and administer price lists</p>
            <p>Excel import and export </p>
            <p>Determine the price according to: call duration or flat fee</p>
            <p>Several possibilities  to define time frames for call rounding.  </p>
            <p>Creation and  management of extra services, fees, and taxes</p>
            <p>&nbsp;</p>
            <h3>Promotions module </h3>
            <p>Create and schedule promotions .</p>
            <p>Promotions by time of the day, day of the week</p>
            <p>Promotions applied to multiple price lists</p>
            <p>&nbsp;</p>
            <h3>Commercial reports</h3>
            <p>Profits by provider</p>
            <p> Customer totals</p>
            <p> Price lists</p>
            <p> Customer list</p>
            <p> Invoices and payments</p>
            <p> Profits</p>
            <p> Daily profits</p>
            <p> Debtors</p>
            <p> Sales</p>
            <p> Total amount invoiced</p>
            <p>Total amount due</p>
            <p> Total accounts receivable</p>
            <p> Invoice summaries</p>
            <p>&nbsp;</p>
            <h3>Technical reports</h3>
            <p>Access per customer</p>
            <p>Non-processed calls</p>
            <p> Usage by provider</p>
            <p> Usage by destination</p>
            <p> Usage by destination carrier</p>
            <p>CDR records exporting</p>
            <p>CDR by destination</p>
            <p>CDR by interface</p>
            <p>CDR by origin</p>
            <p>CDR by country </p>
            <p>Hourly usage</p>
            <p>Trouble ticket follow-ups </p>
            <p>Hardware status</p>
            <p>Active calls</p>
            <hr />
            <b>Cyner UA</b> is the core part of our Billing platform for VoIP operation and is designed to provide the necessary conditions for adequate commercial operations functioning, such as: 
            <p><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()"><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" border="0" align="absmiddle" />	VoIP Wholesale</a></p>
            <p><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" target="_self" onfocus="if(this.blur)this.blur()"><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" border="0" align="absmiddle" /></a><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">	Residential/Retail VoIP Telephony</a></p>
            <p><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" target="_self" onfocus="if(this.blur)this.blur()"><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" border="0" align="absmiddle" /></a><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">	Calling Cards Administrator</a></p>
            <p><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" target="_self" onfocus="if(this.blur)this.blur()"><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" border="0" align="absmiddle" /></a><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">	Cabin booth</a></p>
            <p>&nbsp;</p>
            <p>It's  easy to use our platform to define retail business models to keep existing customers, generate new ones and attract new business by marketing an excellent service, truly decreasing operational costs, and keeping daily issues under control, as well as the most delicate details of the business. </p>
            <hr />
            <h1><img src="../imagenes/ico-mas.gif" width="22" height="23" hspace="3" align="absmiddle" />More information, products and services </h1>
            <strong>What is the business model of your corporation? </strong>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator, Originator</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP Telephony</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Cabin Booth Centers</a></li>
              </ul></td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
    <td width="270" align="center" valign="top"><p>&nbsp;</p>
      <p>&nbsp;</p>
      <table width="240" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-business.gif); background-repeat:no-repeat">
        <tr>
          <td width="227" height="182" align="left" valign="top"><h1>VoIP Platform Billing </h1>
            <p>What is the business model of your corporation?</p>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator, Originator</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP Telephony</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Cabin Booth Centers</a></li>
            </ul></td>
        </tr>
      </table>
      <table width="240" height="258" border="0" cellpadding="0" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-sreenshots.gif); background-repeat:no-repeat">
        <tr>
          <td width="240" height="182" align="left" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','240','height','182','src','../imagenes/s-prod-UA','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','wmode','transparent','movie','../imagenes/s-prod-UA' ); //end AC code
</script>
            <noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="240" height="182">
              <param name="movie" value="../imagenes/s-prod-UA.swf" />
              <param name="quality" value="high" />
              <param name="wmode" value="transparent" />
              <embed src="../imagenes/s-prod-UA.swf" width="240" height="182" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
            </object>
            </noscript></td>
        </tr>
        <tr>
          <td height="44" align="left" valign="top"><a href="images/g1-ua.jpg" rel="lightbox"><img src="images/c1-ua.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a> <a href="images/g2-ua.jpg" rel="lightbox"><img src="images/c2-ua.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a> <a href="images/g3-ua.jpg" rel="lightbox"><img src="images/c3-ua.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a> <a href="images/g4-ua.jpg" rel="lightbox"><img src="images/c4-ua.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a></td>
        </tr>
      </table>
      <br />
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>Descargar m&aacute;s infomaci&oacute;n </h1>
            <p align="right"><a href="VOIP-Cyneric_en.pps" target="_blank"><img src="../imagenes/ico-d-ppt.gif" alt="Plataforma VoIP, powerpoint" width="48" height="39" hspace="0" vspace="9" border="0" /></a><a href="i-ua.pdf" target="_blank"><img src="../imagenes/ico-d-pdf.gif" alt="Cyner CDR whitepaper" width="28" height="39" hspace="3" vspace="9" border="0" /></a><a href="esp-cdr.pdf" target="_blank"></a><img src="../imagenes/ico-d.gif" width="40" height="41" hspace="0" vspace="9" /></p></td>
        </tr>
      </table>
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat; background-position:bottom"><h1>We provide complete support with our products, services and implementations. </h1>
            <p>&nbsp;</p></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><!--#include file="../includes/footer.asp" --></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
