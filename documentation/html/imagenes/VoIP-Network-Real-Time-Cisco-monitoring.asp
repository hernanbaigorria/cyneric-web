<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><%= Session("nombre")%></title>
<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../includes/chromejs/chrome.js">
</script>
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
	
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>

</head>
<body onselectstart="return false">
<table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif"><h1>VoIP Network solution&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
           <!-- <li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
            <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Customer Area </a></li>
            <li><a href="casos-de-exito.asp">Success Histories</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;The best partner for networks and Voip Invoicing
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a> </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="productos-CRA.asp" onfocus="if(this.blur)this.blur()">Cyner CRA</a>  <a href="productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="productos-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			<a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome">
  <tr>
    <td width="507" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top"><h2>CheckMyCisco.com<br />
           Cisco Systems, real-time monitoring and alerts</h2>
            <div id="divfrase">
              <script type="text/javascript" src="../includes/marquee.js"></script>
              <a id="fade_link"></a></div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h1>Quality continuity,  the foundation of business. What will we provide you with? </h1>
            <p> <a href="VoIP-Network-platform.asp">Part of our family of solutions for VoIP Networks.</a> Something  simple and effective for your IP telephony operation. We provide you with <strong>certainty</strong> and <strong>security</strong> for your <strong>Cisco</strong> systems monitoring. </p>
            <p><br />
            You need to be informed and you should know when problems arise. We have the tool that simplifies your business's technical operations.</p>
            <p>&nbsp;</p>
            <h1>Global features and service characteristics  </h1>
            <p>&nbsp;</p>
            <div id="Accordion1" class="Accordion" tabindex="0">
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Surpass expectations</div>
                <div class="AccordionPanelContent">
                  <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/> 
                  <p>                 Tackle the competition using a remote quality control system.</p>
                  <p> Detailed <b>Cisco </b>systems information.</p>
                  <p> Ethernet and Fast Ethernet traffic graphs.</p>
                  <p> Cell phone and e-mail alerts, multiple addresses, and all combinations.</p>
                  <p> Maximize your E1/T1 usage performance.</p>
                  <p> Predefine alert profiles by quality and failure.</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Increase profits by decreasing costs</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>
                                   <p> Improve customer experiences by increasing their level of satisfaction with the service.</p>
                                   <p>With no additional hardware.</p>
                                   <p> With no additional software.</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Better availability of your engineers </span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>
                  <p> With no additional staff.</p>
                  <p>Our tool does not require any additional resources in order to use it at the 100%</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">All this with just one IP address</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>
                  <p>                  With just one IP address and through an SNMP protocol you can be remotely informed, in real-time, 24/7, and via multiple channels.              </p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /><span class="AccordionPanelContent">Real-time information</span></div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:180px"/>
                    <p>Detailed information of your Cisco systems.<br />
                      Ethernet and Fast Ethernet traffic graphs.<br />
                      Monitoring of CPU and memory use.<br />
                      Cell phone and e-mail alerts, multiple addresses,  and every combination.<br />
                      Maximize your E1/T1 usage performance.<br />
                      Predefine alert profiles by quality and failure.</p>
                </div>
              </div>
            </div>
            <p>
              <script type="text/javascript">
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
             </script>
            </p>
            <p>&nbsp;</p>
            <h1>&nbsp;</h1>
            <h1>We have more to show you</h1>
            <p>VWe invite you to visit the <b>Check My Cisco</b> website and see for yourself the versatility and detailed information about this service. <br /> 
            Take advantage of the two-week free trial.  Go to  <a href="http://www.checkmycisco.com" target="_blank">www.checkmycisco.com</a></p>
            <p>&nbsp;</p>
            <hr />
            <p>
              <script type="text/javascript">
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
             </script>
            </p>
            <h1><img src="../imagenes/ico-mas.gif" width="22" height="23" hspace="3" align="absmiddle" />More information, products and services </h1>
            <h3>VoIP Networks & Quality of Service </h3>
            <ul>
              <li> <a href="VoIP-Network-Real-Time-alert-monitoring.asp" onfocus="if(this.blur)this.blur()">Real-time alerts and network monitoring</a></li>
              <li> <a href="VoIP-Network-Specialized-Tech-Support-for-Retail-Wholesale-Networks.asp" onfocus="if(this.blur)this.blur()">Cisco Systems, real-time monitoring and&nbsp;alerts</a></li>
            </ul></td>
        </tr>
      </table></td>
    <td width="270" align="center" valign="top"><p>&nbsp;</p>
      <table width="244" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat">
        <tr>
          <td width="227" height="182" align="left" valign="top"><h1>VoIP Networks, Quality of Service</h1>
              <p>Exhaustive control and monitoring, decrease the uncertainty in the quality of service that you provide, and the service  you receive from your vendors. </p>
              <ul>
                <li> <a href="VoIP-Network-Real-Time-alert-monitoring.asp" onfocus="if(this.blur)this.blur()"> Real-time alerts and control</a></li>
                <li><a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()"> Cisco Systems, real-time monitoring and &nbsp;&nbsp;&nbsp;&nbsp;alerts</a></li>
                <li> <a href="VoIP-Network-Specialized-Tech-Support-for-Retail-Wholesale-Networks.asp" onfocus="if(this.blur)this.blur()"> Specialized technical support for Wholesale &nbsp;&nbsp;&nbsp;&nbsp;and Residential Telephone networks</a></li>
              </ul></td>
        </tr>
      </table>
      <table width="240" height="261" border="0" cellpadding="0" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-sreenshots.gif); background-repeat:no-repeat">
        <tr>
          <td width="240" height="182" align="left" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','240','height','182','hspace','0','vspace','0','src','../imagenes/s-cmc','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','wmode','transparent','movie','../imagenes/s-cmc' ); //end AC code
      </script>
              <noscript>
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="240" height="182" hspace="0" vspace="0">
                <param name="movie" value="../imagenes/s-cmc.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <embed src="../imagenes/s-cmc.swf" width="240" height="182" hspace="0" vspace="0" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
              </object>
            </noscript></td>
        </tr>
        <tr>
          <td height="47" align="left" valign="top"><a href="images/g1-cmc.jpg" rel="lightbox"><img src="images/c1-cmc.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a> <a href="images/g2-cmc.jpg" rel="lightbox"><img src="images/c2-cmc.jpg" alt="" width="57" height="40" border="0" /> </a><a href="images/1.jpg"></a><a href="images/g3-cmc.jpg" rel="lightbox"><img src="images/c3-cmc.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a> <a href="images/g4-cmc.jpg" rel="lightbox"><img src="images/c4-cmc.jpg" alt="" width="57" height="40" border="0" /></a><a href="images/1.jpg"></a></td>
        </tr>
      </table>
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>Download information & screenshots</h1>
              <p align="right"><a href="VOIP-Cyneric_en.pps" target="_blank"></a><a href="eng-cmc.pdf" target="_blank"><img src="../imagenes/ico-d-pdf.gif" alt="Check My Cisco, folleto informativo" width="28" height="39" hspace="3" vspace="9" border="0" /></a><img src="../imagenes/ico-d.gif" width="40" height="41" hspace="0" vspace="9" /></p></td>
        </tr>
      </table>
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat; background-position:bottom"><h1>We provide complete support with our products, services and implementations. </h1>
              <p>&nbsp;</p></td>
        </tr>
      </table>
            <iframe src="../includes/soporte.html" name="soporte" width="250" marginwidth="0" height="116" marginheight="0" align="left" scrolling="No" frameborder="0" hspace="0" vspace="0" id="lasnews" allowtransparency="true" application="true" style="margin-left:7px; margin-top:4px"></iframe></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><!--#include file="../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
