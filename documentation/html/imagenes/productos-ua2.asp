<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>

<!--required library-->
	<!--Yahoo! User Interface Library : http://developer.yahoo.com/yui/index.html-->
	<script type="text/javascript" src="../testes/yahoo_2.0.0-b2.js"></script>
	<script type="text/javascript" src="../testes/event_2.0.0-b2.js" ></script>
	<script type="text/javascript" src="../testes/dom_2.0.2-b3.js"></script>
	<script type="text/javascript" src="../testes/animation_2.0.0-b3.js"></script>
	<script type="text/javascript" src="../testes/accordion-menu-v2.js"></script>

	<link rel="stylesheet" type="text/css" href="../testes/accordion-menu-v2.css" />
<!--//required library-->


<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../includes/chromejs/chrome.js">
</script>
</head>

<body>
<table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif"><h1>Products &amp; Services  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
           <!-- <li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
            <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Customer Area </a></li>
            <li><a href="casos-de-exito.asp">Success Histories</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;The best partner for networks and Voip Invoicing
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution</a> <a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a> </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="productos-CRA.asp" onfocus="if(this.blur)this.blur()">Cyner CRA</a>  <a href="productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="productos-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a> 
			<a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			<a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table><table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome">
  <tr>
    <td width="507" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
      <tr>
        <td align="left" valign="top"><h2>Productos: Cyner User Admin </h2>
          <div id="divfrase">
            <script type="text/javascript" src="../includes/marquee.js"></script>
          <a id="fade_link"></a></div>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <h1>Panorama global</h1>
          <h3>La interfaz de gest&iacute;&oacute;n de nuestra plataforma</h3>
          <p>Dise&ntilde;ada para centralizar todas las operaciones comerciales<strong> Cyner User Admin</strong> es la interfaz a trav&eacute;s de la cual se establece la comunicaci&oacute;n y gestion de la plataforma en tiempoo real.</p>
          <p>Soporta operaciones para clientes:</p>
          <p><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Prepago</p>
          <p><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Postpago </p>
          <p><img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Postpago con l&iacute;mte de cr&eacute;dito</p>
          <p>Contempla m&uacute;ltiples tipos y clases sde usuarios, dot&aacute;ndoles de diferentes niveles de acceso y restricciones, cumpliendo asi con standares de seguridad requeridos para una adecuada gesti&oacute;n de las operacioones.</p>
          <p>&nbsp; </p>
          <h3>CRM, Gesti&oacute;n de contactos y clientes</h3>
          <p> Creaci&oacute;n y definici&oacute;n de clientes bajo diferentes modalidades</p>
          <p> Reportes comerciales y t&eacute;cnicos </p>
          <p> Creaci&oacute;n de listas de precios, asignaci&oacute;n y creaci&oacute;n de promociones y m&aacute;s. </p>
          <p>&nbsp;</p>
          <h3>Multiples niveles de Usuarios </h3>
          <p> <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Administradores<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Resellers<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Agentes<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Clientes<br />
              <img src="../imagenes/ok2b.gif" width="19" height="18" hspace="3" align="absmiddle" />Carriers</p>
          <p>&nbsp;</p>
          <h3>Provisioning</h3>
          <p>Tipos de acceso.</p>
          <p> ANI, (Identificador de N&uacute;mero de Acceso)</p>
          <p> N&uacute;mero de destino</p>
          <p> PIN de Acceso</p>
          <p> N&uacute;mero DID-Ring</p>
          <p> N&uacute;mero de Prefjo</p>
          <p> Interface</p>
          <p> IP de Origen /  IP de destino</p>
          <p> Peer de Origen  / Peer de destino</p>
          <p>&nbsp;</p>
          <h3>Facturaci&oacute;n, cobranza de servicio </h3>
          <p>Asignaci&oacute;n de listas de precio y servicios. </p>
          <p>Aplicac&oacute;n de descuentos, cr&eacute;ditos y m&aacute;s</p>
          <p>Inserci&oacute;n manual de pagos.</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <h3>M&oacute;dulo de lista de precios</h3>
          <p>Creaci&oacute;n , modificaci&oacute;n y administraci&oacute;n de listas de precioss</p>
          <p>Importaci&oacute;n y exportacion en formato Microsoft Excel s </p>
          <p>Precio determinable por criterios de: Duraci&oacute;n de llamada o por tarifa plana</p>
          <p>Amplia posibilidade de definici&oacute;n de  undiades de tiempo para corte de llamada. </p>
          <p>Gestion, creaci&oacute;n de servicios extras, fees, impuestos</p>
          <p>&nbsp;</p>
          <h3>M&oacute;dulo de promoci&oacute;n </h3>
          <p>Creaci&oacute;n y  agenda de futuras promociones.</p>
          <p>Promociones por rangos horarios o fechas especiales.</p>
          <p>Aplicaci&oacute;n de promosciones a m&uacute;ltiples listas de precios </p>
          <p>&nbsp;</p>
          <h3>Reportes Comerciales</h3>
          <p>Ganancia por proveedor</p>
          <p> Totales por usuario/cliente</p>
          <p> Listas de precios</p>
          <p> N&oacute;mina de clientes</p>
          <p> Facturas y pagos</p>
          <p> Ganancias</p>
          <p> Ganancias por d&iacute;a</p>
          <p> Deudores</p>
          <p> Ventas</p>
          <p> Total facturdo</p>
          <p>Total adeudado</p>
          <p> Totales por cobrar</p>
          <p> Resumen de facturaci&oacute;n</p>
          <p>&nbsp;</p>
          <h3>Reportes T&eacute;cnicos </h3>
          <p>Accesos por usuario/cliente</p>
          <p> Llamadas no procesadas</p>
          <p> Consumo por proveedor</p>
          <p> Utilizaci&oacute;n por destino</p>
          <p> Utilizaci&oacute;n por Carrier de destino</p>
          <p>Exportaci&oacute;n de registros de CDR </p>
          <p>CDR por destino</p>
          <p>CDR por interface</p>
          <p>CDR por origen</p>
          <p>CDR por pas </p>
          <p>Util por hora </p>
          <p>Seguimiento de reclamos </p>
          <p>Estado de hardware </p>
          <p> Llamadas activas</p>
          <hr />
          <dl class="accordion-menu">
          <dl>
            <dt class="a-m-t">title 1 </dt>
            <dd class="a-m-d">
              <div class="bd"> Lorem ipsum dolor sit amet, consectetuer adipnia. Vestibulum pellentesque porta enim. Curabitur elementum vulputate lacus. Donec quis ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam dignissim sagittis purus. Nulla sollicitudin mauris sit amet purus. </div>
            </dd>
            <dt class="a-m-t">title 2 </dt>
            <dd class="a-m-d">
              <div class="bd"> Lorem ipsum dolor sit amet, consectetuer adipnia. Vestibulum pellentesque porta enim. Curabitur elementum vulputate lacus. Donec quis ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam dignissim sagittis purus. Nulla sollicitudin mauris sit amet purus. </div>
            </dd>
            <dt class="a-m-t">title 3 </dt>
            <dd class="a-m-d">
              <div class="bd"> Lorem ipsum dolor sit amet, consectetuer adipnia. Vestibulum pellentesque porta enim. Curabitur elementum vulputate lacus. Donec quis ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam dignissim sagittis purus. Nulla sollicitudin mauris sit amet purus. </div>
            </dd>
          </dl>
          <hr />
          <p>asda das</p>
          <p>&nbsp; </p>
          <p>AD </p>
          <p>SAD S</p>
          <p> SD</p>
          <p>DSADSAD </p></td>
      </tr>
    </table>
    <p>&nbsp;</p></td>
    <td width="270" align="center" valign="top"><p>&nbsp;</p>
      <p>&nbsp;</p>
      <table width="240" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-business.gif); background-repeat:no-repeat">
        <tr>
          <td width="227" height="182" align="left" valign="top"><h1>Plataforma VoIP Facturaci&oacute;n </h1>
            <p>&iquest;Usted, en cual modelo de negocio est&aacute;? </p>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">Mayorista VoIP, Terminador, Originador</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Telefon&iacute;a VoIP Residencial</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Administraci&oacute;n de Tarjetas telef&oacute;nicas</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Locutorios</a></li>
            </ul></td>
        </tr>
      </table>
      <table width="240" height="182" border="0" cellpadding="0" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-sreenshots.gif); background-repeat:no-repeat">
        <tr>
          <td width="240" height="182" align="left" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','240','height','182','hspace','0','vspace','0','src','../imagenes/s-prod-UA','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','wmode','transparent','movie','../imagenes/s-prod-UA' ); //end AC code
      </script>
              <noscript>
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="240" height="182" hspace="0" vspace="0">
                <param name="movie" value="../imagenes/s-prod-UA.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <embed src="../imagenes/s-prod-UA.swf" width="240" height="182" hspace="0" vspace="0" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
              </object>
            </noscript></td>
        </tr>
      </table>
      <br />
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>Download Information</h1>
              <p align="right"><a href="VOIP-Cyneric_en.pps" target="_blank"><img src="../imagenes/ico-d-ppt.gif" alt="Plataforma VoIP, powerpoint" width="48" height="39" hspace="0" vspace="9" border="0" /></a><a href="esp-cdr.pdf" target="_blank"><img src="../imagenes/ico-d-pdf.gif" alt="Cyner CDR whitepaper" width="28" height="39" hspace="3" vspace="9" border="0" /></a><img src="../imagenes/ico-d.gif" width="40" height="41" hspace="0" vspace="9" /></p></td>
        </tr>
      </table>
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat; background-position:bottom"><h1>We provide complete support with our products, services and implementations. </h1>
              <p>&nbsp;</p></td>
        </tr>
      </table>
            <p>
              <iframe src="../includes/soporte.html" name="soporte" width="250" marginwidth="0" height="116" marginheight="0"  scrolling="No" frameborder="0" hspace="0" vspace="0" id="lasnews" allowtransparency="true" application="true" style="margin-left:7px; margin-top:4px">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              </iframe>
            </p>
            <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><!--#include file="../includes/footer.asp" --></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
