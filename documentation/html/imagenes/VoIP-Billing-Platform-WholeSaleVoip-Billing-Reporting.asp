<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><%= Session("nombre")%></title>
<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../includes/chromejs/chrome.js">
</script>
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
</head>
<body onselectstart="return false">
<table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif"><h1>VoIP Billing Platform&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
            <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Customer Area</a></li>
            <li><a href="casos-de-exito.asp">Success Histories</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;The best partner for networks and Voip Invoicing
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a> </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="productos-CRA.asp" onfocus="if(this.blur)this.blur()">Cyner CRA</a>  <a href="productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="productos-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a> 
			<a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			<a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome">
  <tr>
    <td width="507" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="11">
        <tr>
          <td align="left" valign="top"><h2>VoIP Wholesale, Terminator, Originator</h2>
            <div id="divfrase">
              <script type="text/javascript" src="../includes/marquee.js"></script>
              <a id="fade_link"></a></div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h1>We provide a global solution </h1>
            <p>Part of our family of   <a href="VoIP-Billing-Platform.asp">solutions for commercial VoIP Operations</a>.We offer a world-class VoIP billing solution tailored to each business model. </p>
            <h3>&nbsp;</h3>
            <h1>Our software's features and performance</h1>
            <p>&nbsp;</p>
            <div id="Accordion1" class="Accordion" tabindex="0">
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /> Traffic volume</div>
                <div class="AccordionPanelContent">
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Our platform supports a large <b>volume of daily traffic.
                    </b><br />
                    We do integrations for large customers with an average monthly traffic of 80 million minutes.               </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"> <img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Balance and redundancy</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>In order to support high volumes, we provide hardware load balancing and  data base redundancy.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Configuration  per customer</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Our system allows define different  billing periods  for each customer, their price list, type of rounding-off, special services, IP access, prefixes, ANI, dialed number, etc.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Price list management</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Prices change all the time. This is why we provide price list import/export from Excel, programmed price changes, country break-up , origination fees, and 800 access fees.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Vendor control</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/> With our platform you can define vendors and the system will identify each call, for each vendor, so you can compare the vendor invoices with those from our system. 

</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Quality control</div>
                <div class="AccordionPanelContent"> 
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>If<b> termination quality</b> decreases, this may lead to loss of profits or business. That is why our platform provides<b> real-time quality control. </b><br />
                    <br /> This allows you to know, without a doubt, the termination quality being offered as well as receive alerts for unwanted traffic conditions. The quality control is flexible and can be done according to customer, vendor, and destinations. 
                  </p>
                  <p>&nbsp;</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Web access for customers</div>
                <div class="AccordionPanelContent"><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Customer communication is fundamental. 
                  <br />
                  We have a suitable website where your customers can see their real-time traffic.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Invoices, Payments, Balances</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Our platform keeps records  of invoices, payments, overdue invoices and customer balances. Alerts can be configured to be sent to administrative staff to block accounts or make automatic payments.</div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Pre-paid customer control</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>In some cases it is necessary to control the customer balances, for call terminations. Our system supports real-time authentication. The customer that goes over his credit limit is automatically disconnected. </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" /> Limit Post- paid customers</div>
                <div class="AccordionPanelContent"> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>In some situations it is necessary to give some credit to customers. 
                  <br />
                  Our platform sets a maximum amount, and once is used , the system will disconnect the customer. This tool allows decreasing the risk of fraud. </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />A large amount of commercial and technical reports.</div>
                <div class="AccordionPanelContent"> 
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>We provide reports regarding profits, customer totals, usage according to vendor, traffic distribution according to customer and destination.
                    <br />
                    <br />
                  Our  platform provides different reports with ASR, ACD, PDD values and traffic  curves per day, as well as others.</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Complaints</div>
                <div class="AccordionPanelContent"> 
                  <p><img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>Our software provides a customer complaint management system, which can be handled by your IT engineers and help desk operators.<br />
                    <br /> 
The customer can file his complaint on the website and your technical team will keep customer service organized.</p>
                </div>
              </div>
              <div class="AccordionPanel">
                <div class="AccordionPanelTab"><img src="../imagenes/more-1.gif" width="13" height="13" hspace="2" vspace="2" align="absmiddle" />Export  reports </div>
                <div class="AccordionPanelContent">
                  <p> <img src="../imagenes/ico-texto.gif" width="14" height="18" hspace="5" vspace="2" align="left"  style="margin-bottom:80px"/>All of our reports can be exported to pdf, excel, word and html. This way reports and documents are easily sent to customers. </p>
                </div>
              </div>
            </div>
            <p>
              <script type="text/javascript">
			var Accordion1 = new Spry.Widget.Accordion("Accordion1");
             </script>
            </p>
            <p>&nbsp;</p>
            <h1>We have even more to show you </h1>
            <p>With our platform is easy  define retail business models to keep existing customers, generate new ones and attract new business by marketing an excellent service, truly decreasing operational costs, and keeping daily issues under control, as well as the most delicate details of your  business. <a href="../a1/contact-cyneric.asp?motivo=Nos interesa evaluar su plataforma" target="_self">Please request a free demostration.</a></p>
            <p>&nbsp;</p>
            <hr />
            <h1><img src="../imagenes/ico-mas.gif" width="22" height="23" hspace="3" align="absmiddle" />More information, products and services </h1>
            <strong>What is the business model of your corporation?</strong>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator, Originator</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP Telephony</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Cabin Booth Centers</a></li>
              </ul>            </td>
        </tr>
      </table></td>
    <td width="270" align="center" valign="top"><p>&nbsp;</p>
      <table width="240" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-business.gif); background-repeat:no-repeat">
        <tr>
          <td width="227" height="182" align="left" valign="top"><h1>VoIP Platform Billing </h1>
            <p>What is the business model of your corporation?  </p>
            <ul>
              <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator, Originator</a></li>
              <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP Telephony</a></li>
              <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a></li>
              <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Cabin Booth Centers</a></li>
              </ul></td>
        </tr>
      </table>
      <table width="240" height="182" border="0" cellpadding="0" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda-sreenshots.gif); background-repeat:no-repeat">
        <tr>
          <td width="240" height="182" align="left" valign="top"><script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','240','height','182','hspace','0','vspace','0','src','../imagenes/s-wholesale','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','wmode','transparent','movie','../imagenes/s-wholesale' ); //end AC code
      </script>
              <noscript>
              <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="240" height="182" hspace="0" vspace="0">
                <param name="movie" value="../imagenes/s-wholesale.swf" />
                <param name="quality" value="high" />
                <param name="wmode" value="transparent" />
                <embed src="../imagenes/s-wholesale.swf" width="240" height="182" hspace="0" vspace="0" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
              </object>
            </noscript></td>
        </tr>
      </table>
      <!--#include file="../includes/es-ban-demo.asp" -->
<table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
        <tr>
          <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat; background-position:bottom"><h1>We provide complete support with our products, services and implementations. </h1>
              <p>&nbsp;</p></td>
        </tr>
      </table>
            <p>
              <iframe src="../includes/soporte.html" name="soporte" width="250" marginwidth="0" height="116" marginheight="0" align="center" scrolling="No" frameborder="0" hspace="0" vspace="0" id="lasnews" allowtransparency="true" application="true" style="margin-left:7px; margin-top:4px"></iframe>
            </p>
            <table width="240" height="75" border="0" cellpadding="8" cellspacing="4">
              <tr>
                <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>Download Information</h1>
                    <p align="right"><a href="VOIP-Cyneric_en.pps" target="_blank"><img src="../imagenes/ico-d-ppt.gif" alt="Plataforma VoIP, powerpoint" width="48" height="39" hspace="0" vspace="9" border="0" /></a><a href="esp-voip-billing.pdf" target="_blank"><img src="../imagenes/ico-d-pdf.gif" alt="Cyner CDR whitepaper" width="28" height="39" hspace="3" vspace="9" border="0" /></a><img src="../imagenes/ico-d.gif" width="40" height="41" hspace="0" vspace="9" /></p></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><!--#include file="../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
