<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
Security, Wiretapping, and the Internet<br />
            </h1> 
<div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">

In a move that is dangerous to <a href="../../a1/VoIP-Network-platform.asp">network</a> security, the US
Federal Bureau of Investigation is seeking to extend the
Communications for Law Enforcement Act to voice over IP.
Such an extension poses national security risks.<br />
<br />
Wiretaps have been used since the invention of
the telegraph and have been a legal element
of the US law enforcement arsenal for more
than a quarter century. In keeping with law
enforcement�s efforts to keep laws current with changing
technologies, in 1994 the US Congress passed the Communications
Assistance for Law Enforcement Act
(CALEA). The law proved to be controversial because it
mandated that digitally switched telephone <a href="../../a1/VoIP-Network-platform.asp">network</a>s
must be built wiretap enabled, with the US Department
of Justice in charge of determining the appropriate technology
standards.<br />
<br />
The law provided a specific exclusion for �information
services.� Despite that explicit exemption, in response
to a request from the US Federal Bureau of
Investigation (FBI), in August 2005, the Federal Communications
Commission (FCC) ruled that broadband
voice over IP (VoIP) must comply with CALEA. Civilliberties
groups and the industry immediately objected,
fearing the ruling�s impact on privacy and innovation.
There is another community that should be very concerned.
Applying CALEA to VoIP requires embedding
surveillance technology deeply into the protocol stack.
The FCC ruling undermines <a href="../../a1/VoIP-Network-platform.asp">network</a> security and, because
the Internet and private <a href="../../a1/VoIP-Network-platform.asp">networks</a> using Internet
protocols support critical as well as noncritical
infrastructure, national security as well. The FCC ruling
is a step backward in securing the Internet, a national�
and international�priority.<br />

<b><br /><a href="#">CALEA�s history</a></b> <span class="credits"></span><br>
In 1992, the FBI was struggling. What had been a boon
for <a href="../../a1/VoIP-Network-platform.asp">telephony</a>�the split-up of AT&T (Ma Bell), which
previously had a
monopoly of the US
market�was a serious problem for the bureau. Instead
of implementing wiretaps by working with a single service
provider and phone company, the FBI found itself
facing a plethora of suppliers of services and telephones.
Even worse from the FBI�s perspective were the new
telecommunications technologies: cell phones, call forwarding,
call waiting, and speed dialing. That same year,
the FBI put forth the Digital <a href="../../a1/VoIP-Network-platform.asp">Telephony</a> proposal, which
would have required wiretapping standards to be included
as part of the design of digital-switching telephone
equipment.<br />
<br />
The FBI claimed that the advanced calling features
impeded court-authorized wiretaps. However, The
Washington Post investigated and discovered that, �FBI officials
said that they have not yet fumbled a criminal probe
due to the inability to tap a phone.�1 At this news, Computer
Professionals for Social Responsibility, a publicinterest
group, initiated Freedom of Information Act
litigation; in response, the FBI released a four-page list of
impeded cases in which, citing �national security,� all information
was blacked out.2<br />
<br />
<b>Digital <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a> proposal</b><br />

The FBI�s Digital Telephony proposal represented a sharp
change in the government�s approach to wiretapping. Instead
of letting service providers determine how to configure
their systems to accommodate wiretaps, the
proposal put government in the middle of telephoneequipment
design. In fact, this bill placed the US Attorney
General, a position not generally known for
technical expertise, into the process of standards design of equipment used by the general public. Industry and civilliberties
groups opposed the FBI proposal, and no one in
Congress would sponsor it.<br />
<br />
In 1994, the FBI reintroduced the bill, and this time,
events played out differently. Over the course of the
Congressional session, the bill�s scope changed, narrowing
down to common carriers (rather than all electronic
communication service providers), adding some protections
for transactional information�the first time
such information was afforded protection in wiretapping
law�and eliminating a clause requiring telephone
companies to decrypt encrypted conversations, regardless
of whether they had an encryption key. There was
also a sweetener for the telecommunications companies:
a US$500 million authorization to help carriers
update their <a href="../../a1/VoIP-Network-platform.asp"> networks</a> to comply with the law�s requirements.
Although other civil-liberties groups had continued
to oppose the bill, the Electronic Frontier
Foundation�s support of the final version�now renamed
CALEA�helped persuade the telephone companies
to support it. This time, the bill passed. Though
the law governed just the US, its impact was far broader.
The FBI pressed other nations to adopt similar laws. In
any case, because the law applied to the US telecom
market, much of the rest of the world was forced to
adopt the standards that CALEA dictated.</p>
<p class="virtualpage3">
<b>Implementing CALEA</b><br />
The law ran into trouble almost immediately. The telephone
companies believed that negotiations on the bill
had left them in a position in which standards would be
determined through consultation with the FBI. After
passage however, the FBI took the stance that the law allowed
it to set requirements without consultation.
The FCC, which has jurisdiction over telecommunications
regulations, and the courts have generally upheld
the FBI�s interpretation�even in cases in which
CALEA excluded the FBI�s proposed standards.3 Although
CALEA required telephone companies to meet
the government�s standards by 1998, disagreements between
telecommunications companies and the FBI on
appropriate standards meant the deadline could not be
met. Congress was unhappy with the delay, and several
Congressmen, including Senator Patrick Leahy (D-Vermont)
and Representative Bob Barr (R-Georgia),
pointed the finger at the FBI.
These disputes�and delays�concerned CALEA�s
application to <a href="../../a1/VoIP-Network-platform.asp">telephony</a>. CALEA�s language specifically
exempted information services: �The term
�telecommunications carriers��(A) means ... a common
carrier for hire; and (B) includes (i) ... commercial
mobile service; or (ii) ... service that is a replacement
for a substantial portion of local telephone exchange
service ...; but (C) does not include ... information services.�
3 Nonetheless, in late 2003, the FBI served notice
to the FCC that VoIP providers would be subject
to CALEA.
As was the case in 1992 with the Digital Telephony
proposal, the FBI did not provide any examples of problems
found in conducting law enforcement wiretaps on
VoIP calls. In March 2004, the FBI, the US Department
of Justice, and the US Drug Enforcement Agency submitted
a joint petition to the FCC requesting that it rule
on CALEA�s applicability to VoIP. Over the next year,
computer companies joined the anti CALEA coalition
of civil-liberties groups and telecommunications
providers that formed in the 1990s. None objected to the
idea of wiretapping voice calls�indeed, many companies
were involved in determining appropriate ways to
enable the tapping of VoIP calls but all objected to the
idea that the government should be involved in setting
standards for interception on the packet-switched Internet.
In August 2005, the FCC announced that broadband
providers of VoIP must comply with CALEA.4
CALEA, which mandates government role in standards
design, is an oddity in US wiretap law, which we will
briefly examine.<br />
<br />

<b><br />US wiretap laws</b> <span class="credits"></span><br />

Wiretaps have had a long and complex history in US jurisprudence.
Their first use was in the mid 19th century,
in response to the invention of the telegraph. Shortly afterward,
they appeared in war: Confederate General Jeb
Stuart traveled with his own wiretapper to tap Union
army lines.5Wiretaps came into their own during Prohibition,
the period between 1920 and 1933 in which the
manufacture and sale of alcohol was illegal. Federal lawenforcement
agents discovered the value of wiretaps in
both investigating and prosecuting bootlegging cases.
The Olmstead case set the stage for the next 40 years of
US wiretap law.6<br />
<br />
<b>A form of search</b><br />
In the 1920s, Roy Olmstead had a major bootlegging
operation in Seattle. Federal agents wiretapped Olmstead
and his co-conspirators, placing taps in the basement of
his office building and on telephone poles outside private
houses. Olmstead�s lawyers argued their case on the basis
of the Fourth Amendment to the US Constitution:<br />
<br />
The right of the people to be secure in their persons,
house, papers and effects against unreasonable
searches and seizures shall not be violated, and no
Warrants shall issue but upon probable cause, supported
by Oath or affirmation, and particularly describing
the place to be searched, and the persons
or things to be seized.6<br />
<br />
The US Supreme Court held that wiretaps were not a
form of search, and thus didn�t require search warrants.<br />
<br />
But the most well-known opinion in the Olmstead case
isn�t that of the majority, but of Justice Louis Brandeis�s
dissent. He said that wiretaps were a special type of search:<br />
<br />
<b>The Court�s rulings of the 1930s did
not end law enforcement wiretapping;
instead, tapping went underground
figuratively as well as literally.</b><br />
<br />
A decade later, citing the 1934 US Federal Communications
Act, which prohibited the �interception and
divulgence� of wired communications, the US Supreme
Court overturned the Olmstead decision in the Nardone
cases.7 In a series of cases over the next 30 years, the
Supreme Court also slowly narrowed the circumstances
under which law enforcement could perform electronic
bugging without a warrant, until 1967, in Charles Katz v.
United States when the Court concluded that an electronic
bug in even so public a place as a phone booth was
indeed a search and therefore should be protected under
the Fourth Amendment.8<br />
<br />


</p>

<p class="virtualpage3">
The Court�s rulings of the 1930s did not end law enforcement
wiretapping; instead, tapping went underground
figuratively as well as literally. After the Nardone
rulings, law enforcement didn�t publicly divulge wiretapped
information (or, it did, but not the fact that the information
came from wiretaps). This legal never-never
land led to abuses by FBI director J. Edgar Hoover�s
agents, including the wiretapping (and bugging) of political
dissidents, Congressional staffers, and US Supreme
Court Justices.9�11 The FBI�s extensive records on political
figures was well known, and this information, some of
which was salacious, ensured that Congress conducted
little oversight of the FBI. When, in reaction to the Katz
decision, Congress decided to pass a wiretapping law, the national legislature was quite concerned about preventing
Hoover-era abuses.<br /><br />

<b>Changing views</b><br />

The complications of investigating organized crime�
including victims� reluctance to testify, so-called victimless
crimes (such as prostitution), and the corruption of
local law enforcement�make electronic surveillance a
particularly valuable tool. In 1967, a presidential commission
investigating organized crime concluded,
�legislation should be enacted granting carefully circumscribed
authority for electronic surveillance to law enforcement
officers ....�12 In response, US President
Lyndon Johnson signed the Omnibus Crime Control
and Safe Streets Act of 1968, of which Title III legalized
law enforcement wiretaps in criminal investigations. Because
of wiretaps� invasive nature, the act listed only 26
crimes that could warrant wiretap investigations, including
murder, kidnapping, extortion, gambling, counterfeiting,
and the sale of marijuana. The US Judiciary
Committee�s report explained that �each offense was
chosen because it was intrinsically serious or because it is
characteristic of the operations of organized crime.�13
Congress decided that stringent oversight of wiretapping
should require a federal district court judge to review
each federal wiretap warrant application. Although
President Johnson had used wiretaps on civil-rights
leader Martin Luther King Jr. during the 1964 Democratic
Party convention and on US Vice President Hubert
Humphrey in 1968, publicly, the president was ambivalent
about wiretaps. Even as he described the Title III
provisions for wiretapping as undesirable,14 he signed the
wiretapping provisions into law.<br />
<br />
<b>Title III</b><br />
For criminal investigations (the only kind Title III addresses),
wiretap warrants are more difficult to obtain
than normal search warrants. The judge must determine
that there�s probable cause to believe<br />
<br />
� an individual is committing, has committed, or is about
to commit an indictable offense;
� communications about the offense will be obtained
through the interception;
� normal investigative procedures have been tried and either
have failed, appear unlikely to succeed, or are too
dangerous; and
� the facilities subject to surveillance are being used or
will be used in the commission of the crime.15<br />
<br />
Title III covers procedures for obtaining wiretaps for
law enforcement investigations. In 1972, in a court case
involving �domestic national security issues,� the US
Supreme Court ordered an end to warrantless wiretapping,
even for national security purposes.16 �Domestic national security� cases had fueled a large number of inappropriate
investigations of Americans, including<br />
<br />
� the US Central Intelligence Agency opening and photographing
nearly a quarter of a million first-class letters
without search warrants between 1953 and 1973;
� 300,000 individuals indexed on CIA computers and
CIA files on 7,200 individuals;
� the US National Security Agency obtaining copies of
millions of private telegrams sent to and from the US
from 1947 to 1975 without search warrants through an
arrangement with three US telegraph companies; and
� US Army Intelligence keeping files on 100,000
Americans.10<br />
<br />
Because of the public outcry over the discovery of
numerous Nixon administration �national security�
wiretaps that had been conducted for political purposes,
10 it took until 1978 for Congress to craft the Foreign
Intelligence Surveillance Act (FISA), which
authorizes procedures for national security wiretapping.
Congress considered it extremely important that
safeguards be in place to prevent such illegal surveillance
in the future.<br />
<br />



<b><br />Foreign Intelligence </b> <span class="credits"></span><br>
In contrast to Title III�s requirements that a judge determine
whether there�s probable cause to believe that an individual
is involved in committing an indictable offense,
in FISA cases, the judge, a member of an FISA court,
must determine whether there�s probable cause that the
target is a foreign power or agent of a foreign power. The
purpose of the surveillance is to obtain intelligence information.
The law provides that �[N]o United States person
may be considered a foreign power or an agent of a
foreign power solely upon the basis of activities protected
by the first amendment to the Constitution of the United
States.�17<br />
<br />
The requirements for foreign intelligence wiretaps are
less stringent than those for law enforcement. United
States v. United States District Court held that domestic national
security wiretapping must be conducted under a
search warrant, and the US Supreme Court stated that,
�Different standards [for gathering intelligence] may be
compatible with the Fourth Amendment if they are reasonable
both in relation to the legitimate need of Government
for intelligence information and the protected
rights of our citizens.�16 National security investigations
are more typically concerned with preventing future
crimes rather than prosecuting previous ones, and the
ability to present a case in court is often orthogonal to the
real concerns of national security cases.<br />
<br />
Title III and FISA form the basis for US wiretap law.
State statutes also exist, with approximately half of all
wiretaps for criminal investigations in the US performed
using state wiretap warrants. The rules governing
state wiretaps must be at least as restrictive as those
governing Title III. There have been several updates
and modifications to the federal wiretap statutes, including
the Electronic Communications Privacy Act,
CALEA, and the PATRIOT Act. In this article, my
focus is solely on CALEA.<br />
<br />
<b>Communications
in an age of terror</b><br />

US wiretap laws were passed during an era in which the
national threats were organized crime and civil unrest. In
evaluating the efficacy of applying a CALEA-like law to
the Internet, it�s important to put today�s threats into context
and understand the new communications context.
</p>

<p class="virtualpage3">
<b>Law enforcement and terrorism</b> <span class="credits"></span><br>
The war on terror is the US�s most pressing security concern,
but it isn�t actually a war on terror: it�s a war against violent
religious fundamentalists who use terror as a weapon.
As a society, we have become accustomed to using �war�
to refer to situations to which the word doesn�t properly
apply: �the war on drugs,� �the war on poverty,� even �the
war on spam.� In the case of violent fundamentalists�for
Osama bin Laden and al Qaeda, this means violent Muslim
fundamentalists�this is indeed a war.<br />
<br />
Law enforcement concerns have governed domestic
approaches to this war. In part, this is because we
know how to catch so-called bad guys (or as Michael
Scheuer says in Imperial Hubris, �[because if] bin Laden
is a criminal whose activities are fueled by money�not
a devout Muslim soldier fueled by faith�[...] Americans
know how to beat well-heeled gangsters.�18). The
FBI�s successful investigations of the Lockerbie plane
crash over Scotland in 1988 and the first World Trade
Center bombing in 1993 led the public and policymakers
into believing the tools of law enforcement
were appropriate for combating terrorism.19 But the
war against violent religious fundamentalists won�t be
won by law enforcement, which provides the wrong
tools and the wrong incentives. For example, one of
the most important functions of law enforcement is its
deterrent value, but law enforcement isn�t a deterrent
to terrorists. Rather, violent fundamentalists often
view a jail sentence as a form of martyrdom and an increased
opportunity for recruiting.20<br />
<br />
Even more basic to the distinction between law enforcement
and national security investigations is that law
enforcement and national security investigations have
substantively different purposes. Law enforcement sees
the solving of a crime as a success. Yet, when terrorism is
concerned, prevention is the only real measure of success.
Law enforcement seeks a level of proof that will convict a
criminal in a court of law, which is inappropriate in a war against terrorists. The type of intelligence work needed
for national security investigations seeks different outcomes
than those of criminal investigations, which are
measured by arrests made and convictions obtained.<br />
<br />
<b>Applying CALEA to VoIP is a mistake:
the insecurities that will result are
likely to extend well past VoIP to
other aspects of the Internet.</b>
<br />

The events of September 11th, 2001 make it dreadfully
clear that the law enforcement focus is inadequate
for this war. Scheuer states it bluntly: �Bin Laden is
leading and inspiring a worldwide anti-US insurgency;
he is waging war while we fight him with counterterrorism
policies dominated by law-enforcement tactics
and procedures. It has not and will not work.�18 National
security�s emphasis on prevention is the critical
aspect of this war.<br />
<br />
<b>Computer security and terrorism</b>
What does all of this have to do with computer security?
The Internet has proved a boon to many industries, and
the last decade has seen a massive shift to it as the preferred
form of conducting business. But the Internet is insecure.
The <a href="../../a1/VoIP-Network-platform.asp">network</a> was originally designed to share resources,
and neither security nor wiretapping were considerations
in its initial design. Security is a serious concern for Internet
users, which include many private industries that
form part of critical infrastructure: energy companies
and the electric-power grid, banking and finance, and
health care. That�s why applying CALEA to VoIP is a
mistake: the insecurities that will result are likely to extend
well past VoIP to other aspects of the Internet, and
the end result will be greater insecurity.<br />
<br />
<b>Enter new technology: The Internet</b>
Although it might look like a duck, walk like a duck, and
quack like a duck, Internet <a href="../../a1/VoIP-Network-platform.asp">telephony</a> is not, in fact, a
duck. It�s a very different species from the circuitswitched
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> of Ma Bell
<br />
<b>Telephony vs. the Internet</b><br />
<br />
At its bottom level�metal wires, optical fibers, microwave
relays, and satellite channels�the publicswitched
telephone <a href="../../a1/VoIP-Network-platform.asp">network</a> (PSTN) and the Internet
use the same resources, but the way they manage those
resources is quite different. In circuit-switched <a href="../../a1/VoIP-Network-platform.asp">telephony</a>,
a dedicated, end-to-end circuit is established for the call.
Calls have time multiplexing, which means several calls
may share a line. Either each call has a dedicated timeslot
in every frame�called synchronized multiplexing�or
there�s an asynchronous version of the slots, in which
each timeslot includes the call�s identifier.<br />
<br />
In the pre-computer telephone <a href="../../a1/VoIP-Network-platform.asp">network</a>, the route a
call took was represented by the states of mechanical
switches in the telephone offices through which the call
passed. In a computerized <a href="../../a1/VoIP-Network-platform.asp">network</a>, the call is represented
by table entries that show which incoming line is connected
to which outgoing line. This is a commitment of
resources that represents a significant cost for the <a href="../../a1/VoIP-Network-platform.asp">network</a>,
a cost related to the traditional cost of a threeminute
phone call. In digital terms, the resource
commitment is optimized for communications that send
thousands or millions of bits at a time.<br />
<br />
The route that packets on the Internet take is determined
not by entries in the network�s tables, but by addresses
carried in the packets. Packet progress through the
<a href="../../a1/VoIP-Network-platform.asp">network</a> is affected by routing tables; these tables reflect
the network�s characteristics and not that of the individual
communications. In theory�though less so in practice�
each packet of a VoIP call can use a distinct path to
reach its destination. This is the first problem that Internet
wiretapping poses. On the Internet, routing control is
distributed. It�s impossible to determine a priori the routing
of the packets the communication is broken into�
this is determined by the routing tables, which change
depending on the <a href="../../a1/VoIP-Network-platform.asp">network</a> <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. Thus, unless the communication
is tapped at the endpoints (at the user, or at
the Internet service provider if the user always accesses
the same provider), it�s impossible to guarantee 100 percent
access to all communication packets. From a privacy
viewpoint and to address law enforcement�s minimization
requirement (that wiretapping be of a designated target�
and not someone else using the line�and that the
tapped call be related to the investigation), a further difficulty
is posed by the fact that many other pieces of <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>
travel along portions of the same path as the communication
to be tapped. Thus, tapping anywhere but at the
endpoints exposes other communications; this was one
of the problems of the FBI�s Carnivore (now renamed
DCS-1000) system for tapping email. (Carnivore was an
FBI Internet <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> system designed to be installed
at an ISP. The system �filtered� Internet communications
and delivered target communications to a remote
site, namely the law-enforcement agency. According to
the FBI, only those communications that were subject to
the wiretap order were forwarded to the FBI [www.f bi.
gov/congress/congress00/kerr090600.htm]. The FBI has
since shifted to using commercial software to conduct
such investigations.)<br />
<br />

</p>



<p class="virtualpage3">
<b>Intelligence at the endpoints</b><br />
voice transmission. The endpoints�the phone receivers�
are dumb. In considering the issue of architecting
wiretaps into communications channels, a crucial
difference between the PSTN and the Internet is that intelligence
is at the endpoints for the Internet. The underlying
<a href="../../a1/VoIP-Network-platform.asp">network</a> is deliberately simple, leaving the
endpoints able to deploy complex systems.
The Internet design paradigm was chosen for flexibility:<br />
<br />
The function in question can completely and correctly
be implemented only with the knowledge
and help of the application standing at the endpoints
of the communications system. Therefore,
providing that questioned function as a feature of
the communication system itself is not possible.21<br />
<br />
The principle of letting the endpoints implement the
function rather than having low-level function implementation
be part of the underlying communication system
is known as the �end-to-end� argument in system
design. It is fundamental to Internet design. 22
Intelligence at the endpoints enables versatility. Applications
can be developed far beyond what the original
designers of the communication system envisioned. Indeed,
innovation on the Internet has flourished precisely
because of applications. The flexibility afforded by the
Internet design paradigm means there are few barriers to
entry for a novel service, and the Internet boom of the
late 1990s was greatly enabled by the low barrier to entry
for new applications.
The layered approach to <a href="../../a1/VoIP-Network-platform.asp">network</a> design�application,
transport, <a href="../../a1/VoIP-Network-platform.asp">network</a>, link, and physical�doesn�t
itself preclude wiretapping. It simply requires that wiretapping,
an application, be handled in the application
layer, or at the target�s <a href="../../a1/VoIP-Network-platform.asp">network</a> connection. The whole
issue of applying CALEA to VoIP exists to achieve 100
percent compliance with court-authorized wiretaps, but
such compliance is impossible to guarantee at the application
layer. Service providers do�and will�seek to
comply with court-authorized wiretaps, but an end user
who is determined to thwart a wiretap, perhaps through
a complicated scheme of directing in which streams the
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> will run, will be able to do so. Without pushing
wiretapping capabilities deeper into the protocol stack, it
will be impossible to achieve 100 percent success for
court-authorized VoIP wiretaps. And pushing wiretapping
capabilities further into the <a href="../../a1/VoIP-Network-platform.asp">network</a> stack violates
the end-to-end principle, which, although not sacrosanct,
has proved quite valuable.<br />
<br />
<b>Building wiretapping into protocols</b>

There is nothing inherent in the design of a communications
<a href="../../a1/VoIP-Network-platform.asp">network</a> that rules out security or wiretapping, and
indeed there are defense communications networks that
provide both security and the capability of tapping. Had security or wiretapping been part of the Internet�s core
requirements, the functionality could have been built in
from the beginning. There are secure government communication
systems which, at (government) customer
request, are capable of doing key recovery, thus enabling
data recovery (wiretapping). The difficulty in designing
these capabilities into the Internet occurs in attempting
to securely implement wiretapping ex post facto into the
current <a href="../../a1/VoIP-Network-platform.asp">network</a> architecture. The FBI petition to apply
CALEA to VoIP is nothing less than a request to redesign
Internet architecture.<br />
<br />
In 2000, an Internet Engineering Task Force (IETF)
<a href="../../a1/VoIP-Network-platform.asp">network</a> Working Group studying the issue of designing
wiretap requirements into Internet protocols observed
that any protocol designed with built-in
wiretapping capabilities is inherently less secure than
the protocol would be without the wiretapping capability.
Adding wiretapping requirements into <a href="../../a1/VoIP-Network-platform.asp">network</a>
protocols makes protocols even more complex, and the
added complexity is likely to lead to security flaws.
Wiretapping is a security breach, and no one wants to
see those breaches, deliberately architected or not. The
IETF <a href="../../a1/VoIP-Network-platform.asp">network</a> Working Group decided not to consider
requirements for wiretapping as part of the IETF
standards process.23<br />
<br />
The IETF�s warning is clear: Internet wiretapping can
be used not only by those interested in protecting US interests,
but also by those who oppose them. A technology
designed to simplify Internet wiretapping by US intelligence
presents a large target for foreign intelligence agencies.
Breaking into this one service might give them
broad access to Internet communications without the
expense of building an extensive intercept <a href="../../a1/VoIP-Network-platform.asp">network</a> of
their own.<br />
<br />
In the spirit of modern computer-based industries, it
seems likely that any intercept capability built into Internet
facilities will be capable of the same remote management
that�s typical of the facilities themselves. This was
the case, for example, with Carnivore. System vulnerabilities
are thus as likely to be global as local. Were foreign
intelligence services to penetrate and exploit
Internet wiretapping technology, massive surveillance of
US citizens, residents, and corporations might follow.<br />
<br />
<b>Wiretapping is a security breach, and
no one wants to see those breaches,
deliberately architected or not.</b><br />
<br />
Used in combination with inexpensive automated
search technology, this could lead to an unprecedented
compromise of American security and privacy. Of
course, Internet protocols govern the entire Internet�and not a US version�and thus the impact of CALEA
on VoIP, were it to succeed, would be international in
scope. Across the globe, Internet security and privacy
would be put at risk.<br />
<b>The FBI�s efforts on CALEA run
completely contrary to the US�s
220-year history of developing its
communication systems.</b><br />
<br />
At present, we�re struggling to achieve adequate security
on the Internet without intentional security compromises
in its design. Although it might one day be
possible to incorporate surveillance into packet-switched
networks with sufficient security that the overall safety
of society is increased rather than decreased, it�s hard to
see how this could be less difficult than the unfinished
task of developing a scalable and economical secure
<a href="../../a1/VoIP-Network-platform.asp">network</a>. At the very least, built-in wiretapping would
require secure communications of its own to carry the
intercepted information to the customers for which
it�s collected.<br />
<br />
This problem is made worse by the unreasonable effectiveness
of the Internet as a communications channel.
Building surveillance capabilities into the Internet infrastructure,
and not into the application endpoints, would
expose to eavesdropping not only current applications,
but also future ones, including, for example, the billions
of small devices such as radio-frequency identification
(RFID) tags and sensors that communicate via the Internet.
The concern expressed earlier that security holes
built into the Internet for wiretapping that are used in
combination with inexpensive automated search technology
could lead to serious security breaches applies
here as well.<br />
<br />

<br />


</p>
<p class="virtualpage3">Over the past several years, the US government
sought improvements in civilian communications infrastructure
security even though some of those improvements
were likely to impede law enforcement
investigations. Intelligence agencies clearly supported the
shift, which found that the advantages provided to society
through increased information security outweighed the
disadvantages to intelligence and law enforcement. In
2000, the US Department of Commerce relaxed its
cryptographic export-control regulations, which simplified
the deployment of communications security in
commercial equipment. This action marked a substantial
change in direction on the civilian sector�s use of strong
cryptography. Similarly, in recent years, the US government,
instead of restricting the use of strong cryptography<br />
<br />
has encouraged several cryptographic efforts, including
the development of the 128-bit Advanced Encryption
Standard and the deployment of Elliptic Curve
Cryptosystems.
These changes don�t mean that Internet communications
can�t be wiretapped. The Internet�s insecurity is
well known, and few communications are routinely protected
(for example, encrypted end to end). As the IETF
<a href="../../a1/VoIP-Network-platform.asp">Network</a> Working Group observed, �the use of existing
<a href="../../a1/VoIP-Network-platform.asp">network</a> features, if deployed intelligently, provide extensive
opportunities for wiretapping.�23 But exploiting current
insecurities and actually building them into Internet
protocols have significantly different effects on society�s
communications security. I�m arguing against the latter; I
take no issue with the former.<br />
<br />
From the very early days of the republic, the US has
treated communications as something �of the people,
for the people, and by the people.� The US Postal
Act of 1792 established two fundamental principles: privacy
of the mails�postal officials weren�t allowed to
open mail unless it was undeliverable�and low rates for
newspapers, thereby encouraging the dissemination of
political information. In these two decisions, the US
acted very differently from Britain and France, which
neither guaranteed the privacy of the mails nor encouraged
the use of the mails for political communication.
Indeed, in Europe, the postal service was a system of
government surveillance. By contrast, the US Post Office
was seen as a facilitator of democracy rather than a
controller of the people and, as a result, it was one of the
few strong federal institutions established in the nascent
US.24 A bedrock reason for the growth of telecommunications
in the US has been the privacy afforded to communications.
This spawned trust in the use of
communication systems and a growing dependence on
them.24 The FBI�s efforts on CALEA run completely
contrary to the US�s 220-year history of developing its
communication systems.<br />
<br />
The attempt to apply CALEA to VoIP poses much
risk to the US economy through the potential loss of corporate
information, to US national security through the
provision of cost-effective massive intelligence gathering,
and to privacy and thus the freedom of US citizens.
Society has seen such risks before. In 1999, a report
prepared for the European Parliament revealed that the
US�as part of a surveillance program known as Echelon,
conducted jointly with the United Kingdom, Australia,
Canada, and New Zealand�was targeting
commercial communication channels.25 In response,
European governments decided to liberalize their cryptographic
export-control policy, even though the US
had pressed for tighter controls on cryptographic exports.
(The US, in part so as not to lose trade to Europe, liberalized its cryptographic export-control policies
shortly afterward.26)<br />
<br />
To law enforcement, it might seem obvious that wiretap
laws should automatically be updated with each
change in communications technology. Looking at the
issues more broadly, this is not a clear proposition. Wiretap
laws were passed at a particular time to satisfy a particular
set of problems. As technology and society change,
so must laws. Society�s security needs aren�t enhanced by
requiring that VoIP implementations be developed as
CALEA-enabled, and CALEA requirements applied to
the Internet are likely to cause serious harm to security,
industrial innovation, and the political efforts in the war
against radical terrorists. Applying CALEA to VoIP is
likely to decrease, rather than increase security. Security
requirements should follow the medical profession�s Hippocratic
oath: �First, do no harm.� The proposed
CALEA requirements don�t pass this test, and shouldn�t
be approved.<br /></p>
</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Internet_Security_and_Wiretapping.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/Security_Wiretapping_and_the_Internet.asp"></a> <a href="../doc/Internet_Security_and_Wiretapping.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
