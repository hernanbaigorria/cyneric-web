<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
What is a Call Center?<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
A Call Center is a centralized office used for the purpose of receiving and
transmitting a large volume of requests by telephone.<br />
<br />
A call center is operated by a company to administer incoming product support or
information inquiries from consumers. Outgoing calls for telemarketing, clientele, and
debt collection are also made. In addition to a call center, collective handling of letters,
faxes, and e-mails at one location is known as a contact center.<br />
<br />
A call center is often operated through an extensive open workspace, with work stations
that include a computer, a telephone set/headset connected to a telecom switch, and one
or more supervisor stations. It can be independently operated or networked with
additional centers, often linked to a corporate computer <a href="../a1/VoIP-Network-platform.asp">network</a>, including mainframes,
microcomputers and LANs. Increasingly, the voice and data pathways into the center are
linked through a set of new technologies called computer <a href="../../a1/VoIP-Network-platform.asp">telephony</a> integration (CTI).<br />
<br />
Most major businesses use call centers to interact with their customers. Examples include
utility companies, mail order catalogue firms, and customer support for computer
hardware and software. Some businesses even service internal functions through call
centers.<br />
<br />
<b>Mathematical theory</b><br />
A call center can be viewed, from an operational point of view, as a queuing <a href="../a1/VoIP-Network-platform.asp">network</a>.
The simplest call center, consisting of a single type of customers and statisticallyidentical
servers, can be viewed as a single-queue. Queuing theory is a branch of
mathematics in which models of such queuing systems have been developed. These
models, in turn, are used to support work force planning and management, for example
by helping answer the following common staffing-question: given a service-level, as
determined by management, what is the least number of telephone agents that is required
to achieve it. <i>(Prevalent examples of service levels are: at least 80% of the callers are
answered within 20 seconds; or, no more that 3% of the customer's hang-up, due to their
impatience, before being served.)</i><br />
<br />
Queuing models also provide qualitative insight, for example identifying the
circumstances under which economies of scale prevail, namely that a single large call
center is more effective at answering calls than several (distributed) smaller ones; or that
cross-selling is beneficial; or that a call center should be <a href="../a1/VoIP-Network-platform.asp">quality</a>-driven or efficiencydriven
or, most likely, both <a href="../a1/VoIP-Network-platform.asp">quality</a> and Efficiency Driven (abbreviated to QED).
Recently, queuing models have also been used for planning and operating skills-basedrouting
of calls within a call center, which entails the analysis of systems with multi-type
customers and multi-skilled agents.<br />
<br />
Call center operations have been supported by mathematical models beyond queuing. For
example, for forecasting of calls, for determining shift-structures, and even for analyzing
customers' impatience while waiting to be served by an agent.<br />
<br />

<b>Accommodation</b><br />
The centralization of call management aims to improve a company's operations and
reduce costs, while providing a standardized, streamlined, uniform service for consumers,
making this approach ideal for large companies with extensive customer support needs.
To accommodate for such a large customer base, large warehouses are often converted to
office space to host all call center operations under one roof.<br />
<br />
<b>Personnel management</b><br />
Centralized offices mean that large numbers of workers can be managed and controlled
by a relatively small number of managers and support staff. They are often supported by
computer technology that manages measures and monitors the performance and activities
of the workers. Call center staff are closely monitored for <a href="../a1/VoIP-Network-platform.asp">quality</a> control, level of
proficiency, and customer service.<br />
<br />
Typical contact center operations focus on the discipline areas of workforce management,
queue management, <a href="../a1/VoIP-Network-platform.asp">quality</a> monitoring, and reporting. Reporting in a call center can be
further broken down into <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">real time</a> reporting and historical reporting.<br />
<br />
The types of information collected for a group of call center agents typically include:
agents logged in, agents ready to take calls, agents available to take calls, agents in wrap
up mode, average call duration, average call duration including wrap-up time, longest
duration agent available, longest duration call in queue, number of calls in queue, number
of calls offered, number of calls abandoned, average speed to answer, average speed to
abandoned and service level, calculated by the percentage of calls answered in under a
certain time period.<br />
<br />
Many call centers use workforce management software, which is software that uses
historical information coupled with projected need to generate automated schedules. This
aims to provide adequate staffing skilled enough to assist callers.<br />
<br />
<i>The relatively high cost of personnel and worker inefficiency accounts for the majority of
call center operating expenses, influencing outsourcing in the call center industry.</i><br />
<br />
Inadequate computer systems can mean staff takes one or two seconds longer than
necessary to process a transaction. This can often be quantified in staff cost terms. This is
often used as a driving factor in any business case to justify a complete system upgrade or
replacement. For several factors, including the efficiency of the call center, the level of
computer and telecom support that may be adequate for staff in a typical branch office
may prove totally inadequate in a call center.<br />
<br />


</p>

<p class="virtualpage3">
<b>Technology</b><br />
Call Centers use a wide variety of different technologies to allow them to manage the
large volumes of work that need to be managed by the call center. These technologies
ensure that agents are kept as productive as possible, and that calls are queued and
processed as quickly as possible, resulting in good levels of service.<br />
<br />
These include;<br /><br />

. ACD (automatic call distribution)<br />
. Agent performance analytics<br />
. BTTC (best time to call)/ Outbound call optimization<br />
. IVR (interactive voice response)<br />
. Guided Speech IVR<br />
. CTI (computer <a href="../../a1/VoIP-Network-platform.asp">telephony</a> integration)<br />
. Enterprise Campaign Management<br />
. Outbound predictive dialer<br />
. CRM (customer relationship management)<br />
. CIM (customer interaction management) solutions (Also known as 'Unified' solutions)<br />
. Email Management<br />
. Chat and Web Collaboration<br />
. Desktop Scripting Solutions<br />
. Outsourcing<br />
. Third Party Verification (Third party verification)<br />
. TTS (text to speech)<br />
. WFM (workforce management)<br />
. Virtual queuing<br />
. Voice analysis<br />
. Voice recognition<br />
. Voicemail<br />
. Voice recording<br />
. VoIP<br />
. Speech Analytics<br />
<br />
<b>Call Center dynamics</b><br />
Types of calls are often divided into outbound and inbound. Inbound calls are calls that
are made by the consumer to obtain information, report a malfunction, or ask for help.
These calls are substantially different from outbound calls, where agents place calls to
potential customers mostly with intentions of selling or service to the individual.<br />
<br />
Call center staff are often organized into a multi-tier support system for a more efficient
handling of calls. The first tier in such a model consists of operators, who direct inquiries to the appropriate department and provide general directory information. If a caller
requires more assistance, the call is forwarded to the second tier, where most issues can
be resolved. In some cases, there may be three or more tiers of support staff. If a caller
requires more assistance, the caller is forwarded to the third tier of support; typically the
third tier of support is formed by product engineers/developers or highly-skilled technical
support staff of the product.<br />
<br />
Many call centers in the UK have been built in areas that are depressed economically.
This means that the companies get cheap land and labor, and can often benefit from
grants to encourage them to improve employment in a given area. There has also been a
trend to move call centers to India, where there is a large pool of English-speaking
people. However, there has been widespread dissatisfaction from consumers, who are
unable to understand Indian call center staff and vice versa, owing to differences between
British and Indian English.<br />
<br />
This trend is on a decline as India rapidly absorbs most of the highly educated people
who are qualified in the science and mathematics streams, and, like in other countries,
call center jobs are increasingly viewed as stopgap jobs rather than as careers.<br />
<br />
Another popular call center site is the Philippines, owing to its abundant English speakers
that are college graduates and Americanized when it comes to English accent and cultural
affinities. The Philippines was an American colony for almost 50 years. The Philippines
is said to be the best outsourcing site outside North America since the accent of Filipinos
is nearer to that of American consumers as compared to other ethnicities.


</p>

<p class="virtualpage3">
<b>Management of call centers</b><br />
Management of call centers involves balancing the requirements of cost effectiveness and
service. Callers do not wish to wait in exorbitantly long queues until they can be helped
and so management must provide sufficient staff and inbound capacity to ensure that the
<a href="../a1/VoIP-Network-platform.asp">qualityof service</a>  is maintained. However, staff costs generally form more than half the
cost of running a call center and so management must minimize the number of staff
present.<br />
<br />
To perform this balancing act, call center managers make use of demand estimation,
Telecommunication forecasting and dimensioning techniques to determine the level of
staff required at any time. Managers must take into account staff tea and lunch breaks and
must determine the number of agents required on duty at any one time.<br />
<br />
<b>Forecasting demand</b><br />
Forecasting results are vital in making management decisions in call centers. Forecasting
methods rely on data acquired from various sources including historical data, trend data
and so on. Forecasting methods must predict the <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> intensity within the call center in
quarter-hour increments and these results must be converted to staffing rosters. Special
attention must be paid to the busy hour. Forecasting methods must be used to pre-empt a
situation where equipment needs to be upgraded as <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> intensity has exceeded the
maximum capacity of the call center.<br />
<br />
<b>Call center performance</b><br />
There are many standard <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> measurements (performance metrics) that can be
performed on a call center to determine its performance levels. However, the most
important performance measures are:<br />
<br />
. The average delay a caller may experience whilst waiting in a queue<br />
. The mean conversation time, otherwise referred to as Average Talk Time (ATT)<br />
. The mean dealing time, otherwise referred to as Average Handling Time (AHT -
equal to ATT plus wrap-up and/or hold time)<br />
. The percentage of calls answered within a determined time frame (referred to as a
Service Level or SL %)<br />
. The number of calls / inquiries per hour an agent handles (CPH or IPH).<br />
. The amount of time spent while an agent processes customer requests while not
speaking to a customer (referred to as Not Ready time/NR, or After Call
Work/ACW, or Wrap-Up.)<br />
. The percentage of calls which completely resolve the customer's issue (if the
customer does not call back about the same problem for a certain period of time, it
is considered a successful resolution or FCR - First Call Resolution).<br />
. The percentage of calls where a customer hangs up or "abandons" the call is often
referred to as Total Calls Abandoned or Percentage of calls abandoned. Calls are
often abandoned due to long hold times when a call center experiences a high call
volume.<br />
. Percentage of time agents spend not ready to take calls, often referred to as Idle
Time.<br />
. <a href="../a1/VoIP-Network-platform.asp">quality</a> Assurance monitored by a <a href="../a1/VoIP-Network-platform.asp">quality</a> assurance (QA) team.<br />
<br />
<b>Refinements of call centers</b><br />
There are many refinements to the generic call center model. Each refinement helps
increase the efficiency of the call center thereby allowing management to make better
decisions involving economy and service.<br />
<br />
The following list contains some examples of call center refinements:<br />
Predictive Dialing - Computer software attempts to predict the time taken for an agent to
help a caller. The software begins dialing another caller before the agent has finished the
previous call. This, because not every call will be connected (think of busy or not
answered calls) and also because of the time it takes to set up the call (usually around 20
seconds before someone answers). Frequently, predictive dialers will dial more callers
than there are agents, counting on the fact that not every line will be answered. When the
line is answered and no agent is available, it is held in a retention queue for a short while.<br />
<br />
When still no agent has become available, the call is hung up and classified as a nuisance
call. The next time the client is called an agent will be reserved for the caller.<br />
<br />
Multi-Skilled Staff - In any call center, there will be members of staff that will be more
skilled in areas than others. An 'Interactive Voice Response' (IVR) Unit can be used to
allow the caller to select the reason for his call. Management software, called an
Automatic Call Distributor, must then be used to route calls to the appropriate agent.
Alternatively, it has been found that a mix of general and specialist agent creates a good
balance.<br />
<br />
Prioritization of Callers - Classification of callers according to priority is a very
important refinement. Emergency calls or callers that are reattempting to contact a call
center are examples of callers that could be given a higher priority.<br />
<br />
Automatic Number Identification - This allows agents to determine who is calling before
they answer the call. Greeting a caller by name and obtaining his/her information in
advance adds to the <a href="../a1/VoIP-Network-platform.asp">quality of service</a> and helps decrease the conversation time.<br />
<br />

</p>

<p class="virtualpage3">
<b>Variations on the generic call center model</b><br />
The various components in a call center discussed in the previous sections are the generic
form of a call center. There are many variations on the model developed above. A few of
the variations are listed below:<br />
<br />
Remote Agents - An alternative to housing all agents in a central facility is to use remote
agents. These agents work from home and use a Basic Rate ISDN access line to
communicate with a central computing platform. Remote agents are more cost effective
as they don't have to travel to work, however the call center must still cover the cost of
the ISDN line.<br />
<br />
VOIP technology can also be used to remove the need for the ISDN, although the desktop
application being used needs to be web enabled or VPN is used.<br />
<br />
Temporary Agents - Temporary agents are useful as they can be called upon if demand
increases more rapidly than planned. They are offered a certain number of quarter hours a
month. They are paid for the amount they actually work, and the difference between the
amount offered and the amount guaranteed is also paid. Managers must use forecasting
methods to determine the number of hours offered so that the difference is minimized.<br />
<br />
Virtual Call Centers - Virtual Call Centers are created using many smaller centers in
different locations and connecting them to one another. The advantage of virtual call
centers is that they improve service levels, provide emergency backup and enable
extended operating hours over isolated call centers.<br />
<br />
<b>Criticism of call centers</b><br />

Criticisms of call centers generally follow a number of common themes:<br />
<br />
From Callers:<br />
. Operators working from a script.<br />
. Non-expert operators (call screening).<br />
. Incompetent or untrained operators incapable of processing customers' requests
effectively.<br />
. Overseas location, with language and accent problems.<br />
. Automated queuing systems.<br /><br />


From Staff:<br /><br />
. Close scrutiny by management (e.g. frequent random eavesdropping on operator's
calls).<br />
. Low pay.<br />
. Restrictive working practices (e.g. there isn't much space for personal creativity
since many operators are required to follow a pre-written script).<br />
. High stress: a common problem associated with front-end jobs where employees
deal directly with customers.<br />
. Poor working conditions (e.g. poor facilities, poor maintenance and cleaning,
cramped working conditions, management interference)<br /><br />
As detailed above, none of these are inherent in the call center model, although many
companies will experience some or all of the above while implementing a call center
approach. As the science suggests, done properly, a call center can offer the quickest
route to resolution of customer queries, capitalizing on the ready availability of highly
skilled and intelligent people in some areas.




</p>

</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/What_Is_a_Call_Center.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/3com-solutions-strategies.as"></a>  <a href="../doc/What_Is_a_Call_Center.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
