<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- © Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- © Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col">
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> OSP Module
User Guide<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
<b><a href="#">1 Introduction</a></b> <span class="credits"></span><br>
This document provides instructions on how to build and configure <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> V1.4 with
the OSP Toolkit to enable secure, multi-lateral peering. The OSP Toolkit is an open
source implementation of the OSP peering protocol and is freely available from
www.sipfoundry.org. The OSP standard defined by the European Telecommunications
Standards Institute (ETSI TS 101 321) www.esti.org. If you have questions or need help,
building <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> with the OSP Toolkit, please post your question on the OSP mailing list
at https://list.sipfoundry.org/mailman/listinfo/osp.<br />
<br />
<b>2 OSP Toolkit</b><br />
Please reference the OSP Toolkit document "How to Build and Test the OSP Toolkit
available from www.sipfoundry.org/OSP/OSPclient .<br />
<br />
<b>2.1 Build OSP Toolkit</b><br />
The software listed below is required ti build and use the OSP Toolkit:<br />
 OpenSSL (required for building) - Open Source SSL protocol and Cryptographic
Algorithms (version 0.9.7g recommended) from www.openssl.org. Pre-compiled
OpenSSL binary packages are not recommended because of the binary compatibility
issue.<br />
 Perl (required for building) - A programming language used by OpenSSL for
compilation. Any version of Perl should work. One version of Perl is available from
www.activestate.com/ActivePerl. If pre-compiled OpenSSL packages are used, Perl
package is not required.<br />
 C compiler (required for building) - Any C compiler should work. The GNU
Compiler Collection from www.gnu.org is routinely used for building the OSP
Toolkit for testing.<br />
OSP Server (required for testing) - Access to any OSP server should work. Open
source OSP servers are available from www.sipfoundry.org/osp, a free commercial
OSP server may be downloaded from www.transnexus.com and an OSP server
osptestserver.transnexus.com is freely available on the internet for testing for testing.
Please contact support@transnexus.com for testing access to
osptestserver.transnexus.com.<br />
<br />
<b>2.1.1 Unpacking the Toolkit</b><br />
After downloading the OSP Toolkit (version 3.3.4 or later release) from
www.sipfoundry.org, perform the following steps in order:<br />
1) Copy the OSP Toolkit distribution into the directory where it will reside, say /usr/src.<br />
2) Un-package the distribution file by executing the following command:<br /><br />

<span class="ventral">gunzip c OSPToolkit-###.tar.gz | tar xvf</span><br />
<br />
Where ### is the version number separated by underlines. For example, if the version
is 3.3.4, then the above command would be:<br />
<br />
<span class="ventral">gunzip c OSPToolkit-3_3_4.tar.gz | tar xvf</span>
<br />
<br />
A new directory (TK-3_3_4-20051103) will be created within the same directory as
the tar file.<br />
3) Go to the TK-3_3_4-20051103 directory by running this command:<br />
<br />
<span class="ventral">cd TK-3_3_4-20051103</span><br />
<br />
Within this directory, you will find directories and files similar to what is listed below
if the command "ls -F" is executed):<br />
<br />

<span class="ventral">ls -F<br />
enroll<br />
RelNotes.txt ....lib<br />
README.txt.....license.txt<br />
bin/................src/<br />
crypto/............test<br />
include/<br />
</span><br />
<br />
<b>2.1.2 Preparing to build the OSP Toolkit</b><br />
4) Compile OpenSSL according to the instructions provided with the OpenSSL
distribution (You would need to do this only if you dont have openssl already).<br />
5) Copy the OpenSSL header files (the *.h files) into the crypto/openssl directory within
the osptoolkit directory. The OpenSSL header files are located under the
openssl/include/openssl directory.<br />
6) Copy the OpenSSL library files (libcrypto.a and libssl.a) into the lib directory within
the osptoolkit directory. The OpenSSL library files are located under the openssl
directory.<br />
<b>Note:</b> Since the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> requires the OpenSSL package. If the OpenSSL package has
been installed, 4~6 are not necessary.
<br />
<br />
<b>2.1.3 Building the OSP Toolkit</b><br />
7) Optionally, change the install directory of the OSP Toolkit. Open the Makefile in the
/usr/src/TK-3_3_4-20051103/src directory, look for the install path variable 
INSTALL_PATH, and edit it to be anywhere you want (defaults /usr/local).
Note: Please change the install path variable only if you are familiar with both the
OSP Toolkit and the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. Otherwise, it may case that the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> does not
support the OSP protocol.<br />
8) From within the OSP Toolkit directory (/usr/src/TK-3_3_4-20051103), start the
compilation script by executing the following commands:<br />
<br />
<span class="ventral">cd src</span><br />
<span class="ventral">make clean; make build</span><br />
<br />
</p>

<p class="virtualpage3">

<b>2.1.4 Installing the OSP Toolkit</b> <span class="credits"></span><br>
The header files and the library of the OSP Toolkit should be installed. Otherwise, you
must specify the OSP Toolkit path for the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
9) Use the same script to install the Toolkit.<br />
<span class="ventral">make install</span>
<br />
<br />
The make script is also used to install the OSP Toolkit header files and the library
into the INSTALL_PATH specified in the Makefile.<br />
<br />
<b>Note:</b> Please make sure you have the rights to access the INSTALL_PATH directory.
For example, in order to access /usr/local directory, normally, you should be root.
By default, the OSP Toolkit is compiled in the production mode. The following table
identifies which default features are activated with each compile option:<br />
<br />
<img src="images/Default_Feature.jpg" /><br />
<br />
The "Development" option is recommended for a first time build. The CFLAGS
definition in the Makefile must be modified to build in development mode.<br /><br />

<b>2.1.5 Building the Enrollment Utility</b><br />
Device enrollment is the process of establishing a trusted cryptographic relationship
between the VoIP device and the OSP Server. The Enroll program is a utility application
for establishing a trusted relationship between and OSP client and an OSP server. Please
see the document "Device Enrollment" at www.sipfoundry.org/OSP/OSPclient for more
information about the enroll application.<br />
10) From within the OSP Toolkit directory (/usr/src/TK-3_3_4-20051103), execute the
following commands at the command prompt:<br />
<br />
<span class="ventral">cd enroll</span> <br />
<span class="ventral">make clean; make linux</span><br />
<br />
Compilation is successful if there are no errors anywhere in the compiler output. The
enroll program is now located in the /usr/src/TK-3_3_4-20051103/bin directory. By
this point, a fully functioning OSP Toolkit should have been successfully built.<br />
<br />
<b>2.2 Obtain Crypto Files</b><br />
The OSP module in <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> requires three crypto files containing local certificate
(localcert.pem), private key (pkey.pem), and CA certificate (cacert_0.pem). <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> will
try to load the files from the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> public/private key directory - /var/lib/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>/key.
If the files are not present, the OSP module will not start and the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> will not support
the OSP protocol. Use the enroll.sh script from the toolkit distribution to enroll the
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> OSP module with an OSP server to obtain the crypto files. Documentation
explaining how to use the enroll.sh script (Device Enrollment) to enroll with an OSP
server is available at www.sipfoundry.org/OSP/ospclient. Copy the files file generated
by the enrollment process to the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> configuration directory.<br />
<br />
<b>Note:</b> The osptestserver.transnexus.com is configured only for sending and receiving
non-SSL messages, and issuing signed tokens. If you need help, post a message on the
OSP mailing list of www.sipfoundry.org or send an e-mail to support@transnexus.com<br />
<br />
The enroll.sh script takes the domain name or IP addresses of the OSP servers that the
OSP Toolkit needs to enroll with as arguments, and then generates pem files 
cacert_#.pem, certreq.pem, localcert.pem, and pkey.pem. The # in the cacert file name
is used to differentiate the ca certificate file names for the various SPs (OSP servers). If
only one address is provided at the command line, cacert_0.pem will be generated. If 2
addresses are provided at the command line, 2 files will be generated  cacert_0.pem and
cacert_1.pem, one for each SP. The example below shows the usage when the client is
registering with osptestserver.transnexus.com. If all goes well, the following text will be
displayed. The gray boxes indicate required input.<br />
<br />

<span class="ventral">./enroll.sh osptestserver.transnexus.com</span><br />
<span class="ventral">Generating a 512 bit RSA private key</span><br />
<span class="ventral">........................++++++++++++<br />
.........++++++++++++<br />
</span> <span class="ventral">writing new private key to 'pkey.pem'<br />
-----<br />
You are about to be asked to enter information that will be incorporated<br />
into your certificate request<br />
What you are about to enter is what is called a Distinguished Name or
a DN.<br />
</span> <span class="ventral">There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.<br />
-----</span><br />
<span class="ventral">Country Name (2 letter code) [AU]: _______<br />
State or Province Name (full name) [Some-State]: _______<br />
Locality Name (eg, city) []:_______<br />
Organization Name (eg, company) [Internet Widgits Pty Ltd]: _______<br />
Organizational Unit Name (eg, section) []:_______<br />
Common Name (eg, YOUR name) []:_______<br />
Email Address []:_______<br />
</span><br />
<span class="ventral">Please enter the following 'extra' attributes<br />
to be sent with your certificate request<br />
A challenge password []:_______<br />
An optional company name []:_______<br />
</span><br />
<span class="ventral">Error Code returned from openssl command : 0</span><br />
<br />
<br />

<span class="ventral">CA certificate received<br />
[SP: osptestserver.transnexus.com]Error Code returned from getcacert
command : 0<br />
<br />
output buffer after operation: operation=request<br />
output buffer after nonce: operation=request&nonce=1655976791184458<br />
X509 CertInfo context is null pointer<br />
Unable to get Local Certificate<br />
</span> <span class="ventral">depth=0 /CN=osptestserver.transnexus.com/O=OSPServer<br />
verify error:num=18:self signed certificate<br />
verify return:1<br />
depth=0 /CN=osptestserver.transnexus.com/O=OSPServer<br />
verify return:1<br />
The certificate request was successful.<br />
Error Code returned from localcert command : 0</span><br />
</span><br />
<br />
</p>
<p class="virtualpage3">
The files generated should be copied to the /var/lib/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>/key directory.<br />
<b>Note:</b> The script enroll.sh requires AT&T korn shell (ksh) or any of its compatible
variants. The /usr/src/TK-3_3_4-20051103/bin directory should be in the PATH variable.
Otherwise, enroll.sh cannot find the enroll file.

<b>3 <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a></b><br />
<b>3.1 OSP Support Implementation</b><br />
In <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, all OSP support is implemented as dial plan functions.<br />
<br />
<b>3.1.1 OSPAuth</b><br />
OSP token validation function.<br />
Input:<br />
<br />
 OSPPEERIP: last hop IP address<br />
 OSPINTOKEN: inbound OSP token<br />
 provider: OSP service provider configured in osp.conf. If it is empty, default
provider is used.<br />

 priority jump<br />
<br />
Output:<br />
<br />
 OSPINHANDLE: inbound OSP transaction handle<br />

 OSPINTIMELIMIT: inbound call duration limit<br />

 OSPAUTHSTATUS: OSPAuth return value. SUCCESS/FAILED/ERROR<br />
<br />
<b>3.1.2 OSPLookup</b><br />
OSP lookup function.<br />
Input:<br />
 OSPPEERIP: last hop IP address
 OSPINHANDLE: inbound OSP transaction handle
 OSPINTIMELIMIT: inbound call duration limit
 exten: called number
 provider: OSP service provider configured in osp.conf. If it is empty, default
provider is used.
 priority jump<br />
<br />
Output:<br />
<br />
 OSPOUTHANDLE: outbound transaction handle<br />
 OSPTECH: outbound protocol<br />
 OSPDEST: outbound destination<br />
 OSPCALLING: outbound calling number<br />
 OSPOUTTOKEN: outbound OSP token<br />
 OSPRESULTS: number of remain destinations<br />
 OSPOUTTIMELIMIT: outbound call duration limit<br />
 OSPLOOKUPSTATUS: OSPLookup return value. SUCCESS/FAILED/ERROR<br />
<br />
<b>3.1.3 OSPNex</b><br /><br />
OSP lookup next function.<br />
Input:<br />
 OSPINHANDLE: inbound transaction handle<br />
 OSPOUTHANDLE: outbound transaction handle<br />
 OSPINTIMELIMIT: inbound call duration limit<br />
 OSPRESULTS: number of remain destinations<br />
 cause: last destination disconnect cause<br />
 priority jump<br />
<br />
Output:<br />
<br />
 OSPTECH: outbound protocol<br />
 OSPDEST: outbound destination<br />
 OSPCALLING: outbound calling number<br />
 OSPOUTTOKEN: outbound OSP token<br />
 OSPRESULTS: number of remain destinations<br />
 OSPOUTTIMELIMIT: outbound call duration limit<br />
 OSPNEXTSTATUS: OSPLookup return value. SUCCESS/FAILED/ERROR<br />
<br />
<br />
<b>3.1.4 OSPFinish</b><br />
OSP report usage function.<br />
Input:<br />
<br />
 OSPINHANDLE: inbound transaction handle<br />
 OSPOUTHANDLE: outbound transaction handle<br />
 OSPAUTHSTATUS: OSPAuth return value<br />
 OSPLOOKUPTSTATUS: OSPLookup return value<br />
 OSPNEXTSTATUS: OSPNext return value<br />

 cause: last destination disconnect cause<br />
 priority jump<br />

Output:<br />

 OSPFINISHSTATUS: OSPLookup return value. SUCCESS/FAILED/ERROR<br />
<br />
</p>

<p class="virtualpage3">
<b>3.2 Build with OSP Support</b><br />
If the OSP Toolkit is installed in the default install directory, /usr/local, no additional
configuration is required. If the OSP Toolkit is installed in another directory, say /myosp,
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> must be configured with the location of the OSP Toolkit.<br />
<br />

<span class="ventral">--with-osptk=/myosp</span><br />
<br />

Note: Please change the install path only if you familiar with both the OSP Toolkit and
the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. Otherwise, the change may results <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> not supporting the OSP protocol.<br />
<br />
Now, you can compile <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> according to the instructions provided with the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
distribution.<br />
<br />

;<br />
; <span class="ventral">Open Settlement Protocol Sample Configuration File<br />
;<br />
; This file contains configuration of providers that<br />
; are used by the OSP subsystem of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. The section<br />
; "general" is reserved for global options. Each other<br /></span>
<span class="ventral">; section declares an OSP Provider. The provider "default"<br />
; is used when no provider is otherwise specified.<br />
 ;<br />
[general]<br />
;<br />
; Should hardware accelleration be enabled? May not be changed<br />
; on a reload.<br /></span> 
<span class="ventral">;<br />
accelerate=no<br />
;<br />
; Defines the token format that <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can validate.<br />
; 0 - signed tokens only<br />
; 1 - unsigned tokens only<br /></span>
<span class="ventral">; 2 - both signed and unsigned<br />
; The defaults to 0, i.e. the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can validate signed tokens
only.<br />
</span>

;<br />
<span class="ventral">tokenformat=0<br />
;<br />
[default]<br />
;<br />
; All paths are presumed to be under /var/lib/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>/keys unless<br />
; the path begins with '/'<br /></span>
;<br />
<span class="ventral">; Specify the private keyfile. If unspecified, defaults to the name<br />
; of the section followed by "-privatekey.pem" (e.g. defaultprivatekey.
pem)<br />
;<br />
privatekey=pkey.pem<br />
</span> ;<br />
<span class="ventral">; Specify the local certificate file. If unspecified, defaults to<br />
; the name of the section followed by "-localcert.pem"<br />
;<br />
localcert=localcert.pem<br />
</span> ;<br />
<span class="ventral">; Specify one or more Certificate Authority keys. If none are
listed,<br />
; a single one is added with the name "-cacert.pem"<br />
;<br />
cacert=cacert_0.pem<br />
;<br /></span>
<span class="ventral">; Specific parameters can be tuned as well:<br />
; maxconnections: Max number of simultaneous connections to the
provider (default=20)<br />
; retrydelay: Extra delay between retries (default=0)<br />
; retrylimit: Max number of retries before giving up (default=2)<br />
; timeout: Timeout for response in milliseconds (default=500)<br />
</span> ;<br />
 <span class="ventral">maxconnections=20<br />
retrydelay=0<br />
retrylimit=2<br />
timeout=500<br />
;<br /></span> 
 <span class="ventral">; List all service points for this provider<br />
;<br />
;servicepoint=http://osptestserver.transnexus.com:1080/osp<br />
servicepoint=http://OSP server IP:1080/osp<br />
;<br />
</span>
 <span class="ventral">; Set the "source" for requesting authorization<br />
;<br />
;source=foo<br />
source=[host IP]<br />
 </span>
 ;<br />
<span class="ventral">; Set the authentication policy.<br />
; 0 - NO<br />
; 1 - YES<br />
; 2 - EXCLUSIVE<br />
; Default is 1, validate token but allow no token.<br />
;<br />
authpolicy=1<br />
</span>
<br />
<br />
</p>

<p class="virtualpage3">
<b>3.3.2 zapata/sip/iax.conf</b><br />
There is no configuration required for OSP.<br />
<br />
<b>3.3.3 extensions.conf</b><br />
An <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> box can be configured as OSP source/destination gateway or OSP proxy.<br />
<br />
<b>3.3.3.1 OSP Source Gateway</b><br />
<br />

<span class="ventral">[PhoneSrcGW]<br />
; Set calling number if necessary<br />
exten => _XXXX.,1,Set(CALLERID(numner)=CallingNumber)<br />
; OSP lookup using default provider, if fail/error jump to 2+101<br />
exten => _XXXX.,2,OSPLookup(${EXTEN}||j)<br />
; Set calling number which may be translated<br /></span>
<span class="ventral">exten => _XXXX.,3,Set(CALLERID(number)=${OSPCALLING})<br />
; Dial to destination, 60 timeout, with call duration limit<br />
exten =><br />
_XXXX.,4,Dial(${OSPTECH}/${OSPDEST},60,oL($[${OSPOUTTIMELIMIT}*1000])<br />
)<br />
 ; Wait 3 seconds<br />
 exten => _XXXX.,5,Wait,3<br /></span>
<span class="ventral">; Hangup<br />
exten => _XXXX.,6,Hangup<br />
; Deal with OSPLookup fail/error<br />
exten => _XXXX.,2+101,Hangup<br />
; OSP report usage<br />
exten => h,1,OSPFinish(${HANGUPCAUSE})<br />
</span>
<br />


<b>3.3.3.2 OSP Destination Gateway</b><br /><br />

<span class="ventral">[PhoneDstGW]<br />
; Get peer IP<br />
exten => _XXXX.,1,Set(OSPPEERIP=${SIPCHANINFO(peerip)})<br />
; Get OSP token<br /></span>
<span class="ventral">exten => _XXXX.,2,Set(OSPINTOKEN=${SIP_HEADER(P-OSP-Auth-Token)})<br />
; Validate token using default provider, if fail/error jump to 3+101<br />
exten => _XXXX.,3,OSPAuth(|j)<br />
; Ringing<br />
exten => _XXXX.,4,Ringing<br />
</span> ; Wait 1 second<br />
<span class="ventral">exten => _XXXX.,5,Wait,1<br />
; Dial phone, timeout 15 seconds, with call duration limit<br />
exten =><br />
_XXXX.,6,Dial(${DIALOUTANALOG}/${EXTEN:1},15,oL($[${OSPINTIMELIMIT}*1<br />
000]))<br />
 ; Wait 3 seconds<br />
</span> <span class="ventral">exten => _XXXX.,7,Wait,3<br />
; Hangup<br />
exten => _XXXX.,8,Hangup<br />
; Deal with OSPAuth fail/error<br />
exten => _XXXX.,3+101,Hangup<br />
; OSP report usage<br />
exten => h,1,OSPFinish(${HANGUPCAUSE})<br />
</span>>
<br />
<br />

<b>3.3.3.3 Proxy</b><br /><br />
<span class="ventral"> [GeneralProxy]<br />
; Get peer IP<br />
exten => _XXXX.,1,Set(OSPPEERIP=${SIPCHANINFO(peerip)})<br />
; Get OSP token<br /></span>
<span class="ventral">exten => _XXXX.,2,Set(OSPINTOKEN=${SIP_HEADER(P-OSP-Auth-Token)})<br />
; Validate token using default provider, if fail/error jump to 3+101<br />
exten => _XXXX.,3,OSPAuth(|j)<br />
</span> <span class="ventral">; OSP lookup using default provider, if fail/error jump to 4+101<br />
exten => _XXXX.,4,OSPLookup(${EXTEN}||j)<br />
; Set calling number which may be translated<br />
exten => _XXXX.,5,Set(CALLERID(number)=${OSPCALLING})<br />
; Dial to 1st destination, 60 timeout, with call duration limit<br />
exten =><br />
</span>
<span class="ventral">_XXXX.,6,Dial(${OSPTECH}/${OSPDEST},24,oL($[${OSPOUTTIMELIMIT}*1000])<br />
)<br />
; OSP lookup next, if fail/error jump to 7+101<br />
exten => _XXXX.,7,OSPNext(${HANGUPCAUSE}||j)<br />
</span>
<span class="ventral">; Set calling number which may be translated<br />
exten => _XXXX.,8,Set(CALLERID(number)=${OSPCALLING})<br />
; Dial to 2nd destination, 60 timeout, with call duration limit<br />
exten =></span><br />
 <span class="ventral">_XXXX.,9,Dial(${OSPTECH}/${OSPDEST},25,oL($[${OSPOUTTIMELIMIT}*1000])<br />
)<br />
; OSP lookup next, if fail/error jump to 10+101<br />
exten => _XXXX.,10,OSPNext(${HANGUPCAUSE}||j)<br />
 </span> <span class="ventral">; Set calling number which may be translated<br />
 exten => _XXXX.,11,Set(CALLERID(number)=${OSPCALLING})<br />
; Dial to 3rd destination, 60 timeout, with call duration limit<br />
exten =><br />
 </span> <span class="ventral">_XXXX.,12,Dial(${OSPTECH}/${OSPDEST},26,oL($[${OSPOUTTIMELIMIT}*1000]<br />
))<br />
; Hangup<br />
exten => _XXXX.,13,Hangup<br />
 </span>
 <span class="ventral">; Deal with OSPAuth fail/error<br />
 exten => _XXXX.,3+101,Hangup<br />
; Deal with OSPLookup fail/error<br />
exten => _XXXX.,4+101,Hangup<br />
 </span>
 <span class="ventral">; Deal with 1st OSPNext fail/error<br />
 exten => _XXXX.,7+101,Hangup<br />
; Deal with 2nd OSPNext fail/error<br />
 </span>
 <span class="ventral">exten => _XXXX.,10+101,Hangup<br />
; OSP report usage<br />
exten => h,1,OSPFinish(${HANGUPCAUSE})<br />
 </span>
</p>
</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Asterisk_OSP_Module_User_Guide.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/Asterisk_OSP_Module.asp"></a><a href="../doc/Asterisk_OSP_Module_User_Guide.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
