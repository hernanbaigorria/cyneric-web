<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
width: 250px;
text-align: center;
padding: 2px 0;
margin: 10px 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
border: 1px solid navy;
margin: 0 15px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col"><h1>
What's SIP Got To Do With It?<br />
            </h1>
              
              <!-- Pagination DIV for Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
      <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select class="acor2">
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div>
<div style="width: 500px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
<b><br /><a href="#">Introduction</a></b> <span class="credits"></span><br>
<b>Recent LAN and WAN technologies
make voice sound good.</b><br />

Since 1998 when 3Com brought to market
the first IP PBX, the enterprise Voice over
IP (VoIP) phenomenon has changed not just
telecommunications, but also enterprise
networking. The 3Com� NBX� system
ushered in a new era of communications.
For the first time, companies were able to
leverage their investments in high performance
local area <a href="../../a1/VoIP-Network-platform.asp">networks</a> (LANs) to deliver
real-time services such as IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a>.<br />
<br />
In fact, the major standardized innovations
in Layer 2 switches and enterprise routers
in the past five years-virtual LAN (VLAN)
segmentation, packet prioritization, Power
over Ethernet (PoE), differentiated services,
multi-protocol label switching-enhance
converged networking and its delivery of
high-<a href="../a1/VoIP-Network-platform.asp">quality</a> audio.<br />
<br />
. VLAN segmentation separates voice
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> from e-mail uploads, web surfing,
and database interactions<br />
. Packet prioritization techniques enable
higher priority for voice packets than
other applications so that jitter and delay
are minimized or eliminated<br />
. PoE enables a combination power-andcommunications
cable to minimize the
required number of 120V power ports<br />
. Differentiated services and Multi-
Protocol Label Switching (MPLS) enable
prioritization for voice packet streams
and rapid transit across IP wide area
<a href="../../a1/VoIP-Network-platform.asp">networks</a> (WANs)<br />
<br />


Enhancements to the wireless LAN environment
through the auspices of the IEEE
802.11 committees also facilitate high
<a href="../a1/VoIP-Network-platform.asp">quality</a> IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> services. Recently
ratified specifications for <a href="../a1/VoIP-Network-platform.asp">quality of Service</a>
over the air (802.11e) and privacy services
(802.11i) address many of the same issues
being addressed by other 802.1 committees.<br />
<br />
Technology advances are driving significant
investment in the development and deployment
of more reliable and high-performance
LANs. This momentum helps maximize the
reliability and audio <a href="../a1/VoIP-Network-platform.asp">quality</a> of IP telephony
implementations and create solutions to
match the cost expectations of enterprises.</p>

<p class="virtualpage3">
<b><br />2005-the Year of VoIP</b> <span class="credits"></span><br>
2005 is a fundamental turning point for the
IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> industry. For the first time,
more IP-PBX ports will ship in the United
States than legacy digital ports . This is a
major milestone, indicative of the reliability
and audio <a href="../a1/VoIP-Network-platform.asp">quality</a> now available in IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> services. The discussion of "if"
users will migrate to IP telephony is now
"when" and with what strategic urgency.<br />
<br />
Classically, the year of inflection represents
an acceleration of the pace of growth in the
market. In the case of IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a>, this
accelerated growth is occurring now for
two primary reasons:<br />

. Depreciation schedules-equipment
purchased in preparation for Y2K is being
retired now, enabling new purchases<br />

. Large enterprise needs-emerging business
requirements necessitate enhanced,
cost-effectives services, already enjoyed
by small businesses with IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> deployments<br />

The value proposition for IP telephony is
moving beyond cost reduction-oriented
business cases, to a framework of "convergence
applications" that offer advantages
difficult to accomplish with legacy telephone
implementations. Applications-such as IP
messaging-can broadcast voice mail and
deliver voice mail as e-mail. IP conferencing
can provide audio, video, and data conferencing
services. IP contact centers offer
freedom from the geographic constraints of
legacy solutions. IP mobility solutions that
provide on-campus voice and data services
via wireless LANs are proving themselves
critical for business productivity and
customer interactions.</p>




<p class="virtualpage3">
<b><br /><a href="#">The Importance of Session
Initiation Protocol in
Enterprise IP Telephony</a></b> <span class="credits"></span><br>
If indee<a href="../../a1/VoIP-Network-platform.asp">d applica</a>tions are the driving force
behind IP telephony implementations, what
will be the key enabler of enterprise applications,
enterprise applications developers,
and integrations into existing enterprise
applications?<br />
<br />
It is the premise of this white paper that
Session Initiation Protocol (SIP) will fill that
need.<br />
<br />
<b>SIP is simple.</b><br />
Using the Internet Engineering Task Force
(IETF) fundamental of technology reuse and
the proven value of simplicity, the SIP message
set is quite an elegant construction-six
messages that appear in clear text to facilitate
call setup. Clear text allows for easy
troubleshooting and avoids complex software
interactions and other processing that
affect interoperability. The six messages are
Invite, Trying, Ringing, OK, ACK, Bye.<br />
<br />
SIP is both elegant and practical. Its base
assumption is that all SIP endpoints and
elements exist in the IP environment, an
arena already equipped with standard mechanisms
to handle packet transport priorities,
privacy, and other required services. These
value-added services do not require specification
in the SIP framework. Whereas, in
legacy environments such as the public telephone
<a href="../../a1/VoIP-Network-platform.asp">network</a> the use of hyper-text transport
protocol (http) is neither integral or readily
available.<br />
<br />
This "building block" approach on an IP
base is unique to IETF initiatives. The results
are nearly trivial protocol definitions such as
SIP in contrast with older session control or
interface protocols such as H.323 or Q.SIG
from the International Telecommunications
Union (ITU). These older, telecom-centric
protocols have considerably more complex
definitions as shown in Table 1.<br />
<br /><br />
<img src="images/T_Contrast_betwee.jpg" /></p>

<p class="virtualpage3">
In civilization and in nature, simple has
been proven to be both useful and enduring.
For example, the sphere and its derivatives
are frequently occurring shapes in fruit and
celestial objects as contrasted to the more
complex pyramid. The steering wheel, rather
than the handle bar, is used in automobile
control designs. The flat CD replaced the
more complex floppy disk cartridge design as
the definitive media storage framework.<br />
<br />
Years ago, a wise man once explained why
Asynchronous Transfer Mode (ATM) was
going to destroy a carrier service of interest to
the phone companies at the time-Switched
Multi-megabit Data Service (SMDS)-that
was a connectionless ATM-type service. Two
arguments were given for his assertion. First,
ATM was simpler (notice three-letter acronym
versus the four-letter acronym). Second,
ATM didn't require a phone company to
implement the technology, resulting in ATM
having a larger addressable market that
could provide ample opportunity for solving
business problems (relevance).<br />
<br />
<b>SIP is secure.</b><br />

There are tradeoffs between interoperability
and cost when considering security. Simple
standards make it easier to interoperate within
a multi-vendor <a href="../../a1/VoIP-Network-platform.asp">network</a>, but they also expose
the <a href="../../a1/VoIP-Network-platform.asp">network</a> to potential abuse. This vulnerability,
however, can be effectively addressed
by strong authentication and privacy services.
that do not interfere with the primary business
benefit of standard-based solutions and
also enable a long investment life through
interchangeable vendors, services, applications,
and devices.<br />
<br />
The IETF framework for SIP offers a rich set
of standards for authentication and privacy,
including secure SIP, secure RTCP, and secure
RTP. These capabilities leverage IETF proposals
for the use of standard implementations such
as Transport Layer Security (TLS) for robust
session privacy service and Secure/Multipart
Internet Mail Extensions (S/MIME) for
session control packet privacy.<br />
<br />

<br />
Most vendors implementing H.323 offer no
such options. Instead, they choose to implement
proprietary derivatives to facilitate
rudimentary forms of privacy in first generation
IP-PBX devices. This approach is
typical of the PBX vendor solutions brought
to market during the past two decades in
which proprietary digital signaling protocols
were implemented on endpoints and PBX
fabric. The result of these decisions was
nominally-better security, but considerably
more vendor-specific lock-in that failed to
provide many useful features-such as call
forward and hold-at a standard service level.<br />
<br />
<b>SIP is a standard.</b><br />

Despite being a standard, SIP implementations
do vary. Some vendors, most notably
legacy TDM-based PBX vendors and first
generation IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> vendors who built
their products around H.323 or around
proprietary protocols, offer SIP interoperability
as an interface into their otherwise
non-SIP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> system. This approach
helps the vendor maintain account and
feature control. They can charge extra for
SIP support for only the most rudimentary
features while offering advanced premiumpriced
feature options with their proprietary
call control protocol and implementation.<br />
<br />
Alone among the leading IP-PBX vendors,
3Com has implemented a different approach.
Using standard SIP among endpoints such as
IP phones, video cameras, call controllers
and gateways to the public telephone <a href="../../a1/VoIP-Network-platform.asp">network</a>,
3Com SIP-based solutions enable a robust
and inexpensive, vendor-neutral environment,
capable of rapidly delivering a portfolio of
cutting-edge applications for presence, conferencing,
contact center, messaging, and
mobility services. Furthermore, 3Com is a
founding member of the SIP Forum and
promotes the 3Com Voice Solutions Provider<br />
<br />
</p>

<p class="virtualpage3">
Program as an initiative to encourage SIPbased
interoperability. A list of more than 30
vendors, service providers, and solutions
that are proven to interoperate with 3Com IP
  <a href="../../a1/VoIP-Network-platform.asp">telephony</a> modules is available at:
www.3com.com/voip/interoperability.html<br />
<br />
SIP applications abound.
Internet search engines can be an amazing
proxy for markets and issues. Their objective
is to scour the Internet, capture prose, process
whatever they find, and quickly present it
to users with some rank ordering that
approximates users' views of relevancy.
Google, in particular, is an effective measure
of "buzz" because of its contentpresentation
in terms of users' views of relevancy.<br />
<br />


Figure 2 shows the contrast between the
number of references found by the Google
search engine for "SIP applications", "H.323
applications", and "<a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> Skinny Call Control
Protocol." In this analysis, the presence of
open, simple (SIP) references clearly outpaces
references for open, complex (H.323)
and proprietary applications. End users are
recognizing the enormous flexibility and
value of SIP.<br />
<br /><br />
<img src="images/F2_Thousands.jpg" />
<br />
<br />

<b>SIP is used by enterprises and carriers.</b><br />

Since SIP transcends carrier/enterprise
boundaries, it can attract applications developers
able to deliver their core applications
technologies in a variety of value packages
and to address a range of financing models
and markets. For example:<br />
<br />
. The stand-alone enterprise market,
addressed by direct sales or resellers<br />

. The stand-alone consumer market, addressed
by retailers, online or direct marketers<br />

. The hosted service model, sold to carriers
but billed to enterprises in monthly charges<br />

. The hosted service model, sold to carriers
but billed to consumers in monthly charges<br /><br />
. The stand-alone enterprise market,
addressed by carrier enterprise equipment
sales organizations<br />
<br />
This multi-market model provides ample
space for initial focus. Depending on vendor
strengths and skills, the model supports
considerable opportunity for both startup
and established developers.<br />
<br />
Since SIP is used by enterprises and carriers
across multiple markets, carriers can offer
innovative services aimed at enterprises. For
example, SIP trunking services allow two
SIP-empowered enterprises to communicate
using RTP from endpoint to endpoint without
the need for a gateway between them. The
growth of this category of services is helping
reduce costs by lowering the load on packetcircuit
gateways and reducing regulatory
and tax burdens.<br />
<br />
SIP trunking also improves audio <a href="../a1/VoIP-Network-platform.asp">quality</a>
since bandwidth allocation can be negotiated
end-to-end instead of end-to-gateway-overdigital-
to-gateway-to-end. Fewer digital
hops with more bandwidth enables wideband
audio <a href="../a1/VoIP-Network-platform.asp">quality</a>, as well as SIP-initiated
video conferencing.<br />
<br />
</p>



<p class="virtualpage3">
<b>Summary</b> <span class="credits"></span><br>
As demand for IP telephony and SIP services
and implementations continues to accelerate,
the public telephone <a href="../../a1/VoIP-Network-platform.asp">network</a> faces change.
A mainstay of the global economy for the
past ten decades, it is losing ground to a
<a href="../../a1/VoIP-Network-platform.asp">network</a> of world-wide IP communications
integrated with simple, secure, standardsbased,
applications-rich implementations
and services.<br />
<br />
There can be little doubt of the important role
that SIP will play in facilitating this transition
and enabling powerful enterprise applications
that reduce cost, improve user productivity,
and strengthen customer interactions. Some
vendors will be slow to appreciate and take
advantage of this opportunity. They may focus
their energies on attempting to retrain the
transition rather than exploring its possibilities.<br />
<br />
3Com embraces innovation and standards.
They are a foundation on which the company
builds business-enhancing solutions. The
question is no longer why, but when will SIP
be integral to every business' future?</p>
</div></td>
  </tr>
</table>








<!-- Initialize Demo 3 -->
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 2, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
