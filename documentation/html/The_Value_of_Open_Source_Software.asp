<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
The Value of Open Source Software:<br />
Sandals, Savings or Services?<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
It's September 2006, and, after eight years as an open source business, Squiz is now in a position to describe the
true value of open source software. Not that much has changed, mind. As those who know us will testify, we've
always espoused the model I'm about to describe. It's just that until now hype, hoopla and market expectations
have tended to cloud things.<br />
<br />
Open source is, and always should be, understood as an innovation in 'value' (in its broadest sense) as opposed
to an innovation in technology. Basically, it has 'cried wolf' on commercial software and led us to question the
substance of what we're paying for.<br />
<br />
On one hand, this can be attributed to maturity in the software market - one dotcom boom later and we realise
we shouldn't pay �1 million for a corporate web site. Competition hots up and so prices naturally fall. But this
only addresses the price-value factor. On the other hand we have the Robin Hood factor - a bunch of merry
developer-pranksters plotting the downfall of Microsoft from the comfort of their sandals. This is revolutionary,
and so support has grown for a 'free software' philosophy as an alternative to the norm. But 'free' doesn't always
mean valuable.<br />
<br />
So why is open source software now a fixture on Main Street? And what's the real value of pursuing an open
source strategy?<br />
<br />
To frame the Squiz philosophy, I'd like to call in some help.<br />
<br />
Firstly, marketing guru Seth Godin, who is clearly fed up with being force fed upgrades to commodity products.
His complaint is as follows:<br />
<br />
"Just about everything that can be improved, is being improved. If you define 'improved' to mean more features,
more buttons, more choices, more power, more cost. The washing machine I used this morning had more than
125 different combinations of ways to do the wash... don't get me started about the dryer. Clearly, an arms race is
a good way to encourage people to upgrade."1<br />
<br />
Secondly, business guru Geoffrey Moore, who sees open source as free, but not free as we know it. He describes
its value as follows:<br />
<br />
"How can there be any economic advantage in [open source]? Open Source is a cost-reduction resource-sharing
mechanism for managing technologies that are required for your business but not core to your differentiation.
Total cost of ownership is amortized across many organizations that contribute to the maintenance of the
technology, work that is supplemented with paid-for services to customize it to their company's specific needs."2<br />
<br />
These are interesting thoughts. Products don't need to be upgraded and re-paid for every year if they work just
fine; and the cost savings created by open source software can be re-channelled to customise things to specific
needs. For Squiz, the latter point is the real value of open source, and the former is a necessary learning process
that one has to follow in order to accept the open source philosophy. The rest of this white paper describes how
and why.
</p>

<p class="virtualpage3">
<b>Popular Myths #1: Open Source is Men in Sandals</b><br />
To describe what open source really is, we first need to describe what it isn't. Popular myth number one is that
the open source software movement is run by rabid geeks with long beards and sandals. Well, with the exception
of one of our staff, I can say that this is wholly untrue.<br />
<br />
As a category and a market trend, open source software is backed by multi-national anti-beardos such as IBM
and HP. It has won respectability through successful commercial firms like Red Hat and MySQL, and it's now in
use in the majority of the Fortune 500 companies. Open source is not a reaction against Microsoft's platform
domination, nor is it the work of idle teenagers. It's not related to hackers or crackers in the sense of fraud or
espionage. It is, however, all to do with finding smart answers to common computing problems - and very often
this requires the brainpower of some of the people mentioned above.<br />
<br />
Open source software is a hot house of development. It generates new answers to difficult tasks faster than any
traditional software firm could manage. The <a href="../a1/VoIP-Network-platform.asp">quality</a> of its code is unsurpassed, and its focus on market dynamics
is unique. It does all of these things because of the way it's organised. Firstly, a problem (or a market
opportunity) is spotted, then a piece of software is developed to address it. Secondly, this software is released to
the outside world in a naked form (accessible source code) and subjected to peer review, then it is tweaked and
improved. Thirdly, it's used rapidly in production environments by people who are empowered to offer
feedback into the development effort, then it is further enhanced to meet market demands. In this way an open
source product can evolve at the speed of light to address complex computing requirements. Note - this is not
driven by a licensing model, or purely by geeks in high places.<br />
<br />
I call particular attention to the market dynamic that propels open source solutions forward. The philosophy of
'release, review, feedback and enhance' is the antithesis of commercial software's tendency to 'embrace and
extend' across solution categories. The former is a model for introducing rapid product enhancements based on
market demands; the latter is a model for squeezing out competitors and gaining market share - without regard
for the customer. This then is our first distinction in value: the point of open source software is not who makes it
but how it's made.<br />
<br />
<b>Popular Myths #2: Open Source is all About Price Savings!</b><br />
My hope is that this first point isn't new to you. Many of these discussions have been played out elsewhere -
most notably in Eric Reymond's seminal book on the subject of open source, "The Cathedral and the Bazaar"3.
It's an important point to reiterate, however, in the context of price..because eight years on from our first open
source system, whilst the <a href="../a1/VoIP-Network-platform.asp">quality</a> of our product is undisputed (happy customers include Warner Music, the
NHS, Oxford University and the Australian Federal Government), we often find that folks still have a tough time
getting their head around our pricing.<br />
<br />
How much people expect to pay for an open source system tends to be determined by their notion of how 'free'
the stuff is. Open source is free in the sense of being 'freely available,' 'freely accessible' and 'free to modify,
adapt and improve.' It's not free in the sense of 'free beer,' as free software pioneer, Richard Stallman so neatly
puts it.<br />
<br />
Since open source software is licence free, one can choose to view it as a simple 'giveaway' product. However, as
with any enterprise level software solution (paid-for or otherwise), price-fixation can tend to muddy the waters
when it comes to doing something useful with it. Every enterprise software solution carries a price. A
sophisticated business application will require custom development, design, implementation, staff training and
support. And so, whilst open source is obviously 'free' in spirit, its value is not solely determined by price at the
surface level. It has enormous financial value, but this lies elsewhere and involves a subtle shift in notions of
ownership.<br />
<br />

</p>

<p class="virtualpage3">
<b>Bloatware vs Customisation and Operational Excellence</b><br />
At this point we should return to the frustration of Seth Godin and his feature-rich washing machine. We can
imagine the scenario here: he bought a new machine because his old one was outmoded, but the value in the
new product lies in washing his smalls whiter, not in the 125 functions that have been tacked on since the last
model.<br />
<br />
Godin's outcry is one of frustrated ownership - because it rankles when you buy something in spite of the way it
is produced and marketed. Godin calls this the 'arms race' approach whereby vendors enter into a feature war to
compete for your wallet. In software this is a familiar scenario: think of Microsoft Word and the number of
features you really use on a daily basis. Not many, right? But the next upgrade cycle always extends the number
of features at your disposal. Much of this is plain old 'bloatware' - unnecessary padding such as Word's
paperclip 'helper.' The question is, we buy the stuff, but where is the real value in ownership, and can we really
justify the price tag?<br />
<br />
This is one fundamental question that open source software addresses directly, and in doing so it's proving to be
a real threat to the dominance of commercial software - for example Open Office vs Microsoft Office, Linux vs
Windows, etc.<br />
<br />
What makes open source so effective is its development model. Commercial software vendors take an 'ivory
tower' approach whereby they set the benchmarks for feature sets and release schedules. In essence, they second
guess the customer requirement then build it in to the next version of the product. The results are sometimes
successful (iTunes/iPod), but more often not (Seth's washing machine). The open source model is different. As
described, the development model is totally in tune with the market - software is released, debugged, tweaked
and enhanced by a community of interested users, and roadmaps are rapid since they adapt to demand. And all
because the source code is open and available - everyone gets the chance to see it, comment on it and improve it.<br />
<br />
What this leads to is the production of broad brush software solutions that are highly customisable - or as Moore
states "technologies that are required for your business but not core to your differentiation." A common business
problem exists and so a team (or a company like Squiz) develops a software solution with the aim of addressing
as many requirements as possible. This usually means that the product is good since it's focused on market
specifics, and when it's open sourced it allows users to take it and customise it to their needs. (Note - commercial
software firms won't let you do this - only they get to tweak the code!). Further, the more useful generic
developments are made available to the rest of the market under the terms of the open source licence. It's a
virtuous circle. The software is free and it can be customised to deliver operational excellence. See the difference?
This form of ownership means so much more than a paper licence and the thinly veiled upgrade threats of
commercial bloatware.<br />
<br />
<b>Value Re-Engineering: It's cheap Jim, but not as you know it!</b><br />
So, the value of open source lies partly in its price tag and partly in the way it's developed. But this new way of
acquiring and owning software is merely the enabler for a bigger value. Essentially, open source creates a
situation where, once you've accepted that there's no over-riding competitive advantage in commercially licensed
products, you can re-engineer your spend to buy a better software solution.<br />
<br />
Nothing needs to change in terms of amount, but everything can change in terms of value. We describe this
switch in focus as a 'eureka' moment - it's that point in time when you realise you can really pull a rabbit out of
the hat just by moving a �� number one column to the right. (Some clients also compare it to quitting smoking:
"You mean I won't feel any worse AND I get to spend my cash on a holiday instead!?")<br />
<br />
With our open source CMS, MySource Matrix, the net effect I'm describing is the opportunity to procure an
enterprise-level software solution that ticks all the right feature boxes (and more), and, since it carries no license
fee, gives you the benefit of freeing up a whole bunch of cash to spend on tweaks that address your bigger
business requirements. Eureka indeed!


</p>

<p class="virtualpage3">
<b>A Word of Warning: Homegrown Homelies</b><br />
Now, one way of redistributing this cash saving is to plough it into an internal open source development effort.
Whilst this works in theory - use the saving in license fees to customise an open source product - we usually find
that it fails. There's a simple reason for this: when a company such as a publishing firm decides to develop their
own software they have to create a new in-house competence: they become a publisher and a software developer.
For whatever reason this usually means that performance in both fields deteriorates. Whilst it's possible to pull it
off, it places a strain on the development efforts of one or more people - and looking ahead, the ability to grow the
product through feature enhancements and customisations becomes a challenge. Experience tells us that it's
better to focus on your knitting....which is where Squiz fits in.<br />
<br />
<b>An Introduction to 'Serviced Source' Software</b><br />
Squiz's business model is to develop a CMS - MySource Matrix, to distribute it under an open source license and
then to provide services around the product to help people to customise it to their specific business needs. This
means that our clients:<br />
<br />
a) .do not have to become software development firms themselves (they leave that bit to us)<br />
b) .benefit from the real value of open source - the ability to spend money on more important
development efforts<br />
c) .always know that they can pick up the phone and talk to someone if something goes wrong or if they
need help<br />
<br />
We call this model of software development 'serviced source.'<br />
<br />
Basically, our business model exists because we're able to exploit the real value of open source software to the
benefit of our clients. For us this means we can develop an industry-leading CMS product and earn respect and
money through implementing it properly. For our clients this means they get a software solution that's totally
aligned with their individual needs.<br />
<br />
<b>Conclusion</b><br />
So, to summarise, the true value of open source software lies not just in spending less, but in its ability to let you
to do smarter things with your cash.<br />
<br />
<b>Author:</b> Roger Warner, Director, Squiz
<br />
<br />
<b>About Squiz</b><br />
Squiz is one of the world's leading open source software development companies.We give you control in a
content-driven world.Our open source CMS, MySource Matrix, helps leading organisations such as the
UK's NHS and the Australian Federal Government to manage their content more efficiently and costeffectively.
It also helps top brands such as Future Publishing and Warner Music to sell more content and
products to more customers via the web.<br />
<br />
Squiz is a privately owned company, founded in Sydney Australia in 1998. Squiz has an international
<a href="../a1/VoIP-Network-platform.asp">network</a> of offices in Sydney, Melbourne, Canberra, Hobart and London that provide a local service to
hundreds of clients across a broad spectrum of industries. Our MySource Matrix open source CMS is
internationally recognized as best in class (Gartner).<br />
<br />
For further information about Squiz and/or MySource Matrix, contact Roger Warner on info@squiz.co.uk.
</p>
</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/The_Value_of_Open_Source_Software.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/3com-solutions-strategies.as"></a>  <a href="../doc/The_Value_of_Open_Source_Software.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
