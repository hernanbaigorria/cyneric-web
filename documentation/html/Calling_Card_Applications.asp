<%@  language="VBSCRIPT" codepage="1252" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>VoIP Billing Documentation</title>
    <link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
    <script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
    <script type="text/javascript" src="virtualpaginate.js">

        /***********************************************
        * Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

    </script>
    <script type="text/javascript" src="../simpletreemenu.js">

        /***********************************************
        * Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

    </script>
    <script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
    <script>
        function detenerError() {
            return true
        }
        window.onerror = detenerError
    </script>
    <link rel="stylesheet" type="text/css" href="../simpletree.css" />
    <style type="text/css">
        /*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/
        
        .virtualpage, .virtualpage2, .virtualpage3
        {
            /*hide the broken up pieces of contents until script is called. Remove if desired*/
            display: none;
        }
        
        .paginationstyle
        {
            /*Style for demo pagination divs*/
            width: 530px;
            padding: 16px 10px 4px 0;
            background-image: url(images/fondo-pag.gif);
            height: 32px;
            background-repeat: no-repeat;
            text-align: right;
            clear: right;
            position: relative;
            background-position: right;
            margin-top: 10px;
            margin-bottom: 5px;
            margin-left: 0;
        }
        .paginationstyle select
        {
            /*Style for demo pagination divs' select menu*/
            border: 1px solid navy;
            margin: 0 5px;
            color: #000000;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
        }
        
        .paginationstyle a
        {
            /*Pagination links style*/
            padding: 0 5px;
            text-decoration: none;
            border: 1px solid black;
            color: navy;
            background-color: white;
        }
        
        .paginationstyle a:hover, .paginationstyle a.selected
        {
            color: #000;
            background-color: #FEE496;
        }
        
        .paginationstyle a.imglinks
        {
            /*Pagination Image links style (class="imglinks") */
            border: 0;
            padding: 0;
        }
        
        .paginationstyle a.imglinks img
        {
            vertical-align: bottom;
            border: 0;
        }
        
        .paginationstyle a.imglinks a:hover
        {
            background: none;
        }
        
        .paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected
        {
            /*Pagination div "flatview" links style*/
            color: #000;
            background-color: yellow;
        }
    </style>
</head>
<body onselectstart="return false">
    <table width="900" border="0" align="left" cellpadding="0" cellspacing="0" id="tope">
        <tr>
            <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif">
                <h1>
                    Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
            </td>
        </tr>
    </table>
    <table width="900" border="0" align="left" cellpadding="0" cellspacing="0" class="ventral"
        id="menuhome">
        <tr>
            <td width="221" align="left" valign="top">

                <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
                    <tr>
                        <td width="726" scope="col">
                            <!-- Pagination DIV for Demo 3 -->
                            <!-- Initialize Demo 3 -->
                            <table width="880" border="0" cellspacing="0">
                                <tr>
                                    <td width="263" valign="top" scope="col">
                                        <table width="201" border="0" align="left" cellspacing="0">
                                            <tr>
                                                <td width="199" scope="col">
                                                    <h1>
                                                        <span style="margin-bottom: 12px">Whitepapers Library</span></h1>
                                                    <div style="visibility: hidden; display: none">
                                                        <a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Expand All</a> |
                                                        <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Contact All</a></div>
                                                    <ul id="Ul1" class="treeview">
                                                        <li><a href="../asterisk-osp.asp">Asterisk OSP</a></li>
                                                        <li>VoIP Telephony
                                                            <ul>
                                                                <li><a href="../3comsolutions.asp">Strategies for successful IP </a></li>
                                                                <li><a href="../IP_telephony_deployments.asp">The Five Considerations </a></li>
                                                                <li><a href="../increasing_business_value_with_voip.asp">Increasing business </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="../VoIP_addressing_QoS.asp">VoIP networking</a></li>
                                                        <li><a href="../asterisk.asp">Asterisk Open Source</a></li>
                                                        <li><a href="../quintum.asp">Life on the Edge</a></li>
                                                        <li><a href="../Open_Source_Telephony.asp">Open Source Telephony</a></li>
                                                        <li><a href="../Strategies_for_Migrating_Corporate.asp">Risks and Rewards</a></li>
                                                        <li><a href="../Asterisk_billing.asp">Asterisk Billing</a>
                                                            <ul>
                                                                <li><a href="../html/Pre-paid-ANI-recognition-PIN.doc.asp">Pre paid ANI </a></li>
                                                                <li><a href="../html/Post_paid_ANI_customers_with_credit_limit.asp">Post paid ANI</a></li>
                                                                <li><a href="../html/Calling_Card_Applications.asp">Calling Card </a></li>
                                                                <li><a href="../html/Call_Shop_Applications.asp">Call Shop </a></li>
                                                                <li><a href="../html/Web_Callback.asp">Web Callback</a></li>
                                                                <li><a href="../html/SMS_Callback.asp">SMS Callback </a></li>
                                                                <li><a href="../html/DID_forwarding_Follow_me.asp">DID forwarding </a></li>
                                                                <li><a href="../html/Web_Voicemail.asp">Web Voicemail </a></li>
                                                                <li><a href="../html/Automatic_Routing_LCR.asp">Automatic Routing </a></li>
                                                                <li><a href="../html/Automatic_Telephone_payment.asp">Automatic Telephone </a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="../A_Proactive_Approach_to_VoIP_Security.asp">Approach to VoIP Security</a></li>
                                                        <li><a href="../Secure_Multi-lateral_Peering.asp">Secure, Multi-lateral Peering</a></li>
                                                        <li><a href="../Security_Wiretapping_and_the_Internet.asp">Security, Wiretapping</a></li>
                                                        <li><a href="../Cisco_2007_AnnualSecurity_Report.asp">Cisco, Anual Security Report</a></li>
                                                        <li><a href="../Compare_Top_PBX_Vendors.asp">Compare Top PBX Vendors</a></li>
                                                        <li><a href="../hosted-pbx-comparison.asp">Hosted PBX Comparison</a></li>
                                                        <li>SIP
                                                            <ul>
                                                                <li><a href="../sip.asp">What�s SIP got to do with it?</a></li>
                                                                <li><a href="SIP_Security_and_the_IMS_Core.asp">SIP Security</a></li>
                                                                <li><a href="SIP_Security_and_Session_Controllers.asp">SIP Security and Session </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="SER_GettingStarted.asp">SER Getting Started</a>
                                                            <ul>
                                                                <li><a href="html/SER_GettingStarted.asp">Part 1</a></li>
                                                                <li><a href="html/SER_GettingStarted-part2.asp">Part 2</a></li>
                                                                <li><a href="html/SER_GettingStarted-part3.asp">Part 3</a></li>
                                                                <li><a href="html/SER_GettingStarted-part4.asp">Part 4</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="Telecom_Security_Threats.asp">Telecom Security Threats</a></li>
                                                        <li><a href="VoIP_Security.asp">VoIP Security</a></li>
                                                        <li><a href="What_is_a_Call_Center.asp">What Is a Call Center?</a></li>
                                                        <li><a href="VoIP_Bandwidth_Calculation.asp">VoIP Bandwidth Calculation</a></li>
                                                        <li><a href="The_Value_of_Open_Source_Software.asp">The Value of Open Source</a></li>
                                                        <li><a href="Understand_security_in_VoIP_networks.asp">Enhancing VoIP</a></li>
                                                    </ul>
                                                    <h4>
                                                        <script type="text/javascript">

                                                            //ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

                                                            ddtreemenu.createTree("treemenu1", true)
                                                            ddtreemenu.createTree("treemenu2", false)

                                                        </script>
                                                    </h4>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="472" valign="top" scope="col">
                                        <h1>
                                            Calling Card Applications</h1>
                                        
                                        <div style="width: 520px; border: 1px dashed gray; padding: 10px;">
                                            <p class="virtualpage3">
                                                Based on PIN Authentication, you can create calling card lots and distribute them
                                                through resellers or agents You can have different configurations for different
                                                markets, as you may want to reach specific market opportunity.
                                                <br />
                                                <br />
                                                Use:<br />
                                                - You define calling card Lot: rate table, first use fee, maintenance charges, rate
                                                tables, minute rounding, expiration, digits for the pin, quantity of cards, etc<br />
                                                - You export to excel, and send to printer company<br />
                                                - You activate the series of calling card you have on the street<br />
                                                - Customer calls the access number, and enters the pin<br />
                                                - <a href="../../a1/VoIP-Billing-Platform.asp">Billing</a> authenticate the pin,
                                                and asks for destination number<br />
                                                - Customer enters destination number<br />
                                                - <a href="../../a1/VoIP-Billing-Platform.asp">Billing</a> check credit limit, if
                                                destination is authorized, and informs <a href="http://www.asterisk-billing.net"
                                                    target="_blank">Asterisk </a>the call is accepted/rejected<br />
                                                - <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>connects
                                                the call with the carrier<br />
                                                - Customer hung up, and <a href="http://www.asterisk-billing.net" target="_blank">Asterisk
                                                </a>will send accounting to <a href="../../a1/VoIP-Billing-Platform.asp">billing</a><br />
                                                - <a href="../../a1/VoIP-Billing-Platform.asp">Billing</a> will rate the call, and
                                                discount the credit on that card<br />
                                                - After the pin is used, you can recharge the card or just discard it<br />
                                                - You can generate usage reports, top destinations, profit/loss reports, etc<br />
                                                <br />
                                                <img src="images/callshop.jpg" /><br />
                                                <br />
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <script type="text/javascript">
                                var whatsnew = new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
                                whatsnew.buildpagination("listingpaginate")
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td scope="col">
                        </td>
                    </tr>
                </table>
                <table width="776" border="0" cellspacing="0">
                    <tr>
                        <td height="132" scope="col">
                            <table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
                                <tr>
                                    <td width="257" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif);
                                        background-repeat: no-repeat; background-position: bottom">
                                        <h1>
                                            &nbsp;Download Information</h1>
                                        <p align="right">
                                            <a href="../pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf" target="_blank">
                                            </a><a href="../pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a>
                                            <a href="../pdf/Calling-Card-application.pdf" target="_blank">
                                                <img src="../imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a
                                                    href="../doc/Calling-Card-application.doc" target="_blank"><img src="../imagenes/ico-d-word.gif"
                                                        width="48" height="57" hspace="0" vspace="9" border="0" /></a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
