<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
Secure, Multi-lateral Peering
with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>TM V1.2
            </h1> 
<div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div>

<div style="width:520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">

<b>Multi-lateral Peering: Why?</b><br />
Are you .<br />
<br />
. A carrier or a large enterprise running multiple <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> servers to manage VoIP  <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>?<br />

. Searching for more flexible, simple ways to manage your VoIP  <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>?<br />

. Reaching a scalability limit for maintaining IP access lists for authentication?<br />

. Eager to benefit from the simpler, more efficient <a href="../../a1/VoIP-Network-platform.asp">network</a> architecture of direct peer-topeer
SIP applications?<br />

. Struggling with the cost and complexity of maintaining growing number of VoIP
interconnect <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> agreements?<br />
<br />
If your answer is "Yes" to any of the above questions, Multi-Lateral Peering is the solution to
your problems. Multi-Lateral Peering - is a highly scalable and secure architecture which
combines the power of the Internet with public key infrastructure (PKI) technology. The result is
secure peer to peer VoIP networking which simplifies operations, saves bandwidth use and
reduces capital costs. Multi-lateral peering increases profits by eliminating costs and creating
new revenue opportunities.<br />
<br />
<b>Current Deployments</b><br />
Most VoIP operators running multiple SIP gateways or SIP proxies have implemented either a
distributed or a centralized architecture. This white paper compares these two traditional
architectural models with Multi-Lateral Peering.<br />
<br />
<b>Distributed Architecture</b><br />
The benefits of a distributed peer to peer
architecture are <a href="../../a1/VoIP-Network-platform.asp">network</a> efficiency,
<a href="../a1/VoIP-Network-platform.asp">quality of service</a> and fault tolerance.
There are no <a href="../../a1/VoIP-Network-platform.asp">network</a> bottlenecks and no
single points of failure. Figure 1
illustrates a distributed architecture:
three <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> servers used as SIP to
PSTN gateways are directly peered to
each other. This architecture is bilateral
peering. Each of the three gateways
independently maintains routing plans,
IP Access Control Lists (ACL), and Call
Detail Records (CDR). Managing this
information in a small network is a
manageable task. However, as the
number of endpoints increases,
configuration and maintenance of
multiple routing tables, Access Control
Lists, and processing of CDRs from multiple peers becomes increasingly difficult. In fact,
operational complexity increases by the square of the number or peers [n*(n-1)/2], making large
scale peer to peer networks virtually impossible.<br />
<br />
<img src="images/Distributed_Architecture.jpg" /><br />
<br />
<b>Centralized Architecture</b><br />

The benefits of a centralized architecture are operational simplicity and control; all call control is
managed by a central softswitch or session controller.<br />
<br />

Figure 2 illustrates the same <a href="../../a1/VoIP-Network-platform.asp">network</a>
shown in Figure 1, but with a
centralized architecture that is managed
by a central <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> server acting as a
softswitch. Each device is peered to the
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> softswitch, which proxies all
calls between peers. Centralized systems
simplify <a href="../../a1/VoIP-Network-platform.asp">network</a> operations, CDR
collection, and eliminate maintenance of
routing tables and Access Control Lists
configured for each peer. One major
weakness, however, is the architecture is
no longer a peer to peer <a href="../../a1/VoIP-Network-platform.asp">network</a>. The
benefits of a peer to peer SIP <a href="../../a1/VoIP-Network-platform.asp">network</a> -
<a href="../../a1/VoIP-Network-platform.asp">network</a> efficiency, <a href="../a1/VoIP-Network-platform.asp">quality of service</a>
and fault tolerance are lost with a
centralized architecture<br />

. The central softswitch becomes
single point of failure.<br />

. Routing all calls through a central softswitch requires additional bandwidth and may
result in lower <a href="../a1/VoIP-Network-platform.asp">quality of service.</a><br />

. Deployment of a central softswitch is expensive. Every call requires a dedicated voice
port. Scaling up to handle large call volumes is a challenge.<br />
<br />

The technical solution to these problems is a new concept called Multi-Lateral Peering.
<br /><br />

<img src="images/Centralized_Architecture.jpg" /><br />
<br />

</p>

<p class="virtualpage3">
<b>Multi-lateral Peering: What ?</b><br />
both distributed and centralized architectures to create a flexible and highly scalable solution for
securely managing VoIP  <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. The idea behind multi-lateral peering is to replace the many
bilateral peering relationships in a distributed architecture with a single multi-lateral peering
relationship for all VoIP peers. The device which enables multi-lateral peering is a peering
server - a single point of administrative contact for direct peering among VoIP <a href="../../a1/VoIP-Network-platform.asp">network</a>s. The
peering server is similar to a softswitch in the centralized model, but without the creation of a
<a href="../../a1/VoIP-Network-platform.asp">network</a> signaling bottleneck. The peering server performs authentication, routing, inter-domain
access control, and call accounting for all the endpoints within its <a href="../../a1/VoIP-Network-platform.asp">network</a> of peered domains,
but it does so without interfering in the call signaling process. As a result, the peering server
facilitates pure peer-to-peer communication without any <a href="../../a1/VoIP-Network-platform.asp">network</a> bottlenecks or bandwidth
constraints. To better understand the simplicity of a multi-lateral peering architecture, refer the
example <a href="../../a1/VoIP-Network-platform.asp">network</a> of three <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> servers in Figure 3. In the middle is a peering server which is
a certificate authority. As a trusted third party to all VoIP peers, the peering server can provide
routing information and secure inter-peer access permission to peers that want to interconnect.<br />
<br />
The multi-lateral peering call scenario
in Figure 3 is described below.<br />
<br />

<img src="images/Asterisk_Interconnections.jpg" />
<br /><br />


1. The source peer requests a route
from the peering server which returns
the destination address and a digitally
signed inter-peer authorization token.<br />
2. The source device uses the routing
information to set up the call, peer to
peer, to the destination. The source
peer includes the authorization token
in the SIP INVITE to the destination.
The destination validates the token
with the public key of the peering
server. If the token is valid, the
destination accepts the call.<br />
3. At the end of the call, both the
source and destination peers send Call
Detail Records to the peering server.
These peering messages, also known as Usage Indication messages, are not shown in Figure 3.<br />
Multi-lateral peering server technology is based on an open and global standard defined by the
European Telecommunications Standards Institute or ETSI (www.etsi.org). ESTI is well known
in the telecom world as the standards body for GSM, the global technology standard for wireless
phones. The official name for the peering server specification is ETSI TS 101 321 or OSP
protocol for inter-domain authorization, usage reporting and pricing indication.<br />
<br />
<b>Benefits of Multi-Lateral Peering</b><br />
Multi-lateral peering provides the benefits of both the distributed and centralized architectures
without incurring the limitations inherent with either architectures.<br />
<br />
<b>Routing Flexibility</b><br />
Multi-lateral peering using OSP offers a broad range of functionality - from a simple, light route
lookup to a feature rich peering messages which convey routing information, trunk group,
destination protocol, allowed usage, bandwidth, type of service and interconnect price.<br />
<br />
<b>Fraud Control</b><br />
The peering server is also a certificate authority that establishes identities, verifies authorization,
and blocks any unauthorized users. The route server acts as the trusted third party without being
directly involved in the communications path between the originating and the terminating
<a href="../../a1/VoIP-Network-platform.asp">networks</a>. It uses PKI (public key infrastructure) based digitally signed, cryptographic
authorization tokens to enforce secure access control and eliminates the need for IP access lists.<br />
<br />
<b>Centralized Accounting</b><br />
The peering server supports centralized <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> by collecting the Call Detail Records from both
the source and the destination at the end of the call. The CDRs written in an XML format defined by the OSP standard and can easily be reformatted to the specifications of any <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> system. In
addition to the ease in operations and maintenance, central collection of CDRs from both parties
ensures non-repudiation and eliminates settlement disputes.<br />
<br />
<b>Freedom to Scale</b><br />
The stateless design of multi-lateral peering gives the VoIP operator a highly scalable, reliable,
and easy to maintain <a href="../../a1/VoIP-Network-platform.asp">network</a> architecture. By centralizing the routing, access control, and<a href="../../a1/VoIP-Billing-Platform.asp"> billing</a> functionalities, VoIP service providers can simplify the process of managing large scale
<a href="../../a1/VoIP-Network-platform.asp">networks</a>. For example, a simple configuration change which enrolls an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> VoIP gateway
with a peering server provides the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> gateway with secure access to exchange VoIP calls
with all other <a href="../../a1/VoIP-Network-platform.asp">network</a>s in the multi-lateral peering domain.<br />
<br />
<b>Superior Performance</b><br />
The architecture is designed as a purely distributed, peer to peer model, which enables
developers to build multi-purpose applications without the constraints of call control
functionality. Unlike the design of centralized architectures, this design keeps the route server
out of the call signaling process and thereby prevents any bottlenecks or bandwidth related
<a href="../a1/VoIP-Network-platform.asp">quality of Service</a> issues. Also, since the peering server is independent from the call signaling,
the technology can work with IP communication protocol such as SIP, H.323, IAX or any other
IP application protocol.<br />
<br />
<b>Cost Effective</b><br />
Multi-laterally peer<a href="../../a1/VoIP-Network-platform.asp">ed networks</a> are cheaper to install, operate, and maintain. By eliminating the
need for a central call signaling platform, significant capital costs are eliminated. The IP
<a href="../../a1/VoIP-Network-platform.asp">network</a> becomes the switch.<br />
<br />

</p>

<p class="virtualpage3">
<b>New Opportunities</b><br /><br />

<b><a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a> <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> Exchange</b><br />
Multi-lateral peering and settlement
provides a new business opportunity for
VoIP service providers to profit from
<a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a> traffic exchange. No single
VoIP service provider can provide global
A to Z termination services to the Public
Switched Telephone <a href="../../a1/VoIP-Network-platform.asp">Network</a> (PSTN).
Low cost, global termination requires
negotiating and managing interconnect
agreements with multiple carriers that
can provide local VoIP termination to
the PSTN around the world.<br />
<br />
<img src="images/Wholesale_Traffic_Exchange.jpg" /><br />
<br />

Multi-lateral peering and settlement
provides a new business opportunity for
VoIP service providers to profit from
<a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a> <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> exchange. No single
VoIP service provider can provide global
A to Z termination services to the Public
Switched Telephone <a href="../../a1/VoIP-Network-platform.asp">Network</a> (PSTN).
Low cost, global termination requires
negotiating and managing interconnect
agreements with multiple carriers that
can provide local VoIP termination to
the PSTN around the world.<br />
<br />
exchange using multi-lateral peering is a new profit opportunity for retail VoIP service providers.
Figure 5 is identical to Figure 3, except the peering server operator is acting as a clearinghouse
for peering <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> - providing <a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a>clearing and settlement services for <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> exchange
among peers. Each peer benefits from outsourcing the back-office intensive operation of
managing bilateral interconnect<a href="../../a1/VoIP-Billing-Platform.asp"> billing </a>agreement. The clearinghouse generates new profits by
charging a small processing fee for providing peering and settlement services.<br />
<br />
Enterprise VoIP VPN<br />
Secure multi-lateral peering is well
suited for enterprises using the Internet
as a global wide area <a href="../../a1/VoIP-Network-platform.asp">network</a> (WAN)
among branch offices. A central peering
server can be deployed to securely
control and account for inter-office and
offnet VoIP calls - creating virtual
private VoIP <a href="../../a1/VoIP-Network-platform.asp">network</a>. In Figure 4, each
branch office manages its own <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
PBX for intra-office or local call. Calls
between the PBXs across the VoIP VPN
or to a terminating VoIP carrier are
managed by centrally operated peering
server<br />
<br />
<img src="images/Enterprise_VoIP_VPN.jpg" /><br />
<br />

By using a secure peering server, the
complexity and additional bandwidth required for routing calls through a central softswitch or
session border controller are eliminated. The result is a simpler, peer to peer, VoIP VPN with
lower operating costs and better <a href="../a1/VoIP-Network-platform.asp">quality of service</a>.<br />
<br />
<b>Multi-lateral Peering with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>: How?</b><br />
The following sections describe how to build Open Source <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> with the OSP Toolkit. These
instructions and have been developed and tested using the following software:<br />
<br />
Fedora Core 3 with Linux kernel version 2.6.9
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> version cvs checkout -D "Oct 6 17:00:00 GMT 2005" zaptel libpri <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
OSP Toolkit version 3.3.4<br />
<br />
<b>Step 1: Get <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a></b><br />
Obtain <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> V1.2<br />
<br />
Step 1-1: Things you will need to install <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>T and/or Zaptel:<br />
&nbsp;&nbsp;&nbsp;&nbsp;ncurses, and associated -devel<br />
&nbsp;&nbsp;&nbsp;&nbsp;openssl, and associated -devel<br />
&nbsp;&nbsp;&nbsp;&nbsp;bison<br />
<br />
Step 1-2: To be sure that you have the required packages, on Fedora, you can do the following:<br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">rpm -q readline readline-devel openssl openssl-devel bison</span><br />
<br />
Step 1-3: To check out source code from Digium's CVS repository:<br />

Login to your Linux machine as root,<br /><br />

<span class="ventral">cd/usr/src</span><br />
<span class="ventral">export CVSROOT=:pserver:anoncvs@cvs.digium.com:/usr/cvsroot</span><br />
<span class="ventral">cvs login - the password is anocvs</span><br />
<span class="ventral">cvs checkout -r v1-2 <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> zaptel libp</span>ri<br />
<br />
<b>Step 2: Get the OSP Toolkit</b><br />
OSP Toolkit version 3.3.4 is available at www.transnexus.com or www.sipfoundry.org as a
gzipped file. Follow the following instructions to install the OSP client stack.<br />
<br />
Step 2-1: Download the OSP Toolkit to the Linux machine, in /usr/scr directory.<br />
<br />
Step 2-2: Unzip and untar the OSP Toolkit in the /usr/src directory. The OSP Toolkit should
create a directory called <span class="ventral">TK-#_#_#-YYYYMMDD</span> where<span class="ventral"> #_#_#</span> indicates the version and <span class="ventral">YYYYMMDD</span> indicates the date.<br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">gunzip -c OSPToolkit-3.3.4.tar.gz | tar xvf -</span><br />
<br />
tep 2-3: Go to src directory
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">cd/usr/scr/TK-#_#_#-YYYYMMDD/src</span><br />
<br />
Step 2-4: Edit Makefile<br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">vi Makefile</span><br />
<br />
Step 2-5: Look for a variable called INSTALL_PATH=<br />
&nbsp;&nbsp;&nbsp;&nbsp;Edit it to be <span class="ventral">INSTALL_PATH=/usr/local</span><br />
<br />
Step 2-6: Compile OSP Toolkit. Check the compilation logs to see that there were no errors
during compilation.<br />
&nbsp;&nbsp;&nbsp;&nbsp;make clean; make install <br />
<br />

Step 2-7: Compile enroll utility<br />

&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">cd/usr/src/TK-#_#_#-YYYYMMDD/enroll</span><br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">make clean; make linux</span><br />
<br />
Step 2-8: Go to bin directory. You should see the enroll utility and the script enroll.sh<br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">cd/usr/src/ TK-#_#_#-YYYYMMDD/bin</span><br /><br />

Step 2-9: If the <span class="ventral">/usr/src/TK-#_#_#-YYYYMMDD/bin</span> is not the the <span class="ventral">PATH</span> variable, edit <span class="ventral">enroll.sh</span><br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">vi enroll.sh</span><br />
&nbsp;&nbsp;&nbsp;&nbsp; Edit the 3 lines that begin with <span class="ventral">"enroll"</span> to read <span class="ventral">"./enroll"</span>.<br />
<br />
</p>
<p class="virtualpage3">
<b>Step 3: Compile <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> with the OSP Toolkit</b><br />
If Zaptel / Digium cards will be used with your <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> installation, install the card in your
machine before building <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. Instructions for installing a TDM400P card are found in the
<span class="ventral">/usr/src/zaptel</span> directory.<br />
<br />
Step 3-1: Navigate to the Zaptel directory.
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">cd /usr/src/zaptel</span><br /><br />

Step 3-2: Compile Zaptel<br />

&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">make clean; make linux26; make install</span><br /><br />

Step 3-3: Execute the depmod command which enables the modprobe command to load Zaptel
drivers.<br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">/sbin/depmod</span><br /><br />

Step 3-4: Compile libpri<br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">cd /usr/src/libpri</span><br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">make clean; make instal</span>l<br />
<br />
Step 3-5: Compile <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a><br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">cd /usr/src/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a></span><br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">make clean; make install</span><br />
<br />
If compilation is successful, you will see:<br /><br />
<span class="ventral"><a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> Installation Complete</span><br />
<br />
<span class="ventral">YOU MUST READ THE SECURITY DOCUMENT</span><br />
<br />
<span class="ventral"><a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> has successfully been installed</span>.<br />
<span class="ventral">If you would like to install the sample configuration files (overwriting any existing config files), run:</span><br />
<br />
<span class="ventral">make samples</span> <br />
<span class="ventral">or</span><br />
<span class="ventral">You can go ahead and install the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a></span><br />
<span class="ventral">program documentation now or later run:</span><br />
<br />
<span class="ventral">make progdocs</span><br />
<br />
<span class="ventral">You can go ahead and install the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> program documentation now or later run:</span><br />
<br />
<span class="ventral">make progdocs</span><br />
<br />
<span class="ventral">**Note** This requires that you have doxygen installed on your local system</span><br />
<br />

 Step 3-6: Install sample <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> configuration files. The sample files are a good reference or
template for building new customized configuration files. <br />
&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">make samples</span><br />
<br />
Your <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> platform is now enabled for secure, multi-lateral SIP peering.<br />
<br />
<b>Step 4: Set up an OSP Server</b><br />
If you are setting up a live/test <a href="../../a1/VoIP-Network-platform.asp">network</a>, contact support@transnexus.com for a free 90 day trial
of the TransNexus commercial OSP server.<br />
<br />
If you are experimenting, a test server is publicly available on the Internet. For access to the OSP
test server write to the OSP mailing list at www.sipfoundry.org
(https://list.sipfoundry.org/mailman/listinfo/osp) with the IP address (public or that of outermost
firewall) of your <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> box. Your <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> device, and other VoIP devices, will be added to
the test server for testing. Once your devices have been added to the OSP test server, you can
proceed to enroll your device with the OSP Server (Step 5). In addition, two open source OSP
servers - RAMS and OpenOSP - are available from www.sipfoundry.org.<br />
<br />
<b>Step 5: Enrollment</b><br />
The next step is to enable the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> gateway to talk to an OSP Server. This requires <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
to enroll securely with the OSP Server.<br />
<br />
Step 5-1: Go to <span class="ventral">/usr/src/TK-#_#_#-YYYYMMDD/bin</span> directory. You should see the utility:
<span class="ventral">enroll</span>, and a script: <span class="ventral">enroll.sh</span>.
&nbsp;&nbsp;&nbsp;&nbsp;cd /usr/src/TK-#_#_#-YYYYMMDD/bin/<br />
<br />
Step 5-2: Enroll with OSP server ospserver.domain.com<br />

&nbsp;&nbsp;&nbsp;&nbsp;<span class="ventral">./enroll.sh ospserver.domain.com or ipaddress</span><br />
<br />
It will ask you for a number of inputs, you can enter random data or you can just press enter. If
enrollment was successful, the last 2 lines of your log will say the following:<br />
<br />
<span class="ventral">The certificate request was successful.</span> <br />
<span class="ventral">Error Code returned from localcert command : 0</span><br />
<br />
You will see 4 files: cacert_0.pem, certreq.pem, localcert.pem, pkey.pem<br />
<br />
</p>
<p class="virtualpage3">
Step 5-3: Check <span class="ventral">localcert.pem</span> file. It will look something like this:<br />
<br />

-----BEGIN CERTIFICATE-----
MIIBejCCASQCEQDAUTw/U3QsPxvQcSDmYgVRMA0GCSqGSIb3DQEBBAUAMDsxJTAj
BgNVBAMTHG9zcHRlc3RzZXJ2ZXIudHJhbnNuZXh1cy5jb20xEjAQBgNVBAoTCU9T
UFNlcnZlcjAeFw0wNDA4MDQyMDU1MTFaFw0wNTA4MDUyMDU1MTFaMEUxCzAJBgNV
BAYTAkFVMRMwEQYDVQQIEwpTb21lLVN0YXRlMSEwHwYDVQQKExhJbnRlcm5ldCBX
aWRnaXRzIFB0eSBMdGQwXDANBgkqhkiG9w0BAQEFAANLADBIAkEAxxRq2vuG6Lx5
93R16CTsz6FXlGELY9Ob4yj12vSVWQn5e4catRf1zGmqmY3Y/as19E/wt3PEDTVN
tEAEoVFjqQIDAQABMA0GCSqGSIb3DQEBBAUAA0EA7ACCJVeysn8dCTxtDUYnpUbt
C4DYfhr31ml5yHhn280BZaAQFzKeYo19ahzCz/lHjLXfrqVuQljnEXafpgaMlw==
-----END CERTIFICATE-----<br /><br />

Step 5-4: Copy the 4 files to <span class="ventral">/var/lib/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>/keys/</span><br />
You now have an OSP enabled <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> which can use the services of an OSP server
(ospserver.domain.com) for secure inter-domain routing, access control and CDR collection.
For more information please refer to the OSP documentation on the www.sipfoundry.org/OSP
web site. Also, the OSP mailing list (https://list.sipfoundry.org/mailman/listinfo/osp) is a
resource for <a href="../../a1/VoIP-Network-platform.asp">technical support</a>.<br />
<br />
<b>Step 6 Configure <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> for OSP</b><br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> must be configured to use OSP for multi-lateral peering. This section explains what
parameters in the extenstion.conf, sip.conf and osp.conf files must be changed.<br />
<br />
<b>extensions.conf</b><br />
There are three applications in the extensions.conf file for OSP protocol. All of the three
applications take string parameters.<br />
<br />
<b>OSPLookup </b>(extension [| provider [| options]])<br />

Extension: called number<br />

Provider: context section in osp.conf, defaults "default"<br />

Options: j - jump to n+101 priority if the lookup was NOT successful.<br />
<br />
<b>OSPNext</b> (cause)<br />

Cause: the failure reason of the prior call<br />

Options: j - jump to n+101 priority if the lookup was NOT successful.<br /><br />

<b>OSPFinish</b> (cause)<br />

Cause: the failure reason of the last call<br />

Options: j - jump to n+101 priority if the final attempt was NOT successful.<br />
<br />
The typical dial plan for <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> with OSP support is as follows:<br />
<br />
[SIPProxy] ; context name
exten => _XXXX.,1,OSPLookup(${EXTEN}) ; route called number<br />
exten => _XXXX.,2,Dial(${OSPTECH}/${OSPDEST},20,tr) ; dial 1st destination<br />
exten => _XXXX.,3,OSPNext(${DIALSTATUS}) ; get 2nd destination<br />
exten => _XXXX.,4,Dial(${OSPTECH}/${OSPDEST},20,tr) ; dial 2nd destination<br />
exten => _XXXX.,5,OSPNext(${DIALSTATUS}) ; get 3rd destination<br />
exten => _XXXX.,6,Dial(${OSPTECH}/${OSPDEST},20,tr) ; dial 3rd destination<br />
exten => _XXXX.,102,Hangup ; hang up<br />
exten => _XXXX.,104,Hangup ; hang up<br />
exten => _XXXX.,106,Hangup ; hang up<br />
exten => h,1,OSPFinish(${DIALSTATUS}) ; finish the call<br /><br />
<br />
<b>sip.conf</b><br />
There are two parameters for OSP protocol.<br />
<br />
allowguest:
yes (default): allow all guest calls<br />
no: no guest calls allowed<br />
osp: check INVITE messages according to rules defined by ospauth<br />
<br />
ospauth:<br />
no (default): allow calls with other valid authentication<br />
gateway: allow calls with valid OSP token or, if no OSP token is present, allow calls with<br />
other valid authentication. Calls with an invalid OSP token will be blocked.<br />
proxy: allow calls with a valid OSP token, or without an OSP token. Calls with an<br />
invalid OSP token will be blocked.<br />
exclusive: only allow calls with a valid OSP token<br /><br />

The configuration for <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> as a Back to Back User Agent (B2BUA) with OSP support is as<br />
follows:<br />
<br />
[general]; default context<br />
context=SIPProxy; context in extensions.conf<br />
allowguest=osp; use OSP rules for inbound call checking<br />
ospauth=proxy; accept calls w/ valid OSP token, or w/o OSP token<br />
realm=transnexus.com; realm for digest authentication<br />
bindport=5060; UDP Port to bind to (SIP standard port is 5060)<br />
bindaddr=0.0.0.0; IP address to bind to (0.0.0.0 binds to all)<br />
srvlookup=yes; Enable DNS SRV lookups on outbound calls<br />
; Note: <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> only uses the first host<br />
; in SRV records<br />
; Disabling DNS SRV lookups disables the<br />
; ability to place SIP calls based on domain<br />
; names to some other SIP users on the Internet<br />


</p>
<p class="virtualpage3">
<b>osp.conf</b><br />
There are several options.<br />
<br />
<b>accelerate:</b><br />
&nbsp;&nbsp;&nbsp;&nbsp;no (default): hardware acceleration disabled<br />
&nbsp;&nbsp;&nbsp;&nbsp;yes: hardware acceleration enabled<br />
<br />
<b>tokenformat:</b>
&nbsp;&nbsp;&nbsp;&nbsp;0 (default): signed token<br />
&nbsp;&nbsp;&nbsp;&nbsp;1: unsigned token<br />
&nbsp;&nbsp;&nbsp;&nbsp;2: both signed and unsigned token<br />
<br />
<b>privatekey:</b> private key file<br />
<b>localcert:</b> local certificate file<br />
<b>cacert:</b> certificate authority key files<br />
<br />
<b>maxconnections:</b> max number of simultaneous connections to the provider<br />
<b>retrydelay:</b> extra delay between retries<br />
<b>retrylimit:</b> max number of retries before giving up<br />
<b>timeout: </b>timeout for response in milliseconds<br /><br />

<b>servicepoint: </b>OSP server address<br />
<b>source: </b>local IP address<br /><br />
The configuration for <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> as a Back to Back User Agent (B2BUA) with OSP support is as
follows:<br />
<br />
[general]; general configuration<br />
tokenformat=0 ; signed token only<br />
<br />
[default]; provider<br />
privatekey=pkey.pem; key files<br />
localcert=localcert.pem;<br />
cacert=cacert_0.pem;<br />
<br />
maxconnections=20; max connections<br />
retrydelay=0; delay between tries<br />
retrylimit=2; max retries<br />
timeout=500; time out<br />
servicepoint=http://osptestserver.transnexus.com:1080/osp<br />
<br />
source=[216.162.34.110]


</p>
</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="pdf/Secure_Multi-lateral_Peering_with_Asterisk.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/Secure_Multi-lateral_Peering.asp"></a> <a href="doc/Secure_Multi-lateral_Peering_with_Asterisk.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
