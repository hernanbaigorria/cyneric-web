<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="0" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
VoIP Addressing QoS Beyond the Provider Network<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" border="0" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
This White Paper explores the issue of voice
<a href="../a1/VoIP-Network-platform.asp">quality</a> in VoIP networking and current status
of QoS technologies and implementations in
today's IP <a href="../../a1/VoIP-Network-platform.asp">networks</a>. We discuss the QoS
mechanisms SmartNodeT employs to ensure
the best-possible voice <a href="../a1/VoIP-Network-platform.asp">quality</a> over the networkaccess
link.<br />

<b><br /><a href="#">Introduction</a></b> <span class="credits"></span><br>
<b>Today's Internet does far more</b> then email and file
transfers. Initially designed for non-real-time (NRT) data
applications, the Internet has matured far beyond these
tasks. In addition to web browsing, online imaging, and
chat rooms, we now expect the Internet to deliver such
real-time (RT) media as streaming music, video, and
Internet phone calls directly to our homes and offices.<br />
<br />
Presently, Voice-over-Internet Protocol (VoIP) is all the
buzz-but it's more than just talk. VoIP technology has
matured. Not only is it the latest hot new Internet application.
Today, VoIP has emerged as a reliable technology
that is commercially viable, competing (and winning)
against traditional phone services in business and
consumer-class markets.<br />
<br />
As a real-time application, VoIP-also known as packet
voice, packet <a href="../../a1/VoIP-Network-platform.asp">telephony</a>, or IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a>-places
increased demands on the evolving Internet. VoIP users
expect the Internet to deliver toll-<a href="../a1/VoIP-Network-platform.asp">quality</a> voice with the
same clarity as the traditional Public Switched Telephone
<a href="../a1/VoIP-Network-platform.asp">Network</a> (PSTN). To meet those expectations, the
Internet connection must be more than merely reliable, it
must be time-sensitive. Each and every voice packet must
be delivered without significant delay and with consistent
time intervals between packets.<br />
<br />
Advanced <a href="../a1/VoIP-Network-platform.asp">quality of Service</a> (QoS) technology is the key
to achieving voice <a href="../a1/VoIP-Network-platform.asp">quality</a> that measures up to today's
high standards. With SmartNode and Patton's advanced
QoS, toll-<a href="../a1/VoIP-Network-platform.asp">quality</a> voice on every call is the benchmark.<br />
<br />
<b><a href="../a1/VoIP-Network-platform.asp">quality of Service</a> (QoS)</b> QoS)-In a successful VoIP
deployment, the perceived voice <a href="../a1/VoIP-Network-platform.asp">quality</a> of VoIP calls
must satisfy user expectations. Specifically, voice <a href="../a1/VoIP-Network-platform.asp">quality</a>
in the VoIP system must compare favorably with that
of a traditional phone call. To ensure VoIP users hear
the best possible voice <a href="../a1/VoIP-Network-platform.asp">quality</a>, Patton's SmartNode
employs a unique combination of varied QoS mechanisms.
<a href="../a1/VoIP-Network-platform.asp">Network</a> administrators can tune SmartNode's
QoS for optimal performance in a broad range of <a href="../../a1/VoIP-Network-platform.asp">networks</a> giving packet voice the clarity and sound <a href="../a1/VoIP-Network-platform.asp">quality</a>
we expect on every call.<br />
<br />
</p>

<p class="virtualpage3">
<b>QoS and voice <a href="../a1/VoIP-Network-platform.asp">quality</a></b> <span class="credits"></span><br>
<b>From a technical perspective</b> good voice <a href="../a1/VoIP-Network-platform.asp">quality</a>
involves minimizing delays and interruptions ("blips") in
the communication stream. For voice communications
over an IP <a href="../a1/VoIP-Network-platform.asp">network</a>, packet delays and losses in the VoIP
<a href="../a1/VoIP-Network-platform.asp">network</a> must be reduced to the levels of established
benchmarks. To achieve a level of perceived voice <a href="../a1/VoIP-Network-platform.asp">quality</a>
that most users find acceptable, end-to-end packet
delay must be reduced to a target of 120ms or less. In
most IP <a href="../../a1/VoIP-Network-platform.asp">networks</a> some degree of packet loss may be
inevitable. However, for a successful VoIP deployment, we
must reduce packet loss to well below 1%. For Fax-over-
IP connections, the packet-loss target is especially critical.<br />
<br />
<b>Packet loss and packet delay </b>may accumulate at
multiple locations within the IP <a href="../a1/VoIP-Network-platform.asp">network</a>. Every router,
switch and transmission line is a potential culprit for harboring
these enemies of voice <a href="../a1/VoIP-Network-platform.asp">quality</a>. Technology standards
such as TOS or DiffServ are designed to achieve
the goal of establishing QoS in each and every node,
thus enforcing QoS mechanisms throughout the <a href="../a1/VoIP-Network-platform.asp">network</a>
from end to end. (These standards will be discussed later
in this paper.) The reality today, however, is an Internet
that does not differentiate between real-time (RT) and
non-real-time (NRT) packets. As a result, VoIP <a href="../../a1/VoIP-Network-platform.asp">networks</a> require alternate methods for ensuring voice <a href="../a1/VoIP-Network-platform.asp">quality</a>.</p>




<p class="virtualpage3">
<b><br /><a href="#">Addressing QoS at the edge of
the network</a></b> <span class="credits"></span><br>
<b>In VoIP systems that traverse the Internet</b>, the
bottleneck typically occurs at the access link-the lowbandwidth
connection between the high-speed Internetbackbone
(WAN) and the user <a href="../a1/VoIP-Network-platform.asp">network</a> (LAN). Both <a href="../../a1/VoIP-Network-platform.asp">networks</a> typically run at 100 Mbps or above. A typical
access link may easily run about 200 times slower than
the LAN residing in the home or office (say, 512 kbps for
example). Congestion, queuing delay, and queue overflows
(resulting in dropped packets) are most likely to
occur on this link. Depending on access-link bandwidth,
packet size and burst size (the number of packets arriving
at once), queuing delay can be especially significant.<br />
<br />
<b>In a typical installation</b>, a single access link serves
both voice and data <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, so special measures must
be employed to ensure good voice <a href="../a1/VoIP-Network-platform.asp">quality</a>. Consider
the case of a 256 kbps access link from the Internet's
edge router to the user's LAN. Suppose the Internet's
transmit queue contains five 1500-byte data packets,
followed by one time-sensitive voice packet. It will take
roughly 270 ms to send those data packets over the
256 kbps link. When the voice packet follows, it
arrives with a delay longer than 120 ms (our target),
resulting in degraded voice <a href="../a1/VoIP-Network-platform.asp">quality</a>.<br />
<br />
<b>Introducing class-of-service in the packet layer</b>
addresses queuing delay by ensuring voice packets
receive priority treatment-in much the same way that
separate queues at airport check-in counters ensure priority
service to first-class customers. By creating separate
queues for Real-Time and Non-Real-Time <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, we can
assign higher priority to the RT queue and serve that <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>
with higher priority.<br />
<br />
<b>Upstream and downstream</b><a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> present different
problems, and are best addressed by different <a href="../a1/VoIP-Network-platform.asp">quality</a> of
Service (QoS) mechanisms. Upstream <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> flows from
the (home or office) user to the Internet, while downstream
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> flows from the Internet to the user.<br />
<br />
<b>Implementing QoS in the upstream direction </b>is
relatively easy. SmartNodeT ensures that outbound
voice packets get served before other packet types to
prevent the Internet access link (the bottleneck) from
becoming overloaded. SmartNode also provides tuning
mechanisms for additional parameters like packet
segmentation and overhead optimization, so <a href="../a1/VoIP-Network-platform.asp">network</a>
administrators can further fine-tune the upstream transmission
for optimum voice <a href="../a1/VoIP-Network-platform.asp">quality</a>. <br />
<br />
<b>Implementing QoS in the downstream direction</b>
is more complex. Typically, customer-premise
equipment (CPE) at user locations exerts no control<br />
<br />
<img src="images/F1_Internet_Access.jpg" /><br />
<br />
<img src="images/F2_The_Problem_of_Downstream_Traffic.jpg" />
<br />
<br />
over incoming <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. For <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> flowing downstream
from the Internet, the access router cannot control the
volume nor the sending users. In addition, local users
sharing the LAN with you can initiate file transfers or
download their email at their convenience. The servers
handling these requests may be located anyplace.
Since the downstream rate normally cannot be controlled,
the ISP's edge router commonly responds to
overloading by discarding VoIP packets with the same
probability as any other packet type. These factors, or
a combination of them, may degrade voice <a href="../a1/VoIP-Network-platform.asp">quality</a> to
a degree that users find objectionable. Data <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, on
the other hand, can be retransmitted so the impact on
the user experience is simpler slower service response.<br />
<br />
<b>To resolve the problem of degraded voice
<a href="../a1/VoIP-Network-platform.asp">quality</a></b> for incoming <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, Patton has devised a leading-
edge technology for SmartNodeT called
DownStreamQoST. Within the SmartNode deployed
at the customer premise, DownStreamQoS dynamically
creates a virtual bottleneck against the incoming packet
stream. This bottleneck can throttle back Non-Real-
Time <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, preventing the edge router from blocking
or impeding voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, to ensure voice packets are
transmitted freely downstream. DownStreamQoS
employs flow-control mechanisms within the TCP standard
to create the bottleneck. Because 80% of Internet
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> is transported via TCP, DownStreamQoS is especially
effective. The dynamic bottleneck adjusts to varying
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> patterns. So whenever there are no VoIP calls
in progress, the full downstream bandwidth is available
for incoming data such as file downloads.</p>

<p class="virtualpage3">
<b>Technology standards for QoS</b> <span class="credits"></span><br>
<b>When implementing QoS </b>to ensure good voice
<a href="../a1/VoIP-Network-platform.asp">quality</a>, VoIP <a href="../../a1/VoIP-Network-platform.asp">networks</a> may employ a selection of mechanisms
from a variety of standard communications protocols.
Such mechanisms may include:<br />
<br />
. Tag or Label within the Packet or Frame<br />
. <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> (QoS) Classes<br />

. <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> (QoS) Conditioning or Packet treatment<br />
<br />
<img src="images/F3_SmartNode_DownStreamQoS.jpg" /><br />
<br />
The table below summarizes the standard protocol
mechanisms Patton has employed in our unique implementation
of QoS for SmartNode VoIP solutions.<br />
<br />
<b>A key point to remember </b>about any QoS-related
standard is the following: a standard is only as effective
as its specific implementation in a specific <a href="../a1/VoIP-Network-platform.asp">network</a>
in the real world. For example, almost every
router in the world claims to support TOS labeled IP
packets. "So why," we want to ask, "is the Internet still
a best-effort <a href="../a1/VoIP-Network-platform.asp">network</a>? Why has the Internet not implemented
QoS?" The answer lies in the additional complexity
of administering a <a href="../a1/VoIP-Network-platform.asp">network</a> that delivers QoS.<br />
<br />
A <a href="../a1/VoIP-Network-platform.asp">network</a> operator who wants to offer different service
classes must do all of the following:<br />
<br />
. Provide Service Level Agreements (SLAs) for the
service classes offered (e.g. throughput guarantees
or maximum delay values)<br />
<br />
. Bill separate service classes individually<br />
<br />
. Ensure that service classes are not
fraudulently misused<br />
<br />
. Dimension the <a href="../a1/VoIP-Network-platform.asp">network</a> in a way so that SLAs for
each service class can be met, even in fallback
and redundancy scenarios <br />
</p>

<p class="virtualpage3">
<img src="images/F4.jpg" /></p>



<p class="virtualpage3">
Administering a QoS <a href="../a1/VoIP-Network-platform.asp">network</a> is much more complex
than ensuring reliable connectivity in a best-effort
<a href="../a1/VoIP-Network-platform.asp">network</a>. This complexity is why <a href="../../a1/VoIP-Network-platform.asp">networks</a> supporting different
service classes have not appeared until quite
recently, and only a limited scale, and only from single
operators. Considering the content of our discussion, one
can begin to grasp the complexity of implementing QoS
end-to-end across the boundaries of multiple <a href="../../a1/VoIP-Network-platform.asp">networks</a>.</p>

<p class="virtualpage3">
<b>Conclusion
</b> <span class="credits"></span><br>
<b>The Internet Engineering Task Force (IETF)</b> and
the networking industry have defined a number of technology
standards and methods for offering levels of IPnetwork
service that exceed the typical best-effort service
levels currently available. Yet the reality today is
that IP <a href="../../a1/VoIP-Network-platform.asp">Networks</a>-and the Internet in particular-continue
to lag behind in implementing these existing technologies
in order to deliver QoS to subscribers. The
reason for the delay lies in the substantial complexity
involved in administrating a QoS <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
<b>SmartNode has implemented QoS mechanisms</b>
that significantly improve service <a href="../a1/VoIP-Network-platform.asp">quality</a> for
voice and data in a crucial section of the <a href="../a1/VoIP-Network-platform.asp">network</a>-
the subscriber access link. Further, SmartNode's QoS
mechanisms operate with complete independence
from the Internet or wide area <a href="../a1/VoIP-Network-platform.asp">network</a>. Finally,
SmartNode is flexibly designed for future adaptation,
ready and able to support leading QoS technology
standards as <a href="../a1/VoIP-Network-platform.asp">network</a> providers begin to
deploy them.<br />
<br />
<b>Today's enterprises, carriers and serviceproviders</b>
are eager to realize the cost-savings and

flexibility of VoIP. Patton's SmartNodeT family of VoIP
solutions offers a complete line of VoIP Gateways and
Broadband Internet Access Routers that deliver those cost
and flexibility benefits now, while offering a smooth
migration path to tomorrow's fully-converged voice-data
<a href="../../a1/VoIP-Network-platform.asp">networks</a>. Scaling from dual-port analog gateways up to
Quad-PRI digital Gateway-Routers, all SmartNodeT
models combine standards-based signaling (SIP, H.323
and MGCP/IUA) with advanced Patton's feature-rich
SessionRouterT software to deliver robust Telephonyover-
IP with industry-leading voice <a href="../a1/VoIP-Network-platform.asp">quality</a>. SmartNode
VoIP solutions integrate seamlessly with legacy communications
infrastructures to deliver end-to-end voice services
over any IP <a href="../a1/VoIP-Network-platform.asp">network</a>. SmartNodeT/SmartLinkT
VoIP.more than just talk!<br />
<br />
Key SmartNode features to make your VoIP project
a success:<br />
<br />
<b>Standards Compliance</b>-Protecting your
investment in VoIP, SmartNode implements key
industry standards for proven interoperability with
third-party equipment and <a href="../../a1/VoIP-Network-platform.asp">networks</a>.<br />
<br />
<b>Advanced Call Routing</b>-SmartNode's flexible
SessionRouter software seamlessly integrates VoIP
with existing <a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems and numbering
plans.<br />
<br />
<b>Feature Transparency</b>-SmartNode forwards
PBX and PSTN features transparently to the user,
preserving the benefits of advanced calling features
in both traditional <a href="../../a1/VoIP-Network-platform.asp">telephony</a> and digital
packet-voice <a href="../../a1/VoIP-Network-platform.asp">networks</a>.
<br />
<br />
<img src="images/smartnode.jpg" /><br />
<br />
<img src="images/products_highlights.jpg" /></p>

</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 3, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Addressing_QoS_Beyond_the_Provider_Network.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/VoIP_Addressing_QoS_Beyond_the_Provider_Network.asp"></a> <a href="../doc/Addressing_QoS_Beyond_the_Provider_Network.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
