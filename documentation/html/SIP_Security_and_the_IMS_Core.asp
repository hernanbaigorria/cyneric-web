<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
SIP Security and the IMS Core<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
Much has been written about the security of SIP- based networks and security of Voice over IP in general, but ultimately good security is a complete architecture, not a single product or protocol. The advent of Fixed/<a href="../a1/productos-cr2.asp">Mobile</a> Convergence and IMS has created some widely accepted standards and has equally highlighted architectural differences in the converging <a href="../../a1/VoIP-Network-platform.asp">network</a>s. Whilst the overall objective of providing a flexible, secure <a href="../a1/VoIP-Network-platform.asp">network</a> and secure services is common, the implementation details differ from <a href="../a1/VoIP-Network-platform.asp">network</a> to <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
Even within the standards themselves there is sometimes an assumption of trust which may not in reality exist. For example, the IMS definition within TISPAN assumes that the signalling elements are able to handle excessive signalling rates and badly formed signalling messages. In reality these elements are designed to process sessions, handling attacks at the same time may not be the best use of the equipment. In a data-centric <a href="../a1/VoIP-Network-platform.asp">network</a> we would expect to see servers ringed by Firewalls and Intrusion Detection and Prevention systems, so why would we build a media-centric <a href="../a1/VoIP-Network-platform.asp">network</a> any other way?<br />
<br />
<b>Fixed/<a href="../a1/productos-cr2.asp">Mobile</a> Differences</b><br />
The <a href="../../a1/VoIP-Network-platform.asp">network</a>s that support <a href="../a1/productos-cr2.asp">Mobile</a> services have developed their own security and authentication mechanisms that reside primarily in the radio access part of the <a href="../a1/VoIP-Network-platform.asp">network</a>. This means that when 3GPP developed their IMS specification there was no need to be overly concerned with those issues. The IMS can assume that the subscriber that is registering has already been authenticated and that the only thing to deal with now is the policies that apply to the caller. In the case of a fixed line service there is no guarantee that the user will be authenticated against a USIM (Universal Subscriber Information Module), thus authentication must rely on other, potentially less secure techniques.<br />
<br />
This ability to connect almost any hardware or software device opens the door to other potential problems in the fixed line <a href="../a1/VoIP-Network-platform.asp">network</a> - that of device malfunction and malicious attack. The <a href="../a1/productos-cr2.asp">mobile</a> radio access <a href="../a1/VoIP-Network-platform.asp">network</a> is a far more controlled environment, with each device having a security association with the <a href="../a1/VoIP-Network-platform.asp">network</a>, meaning that any abuse can be tracked back to a particular device. Another consideration is that there is currently a marked difference in the bandwidth available in fixed and <a href="../a1/productos-cr2.asp">mobile</a> <a href="../../a1/VoIP-Network-platform.asp">networks</a>, thus the fixed line <a href="../a1/VoIP-Network-platform.asp">network</a> offers a potentially larger pipe to deliver disruptive <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. Fixed line <a href="../../a1/VoIP-Network-platform.asp">network</a>s will support a large population of PC based soft-clients, these require minimal testing and therefore the potential for the presence of badly behaved device is much greater, it also means that a familiar environment is available for creating malicious software.<br />
<br />
ETSI's TISPAN architecture takes the 3GPP IMS definition and expands it to include addition elements that help to address some of these concerns. The most noticeable difference is that the 3GPP definition deals only with the signalling path whilst the TISPAN definition includes elements that manage the media path, the BGFs - Border Gateway Functions. There is also a formalised Border Control Function between interconnected <a href="../../a1/VoIP-Network-platform.asp">networks</a> - the I-BCF.<br />
<br />
The 3GPP IMS security architecture is based around IPsec, which works well in the 3G environment which does not have NAT devices. Most NAT devices translate port numbers as well as IP addresses, which due to the way IPsec encodes the packets means that NAPT devices will prevent end-to-end use of IPsec.<br />
<br />
However, in TISPAN with client devices typically being connected through broadband access networks NAPT devices are virtually guaranteed. Thus, TISPAN encryption and authentication must take another route. RFC3261 specifies TLS as the secure transport mechanism for use with SIP, and this was considered by TISPAN, however, the selected encryption method is UDP encapsulation of IPsec. This eliminates the problems encountered when using IPsec across NAPT devices. IPsec, NAT traversal and TISPAN's selection of UDP encapsulated IPsec are examined the White Paper "IPsec in VoIP <a href="../../a1/VoIP-Network-platform.asp">Network</a>s".<br />
<br />
<b>IMS as the Heart of the <a href="../a1/VoIP-Network-platform.asp">Network</a></b><br />
The role of an IMS core is to enable service providers to roll out new services rapidly and deliver them to any device over any <a href="../a1/VoIP-Network-platform.asp">network</a>. It is the glue that sits between the service layer and the <a href="../a1/VoIP-Network-platform.asp">network</a> layer. Whilst these elements can be expected to provide some level of resilience to attacks, pragmatic service providers are looking to create a protected zone in which these highly valuable assets can be located.<br />
<br />
So we are in effect talking about creating a DMZ for the signalling through the use of 'signalling firewalls'. The job of the signalling firewall is to protect the core elements from accidental overload, malicious attack, malformed signalling messages and irrelevant protocols.

<b>Protect the core with signalling firewalls</b><br /><br />

<img src="images/Protect_the_core.jpg" /><br />
<br />
Providing protection for the core requires more than a conventional firewall, it requires a firewall that can understand SIP signalling. This requires a resilient hardware based solution capable of rejecting any unwanted <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>whilst admitting legitimate <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> at a rate which can be supported by the core elements. Not a trivial task - lets look at the requirements one by one.
</p>




<p class="virtualpage3">
<b>Requirement 1: Must stand up under attack</b><br />
A basic requirement of any signalling firewall is that it must remain operational under all attack conditions. Before we get carried away with all of the exotic and innovative application layer attacks, the basics must be in place. The TCP SYN flood, for example, is one of the oldest attacks around and probably one of the most common exploits used to cause resource starvation in vulnerable targets. The IMS core may not need to even respond to any TCP <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> if the SIP signalling is carrier using UDP, thus TCP <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> can be rejected at the perimeter of the IMS. Similarly logic exploits like the Ping of death, should simply be blocked by the firewall at the IMS perimeter. A minimum requirement for the signalling firewall is that is should stand up to vulnerability tests such as ISIC - IP Stack Integrity Checker and Nessus.<br />
<br />
SIP signalling <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> must also be viewed with considerable suspicion, malformed SIP messages should be discarded and not passed through to the IMS core elements. Resistance to this type of attack can be determined by testing against suites such at the IETF SIP Torture test developed through the SIPiT Events or the PROTOS Test-Suite, developed by the University of Oulu.<br />
<br />
<b>Requirement 2: Must prevent propagation of attacks</b><br />
This is the logical extension of the first requirement. The signalling firewall must identify and discards malicious <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> in order to protect the core. Many protocols can simply be discarded, as described in Requirement 1 above, as they have no relevance to the SIP proxies. Thus the IMS elements are effectively protected from both transport and application layer attacks.<br />
<br />
<b>Requirement 3: Must preserve an operational service through pacing</b><br />
Now that the basics are in place we must turn our attention to the applications themselves. The rate of SIP signalling can be the cause of problems for the Softswitch and not just through malicious intent, for example, following the hurricanes in Florida when the power was restored, this caused all the IP phones to register at the same time. This resulted in the service failing due to the rate of registrations. In these situations the core elements must be protected by pacing both registrations and call attempts. The signalling firewall should deliver the registrations and call invitations to the Softswitch at a rate that it can sustain.<br />
<br />
<b>Requirement 4: Must preserve <a href="../a1/VoIP-Network-platform.asp">network</a> anonymity through topology hiding</b><br />
Topology hiding features prominently in most service providers' requirements. As the SIP signalling passes through various servers on route to its destination, the SIP messages acquires information about where the message came from and what devices it passed through. Since global networks are made up of a mesh of service provider network<a href="../../a1/VoIP-Network-platform.asp">s</a> this information gets passed from <a href="../a1/VoIP-Network-platform.asp">network</a> to <a href="../a1/VoIP-Network-platform.asp">network</a>. It is therefore important to strip all this information from the signalling prior to it being passed from one <a href="../a1/VoIP-Network-platform.asp">network</a> to another. This prevents internal <a href="../a1/VoIP-Network-platform.asp">network</a> addresses and client address details from being propagated. This benefits the service provider by effectively shielding both <a href="../a1/VoIP-Network-platform.asp">network</a> and subscriber from prying eyes.<br />
<br />
<b>Requirement 5: Must preserve service <a href="../a1/VoIP-Network-platform.asp">quality</a> and protect revenue through media policing</b><br />
This requirement is present to counter some of the innovative ploys to steal services. When SIP establishes a call it uses a server to locate and communicate with the destination, once the addresses of the source and destination have been exchanged there is no reason why the two parties cannot communicate directly - without the intervention of the server. Thus, a party can request a voice call which, once establish can be renegotiated as a video call without the knowledge of the SIP servers from which the<a href="../../a1/VoIP-Billing-Platform.asp"> billing</a> may be derived. The service provider is unaware of the addition bandwidth being used. This results in loss of revenue and potential degradation of service <a href="../a1/VoIP-Network-platform.asp">quality</a> for other users.<br />
<br />
To prevent this service theft it is necessary to link the media path with the signalling path. This is carried out by session border controllers, or in the case of a TISPAN IMS, by a combination of the P-CSCF and A-BGF managing the signalling and media respectively. The signalling and media elements exchange information to ensure that the media remains within the requested limits, any deviation from the requested bandwidth can be blocked.<br />
<br />
A key benefit of this process is to preserve the <a href="../../a1/VoIP-Network-platform.asp">qualityof Service of calls, particularly within the access networks, by preventing over-booking of the <a href="../a1/VoIP-Network-platform.asp">network</a> resources.</a></p>

<p class="virtualpage3">
<b>The Survivable Core</b><br />
With all of the above requirements satisfied we have helped to create a survivable core. Through a combination of firewalling, signal pacing and <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> management, the valuable assets that make up the IMS core can get on with doing their job: providing any service to any device over any <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
Is the signalling firewall a logical component or a physical device? The answer is actually both. There are several functions within the 3GPP and TISPAN IMS definitions that can be considered to be border devices. The P-CSCF is the first point of contact for registration and routing of new calls and is therefore a suitable device in which to implement the signalling firewall. The I-CSCF provides topology hiding for the interconnect point. Within TISPAN, the BCF and BGF functions define signalling and media borders. All these functions may be extended to include the requirements defined above as part of a fully integrated solution. Equally, it is possible to treat the whole IMS as a target and implement the signalling and media protection externally.<br />
<br />
<b>What next? SPIT - Detection and Deflection</b><br />
SPAM over Internet <a href="../../a1/VoIP-Network-platform.asp">Telephony</a>, or SPIT as it so colourfully known, is being touted as the next e-plague to descend upon us. This is the voice equivalent of email SPAM, i.e. machine driven mass dialling to subscribers to deliver junk voice mail. So how can we deal with this? The answer may lie in a two layered defence of detection and deflection. Many of the best SPAM filters around today are in fact great learning machines, they constantly learn what is SPAM and what is not, they build black lists of known sources and can achieve high rates of successful blocking. However, SPAM is non-real-time, in order to apply this technology to SPIT the detection engine must employ pattern recognition on calls to determine a potential source, this can be carried out non-intrusively in near-real-time, the results written to a policy database which is accessed by the signalling firewall which deflects or blocks the signalling and prevents the call from being established.<br />
<br />
<b>Conclusion</b><br />
IMS offers the potential to deliver a great range of innovative services to a range of different <a href="../../a1/VoIP-Network-platform.asp">networks</a>. In doing so it offers an attractive target for disruption. The IMS core must be protected through the use of an effective security architecture either intrinsically as part of the perimeter of the IMS e.g. a hardened P-CSCF acting as a signalling firewall, or extrinsically by using signalling firewalls to create a DMZ for the core elements.<br />
<br />
Newport <a href="../../a1/VoIP-Network-platform.asp">Networks</a> 1460 provides the carrier class hardware platform required for these duties, either as a P-CSCF itself, or as a signalling firewall protecting a third party P-CSCF. The 1460 can also offer separated signalling and media elements capable of offering full topology hiding and media policing.
Guaranteeing continuity of service is an impetrative for service providers deploying IMS cores. The survivable core also forms the cornerstone of delivering reliable Emergency Call Handling and key worker prioritisation - this is discussed in more detail in Newport Networks Emergency Call Handling White Paper.</p>


</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/SIP_Security_and_the_IMS_Core.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../doc/SIP_Security_and_the_IMS_Core.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
