<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
VoIP Security - Does it exist?<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">


VoIP is becoming one of the hottest services being offered by start up providers and adopted even
by large telecommunications corporations in order to lower their operating costs. But how secure
is VoIP really? Scanit R&D Labs conducted extensive research on VoIP security and found the
truth to be a little startling.<br />
<br />
The pace at which technology has developed and the reduction in price of computer and <a href="../a1/VoIP-Network-platform.asp">network</a>
equipment has contributed significantly to the evolution and advancement of home <a href="../../a1/VoIP-Network-platform.asp">networks</a>.
The recent penetration of broadband connectivity to the household has grown rapidly and it has
become progressively easier to "get connected". With broadband on the rise, the requirements for
bandwidth hungry implementations such as Video or Voice over IP have been easily met. This
has meant that concepts such as video blogging, podcasting, streaming internet video and VoIP
have managed to gain a strong foot-hold in present day online services.<br />
<br />
<b>What is VoIP?</b><br />
VoIP or Voice over Internet Protocol is the term used for voice conversations that are routed over
the Internet or via an IP based <a href="../a1/VoIP-Network-platform.asp">network</a>. While the fundamentals between VoIP and PSTN
(Public Switched Telephone <a href="../a1/VoIP-Network-platform.asp">Network</a>) remain more or less the same, the protocols used in each
implementation differ. VoIP relies on protocols such as H323 or SIP for the purpose of signalling
and RTP (Real-time Transport Protocol) for Media transmission.<br />
<br />
RTP is a UDP protocol where a stream of voice data is
sent from one IP to another. The voice data is encoded
in one of several existing codecs depending on voice
<a href="../a1/VoIP-Network-platform.asp">quality</a> or bandwidth required in the implementation.
Some of the more popular codecs are: GSM, G723 and
G729. The codecs that take up a little extra bandwidth
offer call <a href="../a1/VoIP-Network-platform.asp">quality</a> that far exceeds a standard telephone
line.<br />
<br />
<span class="ventral">Signalling: Signalling
protocols handle all aspects of
setting up a call initiating
media delivery and tearing down</span> <span class="ventral">a call after it is complete.
SIP has a very similar HTTP
style "Request" and "Response"
system complete with SIP</span> <span class="ventral">response codes such as 403 -
Forbidden or 404 - Not found.</span>
<br />
<br />
VoIP presents significant advantages to the end-user in terms of being extremely cost-effective
and easy to setup. Several VoIP providers have popped up all over the place offering
international call rates at least 6 to 10 time cheaper than regular PSTN IDD calls. All an end-user
would have to do is to download a softphone client, install it on their PC, charge their account
online through their credit card and start dialling their relatives in far away countries. The
simplicity involved is far too tempting to resist and by this, the number of VoIP users is on a
sharp increase. VoIP is not without its flaws by any means. Users also have to put up with bad
call <a href="../a1/VoIP-Network-platform.asp">quality</a>, <a href="../a1/VoIP-Network-platform.asp">network</a> congestion and<a href="../../a1/VoIP-Billing-Platform.asp"> billing</a> issues, but the disadvantages are far outweighed by
the advantages. One more disadvantage of VoIP is its security.<br />
<br />
The team at Scanit R&D Labs have conducted a significant amount of research into VoIP and it's
inherent vulnerabilities. Broadly, VoIP attacks can be divided into two groups: Signalling attacks
and Media stream attacks. We tested the most popular SIP routers that are being used by the
majority of VoIP providers and uncovered some startling results.<br />


</p>

<p class="virtualpage3">

Signalling attacks can be used to eavesdrop on conversations and re-route or hijack calls. Due to
the fact that the SIP protocol presently does not support message integrity, it is extremely easy to
re-play or re-send SIP messages to the SIP registrar or proxy and have it perform functions such
as adding another client to a conversation or re-routing of a call. Since SIP messages are also
sent over a clear-text channel, it becomes a trivial task for an attacker to perform ARP poisoning
and inspect, intercept and modify all SIP messages on the local <a href="../a1/VoIP-Network-platform.asp">network</a>.

<br />
<br />

<b>An example of Registration Hijacking</b><br />
A typical SIP REGISTER Message will have fields similar to the following<br />
<br />

<img src="images/SIP_register.jpg" /><br />
<br />
A message similar to this will always be used by a client to announce itself to a SIP registrar.
Once announced, the SIP registrar is aware of the client's location and the fact that it is ready to
accept calls.<br />
<br />
The Contact field is the important field here as it contains the location or IP address of the client
being registered. When a call to the above user is placed, the SIP proxy will perform a lookup to
ascertain the location of the client. In this particular instance, the user with the phone number 01-
141337 can be reached at IP address 192.168.133.7.<br />
<br />
An attacker can easily sniff this information, modify it and re-send it to the SIP registrar.
Consider the scenario where the above packet is sniffed by an attacker, then modified to look like
this:<br />
<br />
<img src="images/SIP_register2.jpg" /><br />
<br />
The packet is re-sent to the Registrar and now any calls made to the number 01-141337 will be
routed to IP Address 192.168.39.69 rather than the original IP Address of 192.168.133.7.<br />
<br />






</p>

<p class="virtualpage3">
<b>Media Stream attacks</b><br />
Media Stream attacks are as easy to perform in a typical VoIP implementation. As stated
previously, voice data is sent over the Real-time Transport Protocol. This is a UDP based
protocol which streams voice data from one IP to another. The voice data that traverses an RTP
stream is generally encoded by a specific audio codec and not encrypted. This means that any
RTP streams intercepted by an attacker can easily be decoded with the relevant audio codec and
the actual voice call can be recorded or listened to.<br />
<br />
Media stream attacks bring with them their own high risk threats such as injection of data. An
instance where data injection can be used effectively could be in a situation where an attacker
replays a message to enter a PIN code and then captures the subsequent touch tones which he can
use later on.<br />
<br />
<img src="images/Media_stream.jpg" /><br />
<br />
VoIP is definitely here to stay. However, the rapid deployment of VoIP has seen it progress with
little or no attention given to security. While the technological advancements of VoIP have
grown by leaps and bounds, security is still left behind to catch up. The vulnerabilities listed in
this article are merely a small percentage. Several more vulnerabilities have presented
themselves during internal tests and a large number of Proof of Concept attacks have been
developed in-house.<br />
<br />
This does not mean that larger organizations should abandon the idea of a VoIP implementation.
VoIP implementations can be secured with a little effort placed during the design and
implementations phases. Being fully aware of the risks involved with VoIP implementations
goes a long way into understanding how to secure it.

</p>



</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/VoIP_Security.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/3com-solutions-strategies.as"></a>  <a href="../doc/VoIP_Security.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
