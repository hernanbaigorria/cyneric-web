<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <th valign="top" scope="col"><h1>
How Quintum's Access Solutions Bring Service Providers and
Enterprises Together to Make the World Safe for VoIP<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">
<b><a href="#">Executive Summary</a></b> <span class="credits"></span><br>
Voice-over-IP (VoIP) offers a wide range of benefits -
including cost savings, the convenience of managing
one <a href="../../a1/VoIP-Network-platform.asp">network</a> instead of two, simplified provisioning
of services to remote locations, and the ability to
deploy a new generation of converged applications.
However, implementation of VoIP poses a variety
of challenges for enterprises and service
providers alike.<br />
<br />
For service providers, these challenges include the
tremendous diversity in the customer environments.
These environments range from legacy
PBXs to state-of-the-art all-IP infrastructure. To
profitably deliver VoIP in this diverse market, service
providers must be able to quickly and inexpensively
accommodate whatever type of equipment
exists on a customer's premises. Service providers
must also be able to ensure their customers that the
VoIP solutions they deliver are reliable and secure.
And they have to be able to remotely manage their
customers' on-premise equipment, even if it's
behind a firewall running <a href="../../a1/VoIP-Network-platform.asp">network</a> address translation
(NAT).<br />
<br />
For enterprise IT managers, the challenges are similar.
Corporate VoIP implementers need to be sure
that their environments can interface seamlessly
with their chosen service provider. They need to
know that their voice communications will be 100%
reliable. They need their solution to be secure and
easily manageable. And they, too, need to keep their
total infrastructure costs as low as possible.<br />
<br />
The good news is that both service providers and
enterprises have generally done a good job of
addressing these challenges within their own net work environments. The bad news is that the
interface between their respective <a href="../../a1/VoIP-Network-platform.asp">networks</a>
remains problematic. It is at the edge of these two
<a href="../../a1/VoIP-Network-platform.asp">network</a> that compatibility, survivability, manageability,
security and cost become obstacles to successful
VoIP implementation.<br />
<br />
In fact, the future of convergence is largely contingent
upon solving the problem of VoIP access at the
edge. If service providers and enterprises continue
to struggle to connect their <a href="../../a1/VoIP-Network-platform.asp">networks</a> to each other
- and if they find the <a href="../../a1/VoIP-Network-platform.asp">network</a>-to-<a href="../../a1/VoIP-Network-platform.asp">network</a> interface
overly difficult to manage - then the majority of
enterprises will never reap the benefits of convergence,
and service providers will never realize the
full profit potential of VoIP. If, on the other hand,
life on the edge can somehow be made easier and
safer, the convergence market will experience
explosive growth and rapidly move upmarket.<br />
<br />
Quintum's intelligent Tenor VoIP access switching
solutions uniquely address the challenges posed by
life on the edge. They provide the universal interoperability
with premises equipment that is essential
for fast, pain-free VoIP access. They deliver the
survivability and QoS mechanisms essential to
ensure five-9s reliability for packetized voice services.
And they give service providers and enterprise
IT teams alike the management capabilities they
need to simplify both installation and ongoing VoIP
ownership.<br />
<br />
VoIP should and will become the dominant modality
for voice in the future across all markets. But
that future belongs exclusively to those who can
master life at the edge.<br />
<br />
<b><a href="#">Issues at the Edge</a></b><br />
<b>Where the service provider <a href="../../a1/VoIP-Network-platform.asp">network</a>
meets the enterprise</b><br />
<br />
Service providers and their business customers
are both eager to reap the benefits of VoIP and
convergence. Service providers want to capture
new customers, reduce operational costs and
deliver new revenue- and profit-driving offerings
- such as IP Centrex and presence-enabled
conferencing. Business customers are seeking
reduced costs, simplified management, and the
ability to implement those new service provider
offerings.<br />
<br />
Despite these common interests, however, service
providers and their customers have not been
able to move forward with their convergence
plans as quickly as either of them would like.
That's because they face a variety of nagging
issues as they try to interface their <a href="../../a1/VoIP-Network-platform.asp">networks</a>.
These issues include:<br />
<br />
<b>Compatibility and ease of installation</b><br />

Service providers typically maintain highly
standardized environments in order to keep
operating costs low and optimize economies of
scale. Their customers, on the other hand, have
an extremely diverse range of equipment and
configurations. This creates a fundamental conflict.
Service providers can't afford to perform a
lot of highly customized "one-off" installations.
They want to have a simple, uniform way to connect
their customers' <a href="../../a1/VoIP-Network-platform.asp">networks</a> to their own.
Business customers, on the other hand, want
their service provider to fully support their
entire existing environments - whether they include traditional PBXs, IP PBXs, and/or analog
devices such as fax machines or facilities
management systems that use built-in modems
to provide centralized <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> capabilities.<br />
<br />
In other words, service providers need the <a href="../../a1/VoIP-Network-platform.asp">network</a>-
to-<a href="../../a1/VoIP-Network-platform.asp">network</a> interface to be very easy to
deploy. But, because of the diversity of the
enterprise environments they have to plug into,
they historically have not been able to achieve
this goal.</p>

<p class="virtualpage3"><b>Survivability/reliability</b><br />
Business users depend on voice service every
minute of the day. So they are reasonably reticent
about moving forward with any solution
that doesn't promise rock-solid reliability.
Service providers want to fulfill this requirement,
because they know that reliable service is
essential for customer retention and strong
brand identity. The problem is that the interface
between their respective <a href="../../a1/VoIP-Network-platform.asp">networks</a> potentially
represents a single point-of-failure. This
creates both a real risk of service interruption
and a heightened perception of potential risk.
Both service providers and business customers
need a way to mitigate or even eliminate this
risk factor.<br />
<br />
<b>Manageability and security</b><br />
Both service providers and their business customers
have other challenges beyond simply
transporting voice packets from one end-point to
the other. They also have to be able to keep
their <a href="../../a1/VoIP-Network-platform.asp">network</a> environments safe and secure.
They thus need visibility into and control over
remote devices, regardless of where they're situated. <br />
<br />
This sometimes sounds simpler than it is
in the real world. For example, many businesses
employ <a href="../../a1/VoIP-Network-platform.asp">network</a> address translation (NAT)
technology on their firewalls in order to mask
their internal IP infrastructure from malicious
intruders. Unfortunately, NAT implementations
typically also make the enterprise <a href="../../a1/VoIP-Network-platform.asp">network</a>
opaque to external VoIP management - hampering
the ability of service providers to support
convergence. Service providers also have to
address scalability when it comes to management,
since they may have to support thousands
of premises devices.<br />
<br />
<b>Cost</b><br />
Service providers and business customers obviously
both want to keep the cost of interfacing
their <a href="../../a1/VoIP-Network-platform.asp">networks</a> as low as possible - especially if
the business has multiple locations. This has
been difficult to do, because of the price and
number of devices that have historically been
required to ensure a manageable VoIP environment.
Between modifications to the PBX, the
purchase and installation of a VoIP gateway, and
the implementation of other devices such as SIP
controllers, the cost of provisioning VoIP connectivity
can run to several thousand dollars. The
upfront costs are not acceptable in today's market
and represent a significant inhibitor to VoIP
adoption.<br />
<br />
With all these obstacles, it's no wonder that
service providers have not been signing up
business customers at a faster rate - and that
VoIP hasn't quickly become the market standard
for voice communications. Convergence
certainly offers enough potential benefits to
drive more widespread adoption. But the price
and risk of conversion are too high for service
providers and their customers alike. So, for
the market to move forward, an alternative
approach is clearly required.<br />
<br />
<img src="images/Traditional_VoIP_Gateways.jpg" /><br />
<br />
</p>

<p class="virtualpage3">

<b>Quintum's solutions for the edge</b> <br />
As one of the industry's leading developers of
VoIP switching and gateway solutions, Quintum
Technologies has uniquely focused on the issues
surrounding the connection of enterprise voice
infrastructure to service provider <a href="../../a1/VoIP-Network-platform.asp">networks</a>.
The result of this focus is a switching architecture
ideally suited for both the realities facing
both enterprise <a href="../../a1/VoIP-Network-platform.asp">network</a> managers seeking a
reliable platform for voice/data convergence and
service provider engineering teams seeking to
profitably deliver dependable VoIP access.<br />
<br />
<b>Adaptable, zero-hassle implementation</b><br />
Quintum's Tenor VoIP switches and gateways
are designed to fit into virtually any enterprise
environment, installing easily between the PBX
and the IP <a href="../../a1/VoIP-Network-platform.asp">network</a>. They interoperate with and
are fully transparent to all types of traditional
and IP-based PBXs - thus eliminating the need
for PBX reconfiguration. Voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> leaving the
PBX is directed to the appropriate "path" (i.e.
the service provider's <a href="../../a1/VoIP-Network-platform.asp">network</a>, the enterprise
WAN and/or the PSTN) based on easily programmed
rules.<br />
<br />
Because Quintum switches don't require modification
of the PBX, they support whatever dial
plan is already in place. They also support analog
fax machines, conventional paging systems,
and other types of existing infrastructure. So, in
addition to requiring minimal work on the part
of technicians, Quintum's switching solutions
also ensure that the transition to VoIP doesn't
disrupt the business or require users to change
their entrenched work habits.<br />
<br />
The ability to use a common set of easily
installed hardware solutions across all customer
sites has obvious benefits to service providers -
including lower equipment costs, reduced provisioning
costs, and fewer opportunities for installation
error. At the same time, the non-intrusive
nature of Quintum's switching technology
enables enterprise buyers to avoid counter-productive
disruptions as they take their first steps
towards convergence - whatever those first steps
may be.<br />
<br />
<b>Unmatched protection of voice <a href="../a1/VoIP-Network-platform.asp">quality</a>
and availability</b><br />
No enterprise can afford to lose its ability to
make and receive phone calls for even a short
amount of time. And no service provider wants
a paying customer to experience such a loss of
service. Unfortunately, problems can and do
occur in real-world IP <a href="../../a1/VoIP-Network-platform.asp">networks</a> - which is why
service interruptions are a major concern for
both enterprises and service providers.<br />
<br />
Quintum has uniquely addressed this issue in
several important ways. First, Quintum switches
provide a local SIP proxy agent that allows IP
phones in remote offices to continue functioning
even if their connection to a central IP PBX is
lost. This ensures the survivability of voice
service in the event of a problem on the IP <a href="../../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
The presence of a local SIP proxy agent (along
with the built-in intelligence provided by Tenor
switches) is especially important for service providers offering hosted IP PBX implementations
and/or IP Centrex services - since it keeps
customers' phones alive with basic <a href="../../a1/VoIP-Network-platform.asp">telephony</a> capabilities and allows calls to continue to be
routed to both IP addresses and over the PSTN.<br />
<br />
Second, Quintum switches continually monitor
conditions on the IP connections and can immediately
detect when congestion or an outage
threatens call <a href="../a1/VoIP-Network-platform.asp">quality</a>. When that happens, calls
can automatically be re-directed over the PSTN
until appropriate service levels are restored on
the IP <a href="../../a1/VoIP-Network-platform.asp">network</a>. This failover is so quick and
transparent that even active calls can be recovered
without interrupting ongoing phone conversations.<br />
<br />
Quintum switches even protect voice services
from themselves. If a switch loses power, the
switch simply passes voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> through to its
PSTN connection. So users can continue making
calls even in the event of a catastrophic failure.<br />
<br />
<b>Ease of ownership</b><br />
Quintum ensures ease, scalability and security
of management through the use of a Remote
Management Session Server (RMSS) that sits
between managed devices and a central management
console. Each of these servers can register
up to 5,000 devices and manage up to 100
Tenors simultaneously.<br />
<br />
Because managed devices are registered with
the RMSS, it can set up management sessions
across firewalls running <a href="../../a1/VoIP-Network-platform.asp">network</a> address translation
(NAT). Historically, such firewalls have
made remote management of VoIP devices problematic,
because of the way they conceal IP
addresses. The RMSS also encrypts management
session <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, bringing a critically important
added level of security to the VoIP environment.<br />
<br />
Tenors can be managed via telnet, FTP or
Quintum's own graphical Tenor Configuration
Manager. This gives enterprise and service
provider management teams alike the flexibility
to integrate VoIP management into their overall
<a href="../../a1/VoIP-Network-platform.asp">network</a> ownership operations.<br />
<br />
</p>

<p class="virtualpage3">
<b>Reduced costs</b><br />
By eliminating both the hardware and the labor
costs associated with PBX reconfiguration,
Quintum's solutions significantly reduce the capital
outlay required to make enterprise <a href="../../a1/VoIP-Network-platform.asp">networks</a>
VoIP-ready to interface them with service
provider access. Quintum's broad product range
also eliminates wasteful spending by ensuring a
close fit between the requirements of any site -
from the largest corporate headquarters to the
smallest remote office - and the cost of the device
installed there. These savings can be especially
substantial for organizations with large numbers
of offices, retail locations, or other facilities.<br />
<br />
Quintum switches offer many other attractive
features and capabilities - such as highly efficient
multiplexing of VoIP packets for reduced
bandwidth utilization, support for both H.323
and SIP, and simple implementation of "hop-on"
and "hop-off" calling. All of these features are
designed to maximize returns on VoIP infrastructure
investments and make life easier for
technical staffs.<br />
<br />
<img src="images/Quintums_VoIP_Solutions.jpg" /><br />
<br />
In fact, no other vendor provides a more robust and
cost-efficient solution for linking existing corporate
voice environments to private or public IP <a href="../../a1/VoIP-Network-platform.asp">networks</a>.
In terms of simplicity, reliability, ease of
ownership and cost, Quintum's survivable switching
platform offers the optimal solution for both the
enterprise and VoIP service providers.<br />
<br />
<b>The benefits of a better edge</b><br />
Because of their unique suitability for the edge,
Quintum's VoIP switching solutions offer both enterprises
and service providers significant benefits.<br />
<br />
<b>For service providers, these
benefits include:</b><br />
. Easier, less costly installation on
customer premises<br />
. Faster time-to-delivery for new sites<br />
. Fewer technical problems and truck rolls
for initial provisioning<br />
. More reliable service, resulting in higher
customer satisfaction<br />
. Easier <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> and maintenance<br />
. More efficient use of technical staff resources<br />
<br />
<b>For enterprises, these
benefits include:</b><br />
. Less expensive and time-consuming VoIP
implementation<br />

. Optimum reliability of voice service<br />

. No disruption of users' entrenched calling
habits and no re-training<br />

. Maximum flexibility in VoIP implementation<br />

. Minimum disruption to existing <a href="../../a1/VoIP-Network-platform.asp">network</a>
infrastructure<br />

. Preservation of investments in analog
equipment<br /><br />

Just as important as the benefits Quintum switches
offer each of these constituencies is the fact that
Quintum solutions offer significant benefits to both.
Enterprise customers and their service providers no
longer have to struggle with conflicting interests and
priorities as they determine what kind of equipment to
install as they seek to provision next-generation VoIP
services. Instead, Quintum's platform delivers a winwin
solution that effectively addresses both of their
needs. This common suitability for customer and vendor
alike decisively differentiates Quintum from the
rest of the market and makes its switches a perfect fit
for the enterprise-service provider edge.<br />
<br />
<img src="images/Quintum_Offers_the_Most.jpg" /><br />
<br />
</p>

<p class="virtualpage3">
<b>About Quintum</b><br />
Headquartered in Eatontown, NJ, Quintum
delivers VoIP solutions that bring the reliability
and voice clarity of public telephone <a href="../../a1/VoIP-Network-platform.asp">networks</a>
to Internet <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>. Quintum's intelligent
VoIP access solutions integrate easily into
existing PBX and IP infrastructures, making
them the ideal choice for service providers and
enterprise alike.<br />
<br />

According to InStat/MDR, Quintum has the second-
largest marketshare in the low density VoIP
market. The company was picked by Forbes for its
"Top Ten To Watch in 2005" list of top privately-held
technology companies and was ranked number 205
on the INC. 500 list of fastest growing private companies.
Quintum is also a Nortel Developer Partner
and an Avaya DevConnect Partner.<br /><br />
</p>



</div></th>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Life_At_The_Edge.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/Life_on_the_Edge.asp"></a> <a href="../doc/Life_At_The_Edge.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
