<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}


.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
A Proactive Approach to VoIP Security<br />Understanding VoIP security requirements, threats and architectures
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">
<b><a href="#">Introduction</a></b> <span class="credits"></span><br>
The emergence of Voice-Over-IP (VoIP)
technology is creating a major discontinuity
in telecommunications. The promise of
reduced hardware and operations costs
coupled with new value-added services
makes VoIP, as well as Internet Protocol
(IP) TV, videoconferencing, IP Multimedia
Subsystem (IMS) and presence services, a
compelling solution for enterprises and
service providers. Current voice services
which are delivered using Public Switched
Telephone <a href="../../a1/VoIP-Network-platform.asp">Networks</a> (PSTN) provide high
voice <a href="../a1/VoIP-Network-platform.asp">quality</a>, very high reliability (99.999 per
cent), carry critical services such as E911,
enable federal agencies with ability for lawful
intercept, all while offering an extremely high
level of security. For VoIP <a href="../../a1/VoIP-Network-platform.asp">networks</a> to
become a reality, both enterprises and
service providers must be able to ensure
that voice <a href="../a1/VoIP-Network-platform.asp"> networks</a> are able to deliver the
identical <a href="../a1/VoIP-Network-platform.asp">quality</a>, reliability, flexibility and
security to that of PSTN.<br />
<br />
With enterprises, carriers and cable
companies publicly committing to VoIP
deployments, security has quickly emerged
as one of the biggest barriers to the
successful deployment of VoIP. To securely
implement VoIP  <a href="../a1/VoIP-Network-platform.asp">networks </a> a proactive
approach and an understanding of the
differences between VoIP and traditional
data <a href="../a1/VoIP-Network-platform.asp"> networks </a>is required. This document
examines these differences, new types of
attacks, and provides a comprehensive
security architecture for VoIP  <a href="../a1/VoIP-Network-platform.asp">networks </a>in
the context of practical VoIP security
problems.<br />
<br />
<b>VoIP Security Requires a Different
Approach</b><br />
VoIP is not just another application running
on the top of the IP infrastructure. VoIP is a
complex service. Similar to existing PSTN
and Private Branch Exchange (PBX)
offerings, VoIP has its own business models
and features which are offered to the enduser.
Over the years, service providers and
PBX vendors have established their
respective brands as being synonymous
with high levels of reliability, <a href="../a1/VoIP-Network-platform.asp">quality</a> and
security and must preserve these attributes
in their VoIP offerings.<br />
<br />
VoIP characteristics include: high sensitivity
to <a href="../a1/VoIP-Network-platform.asp">quality of Service</a> (QoS) parameters, the
real-time nature of services, a wide range of
infrastructure devices, protocols and
applications, and interaction with the existing
phone  <a href="../a1/VoIP-Network-platform.asp">networks</a>. These characteristics
require different techniques and
methodologies that will support PSTN-level
security and reliability.<br />
<br />
<img src="images/VoIP_is_a_complex.jpg" /><br />
<br />
For example, in the data security realm,
common attacks, such as Denial of Service
(DoS), often result in email or computer
 <a href="../a1/VoIP-Network-platform.asp">networks</a> being unusable for several hours.
To meet the high reliability requirements of
VoIP  <a href="../a1/VoIP-Network-platform.asp">networks</a>, which are approaching
99.999 per cent or a total of less than five
minutes of downtime per year, any type of
attack would have to be stopped in a matter
of seconds. With such high reliability
requirements, VoIP <a href="../a1/VoIP-Network-platform.asp"> networks</a> must have a
security approach which enables an
automated, real-time response. Any attack
must be addressed before there can be any
service disruption.<br />
<br />
The high sensitivity of VoIP to QoS
parameters such as packet delay, packet
loss, and packet jitter makes most currently
available data security solutions inadequate.
Existing firewalls cannot efficiently handle
new VoIP protocols such as Session
Initiation Protocol (SIP) and a wide range of
vendor proprietary protocols since they relay
on dynamic port ranges and do not support
 <a href="../a1/VoIP-Network-platform.asp">Network</a> Address Translation (NAT) very
well. A new generation of the firewalls called
Session Border Controllers (SBC) is
addressing most of these problems.<br />
<br />
Most of the firewalls, Intrusion Detection
Systems (IDS), Intrusion Prevention
Systems (IPS) and similar security devices
rely on deep packet inspection techniques.

</p>

<p class="virtualpage3">

These techniques introduce delay and jitter
to the VoIP packet streams thus impacting
overall QoS. In VoIP world, maximum
packet delay is set to 150 ms (in some
cases higher) but the multi-layer nature of
security infrastructure could add significant
delays and jitter that would make the VoIP
services unusable.<br />
<br />
There is also the issue of balance between
encryption and QoS. Existing encryption
engines will introduce additional jitter and
delay that would be cumulative due to hopby-
hop encryption schemas foreseen to be
used by VoIP calls. As PSTN and VoIP
 <a href="../a1/VoIP-Network-platform.asp">networks </a>coexist media gateways that
provide internetworking between carrier's IP
 <a href="../a1/VoIP-Network-platform.asp">network</a> and TDM based PSTN  <a href="../a1/VoIP-Network-platform.asp">networks</a> will
be required. This could enable crossnetwork
security attacks which impact
existing PSTN  <a href="../a1/VoIP-Network-platform.asp">networks</a>.<br />
<br />
VoIP is a real-time service. All
communications are happening in real-time
and no information is stored anywhere on
the <a href="../a1/VoIP-Network-platform.asp"> network</a>. As result, any loss of
information cannot be recovered or
retransmitted. This makes VoIP services
very susceptible to worms and DoS attacks
that could very easily disrupt voice
communication.<br />
<br />
Finally, the complex nature of VoIP
infrastructure demands a different approach
to security. A VoIP <a href="../a1/VoIP-Network-platform.asp"> network</a> consists of a
wide range of components and applications
such as telephone handsets, conferencing
units,<a href="../a1/productos-cr2.asp">mobile</a> units, call processors/call
managers, gateways, routers, firewalls and
specialized protocols. As a result, a systemlevel
approach where security is built into all
the infrastructure layers and coordinated via
a centralized control center is required.<br />
<br />
<b>VoIP Security Threats</b><br />

For VoIP infrastructure and services to
function properly appropriate hardware,
operating systems, supporting services such
as Domain Name System (DNS), Dynamic
Host Configuration Protocol (DHCP) and
Authentication, Authorization and
Accounting (AAA), IP and VoIP specific
protocols such as SIP, H.323, Real-Time
Transport Protocol (RTP) and Transmission
Control Protocol (TCP)/User Datagram
Protocol (UDP) are required. A number of
VoIP applications such as call managers,
voice mail, SIP servers, call centers and
soft-switches are running on the top of this
infrastructure. In turn, these applications are
part of VoIP service offerings with
appropriate <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> mechanisms, large
number of features and binding Service
Level Agreements (SLA).<br />
<br />
<img src="images/Layers_of_VoIP.jpg" /><br />
<br />
Since VoIP security attributes and
characteristics are different than those of
existing data <a href="../a1/VoIP-Network-platform.asp"> networks</a> there are also
different types and categories of security
attacks specific to <a href="../a1/VoIP-Network-platform.asp"> VoIP networks</a>. VoIP
security threats can be categorized in many
ways and fall into four main categories:
attacks that aim at disrupting or VoIP service
availability, malicious activities the goal of
which is to compromise integrity of services,
Spam over IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a> (SPIT) and
eavesdropping.<br />
<br />
<b>Service Availability</b><br />
Service availability attacks are currently
viewed as the most significant VoIP security
threats to  <a href="../a1/VoIP-Network-platform.asp">VoIP networks</a>. This type of
attack has the potential to quickly impact
customers, resulting in lost revenues,
system downtime, lost productivity and
unplanned maintenance costs. Furthermore,
such attacks are a major concern for service
providers providing public services such E-
911, as even the smallest disruption could
have significant or even catastrophic
consequences.<br />
<br />
VoIP availability is the most important
component of any commercial VoIP offering.
If the availability of VoIP services is
impacted, service providers and enterprise
revenues could be affected. The very high
QoS levels required by any voice service,
Fig 2: Layers of  <a href="../a1/VoIP-Network-platform.asp">VoIP network</a>
A Proactive Approach to VoIP Security 4
amplifies the threat of attacks such as DoS,
viruses and worms. A generic worm attack
can very quickly cripple a  <a href="../a1/VoIP-Network-platform.asp">VoIP network</a>,
long before the data <a href="../a1/VoIP-Network-platform.asp"> network</a> is impacted,
since it has a much higher sensitivity to
QoS. What may cause a data <a href="../a1/VoIP-Network-platform.asp"> network</a> to
slow down temporarily has the potential to
rapidly knock out voice <a href="../a1/VoIP-Network-platform.asp"> networks</a>.
</p>

<p class="virtualpage3">
DoS, virus and worm-based threats will also
use VoIP specific protocols and VoIP
application vulnerabilities. These directed
threats will be much more difficult to detect
and stop in VoIP <a href="../a1/VoIP-Network-platform.asp">networks </a>since the existing
security devices do not have specific
knowledge of these threats and
vulnerabilities. Attacks will target critical
VoIP applications such as end-user phones
and soft-clients, call managers,
authentication servers and <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> applications.<br />
<br />
Other common scenarios which impact
service availability is the flooding of VoIP
components with signaling protocol packets
causing exhaustion of resources or DoS
attacks that exploits loop and spiral
implementation on a call manager. Open
source call manager software provides easy
access to potential attackers, enabling them
to create peer-to-peer attacks on
commercial soft switches and PBXs. For
example, attackers could have two or more
call managers continually forward a single
request message back and forth to each
other until resources on the targeted call
manager/softswitch are exhausted. This
method of attack can quickly affect a large
number of phones leaving them unable to
initiate or receive calls.<br />
<br />
A final area of concern is that in the nearterm
VoIP and PSTN <a href="../a1/VoIP-Network-platform.asp">networks</a> will co-exist.
To facilitate internetworking between PSTN
and VoIP <a href="../a1/VoIP-Network-platform.asp">networks</a>, media gateways are
required. Since placing of the calls from
VoIP<a href="../a1/VoIP-Network-platform.asp"> network </a>via PSTN requires interaction
between VoIP signaling protocols and PSTN
SS7 infrastructure new vectors of attack are
being introduced. Attackers now have new
opportunities for attacks on PSTN through
the VoIP <a href="../a1/VoIP-Network-platform.asp">network </a>and the media gateway.
Examples of these attacks include
Destination Unavailable (DUNA) or
Signaling Congestion (SCON) attacks.
<br /><br />


<b>Examples of VoIP Security
Vulnerabilities</b><br /><br />

<b>Services</b><br />

. Voicemail exploits<br />

. Dialing plan security (calls to 1-900
or other numbers)<br />

. Transferring to '9011' extension for
long distance exploits<br />
<br />
<b>Applications</b><br />

. Buffer overflows<br />

. Format-string exploits<br />

. Scripts<br />

. Password cracking<br />

. Overload (DoS, DDoS)<br />
<br />

<b>VoIP Protocols</b><br />

. Session tear-down<br />
. Impersonation<br />
. Session hijacking<br />
. SIP-SS7 boundary messages
tampering<br />
. Malformed messages<br />
. Loops and spirals<br />
. Overload (DoS, DDoS)<br />
<br />

<b>VoIP Supporting Services</b><br />

. DNS<br />

. SNMP<br />

. LDAP<br />

. Web servers<br />

. VPN<br />

. Email<br />

. SQL Database<br />
<br />
<b>VoIP OS and Networking</b><br />

. Buffer overflows<br />
. Format-string exploits<br />
. Scripts<br />
. Password cracking<br />
. Overload (DoS, DDoS)<br />
. ARP cache poisoning<br />
. WEP<br />
</p>

<p class="virtualpage3">
<b>Service Integrity</b><br />
Service integrity threats are focused on
compromising VoIP services through toll
fraud, identity theft and other fraudulent
acts. Service integrity threats have the
potential to impact service providers through
lost revenue and damage to their reputation,
while government organizations and
enterprises may be impacted by the leakage
of sensitive or proprietary information.
Individual consumers may also be impacted
as scams are developed which victimize
them through identity and service theft.<br />
<br />
As new, converged services such as IP TV
are being deployed, content theft has the
potential to become a major problem for
service providers. In this scenario, a hacker
could record the content of the broadcast
and then sell it illegally. Another possible
scenario is broadcast hijacking where a
potential attacker intercepts an IP TV
broadcast and re-broadcasts it to
unsuspecting users.<br />
<br />
VoIP services are offered with many
features such call ID, call forwarding, voice
mail and three-way calling which could be
used for toll fraud, identity theft and spam.
For example, a false identity in the form of a
false caller ID, voice mail or phone number
could be easily implemented by relatively
unsophisticated hackers.<br />
<br />
More sophisticated attackers could use
interception/modification attacks that include
conversation alternation, impersonation and
hijacking. Conversation alternation,
impersonation and hijacking includes
various modifications of any voice, video,
text and/or imaging data. First, attackers
would collect and translate VoIP information.
Then, the content of the conversation would
be altered in real-time in order to deliver
false and misleading information to a third
party. Finally, VoIP conversations would be
hijacked and the caller would be misled into
communicating with the attacker,
masquerading as a party to this call.<br />
<br />
A hacker could also commit toll fraud via a
VoIP phone that is registered using a stolen
or guessed user account and password.
Once this is accomplished, the hacker can
place phone calls at the victim's expense.<br />
<br />
<b>SPIT</b><br />
Spam has become a major concern in the
data security world as millions of unwanted
messages are sent around the world each
day. It is expected that SPIT will fill up
users' voicemail boxes just like email spam
does today.<br />
<br />
SPIT presents another potentially critical
threat to VoIP services and to existing PSTN
users. While there are advanced solutions
that address e-mail spam such as blacklists
and quarantines, combating SPIT is much
more difficult due to the real-time nature of
voice services. Also, the impact of having to
deal with many unsolicited phone calls
during the day and night may have a chilling
impact on the deployment of VoIP services.<br />
<br />
For example, a telemarketer interested in
sending out an unsolicited advertisement
could easily collect phone numbers for VoIP
services. Any of these phones could be
targeted. Then, the telemarketer would
create an audio ad and send it to all VoIP
users at the same time.<br />
<br />
<b>Eavesdropping</b><br />
Eavesdropping on signaling and media
paths enables attackers to obtain sensitive
business or personal information. Once the
information is collected and translated,
various man-in-the-middle attacks altering
the content of the conversation could be
launched. Examples of these attacks are
insertion and disruption, masquerading,
registration hijacking, impersonation and
replay.<br />
<br />
A simple example of the eavesdropping
threat could be the collection of VoIP
information included in packets and then the
translation of this information into plain
speech. In this scenario, calls related to
national security or financial information,
could be intercepted and provide third
parties with confidential information. In more
sophisticated scenarios, valid VoIP calls to a
financial institution could be intercepted and
then re-directed to a bogus bank
representative.<br />
<br />
<b>A Proactive Security Architecture for
VoIP</b><br />
With new challenges and types of attacks,
VoIP clearly requires a more sophisticated
approach to security than those currently
used to secure data <a href="../a1/VoIP-Network-platform.asp">networks</a>. Solutions
based on network-based devices and
signature-based applications simply are not
going to address the real-time nature and
complexity of <a href="../a1/VoIP-Network-platform.asp">VoIP networks</a>. A systembased
approach that combines <a href="../a1/VoIP-Network-platform.asp">network</a> and
host-based security devices and
applications with sophisticated, systemslevel
threat mitigation systems will be
required to efficiently protect the entire VoIP
infrastructure.<br />
<br />
In building a systems-level approach to VoIP
security, a unified VoIP specific security
infrastructure architecture consists of three
functional components: prevention,
protection and mitigation.<br />
<br />
<b>Prevention</b><br />
Prevention enables organizations to
proactively identify and fix VoIP-specific
vulnerabilities before they impact end-users.
A commonly used approach from the data
security world, vulnerability assessment
(VA) is particularly effective as a proactive
strategy. By performing a VoIP VA in the
lab, before any VoIP equipment and
applications are deployed, organizations are
able to verify vendor claims and identify
security flaws early in the deployment cycle.
Executing a VoIP VA of all components prior
to the commissioning of the VoIP
infrastructure is recommended. This
process enables the identification of security
vulnerabilities not revealed during earlier
testing. Once VoIP is deployed, periodic or,
where required, continuous vulnerability
assessments should become cornerstone of
an overall proactive VoIP security strategy.
Once security vulnerabilities are identified
they should be addressed by appropriate
actions such as patching, re-configuration
and <a href="../a1/VoIP-Network-platform.asp">network</a> tuning. These actions should
be clearly defined as part of the company's
overall security policy to provide a
framework for dealing with possible threats
to VoIP security.<br />
<br />

</p>
<p class="virtualpage3"><b>Protection</b><br />

Within the<a href="../a1/VoIP-Network-platform.asp"> VoIP network</a>, various security
architectures and solutions should be
deployed to protect VoIP services from
security threats during their life cycle. Any
security architectures and solutions
deployed must be "VoIP aware" so they do
not impact VoIP service <a href="../a1/VoIP-Network-platform.asp">quality</a> and
reliability. It is recommended to deploy a
multi-layer security infrastructure that
provides both perimeter as well as internal
<a href="../a1/VoIP-Network-platform.asp">network</a> protection. In most cases, it will
consists of a number of security devices and
host based applications to protect <a href="../a1/VoIP-Network-platform.asp">VoIP
networks</a> such as SBCs, <a href="../a1/VoIP-Network-platform.asp">VoIP Network</a>
Intrusion Prevention Systems (NIPS), VoIP
DoS defenses, <a href="../a1/VoIP-Network-platform.asp">VoIP Network</a> Intrusion
Detection Systems (IDS), Host IPSs,
Authenication, Authorization and Accounting
(AAA) servers, encryption engines and VoIP
anti-virus software. All the devices and
applications have to be coordinated via a
higher level application providing unified
view of the end-to-end VoIP infrastructure.
<br />
<br />
<img src="images/Security_Architecture.jpg" /><br />
<br />
<b>Mitigation</b><br />
It is already widely accepted that no matter
how good the prevention and/or protection in
place may be, sooner or later an attacker or
worm will successfully penetrate all the
defenses and wreak havoc on VoIP
infrastructure. To date, there have not been
many widely publicized VoIP security
attacks. However, as VoIP becomes more
mainstream, it is really a matter of when and
not if widespread attacks will occur. In
dealing with these attacks, VoIP presents a
unique challenge as IP services cannot rely
on a human-based response for mitigation,
as QoS will already be compromised.<br />
<br />
Currently, a combination of human
intervention and security management tools
are being used to mitigate the impact of
these attacks. As the VoIP market matures,
and VoIP-specific attacks become more
prevalent, these methods will not be
sufficient as <a href="../a1/VoIP-Network-platform.asp">VoIP networks </a>cannot tolerate
multi-hour or multi-day downtimes if they are
required to support 99.999 per cent
availability. Expect to see solutions emerge
which are designed to provide the real-time,
automated VoIP security mitigation solutions
needed to keep VoIP services running in the
presence of major security threats such as
SPIT, DoS or fast-spreading worms. Threatmitigation
systems should be able to
respond autonomously to the detected
security threats and keep their impact at the
levels where VoIP services can still function
albeit at lower QoS. While VoIP threat
mitigation systems are not currently
available, they will become a key part of the VoIP security infrastructure in the next two
to three years, and should be planned for.<br />
<br />
<b>Summary</b><br />
VoIP requires a different approach to
security which takes into account the unique
nature of telecommunications <a href="../a1/VoIP-Network-platform.asp">networks</a> and
how greatly VoIP differs from traditional data
security. The specific characteristics of VoIP
<a href="../a1/VoIP-Network-platform.asp">networks </a>combined with the mission-critical
importance of many voice applications
imposes strict requirements on security
applications. To effectively secure VoIP
<a href="../a1/VoIP-Network-platform.asp">networks</a>, organizations need to proactively
address security at three levels -
prevention, protection, and mitigation. By
taking a holistic approach to VoIP security,
enterprises, carriers and cable operators will
be able to preserve the attributes of <a href="../a1/VoIP-Network-platform.asp">quality</a>,
reliability, and security that we've come to
expect from the existing phone <a href="../a1/VoIP-Network-platform.asp">networks</a>.<br />
<br />

</p>


</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Proactive_Security_Approach.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../doc/Proactive_Security_Approach.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
