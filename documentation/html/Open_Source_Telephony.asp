<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
Open Source <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a><br />
The Evolving Role of Hardware as a Key
Enabler of Open Source <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a> in the
Business Market
            </h1> 
    <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">
<b>The Evolving Role of Hardware as a Key Enabler of Open Source
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a> in the Business Market</b><br />


<b><br /><a href="#">Introduction</a></b> <span class="credits"></span><br>
The business <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> market is undergoing radical transformation due to the advent of
disruptive technologies such as Internet Protocol and open source software. Traditional
PBX systems have dominated the enterprise market, while Key systems have been the
primary solution for small and medium sized businesses. IP-based alternatives are now
gaining acceptance, and open source-based <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> solutions are opening up new
markets for the functionality that previously required an expensive PBX system.<br />
<br />
Open source <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> solutions are largely software-based but require a hardware
component for PSTN connectivity. This White Paper addresses the drivers behind open
source <a href="../../a1/VoIP-Network-platform.asp">telephony</a> and how open source solutions are evolving. There is a continuing trend
for <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> functions to be software-based, but for now, functions such as PSTN
connectivity will be hardware-based. Sangoma Technologies is one such vendor, and this
White Paper focuses on its products and how it is enabling open source <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> for
businesses of all sizes.<br />
<br />
<b>Why Enterprises Are Migrating From TDM to IP</b><br />
In the enterprise and small business market, <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> has long been the domain of
equipment vendors, primarily for PBX or Key systems. Reliability has been the hallmark
of these systems, and businesses have come to rely on them for their voice connections to
the outside world, one of their most mission-critical functions. In the TDM world, voice
was a dedicated service, and <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> systems were built around this one application.
Until recently, phone systems were not integrated with data <a href="../../a1/VoIP-Network-platform.asp">networks</a>, and there were no
alternatives to TDM for voice.<br />
<br />
While these conditions have given rise to <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> systems that work very well for their
intended purpose, businesses have paid a high price. Aside from being costly, these
systems were proprietary and closed. Each vendor had their own technology and
competing systems were not compatible with each other. Vendors retained full control,
leaving customers totally dependent on them for fixes, enhancements and upgrades. In
short, acquisition costs were high, support was expensive and hard to find, and feature
sets were fixed.<br />
<br />
The advent of IP (Internet Protocol) has changed the business <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> market on many
levels. First and foremost, it offered an alternative to TDM for voice, and with it, IPbased
solutions for <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> systems. One of the reasons large enterprises are drawn to
IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> is the potential efficiency gained from combining the voice and data
functions in an organization. All businesses - both large and small - recognize the
economic benefits of IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>, especially in areas such as toll bypass, reduced
trunking costs and eliminating MAC costs - moves, adds and changes.<br />
<br />
By nature, IP is flexible, and has enabled the development of a wide variety of innovative
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> solutions that are gaining acceptance among businesses. It is now possible for
businesses to have the richness of a PBX feature set without paying PBX prices. Legacy
vendors have introduced less costly IP PBX systems to help their installed base migrate
to IP, as well as bring PBX functionality to a new set of customers they could not
previously reach with a TDM-based PBX.<br />
<br />
Similarly, service providers are now offering IP Centrex as a way for traditional Centrex
customers to get more for less. Carriers are also offering IP Centrex on a hosted basis to
smaller businesses that could not justify a full PBX system. Getting more features and
performance at a lower cost is attractive for any business, and these developments
indicate that IP technology has matured to the point where the days of the traditional
PBX are now numbered.<br />
<br />
<b>The Rise of Open Source <a href="../../a1/VoIP-Network-platform.asp">Telephony</a></b><br />
Most IP-based solutions have been targeted at the PBX market for a number of reasons.
The installed base is quite large, and these enterprises have been accustomed to investing
significant capital in high performance <a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems. Eventually, TDM PBX
deployments will transition to IP, but this process is expected to take several years.<br />
<br />
The enterprise PBX market is substantial, especially in terms of the revenue opportunity
for PBX vendors. However, there is another substantial portion of the business <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> market that does not use PBX systems. For these businesses the capital investment is not
justified, and less expensive TDM-based systems can provide the functionality they need.
IP-based alternatives exist for this market, but both vendors and carriers have yet to
develop strong channels to educate and support these businesses with IP.<br />
<br />
These conditions set the stage for open source <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>. Over the past two years, open
source software has gained considerable acceptance throughout the enterprise
environment and more recently has been applied to <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>. The inherent appeal of
open source is lower cost, and with the advent of PC-based PBX solutions, the
addressable market opportunity is substantial. There are millions of small businesses that
cannot afford a PBX but would certainly desire its feature set.<br />
<br />


</p>

<p class="virtualpage3">

Open source <a href="../../a1/VoIP-Network-platform.asp">telephony</a> is largely software-based, and is by nature less costly than
hardware-based PBX systems. Aside from cost, however, there are several other factors
that make open source attractive for business <a href="../../a1/VoIP-Network-platform.asp">telephony</a>. These are summarized in Table 1
below.<br />
<br />
<img src="images/Key_Features_Making.jpg" /><br />
<br />
Open source <a href="../../a1/VoIP-Network-platform.asp">telephony</a> solutions have two primary components - software and hardware.
The most widely deployed software platform is <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, and is the most strongly
associated with PC-based <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> solutions for business. In addition to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, SIPX,
FreeSwitch and Yate are notable open source <a href="../../a1/VoIP-Network-platform.asp">telephony</a> software platforms.<br />
<br />
As the open source community has become more focused on the telecom market, these
solutions have become more complete in their ability to meet the needs of businesses on
many fronts:<br />
<br />
. Feature support - voice mail, fax, conferencing, etc.<br />

. Operating system support - Linux, Windows, Unix, Solaris, etc.<br />

. Hardware interfaces - analog, digital, PRI, etc.<br />

. IP telephony vendor support - Nortel, Avaya, <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a>, Polycom, etc.<br />

. Protocol support - TDM, ISDN, SIP, H.232, IAX, MGCP, etc.<br />
<br />
In an all-IP world, <a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems could be completely software-based. Today,
however, businesses still need PSTN connectivity, and for that, a hardware component is
required. Traditionally that function was performed by a standalone media gateway,
which housed the DSPs necessary to enable carrier-grade VoIP. As PC-based processors
become more powerful, processing cards have recently been developed that can enable
<a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">real time</a> voice communication without costly DSPs. The leading providers of these DSPfree
processing cards for <a href="../../a1/VoIP-Network-platform.asp">telephony</a> are Sangoma, Digium and Varion. Combined with
open source software, this has led to the development of PC-based IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems,
where no external hardware is needed. For the first time, businesses had an alternative for
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> that was not based on costly, purpose-built hardware components.
<br />
<br />
Figure 1 below illustrates the basic components of an open source <a href="../../a1/VoIP-Network-platform.asp">telephony</a> system.<br />
<br />
<b>Figure 1 - Open Source <a href="../../a1/VoIP-Network-platform.asp">Telephony</a> Topography</b><br />
<br />
<img src="images/Open_Source_Telephony.jpg" /><br />
<br />
<br />

</p>

<p class="virtualpage3">
<b>How Open Source Telephony is Evolving</b><br />
The evolution from DSP-based media gateways to DSP-free processing cards is an
important one in the open source continuum, but is really just one of many. On a broader
scale, open source is really moving along a path where the starting point for telephony
was a system based on hardware-based components. These components were expensive,
closed and proprietary. As software expertise developed, and as IP technologies matured,
more and more hardware<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">-based fu</a>nctions have become software-based, and telephony
solutions have come to support a mix of both.<br />
<br />
At this time, PSTN connectivity still requires a hardware interface, and the PSTN is still
the dominant mode for voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. However, much of the PSTN voice processing is
software-based, including signaling, switching, conferencing, voice-mail handling and
compression. Only the functions that are very dependent on hardware are done on the
voice card itself. These include HDLC handling, voice channelization and sometimes
echo cancellation and voice compression.<br />
<br />
Many aspects of telephony are becoming software-based, and open source is continuing
to reshape what is possible in telephony, especially so long as PC processing power
continues to increase and costs continue to fall.<br />
<br />
One example of the mainstream embrace of the software model is Intel's Host Media
Processing software. General purpose computer platforms can use Intel's DSP-free
software to enable a wide range both voice and video applications such as IVR, unified
messaging, video mail and video caller ID. These tools are enabling innovative, lower
cost alternatives to hardware-based solutions, and increasingly, businesses will be less
dependent on traditional vendors for their <a href="../../a1/VoIP-Network-platform.asp">telephony</a> and networking solutions.<br />
<br />
The business IP telephony market has clearly progressed since the days of purpose-built
hardware for DSPs and media processing. Vendors such as Dialogic (now Intel),
BrookTrout (now Cantata), AudioCodes and NMS developed high performing<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp"> telephony</a> components that interfaced with the PC only for control functions, all the voice
processing being handled on board. Their technologies were proprietary and expensive,
being priced generally beyond the reach of PBX customers and being used mostly for
Interactive Voice Response and call center applications.<br />
<br />
PBX systems themselves have followed the same path, and the legacy telecom vendors
have long controlled the market. That began to change with the advent of IP PBXs, and
the entry of data vendors such as <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> into the market. The open source movement has
further accelerated the transition by introducing software-based PBX systems that bypass
the hardware PBX vendors altogether. In terms of components, the market has also seen
the rise of alternatives to purpose-built boards in the likes of Sangoma and Digium, who
can provide PTSN connectivity using hardware built on general purpose computing
platforms.<br />
<br />
New companies such as Fonality, with its low cost, do-it-yourself PBX model based on
open source software, are showing the way towards highly integrated and easy to use
PBX systems that never existed before. Open source has been the real catalyst behind this
trend, and we feel it is one of the most important indicators as to where IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> is
headed.<br />
<br />
Furthermore, we also view voice as the entry point for open source communications
platforms. Once IP telephony becomes proven in open source, we anticipate more
advanced voice applications such as text to speech and speech recognition. Building on
that, video applications and mobility via WiFi and WiMax are logical extensions, all of
which can be enabled with open source software.<br />
<br />
<b>Sangoma Technologies - Enablers of Open Source Telephony</b><br />
<br />
Sangoma Technologies has been in the data and voice hardware business for more than
20 years. This is a considerable history for any company in the IP telephony market, and
its pedigree is reflected in how it has approached open source PBX. Prior to open source,
Sangoma was - and remains - well established in the data world, primarily in the area of
WAN connectivity hardware.<br />
<br />
For the open source PBX market, Sangoma produces a family of PCI cards that support
both T1/E1 and analog environments. Ranging from 2 to 240 channels, these cards are
built around Sangoma's AFT architecture - Advanced Flexible Telecommunication. AFT
reflects its approach towards building an integrated family of similar, carrier grade cards
that take advantage of today's higher capacity processors. This allows Sangoma's cards
to handle some PSTN functions on board at low or no cost, hence reducing the workload
for voice communication on the host CPU.<br />
<br />

</p>

<p class="virtualpage3">
Our research indicates that Sangoma's long experience in the data hardware market has
translated well into the voice market. It understands the engineering issues related to
making high performance PCI interfaces work reliably, while providing the software
drivers for both voice and data environments. In addition, Sangoma has paid close
attention the nuances of open source software, and it understands the value its cards bring
in making open source PBX a viable alternative to hardware-based PBX solutions.<br />
<br />
In our view, it is the overall <a href="../a1/VoIP-Network-platform.asp">quality</a> of the AFT architecture that is making Sangoma
successful in the open source PBX market. Its experience results in products that work
reliably in all types of systems and software environments. There is no singular feature
that stands out, and that is perhaps its virtue, which should bode well as the market
becomes more competitive. Sangoma's key points of differentiation are summarized
below.<br />
<br />
. Very high level of compatibility with the many thousands of different<a href="../../a1/VoIP-Network-platform.asp"> motherbo</a>ard
and peripheral combinations for their entire range of AFT cards<br />

. Supports a wide range of PC operating systems - Linux, Windows, Unix, Solaris,
FreeBSD and OpenBSD<br />

. Support across multiple open source telephony platforms - <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, FreeSwitch,
Yate and others<br />

. Ability to support both voice-only and voice and data environments - Sangoma's
Wanpipe router enables the server to better manage mixed <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> by streaming off the
data, leaving the open source PBX platform free to focus its resources solely on voice<br />

. Design to the 2U form factor so that cards can be deployed in quantity on any PC
configuration<br />

. Hardware firmware is field-upgradeable - this provides critical reliability that
businesses need from their telephony systems<br />

. Carrier class echo cancellation that does not degrade as the scale increases - Sangoma
has partnered with Octasic for best-of-breed capability<br />

. Forward-thinking architecture - both multiple and single channel SS7 support for
Linux as well as Windows<br />

. Strong customer support - approachable, responsive and knowledgeable<br /><br />

<b>The Customer Experience</b><br />
Several of the strengths cited above have been validated through direct contact with
Sangoma customers. Most of its customers are resellers and distributors who develop
open source PBX solutions for direct use by business customers, either as their internal
system or in their call centers. They also build these solutions for service providers who
are offering hosted PBX solutions to their enterprise, small business and SOHO
customers.<br />
<br />
A common strength heard from customers is Sangoma's ability to deliver reliable, high
<a href="../a1/VoIP-Network-platform.asp">quality</a> connectivity across all sizes of deployment. Its boards "don't fail", "they just
work", and the voice <a href="../a1/VoIP-Network-platform.asp">quality</a> is "consistently good" for both large and small solutions. For
resellers focused on larger customers, Sangoma makes it easy for them to offer a scaled
down version, allowing them to sell into new markets and expand their business.
Conversely, Sangoma gives other resellers the confidence they need to go upmarket and
offer larger scale solutions for higher-end customers (typically 2 T1s or more).<br />
<br />
<i>"Sangoma's configuration tools are more complete than those of other vendors, and the
performance and reliability of their boards is better, as is the support. Another plus is
that its boards also work for data connections."</i><br />
<br />
<i>"They are really friendly, collaborative and competent, both commercially and
technically speaking. Regarding their support, we can say it is really good, because your
problem involving their hardware becomes immediately their problem. We think this is
one of the most important characteristics for a vendor of hardware."</i><br />
<br />
Dimitri Osler, Project Manager, Wildix<br />
<br />
"We're seeing a migration away from expensive, proprietary telephone systems.
Sangoma's boards are helping us meet this demand, for larger deployments with multiple
T1s as well as smaller systems."<br />
<br />
William Boehlke, CEO, Signate
</p>
<p class="virtualpage3"><b>Conclusions</b>
<br />
Open source has made significant advances in telephony, and its developer community
has proven that PC-based solutions can be viable alternatives to existing PBX systems.
These platforms are not perfect, but in the right hands, can deliver tremendous value and
performance for businesses of all sizes. As open source matures, standards will evolve,
and the solutions will have more checks and balances to ensure the reliability and <a href="../a1/VoIP-Network-platform.asp">quality</a>
that businesses need for larger scale deployment. PC processing power will continue to
improve as well, but for the time being, functions such as PSTN connectivity still need to
be hardware-based.<br />
<br />
In that regard, it is our view that Sangoma Technologies offers industry-leading solutions
that complement <a href="../../a1/VoIP-Network-platform.asp">all the m</a>ajor open source telephony platforms. At this point in the
evolution of open source, Sangoma has a key role to play as an enabler of reliable,
carrier-grade telephony. The company is setting the standard for performance, and until
software-based alternatives emerge that are on par or better, we see Sangoma maintaining
this position. Given its extensive history in the data connectivity world, and the much
shorter tenure of other vendors in this market, we have good reason to believe this will be
the case.<br />
<br />
J Arnold & Associates, an independent telecom consultancy, produced this White Paper.
The contents herein reflect the conclusions drawn based on general research about
Sangoma Technologies, as well as interviews with open source experts, and Sangoma
customers. For more information please contact us at: www.jarnoldassociates.com
</p>


</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Open_Source_Telephony.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/Open_Source_Telephony.asp"></a> <a href="../doc/Open_Source_Telephony.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
