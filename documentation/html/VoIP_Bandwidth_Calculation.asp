<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
VoIP Bandwidth Calculation<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
<b>Executive Summary</b><br />
Calculating how much bandwidth a Voice over IP call occupies can feel a bit like trying to answer the question; "How elastic is a piece of string?" However, armed with a basic understanding of the parts that make up the whole, the question becomes easier to understand. This White Paper examines the process that turns voice into Voice over IP.<br />
<br />
<b>Overview</b><br />
The amount of bandwidth required to carry voice over an IP <a href="../a1/VoIP-Network-platform.asp">network</a> is dependent upon a number of factors. Among the most important are:<br />
<br />
-Codec (coder/decoder) and sample period<br />
-IP header<br />
-Transmission medium<br />
-Silence suppression<br />
<br />
The codec determines the actual amount of bandwidth that the voice data will occupy. It also determines the rate at which the voice is sampled. The IP/UDP/RTP header can generally be thought of as a fixed overhead of 40 octets per packet, though on point-to-point links RTP header compression can reduce this to 2 to 4 octets (RFC 2508). The transmission medium, such as Ethernet, will add its own headers, checksums and spacers to the packet. Finally, some codecs employ silence suppression, which can reduce the required bandwidth by as much as 50 percent.<br />
<br />
<b>The Codec</b><br />

The conversion of the analogue waveform to a digital form is carried out by a codec. The codec samples the waveform at regular intervals and generates a value for each sample. These samples are typically taken 8,000 times a second. These individual values are accumulated for a fixed period to create a frame of data. A sample period of 20 ms is common. Some codecs use longer sample periods, such as 30 ms employed by G.723.1. Others use shorter periods, such as 10 ms employed by G.729a.<br />
<br />
The important characteristics of the codec are:<br />
<br />
The number of bits produced per second<br />
The sample period - this defines how often the samples are transmitted<br />
<br />
Together, these give us the size of the frame. For example, take a G.711 codec sampling at 20 ms. This generates 50 frames of data per second. G.711 transmits 64,000 bits per second so each frame will contain 64,000� 50 = 1,280 bits or 160 octets.<br />
<br />
<b>Frames and Packets</b><br />
Many IP phones simply place one frame of data in each packet. However, some place more than one frame in each packet. For example, the G.729a codec works with a 10 ms sample period and produces a very small frame (10 bytes). It is more efficient to place two frames in each packet. This decreases the packet transmission overhead without increasing the latency excessively.<br />
<br />
<b>Latency and Packet Overhead</b><br />
The choice of the number of frames per packet is a trade-off between two characteristics: latency and packet overhead.<br />
Long sample periods produce high latency, which can affect the perceived <a href="../a1/VoIP-Network-platform.asp">quality</a> of the call. Long delays make interactive conversations awkward, with the two parties often talking over each other. Based on this fact alone, the shorter the sample period, the better the perceived <a href="../a1/VoIP-Network-platform.asp">quality</a> of the call. However, there is a price to pay. The shorter the sample period, the smaller the frames and the more significant the packet headers become. For the smallest packets, well over half of the bandwidth used is taken up by the packet headers; clearly an undesirable case. See figure 1 below:<br />
<br />
<img src="images/IP_header_forms.jpg" />
</p>

<p class="virtualpage3">
<b>The IP Header</b><br />
The term 'IP header' is used to refer to the combined IP, UDP and RTP information placed in the packet. The payload generated by the codec is wrapped in successive layers of information in order to deliver it to its destination. These layers are:<br />
<br />
IP - Internet Protocol<br />
UDP - User Datagram Protocol<br />
RTP - Real-time Transport Protocol<br /><br />
RTP is the first, or innermost, layer added. This is 12 octets. RTP allows the samples to be reconstructed in the correct order and provides a mechanism for measuring delay and jitter.<br />
<br />
UDP adds 8 octets, and routes the data to the correct destination port. It is a connectionless protocol and does not provide any sequence information or guarantee of delivery.<br />
<br />
IP adds 20 octets, and is responsible for delivering the data to the destination host. It is connectionless and does not guarantee delivery or that packets will arrive in the same order they were sent.<br />
<br />
In total, the IP/UDP/RTP headers add a fixed 40 octets to the payload. With a sample period of 20 ms, the IP headers will generate an additional fixed 16 kbps to whatever codec is being used.<br />
<br />
The payload for the G.711 codec and 20 ms sample period calculated above is 160 octets, the IP header adds 40 octets. This means 200 octets, or 1,600 bits sent 50 times a second - result 80,000 bits per second. This is the bandwidth needed to transport the Voice over IP only, it does not take into account the physical transmission medium.<br />
<br />
There are other factors, which can reduce the overhead incurred by the IP headers, such as compressed RTP (cRTP). This can be implemented on point-to-point links and reduces the IP header from 40 to just 2 or 4 octets. Though this is not that common today, its use will become more widespread with it being implemented with 3G <a href="../a1/productos-cr2.asp">mobile</a> <a href="../../a1/VoIP-Network-platform.asp">networks</a>.<br />
<br />
<b>The Transmission Medium</b><br />
In order to travel through the IP <a href="../a1/VoIP-Network-platform.asp">network</a>, the IP packet is wrapped in another layer by the physical transmission medium. Most Voice over IP transmissions will probably start their journey over Ethernet, and parts of the core transmission <a href="../a1/VoIP-Network-platform.asp">network</a> are also likely to be Ethernet.<br />
<br />
Ethernet has a minimum payload size of 46 octets. Carrying IP packets with a fixed IP header of 40 means that the codec data must be at least 6 octets - typically not a problem. The Ethernet packet starts with an 8 octet preamble followed by a header made up of 14 octets defining the source and destination MAC addresses and the length. The payload is followed by a 4 octet CRC. Finally, the packets must be separated by a minimum 12 octet gap. The result is an additional Ethernet overhead of 38 octets.<br />
<br />
Ethernet adds a further 38 octets to our 200 octets of G.711 codec frame and IP header. Sent 50 times a second - result 95,200 bits per second, see example 1 below. This is the bandwidth needed to transmit Voice over IP over Ethernet.<br />
<br />
Transmission of IP over other mediums will result in different overhead calculations.<br />
<br />


</p>

<p class="virtualpage3">
<b>Voice over IP over Ethernet, Example 1: G.711</b><br />
<br />
<img src="images/G711_codec.jpg" /><br />
<br />
-Codec G.711 - 64 kbps, 20 ms sample period<br />
-1 frames per packet (20 ms)<br />
-Standard IP headers<br />
-Ethernet transmission medium<br />
<br />
One packet is sent every 20 ms, 50 packets per second. Payload is 64,000 � 50 = 1,280 bits (160 octets). Fixed IP overhead 40 octets, fixed Ethernet overhead 38 octets. Total size 238 octets. Bandwidth required is (160 + 40 + 38) x 50 x 8 = 95,200 kbps.<br />
<br />
<b>Voice over IP over Ethernet, Example 2: G729a</b><br /><br />

<img src="images/G729a.jpg" /><br />
<br />
Codec G.729a - 8 kbps, 10 ms sample period<br />
2 frames per packet (20 ms)<br />
Standard IP headers<br />
Ethernet transmission medium<br />
<br />
One packet is sent every 20 ms, 50 packets per second. Payload is 8,000 � 50 = 160 bits (20 octets). Fixed IP overhead 40 octets, fixed Ethernet overhead 38 octets. Total size 98 octets. Bandwidth required is (20 + 40 + 38) x 50 x 8 = 39,200 kbps.<br />
<br />

One packet is sent every 20 ms, 50 packets per second. Payload is 8,000 � 50 = 160 bits (20 octets). Fixed IP overhead 40 octets, fixed Ethernet overhead 38 octets. Total size 98 octets. Bandwidth required is (20 + 40 + 38) x 50 x 8 = 39,200 kbps
<b>Silence Suppression</b><br />
Certain codecs support silence suppression. Voice Activity Detection (VAD) suppresses the transmission of data during silence periods. As only one person normally speaks at a time, this can reduce the demand for bandwidth by as much as 50 percent. The receiving codec will normally generate comfort noise during the silence periods.<br />
<br />
<b>Additional Codec Data</b><br />
<br />
<img src="images/Additional_Codec_Data.jpg" /><br />
<br />
<b>Summary</b><br />
Although there are many factors that influence the amount of bandwidth required to transmit a voice call over an IP <a href="../a1/VoIP-Network-platform.asp">network</a>, by approaching the problem one element at a time the final calculation becomes relatively simple. Other factors may influence the actual bandwidth used, such as RTP header compression, silence suppression and other techniques still under development.<br />
<br />

</p>



</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/VoIP_Bandwidth_Calculation.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/3com-solutions-strategies.as"></a>  <a href="../doc/VoIP_Bandwidth_Calculation.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
