<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1><a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> Billing Platform</h1> 
      <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
Cyneric offers a complete VoIP <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> solution customized to your business model, fully integrated with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
-	Full integration with production <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a><br />
-	<a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">Real time</a> customer definition, authentication, accounting<br />
-	Centralized operations : customers, rates, routing<br />
-	Virtual DID, Call forwarding, web callback, sms callback, voicemail, fax applications, etc<br />
<br />
<b><a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> <a href="../../a1/VoIP-Billing-Platform.asp">Billing</a> � Cyneric integration</b><br />
<br />
A)	Customer Authentication / Accounting<br />
<br />
Cyneric Platform can integrate with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> internally. Based on this interconnection, our <a href="../../a1/VoIP-Billing-Platform.asp">billing</a> platform will permit:<br />
<br />
-	Define customers in <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">real time</a> : no need to define customer or access numbers in two different platforms. &nbsp;&nbsp;You define the customer in our <a href="../../a1/VoIP-Billing-Platform.asp">billing</a>, and customer will be defined in <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> box automatically<br />

-	Pre-paid Customers: calls arriving to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, will be authenticated by Cyneric voip <a href="../../a1/VoIP-Billing-Platform.asp">billing</a>. Authentication is &nbsp;&nbsp;done in real-time, checking all the customer parameters<br />

-	Post-paid customers: you can define post-paid customers with credit limit. When the limit is reached, call &nbsp;&nbsp;will be rejected.<br />

-	Accounting : All cdr�s will be processed by our rate engine. Customer will be able to check their calls, using<br />
&nbsp;&nbsp;our web portal.<br /><br />


B)	Routing<br />
<br />
Cyneric Platform can define automatically routing inside <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. Using Cyneric Voip Platform, you will be able to handle routing through our interface<br /><br />
-	Define routing per customer or generic<br />
-	Define termination carriers to know your cost, and your profit<br />
-	Define alternative carriers per country code, with preferences<br />
-	Define Access numbers for customers, voice mail numbers, fax numbers, DID routing, call forwarding�all <br />
&nbsp;&nbsp;from one place<br />
<br />
C)	Voip Network � <a href="../../a1/VoIP-Network-platform.asp">Quality</a> of service<br />
<br />


Cyneric <a href="../../a1/VoIP-Network-platform.asp">quality</a> control Software, is fully integrated to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> applications. With this tool, you will able to control you traffic in <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">real time</a>.<br />
<br />

-	Real-time web based traffic monitor (ASR, ACD, PDD)<br />
-	Mobile enabled QoS tools<br />
-	Alert definition (email, sms)<br />
-	Volume, ASR, ACD Graphs<br />
<br />
D)	Calling Card Applications<br />
<br />


Cyneric Calling Card module, can be fully integrated with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. From pin lot definition, to profits reports, the system will give you the flexibility you need<br />
<br />

-	Calling Card lot creation : pins, maintenance fee, first use charge, minute rounding, rate table, etc<br />

-	Templates with lot definition, to easily create more lots<br />
-	Reports with first usage, top destinations, traffic reports<br />
-	Profit & Loss report, carrier termination<br />
<br />

E)	Call Shop Applications<br /><br />


Using Cyneric Voip <a href="../../a1/VoIP-Billing-Platform.asp">Billing</a> and <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, you can have a complete solution for call shops.<br /><br />


-	Cabin boot definition<br />
-	<a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">Real time</a> operations<br />
-	Web interface to handle rate tables, cabins, tickets and more<br />
-	Reports per day, operator, carrier, cabin<br />
-	Profit and loss reports<br />
-	Different access levels, to manage permission for operators<br />
-	Remote login to control call shop operation<br />


</p>


</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf" target="_blank"></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../pdf/Asterisk_billing.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="../doc/Asterisk_billing.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
