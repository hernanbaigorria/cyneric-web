<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="735" border="0" cellspacing="0">
  <tr>
    <td width="201"  valign="top" scope="col">  <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="530" valign="top" scope="col"><h1>
The Five Critical Considerations for
Successful IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a> Deployments<br />
            </h1><div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a>
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
<b><a href="#">Introduction</a></b> <span class="credits"></span><br>
Based on its experience of deploying over 16,000 IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> systems around the world, 3Com can
point to three areas of business activities on which the technology can have significant impact:<br />
<br />
. Cost control-<a href="../a1/VoIP-Network-platform.asp">network</a> convergence reduces cabling costs; use of the "pass-thru" port of 3Com IP
phones can minimize Ethernet port counts at the LAN switch. Users handle their own moves,
adds, and changes, reducing the expense of service provider visits<br /><br />

. User productivity-workers at home, in branch offices, and at remote operations can fully participate
in business activities with four or five-digit e<a href="../../a1/VoIP-Network-platform.asp">xtensions</a> and centralized voicemail services<br />
<br />

. Customer interactions-customer call completions can increase with advanced IP telephony applications
capable of delivering services such as voice mail as e-mail, find me/follow me, interactive
voice response, and call announcement-not to mention support over the IP wide area <a href="../a1/VoIP-Network-platform.asp">network</a>
(WAN) for call center agents distributed across the business<br /><br />
2005 will be a watershed year for the IP telephony industry. For the first time, more IP telephony lines
will ship than digital <a href="../../a1/VoIP-Network-platform.asp">telephony</a> lines1. Even with this market momentum, there are risks and challenges in
effectively choosing a competent supplier and a solution that meets business needs. This paper presents
five key factors to consider when evaluating, selecting, and implementing an IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> solution:<br />
<br />
1. Carrier-class availability<br />

2. Audio <a href="../a1/VoIP-Network-platform.asp">quality</a><br />

3. Open, extensible product architecture and standards<br />

4. Convergence applications<br />

5. Vendor strength in <a href="../a1/VoIP-Network-platform.asp">network</a> convergence (voice and data)<br /><br />
Companies considering these factors in their decision cycles are better positioned to create a
competitive advantage from the way their employees interact with each other and with customers.<br />
<br />
<b><br />Consideration #1: Carrier-Class Availability</b> <span class="credits"></span><br>
Availability is the probability that service is
available when requested. Carrier-class
performance generally means greater than
five 9's or 99.999%-a maximum of about
five minutes of downtime per year. IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> vendors should be able to provide
metrics of the performance of their software
systems including an estimate of availability.
Customers should expect a vendor to demonstrate
how a <a href="../a1/VoIP-Network-platform.asp">network</a> design uses both
active and passive redundancy to achieve
the availability metric, and how a recovery
might occur in the event of a failure of the
IP <a href="../a1/VoIP-Network-platform.asp">network</a>, public switched telephone
<a href="../a1/VoIP-Network-platform.asp">network</a> (PSTN), or call controller device.<br />
<br />
Availability of end point stations should
be considered also. Their vulnerability to
failure or power outages can be mitigated by
devices that support IEEE 802.3af Power
over Ethernet (PoE). 3Com IP phones with
PoE support, for instance, provide a degree
of availability over and above server
performance and eliminate the need for
power bricks (transformers) attached to the IP phone. Additionally, the presence of
"pass-thru" Ethernet ports-when connected
to laptops <a href="../../a1/VoIP-Network-platform.asp">with batt</a>ery-support- strengthens
computing and telephony availability in the
event of power failure. Of course, the servers
and switches should be on an uninterrupted
power supply (UPS), too. These devices can
ensure no single point of failure, enable
hours of business continuity, and provide for
stable turndowns during sustained power
outages such as those that might accompany
natural disasters.<br />
<br />
It shouldn't be surprising that availability is
first on the list of considerations. It is a truly
critical concern. If a solution gets high marks
in all other categories except this one, that
solution should be disqualified. The ability
to pick up the handset of a telephone, get a
dial tone, and place a call is an expectation
resulting from over a hundred years of
successful <a href="../../a1/VoIP-Network-platform.asp">telephony</a> and the demands of
business obligations. It has to be there.
And it has to be there all the time.</p>


<p class="virtualpage3">
<b>Consideration #2: Audio <a href="../a1/VoIP-Network-platform.asp">quality</a></b> <span class="credits"></span><br>
Audio <a href="../a1/VoIP-Network-platform.asp">quality</a> ranks high in customer
satisfaction indices. If users get frustrated
with audio <a href="../a1/VoIP-Network-platform.asp">quality</a>-are frequently asking
customers to repeat themselves, or are disadvantaged
by being "harder to work with"-
complaints will overshadow good intentions
and even otherwise excellent customer service.<br />
<br />
Many factors determine audio <a href="../a1/VoIP-Network-platform.asp">quality</a>, including
the physical plant, <a href="../a1/VoIP-Network-platform.asp">network</a> design,
operations, and end devices. For modern
communications, the physical integrity of
the wiring, magnetic interference, and
electrical noise are "table stakes". Practices
for detecting and mitigating wiring issues
are well documented, easily isolated, and
inexpensively handled.<br />
<br />
More common and more difficult to manage
are the issues in <a href="../a1/VoIP-Network-platform.asp">network</a> design and operations.
Here the elements affecting audio
<a href="../a1/VoIP-Network-platform.asp">quality</a> include voice packet prioritization
settings in LAN switches, configuration of
voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> on a virtual local area <a href="../a1/VoIP-Network-platform.asp">network</a>
(VLAN), presence of echo cancellation mechanisms,
and the physical distance between
<a href="../a1/VoIP-Network-platform.asp">network</a> endpoints. To minimize the administration
of these factors, 3Com LAN switches
can detect the presence of a 3Com IP phone
on a switch port and automatically configure
the LAN infrastructure for 802.1p packet
prioritization service. The alternative solution
might require an investment in time and engineering
effort that would likely remove
resources from more critical business functions.<br />
<br />
<img src="images/f1S_Sampling_rates.jpg" /><br />
<br />
Unlike legacy digital telephony systems, IP
<a href="../../a1/VoIP-Network-platform.asp">networks</a> are not generally concerned with
the bandwidth assigned to any one conversation.
This capability lets wideband phones
improve the fidelity of audio-enabling
frequencies higher than 3.3 kHz and as high
as 7 kHz. In fact, as shown in Figure 1,
audio <a href="../a1/VoIP-Network-platform.asp">quality</a> is related to sampling rate.
DVD-<a href="../a1/VoIP-Network-platform.asp">quality</a> audio is sampled at 192 kHz,
CD-<a href="../a1/VoIP-Network-platform.asp">quality</a> audio is sampled at 44,100 Hz,
and classic telephony implementations use
8,000 Hz. Higher sampling rates enable
harmonics that heighten satisfaction with
"music on hold" and enhance perceived
audio <a href="../a1/VoIP-Network-platform.asp">quality</a>.<br />
<br />
For hearing-disabled or older employees,
wideband telephones may offer less frustration,
fewer misunderstandings and repeated
words or phrases, and higher productivity.
The standards for wideband audio that are
now appearing in <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> products are
embodied in the <a href="../a1/VoIP-Network-platform.asp">quality</a> of telephone microphones,
speakers, and sampling rates of the
audio devices. The 3Com 3102 IP phone was
among the first to market devices designed
to maximize audio <a href="../a1/VoIP-Network-platform.asp">quality</a> in these ways.<br />
<br />

</p>



<p class="virtualpage3">
Audio compression is also important in
deploying effective IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems.
Equipment should support industry standards
such as G.711 ADPCM (Adaptive Differential
Pulse Code Modulation) or G.729 compression.
Standard methods for compression should
not be left to the WAN routers. For better
application performance and to help remote
users more fully participate in end-to-end
control of the session bandwidth, compression
control should reside at the IP phone.<br />
<br />
Additionally, the ability to view and adjust
comprehensive aspects of a converged
network's operation through an enterprise
<a href="../a1/VoIP-Network-platform.asp">network</a> management application is critical
to <a href="../a1/VoIP-Network-platform.asp">network</a> performance, and by logical
extension, audio <a href="../a1/VoIP-Network-platform.asp">quality</a>. Comprehensive
management applications should provide
status and operations data on each <a href="../a1/VoIP-Network-platform.asp">network</a>
component, including the IP<a href="../../a1/VoIP-Network-platform.asp"> telephony</a> infrastructure (gateways, call controller, and
IP phones). Such tools for effective management
of employee and customer experiences
should include <a href="../a1/VoIP-Network-platform.asp">network</a> performance reports
and engineering analyses of audio <a href="../a1/VoIP-Network-platform.asp">quality</a>,
such as packet loss by route. Equipped with
these resources, staff can respond rapidly to
reports of jitter or ineffective echo cancellation.<br />
<br />
Each of these mechanisms for optimizing audio
<a href="../a1/VoIP-Network-platform.asp">quality</a> should be considered, but only after
specialized tools and highly qualified technical
experts have developed a thorough analysis
of the WAN's IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> readiness. A thirdparty
consultant or the professional services
organization of a global vendor can provide
an objective report card and roadmap that
balances economics and audio <a href="../a1/VoIP-Network-platform.asp">quality</a>.

<br />
<br />

<b>Consideration #3: Open, Extensible Product
Architecture and Standards</b> <span class="credits"></span><br>
The open and flexible architecture required
for IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> goes beyond dial tone. It
extends from <a href="../a1/VoIP-Network-platform.asp">network</a> devices through
enterprise-wide IT infrastructures to applications
that directly impact user activities.
Businesses need an over-arching architecture
that brings together, within a shared, standards-
based and scalable architecture, IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> and its related services-IP
messaging, IP conferencing, and IP contact
center. Support for common authentication,
presence, location and privacy services across
a portfolio of such convergence applications
unifies administration, policy management,
and configuration requirements for a host of
application services. Even support for
popular enterprise IT infrastructures such as
Lightweight Directory Access Protocol (LDAP), used for enterprise directory services or
integration with the enterprise authentication
service to enable single sign-on for both
communications and enterprise applications,
can balance effective security, user productivity,
and IT support costs (a major use of
IT support time and effort is responding to
"forgotten password" help requests).<br />
<br />
There are an estimated 945 million Internet
users around the world today, making IP the
world's most popular protocol and its most
popular standard. Clearly, IP's open architecture
and widespread support are promoting
the rapid pace of communications innovation.
Together, they have given organizations the
freedom to choose from among a wide variety
of vendors.<br />
<br />
<img src="images/f2_Four_phases_of_convergence.jpg" /><br />
<br />
Achieving Complete Enterprise Convergence<br />
<br />
Depending on business goals, the use of an
open and scalable communications architecture
can promote innovation and deliver the
freedom of multi-vendor <a href="../a1/VoIP-Network-platform.asp">network</a> options. In
practice, however, a business' choices may be
limited by earlier decisions. Therefore, the
goal should be to both maximize the integrity
of previous investments as well as achieve
the comprehensive business benefits of
convergence. 3Com experience shows that a
four-phased approach is often the path to a
complete transformation of business communications
for lower cost, higher productivity,
and stronger customer interactions. These
four phases include:<br />
<br />
1. <a href="../a1/VoIP-Network-platform.asp">Network</a> Convergence<br />

2. IT Infrastructure Convergence<br />

3. Convergence Applications<br />

4. User Experience Convergence<br />
<br />
As shown in Figure 2, convergence permeates
multiple layers of a communications
infrastructure. Beginning with <a href="../a1/VoIP-Network-platform.asp">network</a>
convergence, organizations can deploy
switches and routers that support Power
over Ethernet and <a href="../a1/VoIP-Network-platform.asp">quality of Service</a> so that
real-time packetized voice can co-exist on a
LAN infrastructure and within an IP WAN.
As part of a genuine movement toward an
integrated architecture, IT must also develop
a convergence layer which includes shared
call control, authentication (how many logins
do you really want?), privacy, location, and
presence management services. IT must also
provide media services to ensure <a href="../a1/VoIP-Network-platform.asp">quality</a> of
service, user satisfaction, and cost control.
Whether these are video- or speech-driven,
they involve deployment of incremental and
specialized application-sensitive hardware to
support activities such as conferencing.<br />
<br />


</p>



<p class="virtualpage3">
Relying on the common services of the IT
convergence infrastructure, a Convergence
Applications layer-including IP telephony,
IP messaging, IP conferencing, IP contact
center, personal assistants, and self-service
applications-extends the advantages of
convergence throughout an enterprise. For
example, the value of knowing that someone
is online can be enhanced by the knowledge
that the person being sought is also off-hook
in a conversation with someone else. With
this type of information, voice mails can be
reduced and callers can use their time more
productively.<br />
<br />
In many businesses, calling patterns follow
the workflow of enterprise applications such
as Customer Relationship Management or
Enterprise Resource Planning. In these environments,
it may be advantageous to integrate
more closely convergence applications with
enterprise applications to support customers,
partners, and employees connecting with the
business via its portal layer. Convergence
services can facilitate access for <a href="../a1/productos-cr2.asp">mobile</a> or
handicapped users and for workers with
bandwidth contraints.<br />
<br />
Security and management services pervade
all phases of an integrated converged business
structure. Their common implementation
can balance control and productivity. The
applications builder referenced in Figure 2
adds customization capabilities to this
shared structure, enabling convergence
applications to be embedded into business
processes and their supporting systems, into
enterprise applications, and into portals. The
builder can also create new applications that
combine specific elements of each business
process layer.<br />
<br />
For similar deployment flexibility, the availability
of well-understood management
procedures-such as Simple <a href="../a1/VoIP-Network-platform.asp">Network</a>
Management Protocol (SNMP) for reporting
device health-lets users select among
various SNMP management applications
such as HP OpenView or 3Com Enterprise
Management System (EMS). To avoid
attempting session setups with devices that
are in use or out of service, Java Database
Connectors (JDBC) can be deployed to
publish a device's status to the database
system and the call control function.<br />
<br />
In addition, both Voice eXtensible Markup
Language (VXML) and its sister language
Call Center XML (CCXML) are XML-based
languages for creating and managing speech
dialogs, including call control functions
such as multi-party conferencing and
outbound calling. These mechanisms enable
advanced services for the personalized navigation
of interactive voice response systems,
auto attendants, and the like.
<br />
<br />
<br />

<b>Considerati<a href="../../a1/VoIP-Network-platform.asp">on #4: Co</a>nvergence Applications</b> <span class="credits"></span><br>
IP telephony is the catalyst for a host of new
applications for both central and remote
users. Since these new applications can help
justify a rapid IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> investment, they
are an important consideration in the selection
process. At a minimum, the following
applications should be available within most
effective organizations:<br />
<br />
. An IP telephony PC application for mobileuser
call support that works with leading
operating systems including Windows,
Mac OS, and Linux.<br />

. Call management services for both the inoffice
knowledge worker and the <a href="../a1/productos-cr2.asp">mobile</a>
worker, allowing simplified outbound
dialing, enterprise applications-coordinated
screen pops to the desktop, call
screening, and call optioning. Extensive
call logs for analysis and user communications
management should be available also.<br />

. IP messaging to deliver a full-featured
voicemail system that can be managed
centrally, provide the ability to have
messages forwarded among all users
within the organization, and support
VPIM networking. This application should
let workers receive, manage, and send email,
voice mail, and faxes from a single
inbox and enable access to all message
types from a desktop e-mail application or
via a telephone interface. It should also
support a mix of media types to support
capabilities such as voice message responseto an e-mail. Find me/follow me services
can add to the productivity value of IP
messaging solutions, allowing the system
to 1) locate a user based on user-defined
caller types-for instance, "I am not in the
office, press one to have my agent locate
me"; 2) attempt to reach the user in at least
three locations-e.g., cell phone, home
phone, alternate office; and 3) be configurable
by caller ID information.<br />

Believing in the value of sending
employees in a time sensitive framework
encouraging messages such as scheduled
voice mail from the division president,
many executives see a link between
communications and performance. Being
able to easily create distribution lists and
record and schedule messages across a
geographically-diverse business unit is a
good business practice.<br />

. IP conferencing for instant messaging, audio
conferencing, video conferencing, and
document sharing. Visual collaboration is
an increasingly important mode of work
and customer collaboration. Conferencing
a third person into an existing call should
be, and can be, as easy as click and drag.<br />

. IP contact center software to control
customer interactions by leveraging IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems with industry-leading
call routing, management, and <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a>
capabilities from vendors such as Aspect,
Genesis, and Siebel.<br />
<br />

Minimum performance is never enough for
creating competitive advantage. In addition
to this list of enterprise-wide convergence
applications, leading enterprises will seek to
benefit from additional services, such as:<br />
<br />

. Centralized management of LAN, WAN,
and IP telephony products<br />

. Configuration and administration of all
applications across a common call control
function<br />

. Branch office survivability2 strategies for
enabling distributed call control and backup that is instantly engaged in the event of
call controller outage, IP <a href="../a1/VoIP-Network-platform.asp">network</a> failure,
or PSTN failure<br />

. Call recording for compliance with
industry oversight management<br />
<br />

Convergence applications can enable new
levels of user productivity and stronger
customer interactions. In fact, they should
become a standard component of IP telephony
products and services and<a href="../../a1/VoIP-Network-platform.asp"> are gath</a>ering
momentum as the next enabler of strong
business performance.</p>


<p class="virtualpage3">

<b>Consideration #5: Vendor Strength in <a href="../a1/VoIP-Network-platform.asp">Network</a>
Convergence (Data and Voice)</b> <span class="credits"></span><br>
Today's IP telephony solutions need to be
integrated with telecommunications infrastructures
(telephone interfaces, PSTN
interface cards, voicemail administration,
audio <a href="../a1/VoIP-Network-platform.asp">quality</a> components, etc.) as well as
with the data <a href="../a1/VoIP-Network-platform.asp">network</a> (switching, IP routing,
configuration management, bandwidth planning,
etc.). Consequently, it is essential that
a vendor have proven expertise in both data
networks and IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> systems-and be
able to demonstrate this integrated value.<br />
<br />
For example, effective <a href="../a1/VoIP-Network-platform.asp">network</a> convergence
that enables functionality across both the IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> application environment and the
data <a href="../a1/VoIP-Network-platform.asp">network</a> requires:<br />
<br />
. The LAN be able to automatically detect IP
phones and enable IEEE 802.2q prioritization
service within the LAN switching
infrastructure<br />
<br />
. IP phones support "pass-thru" Ethernet
service and handle 802.1af PoE. This functionality
can minimize total port counts and
allow laptop users connecting through these
Ethernet ports to sustain service in the
event of power failure, provided switches,
routers, and servers are on a UPS device<br />
<br />
. The LAN and IP telephony applications
work together to enable E911 emergency
management support. For example, by
linking the Media Access Control (MAC)
address of an IP phone to its IP address
and extension number, the emergency
resource locator file can provide required
information<br />
<br />
. The enterprise management application
present an integrated perspective on
system operations, administration, and
configuration<br />
<br />

These interworking features simplify the job
of the <a href="../a1/VoIP-Network-platform.asp">network</a> manager and communications
staff and reduce the time to implement IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a>. However, this technology integration
is probably easier in most organizations
than integrating the voice and data teams of
the company. That's where the convergence
experience of the vendor can be invaluable.
Working with professional and knowledgeable
convergence experts who can speak
"telecom" to the voice department, and
speak "data" to the data networking team,
is often more important than data vendors
or voice vendors are willing to admit.<br />
<br />
Vendor experience and products with proven
suitability for both <a href="../a1/VoIP-Network-platform.asp">network</a> convergence
and IP telephony reduce risk to the business
and increase the speed with which the
corporation is able to capture the benefits of
advanced business solutions.<br />
<br />
<b>Summary: Choosing the
Company You Keep</b> <span class="credits"></span><br>
Businesses today have choices they may not have had even a few short years ago. Open,
standards-based convergence solutions are now available to provide the productivity and
cost savings often more difficult to achieve with proporietary, hardware-based systems.
Vendors with a proven track record in both data and voice networking now offer enterprises
the expertise needed for a smooth transition to secure converged voice and data communications.
Together, standards support and vendor experience enable an overlay solution that can leverage
existing systems and provide the immediate advantages of the latest converged technology-
secure, feature-rich communications, end-to-end system management, and increased support
for <a href="../a1/productos-cr2.asp">mobile</a> workers and geographically distributed organizations.<br />
<br />
After critical consideration of the issues involved in successfully deploying IP telephony and
establishing an effective converged infrastructure, enterprises will realize that 3Com is their
best choice now and the company they will want to keep as their communications needs
evolve. 3Com solutions deliver<br />
<br />
. Carrier-class availability<br />
. Exceptinal audio <a href="../a1/VoIP-Network-platform.asp">quality</a><br />
. Open, extensible product architecture and standards<br />
. Robust converged applications<br />
. Vendor strength in <a href="../a1/VoIP-Network-platform.asp">network</a> convergence<br />
<br />
There is no better time to advance your business goals with a next generation communications
architecture that can lower cost, increase end-user productivity, and strengthen customer
interactions. You can improve communications with your customers, partners, employees,
and shareholders. 3Com can help. We're changing the way business speaks.<br />
<br />
3Com has been a major contributor to the
convergence industry since the introduction
in 1998 of the world's first IP-PBX, the NBX�
solution. In 1999, the company developed
an architecture for a distributed softswitch
for AT&T and brought the first commerciallydeployed
carrier softswitch to market. The
3Com VCXT, introduced in 2003, offers the
world's first convergence applications suite.
Transforming business through innovation is
not new to 3Com. The company holds over
900 patents and is the market leader in IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> for small to medium enterprises.
3Com operates in over 45 countries and has
1,900 employees.</p>




</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/3Com_The_Five_Critical_Considerations_for_Successful_IP_Telephony_Deployment.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/The_Five_Critical_Considerations.asp"></a><a href="../doc/3Com_The_Five_Critical_Considerations_for_Successful_IP_Telephony_Deployment.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
