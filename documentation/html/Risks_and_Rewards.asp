<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
Risks and Rewards<br />
Strategies for Migrating Corporate
Voice Traffic to the Data <a href="../../a1/VoIP-Network-platform.asp">Network</a>            </h1> 
    <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> 
<a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a> </div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">
<b><a href="#">Executive summary</a></b> <span class="credits"></span><br>
With the rapid changes taking place in communications today, business decision-makers face both
compelling opportunities and potentially costly pitfalls. The issue of "convergence" - whereby
voice, fax, data and multimedia <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> are transmitted over a single multi-purpose  <a href="../../a1/VoIP-Network-platform.asp">network</a> - is a
particularly sensitive one. The business and technological advantages of combining a company's
various types of communications over a common infrastructure are appealing.<b> These advantages
include: 1) lower recurring transmission charges, 2) reduced long-term  <a href="../../a1/VoIP-Network-platform.asp">network</a> ownership
costs, and 3) the ability to deploy powerful voice-enabled applications for collaboration,
customer service and supply-chain integration.</b><br />
<br />
On the downside, technology professionals are concerned with the <a href="../a1/VoIP-Network-platform.asp">quality</a> of voice calls on the
data  <a href="../../a1/VoIP-Network-platform.asp">network</a>, the stability of voice-over-IP (VoIP) solutions, and the consequences of being
prematurely "locked in" to a given vendor's architecture. A lack of expertise and experience with
VoIP technology is also preventing many organizations from taking even the most tentative first
steps into the world of converged networking.<br />
<br />
Fortunately, with the right strategy and the right technical architecture, business and IT
decision-makers don't have to postpone their exploration of VoIP indefinitely. <b>By using an
intelligent multi-path gateway switch that links the PBX, the data  <a href="../../a1/VoIP-Network-platform.asp">network</a> and the public
switched telephone  <a href="../../a1/VoIP-Network-platform.asp">network</a> (PSTN), companies can effectively "hedge their bets" even as
they move ahead with their initial VoIP deployments.</b> This type of intelligent switching solution
allows organizations to enjoy the benefits of converged communications without financial risk or
organizational disruption.<br />
<br />
<b>The Rewards of Voice-over-IP</b><br />
While  <a href="../../a1/VoIP-Network-platform.asp">network</a> managers may debate about the hows and whens of  <a href="../../a1/VoIP-Network-platform.asp">network</a> convergence, there is
complete consensus on the whys. In fact, there is nothing speculative about these core benefits;
they are already being experienced by early adopters of voice-over-IP, voice-over-frame and other
convergence technologies.<br />
<br />
In general, these benefits can be classified into three main categories:<br />
<br />
<b>1) Lower recurring transmission charges</b>
By directing voice calls over the corporate data  <a href="../../a1/VoIP-Network-platform.asp">network</a>, rather than through a carrier,
companies can significantly reduce their monthly phone bills. These savings are obviously
dependent on several factors, including the volume of intra-company calls and the
distances between company offices. Companies with overseas offices can experience the
greatest savings, since they can eliminate a great deal of international long distance
charges. These charges are often particularly high when the call originates in a foreign
country that still has a highly monopolistic telecom market.<br />
<br />
In some configurations, these savings can be extended to calls outside of the company as
well. This is done by first routing calls bound for destinations outside of the company over
the corporate  <a href="../../a1/VoIP-Network-platform.asp">network</a> to the closest remote office, and then interfacing with the public
switched telephone  <a href="../../a1/VoIP-Network-platform.asp">network</a> (PSTN) at that point. Thus, for example, a company with offices
in Chicago and Tokyo could route a Chicago-to-Osaka call to its Tokyo office, and then hand
it off to a local carrier for the last leg of its trip. This can further reduce long distance charges.<br />
<br />
The economic appeal of transmitting voice calls over the data  <a href="../../a1/VoIP-Network-platform.asp">network</a> arises from two
technical factors. First, data <a href="../../a1/VoIP-Network-platform.asp">networks</a> almost always have spare capacity.  <a href="../../a1/VoIP-Network-platform.asp">Network</a> managers
typically over-provision IP <a href="../../a1/VoIP-Network-platform.asp">networks</a> to allow room for growth and to avoid congestion
during periods of peak utilization. At the same time, voice calls consume relatively little
bandwidth. The characteristics of human speech - especially the comparatively large
amount of silence that takes place during conversations - allows for a great deal of
2
compression in the digitized transport of the call. This makes it possible for voice to
"piggyback" on existing data  <a href="../../a1/VoIP-Network-platform.asp">network</a> connections without requiring investments in
adding to the capacity of those connections. Even when such additions have to be
made because of call volumes, those costs are typically a fraction of the recurring costs
charged by carriers to carry that same calling volume.<br />
<br />
<b>2) Reduced long-term  <a href="../../a1/VoIP-Network-platform.asp">network</a> ownership costs</b><br />

In addition to reducing a company's monthly phone bills, a converged  <a href="../../a1/VoIP-Network-platform.asp">network</a>
architecture also reduces the ongoing costs of owning two separate <a href="../../a1/VoIP-Network-platform.asp">networks</a> - one
for voice and one for data. These costs include the need to buy two separate sets of
equipment, the staff time dedicated to the "care and feeding" of that equipment, the
licensing of any software relating to the management of that equipment and the
<a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> of <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> on the two <a href="../../a1/VoIP-Network-platform.asp">networks</a>.<br />
<br />
Personnel costs are of particular concern to communications and IT departments at this
time. The demand for skilled, experienced technicians continues to outstrip supply.
This has driven up salaries for voice and data  <a href="../../a1/VoIP-Network-platform.asp">network</a> staff, and has also made it
difficult to recruit and retain such engineering talent. Companies that are able to
reduce their need for technical staff by streamlining their  <a href="../../a1/VoIP-Network-platform.asp">network</a> operations can cut
overhead costs and eliminate many human resource management headaches that
plague their competitors.

</p>

<p class="virtualpage3">

<b>3) The ability to deploy powerful new integrated voice-and-data applications</b><br />

Of course, businesses don't exist just to save money. They exist to make money, gain
marketshare, and serve customers. That's why the most compelling aspect of
converged voice/data networking may well be the new generation of applications it
enables. These applications include Web-enabled call centers, unified messaging and
real-time collaboration.<br />
<br />
Take the example of a Web-enabled call center. One of the biggest obstacles that companies
face in converting Web site visitors into Web site buyers is poor online interaction. In a bricksand-
mortar store, customers can ask a nearby salesperson a question that may end up
determining whether or not they head for the check-out line. On a Web site, that kind of
interaction is more problematic. But using voice-over-IP, site visitors can click a button and
open up a voice conversation with a real, live call center staffer - who can quickly address any
question or problem the customer might have.<br />
<br />
Other examples include real-time multimedia video/audio-conferencing, distance learning, and
the embedding of voice links into electronic documents. In fact, the full business potential of
such applications is only beginning to be discovered. But one thing is clear: these integrated
voice-and-data applications will require a converged IP  <a href="../../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
This last point is indicative of what is perhaps the most critical reason that business executives
must move forward and at least pilot VoIP in one way or another within their organizations:
future-readiness. In the 90's, the Internet was not ready for prime-time as a medium for
commerce. Now it is. The companies that were best positioned to take advantage of the
Internet's maturity were those that had already dipped their toes in the water and developed a
reasonable amount of expertise and experience with Internet technology.<br />
<br />
The same is true of VoIP. <b>Regardless of exactly when and how voice-over-data technology
eventually emerges as an e-business necessity, the fact is that it inevitably will. And when
it does, companies that have stayed on the VoIP sidelines until then, without any hands-on
experience with converged networking, will be at a significant competitive disadvantage.</b>
They won't be able to gain the internal operational benefits that convergence offers. They
won't be able to effectively partner with other companies that have incorporated VoIP into
3
their technology strategies. And they won't be able to effectively service customers who will expect
voice-over-IP capabilities. Chances are that they will wind up spending more money in an effort to
"play catch-up" than would have been necessary if they had started moving in the right direction
earlier in the game.<br />
<br />
It is therefore essential that any company with intentions to survive and thrive in a communicationsbased
economy take steps now to ensure their ability to compete in a future that will include
universal adoption of some form of voice-over-IP technology.<br />
<br />
<b>Risk Factors in Voice-over-IP Migration</b><br />
As every decision-maker knows, any business gain comes with a variety of potential business risks.
Voice-over-IP is no exception. Market research indicates that the following risks are of greatest
concern to business and technology executives as they consider a migration to voice-over-data
convergence:<br />
<br />
<b>1) Loss of voice <a href="../a1/VoIP-Network-platform.asp">quality</a></b>
Technologists understand that data <a href="../../a1/VoIP-Network-platform.asp">networks</a> are very different from voice <a href="../../a1/VoIP-Network-platform.asp">networks</a>. On the
data  <a href="../../a1/VoIP-Network-platform.asp">network</a> - especially the Ethernets that dominate corporate computing environments -
packets bounce around indeterminately. They can collide and get distorted or even lost.
Error-correction mechanisms in Ethernet hardware and the IP protocol itself can readily
compensate for these phenomena on the data side, since the millisecond delays that occur as
the  <a href="../../a1/VoIP-Network-platform.asp">network</a> re-adjusts don't affect most computer applications. But such problems can
adversely affect voice calls - which require consistent, real-time flow of packets from one end
of the  <a href="../../a1/VoIP-Network-platform.asp">network</a> to the other. And, while the human brain can comprehend human speech even
when there is a lot of distortion, users have become accustomed to a high level of call <a href="../a1/VoIP-Network-platform.asp">quality</a>.
Any deterioration in that <a href="../a1/VoIP-Network-platform.asp">quality</a> is perceived as very disruptive to the normal way of doing
business - and users won't tolerate it.<br />
<br />
<b>2) Loss of reliability</b>
Technologists and business people both know from experience that data <a href="../../a1/VoIP-Network-platform.asp">networks</a> are not
yet as reliable as voice <a href="../../a1/VoIP-Network-platform.asp">networks</a>. We all know what it is like to have our computer freeze or
to be told that the network is "down." But this rarely happens with our phones or our
telephone carriers. Immediate and uninterrupted access to others over the phone is such an
essential aspect of conducting business that few executives want to put voice communications
at risk, regardless of how attractive the potential savings or converged applications
may be.<br />
<br />
<b>3) Being prematurely "locked-in" to a given vendor's architecture</b>
The pace of change in computing and communications technology today makes vendor
"lock-in" a major concern for any potential buyer. There are really two aspects of lock-in
that trouble most decision-makers. One is the possibility that another, superior solution for
voice-over-IP will come along shortly after a commitment has been made to a particular
vendor's product. If the investment in that product is substantial, it's usually impractical to
scrap it and switch to the better approach.

</p>

<p class="virtualpage3">
Of even greater concern for technology managers, however, is the fact that selection of one
vendor's approach to voice/data convergence may cause a lock-in that extends far beyond
the VoIP solution itself - forcing a long-term commitment to that vendor's overall
networking architecture. This is something that many companies have already experienced
with their desktop applications and data <a href="../../a1/VoIP-Network-platform.asp">network</a> hardware, where use of a particular
operating system or routing technology has narrowed their choices in many other areas -
such as applications and management tools.<b>No one wants their VoIP implementation to
result in the same type of limitation of long-term choices.</b> <br />
<br />

This concern is exacerbated by the lack of clear standards in the VoIP market. In the
absence of such standards, technology managers have legitimate concerns about
committing their companies to any proprietary architecture.<br />
<br />
<b>4) Lack of expertise and experience in convergence technologies</b>
Technologists, by their nature, like to really know what they're doing. And, in recent years,
they have already had to put a great deal of effort into rapidly assimilating a wide range of
new technologies - Web development, Internet security, networked storage architectures,
etc. The addition of VoIP technology to this knowledge base is not an especially attractive
prospect to already-overburdened staff, especially if convergence has not yet gained strong
support from the executive suite.<br />
<br />
There is one other factor that should be discussed in relation to corporate convergence initiatives.
The <b>very idea of convergence can potentially disrupt the roles and structures of corporate
technology staff.</b> As things stand now, data applications are typically the responsibility of an IT
team - while voice communications fall under the aegis of a separate and distinct telecommunications
group. Convergence can therefore pose a threat to the positions and even the very jobs of
individuals or groups of individuals in the company. While in an ideal world such personal and
political agendas shouldn't have an impact on corporate decision-making, the fact is that in reality
they do. So any executive considering a foray into VoIP should carefully consider how implementation
would impact the company's technology organization and staff.<br />
<br />
<b>Voice-over-IP Implementation Options</b><br />
While the voice-over-IP market continues to evolve, the current universe of VoIP implementation
options can be segmented into four broad categories. Each of these has its own advantages and
disadvantages:<br />
<br />
<b>PBX-based gateways</b>
The leading manufacturers of PBX equipment are all introducing their own solutions to the VoIP
challenge. With their significant marketshare and mindshare among corporate telecom managers,
these companies are well-positioned to capture a sizable piece of the VoIP market. From a technical
point of view, these companies can effectively integrate the management of VoIP functionality with
the existing corporate voice communications platform. They also have many years of experience
building hardware and software that meets the reliability standards of the voice market.<br />
<br />
The downside is that these vendors have minimal experience in IP-centric data networking.
<b>Without expertise in the vagaries of connectionless, non-determinate protocols, it is unclear
how well they will be able to address the issues of voice signal <a href="../a1/VoIP-Network-platform.asp">quality</a> in the IP world.</b> And, as
strong as their market position may be among telecom buyers and distribution channels, it is
relatively weak on the data communications side of the equation. This may hamper PBX vendors'
long-term ability to achieve dominance in a world dominated by IP innovators.<br />
<br />
Perhaps the biggest drawback to the PBX-based approach is that this class of solution is tied so
directly to highly proprietary PBX platforms. The leading PBX vendors have no demonstrable track
record in either defining or adopting the types of open technical standards that have accelerated
adoption of the Internet over the past decade. Without this commitment to standards-based
technology, PBX vendors' VoIP solutions won't be a very good bet for companies seeking broad
interoperability and flexible migration paths.<br />
<br />
<b>Router-based gateways</b>
Manufacturers of routers and other data networking hardware are also attacking the VoIP market,
albeit from the other direction. Like their PBX counterparts, these suppliers have healthy
marketshare and mindshare among an entrenched constituency - in this case, data communications
managers and networking equipment resellers. Their expertise in IP technology should also
stand them in good stead, especially when it comes to solving voice <a href="../a1/VoIP-Network-platform.asp">quality</a> problems using the IP
<a href="../a1/VoIP-Network-platform.asp">quality</a>-of-service (QoS) techniques they have been developing for some time.<br />
<br />
Unfortunately, router vendors' vulnerabilities mirror those of PBX vendors. Their unfamiliarity with
voice technology and call management continues to hamper their ability to deliver corporate-class
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> solutions. And continuing failure to rise to the reliability standards that have been
perfected among <a href="../../a1/VoIP-Network-platform.asp">telephony</a> vendors over the past several decades essentially disqualifies
them from serious consideration when it comes to corporate voice infrastructure.<br />
<br />
Finally, it must be noted that the standards-based approach that has characterized recent advances in
data networking is beginning to show signs of unraveling. In their pursuit of competitive advantages,
many networking vendors are introducing an increasing - though subtle - element of proprietary
flow control mechanisms. More and more, they are steering their customers to so-called "end-toend"
solutions - which is really a code-phrase for requiring a single manufacturer's equipment across
the edge, access and core strata of the <a href="../../a1/VoIP-Network-platform.asp">network</a>. This, again, represents the type of lock-in that most
technology managers would prefer to avoid at this early stage of their VoIP plans.


</p>

<p class="virtualpage3">
<b>PC-based gateways</b><br />

Several vendors are bringing stand-alone gateways to market. These products offer a router- and
PBX-independent solution, since they are not tied to a particular manufacturer's platform. These
smaller, more nimble vendors exhibit a greater ability to rapidly adopt - and even help define -
emerging standards.<br />
<br />
However, stand-alone gateways are also typically based on a PC platform, which calls into question
their inherent reliability. Also, these vendors do not have access to the type of manufacturing scale
that lends itself to cost-efficiency. <b>It is doubtful that expensive, unstable products will find broad
acceptance among buyers and distributors of corporate voice equipment.</b><br />
<br />
<b>Intelligent, multi-path switching</b><br />

A fourth alternative to the above categories has recently appeared on the VoIP scene: the multipath
switch. This type of switch is specifically designed to address the issues unanswered by the
product categories described above - including voice <a href="../a1/VoIP-Network-platform.asp">quality</a>, network reliability, and vendor
independence. Such a multi-path architecture is discussed in greater detail in the following section.
<br />
<br />
<b>Eliminating Risk with Intelligent, Multi-path
Voice/Data Switching</b>
<br />
The underlying architecture for intelligent, multi-path voice/data switching was developed by
Cheng T. Chen and Dr. Rajiv Bhatia of Quintum Technologies, Inc. in 1997. A multi-path voice/data
switch sits between the corporate PBX and both the PSTN and the corporate IP data <a href="../../a1/VoIP-Network-platform.asp">network</a>.
Its most prominent distinguishing characteristic is its ability to continuously monitor the
condition of the data <a href="../../a1/VoIP-Network-platform.asp">network</a>, and to route voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> accordingly. This intelligent switching
capability is the key to its value for safe, controlled migration to VoIP. If conditions on the corporate
<a href="../../a1/VoIP-Network-platform.asp">network</a> are sufficient to support the required level of voice <a href="../a1/VoIP-Network-platform.asp">quality</a>, voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> is switched to the
appropriate router. If conditions on the corporate <a href="../../a1/VoIP-Network-platform.asp">network</a> deteriorate for any reason, the switch
automatically and transparently re-directs voice traffic over the public switched <a href="../../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
<img src="images/Eliminating_Risk.jpg" />
<br />
Intelligent multi-path switching addresses the foremost concerns of both business and technology
managers who are considering a first-stage implementation of VoIP. It is particularly appealing at
this stage of VoIP's evolution, because:<br />
<br />
<b>It eliminates the risk of sub-standard voice <a href="../a1/VoIP-Network-platform.asp">quality</a></b><br />

If conditions on the <a href="../../a1/VoIP-Network-platform.asp">network</a> threaten the <a href="../a1/VoIP-Network-platform.asp">quality</a> of voice-over-data transmission - such as
overall <a href="../../a1/VoIP-Network-platform.asp">network</a> congestion or a high packet error rate - an intelligent switch immediately and
automatically responds with a "failover" to the PSTN. While this means that the company will
be charged by its carrier for those calls temporarily, that is a small price to pay for avoiding
any potential interruption to normal business operations.<br />
<br />
<b>It eliminates the risk of inadequate voice reliability</b>
An intelligent multi-path switch protects against loss of voice service in two ways. First, it reroutes
calls over the PSTN in the event that the failure of a data networking device threatens
the continuity of voice service. Second, it protects against its own failure by becoming a
transparent "wire" to the PSTN in the event of shutdown. Thus it provides 100% uptime for
voice services. It should also be noted that, because it is essentially a telecom switch rather
than a piece of data networking hardware, such a device is far less prone to failure than
conventional routers and access hardware.
</p>
<p class="virtualpage3"><b>Protection</b><br />
<b>It eliminates the possibility of vendor "lock-in"</b><br />

Such a switch can be used with virtually any PBX and any router architecture. Because it is not
directly tied to either device, it can serve as a highly flexible "shim" during any type of PBX
and/or router migration. If properly architected, it can also co-exist effectively with other
important <a href="../../a1/VoIP-Network-platform.asp">network</a> resources, such as firewalls and IVR servers.<br />
<br />
It eliminates pain and risk from the VoIP learning curve
Because a multi-path switch allows networking staff to turn VoIP off or on at a moment's
notice, it significantly raises the comfort level that both technical and business managers have
with their initial foray into VoIP implementation. Also, because a multi-path switch can easily
be programmed to selectively route only specified types of calls over the corporate data
<a href="../../a1/VoIP-Network-platform.asp">network</a>,<b> it provides an ideal method for testing, piloting and benchmarking VoIP traffic in
advance of any incremental "production" deployments.</b> In other words, it allows technicians
to safely conduct a wide range of point-to-point experiments on the corporate <a href="../../a1/VoIP-Network-platform.asp">network</a>
without impacting business-as-usual.<br />
<br />
<b>It eliminates the hidden costs of implementing VoIP</b><br />
The simplicity and technical elegance of the multi-path switch make it an extremely attractive
solution from an economic point-of-view. Early VoIP adopters have encountered many
"hidden" costs in their VoIP implementations. These include:<br />
<br />
. additional PBX tie trunk ports<br />
. PBX reconfiguration<br />
. upgrades to router hardware and operations systems<br />
. data <a href="../../a1/VoIP-Network-platform.asp">network</a> reconfiguration<br />
. additional WAN bandwidth to ensure voice <a href="../a1/VoIP-Network-platform.asp">quality</a> during peak traffic loads<br />
. training employees to use special dialing prefixes<br />
. gatekeeper software<br /><br />
The multi-path solution eliminates these and other additional costs, thus reducing both financial
and implementation risks associated with VoIP migration, and substantially increasing the resulting
return-on-investment.<br />
<br />
A multi-path switch can also allay the personal and political concerns alluded to earlier, since it
neither displaces nor interferes with telecom or data <a href="../../a1/VoIP-Network-platform.asp">network</a> infrastructure. In this way, it provides
an excellent means for initiating a migration to convergent networking while executives are still
undecided about how to tackle the thornier issues associated with re-structuring their technical
organizations.<br />
<br />
Convergence is on its way. Companies that delay adoption of first-wave convergence technologies -
most notably VoIP - for too long will suffer a substantial competitive disadvantage as these
technologies enter the mainstream. There are, however, clear risks that need to be avoided at this
early stage of the convergence evolution. To avoid these risks while still practicing due diligence in
advancing the corporate technology portfolio, decision-makers should carefully consider the use of
intelligent multi-path gateway switching. In a time of uncertainty and doubt, these switches offer an
ideal first step towards the exciting and lucrative future of convergence.<br />
<br />
In order to show you how easily the Tenor VoIP MultiPath Switches fit into existing <a href="../../a1/VoIP-Network-platform.asp">networks</a>, here
are two typical scenarios for the simple deployment of Tenor VoIP MultiPath Switches:<br />
<br />
<b>Enterprise</b><br />
<br />
<img src="images/Enterprise.jpg" /><br />
<br />
<b>Service Provider</b><br />
<br />
<img src="images/Service_Provider2.jpg" /><br />
<br />
</p>
<p class="virtualpage3">
<b>About Quintum</b><br />
Quintum Technologies is an innovator in the voice-over-IP (VoIP) market. The company offers highly reliable VoIP products that deliver
superior voice <a href="../a1/VoIP-Network-platform.asp">quality</a> and provide an easy, risk-free migration path to the convergent future of networking. Quintum was founded by
Cheng T. Chen and Dr. Rajiv Bhatia, both of whom have over 20 years of experience as lead engineers at companies including Bell
Laboratories, Teleos, Madge and 3Com. The company's mission is to deliver enterprise-class VoIP solutions that provide:<br />
<br />
. Outstanding value to customers<br />

. Ease of installation, ease of use, and ease of management<br />

. Superior <a href="../a1/VoIP-Network-platform.asp">quality</a> and reliability<br />

. Open architectures and standards compliance<br />

. Flexible migration to succeeding generations of convergence technology<br />
<br />
Quintum's unique Tenor VoIP MultiPath Switch is the first VoIP switch to intelligently switch calls over both IP <a href="../../a1/VoIP-Network-platform.asp">networks</a> and the PSTN in
order to ensure high voice <a href="../a1/VoIP-Network-platform.asp">quality</a> and provide failover capability. Unlike conventional VoIP gateways that only route calls over IP
<a href="../../a1/VoIP-Network-platform.asp">networks</a>, Quintum's Tenor Switch can transparently switch calls over to the PSTN if IP <a href="../../a1/VoIP-Network-platform.asp">network</a> congestion or a device failure impacts
voice <a href="../a1/VoIP-Network-platform.asp">quality</a>. The Tenor thus addresses the reliability concerns that have heretofore prevented many corporate decision-makers from
moving ahead with VoIP and receiving all of its benefits.<br />
<br />
Quintum Technologies Inc. is a privately-held corporation headquartered in Eatontown, N.J. More information on the company, its
management team, and its products can be found at www.quintum.com.
</p>

</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Risk_and_Rewards_of_VoIP_Implementations.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/Risks_and_Rewards.asp"></a> <a href="../doc/Risk_and_Rewards_of_VoIP_Implementations.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
