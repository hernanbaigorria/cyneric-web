<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
The Value of Open Source Software:<br />
Sandals, Savings or Services?<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">
<b>Summary</b><br />
As enterprises and operators role out real-time <b>Internet Protocol (IP) communications applications</b> such as <b>Voice-over IP (VoIP), instant messaging (IM), video and multimedia</b>, the need to protect end-users and <a href="../a1/VoIP-Network-platform.asp">network</a> infrastructures from multiple catastrophic attacks, misuse, and abuse of session-based protocols is
becoming imperative.<br />
<br />
At the same time, the encryption and authentication that many advertise as VoIP security only scratches the surface of the required protection. In fact, there are many <b>VoIP-specific vulnerabilities </b>that have been discovered, along with thousands of threats that can be launched against SIP/UMA/IMS <a href="../../a1/VoIP-Network-platform.asp">networks</a>, that encryption and authentication alone do not address.
<br />
<br />
This white paper will look at a number of these threats that target the enterprise <a href="../a1/VoIP-Network-platform.asp">network</a> and users including <b>reconnaissance, Denial of Service (DoS)/Distributed Denial of Service (DDoS), Stealth DoS/DDoS, Spoofing</b> and <b>VoIP spam</b> in order to explore the unique methods and techniques to protect VoIP infrastructure as well as
end users from threats that endanger the continued exchange of time-critical, business-sensitive information.<br />
<br />

<b>Introduction</b><br />
Real-time, Internet Protocol (IP) communications applications have a significant and obvious appeal for enterprises and end-users because they allow the Internet and existing data <a href="../../a1/VoIP-Network-platform.asp">networks</a> to become a cost-effective transport for things most people want to do such as: placing voice calls, participating in video conferences, exchanging Instant Messages (IMs), and a host of other communications applications. It can also allow you to realize the benefits of using a Session Initiation Protocol (SIP) trunk for hosted Voice over IP (VoIP) services. But cost is only part of the appeal, these new communications applications enable increased efficiencies and collaboration with integration of soft clients on PCs, IT infrastructure such as Microsoft Live Communication Server (LCS) and voice extranets into one converged <a href="../a1/VoIP-Network-platform.asp">network</a>, as shown in Figure 1.<br />
<br />
<img src="images/Adding_VoIP.jpg" /><br />
<br />
These benefits do not come without a significant tradeoff as we can see by taking a step back and looking at what happened with IP <a href="../../a1/VoIP-Network-platform.asp">networks</a>. Because the IP <a href="../a1/VoIP-Network-platform.asp">network</a> is an 'open' system, any user can freely connect to it at any time from any place with little effort or oversight. This makes the IP <a href="../a1/VoIP-Network-platform.asp">network</a> a fertile breeding ground for a wide variety of malicious and unauthorized activities that can affect any enterprise, group, or user. <a href="../a1/VoIP-Network-platform.asp">Network</a> protocols, operating systems, web browsers,
e-mail clients and other applications are persistent targets of attacks.<br />
<br />
Traditionally, the Internet security industry reacts to these attacks by developing a collection of piecemeal solutions to protect the enterprise from attacks. As a result, threats have been effectively mitigated to manageable levels by the development and deployment of a number of increasingly sophisticated solutions including firewalls, Intrusion detection/intrusion prevention system (IDS/IPS), anti-spam filters and others.<br />
<br />
However, problems still persist and if history is any indication, IP communications applications will also be subject to many of the same security threats that are prevalent in traditional Internet data applications, and to many additional ones as well. These new attacks include deliberate application-specific assaults against the VoIP infrastructure and end-points, such as denial of service (DoS) and distributed denial of service (DDoS) attacks as well as stealth attacks and VoIP spam.<br />
<br />
Because of these risks, many enterprises have deployed their VoIP infrastructure as an "island" utilizing a separate Virtual Local Area <a href="../a1/VoIP-Network-platform.asp">Network</a> (VLAN) to protect it against these attacks, but this does not allow them to realize the full potential of IP communications applications. Even worse from a security perspective, some enterprises feel they are safe by simply using the encryption and authentication techniques embedded into the VoIP infrastructure. While this is important, encryption and authentication do not protect against a variety of external threats from malicious users and spammers as well as internal threats from infected PCs. Frequently, these malicious endpoints are "authorized" users of VoIP and will easily pass the authentication and encryption hurdles.<br />
<br />
At the same time, it's important to understand that IP communications applications, such as VoIP, are very different than web applications and email, as shown in Figure 2. VoIP is real-time by its very nature and involves complex state machines which may need to track several dozen states at the same time. The protocols themselves, such as SIP, are feature-rich and involve the use of separate signaling and media planes which allow devices to talk peer-to-peer rather than the traditional
client-server methods of the data world. Finally, there is an extremely low tolerance to false positives and negatives as compared to the data world.<br />
<br />
<img src="images/IP_Communication.jpg" /><br />
<br />
It's easy to see that IP communications applications demand a security solution that not only "borrows" from the best security functionality of the data world but adds specific VoIP protection techniques that take into account the real-time, peer-to-peer, and feature-rich nature of these session-based protocols.





</p>

<p class="virtualpage3">
<b>VoIP Risks and Vulnerabilities</b><br />
VoIP <a href="../../a1/VoIP-Network-platform.asp">networks</a> have thousands of unique vulnerabilities that can be exploited to launch a variety of attacks. In fact, the Sipera VIPER lab, which is comprised of the most knowledgeable and capable VoIP and security developers, architects, and engineers, has identified over 20,000 threats in the last two years that can be launched against SIP <a href="../../a1/VoIP-Network-platform.asp">networks</a>, as shown in Table 1.<br />
<br />
<img src="images/Unique_SIP.jpg" /><br />
<br />
All told, enterprises need to be aware of, and effectively protect their <a href="../a1/VoIP-Network-platform.asp">network</a> from, these attacks against their infrastructure and the additional ones against end-users which are unique to IP communications applications. These application-specific threats are in addition to attacks such as call hijacking, fraud and eavesdropping that are secured using encryption and authentication. Let's look at some of the more prevalent and potentially damaging VoIP-specific application level attacks.<br />
<br />
<b>Reconnaissance Attacks</b><br />
Pre-DoS attacks are probes conducted against a <a href="../a1/VoIP-Network-platform.asp">network</a> to ascertain its vulnerabilities, the behavior of its equipment and users, and what services might be available for exploitation or disruption. Once this information has been gathered, focused attacks against the network's assets, services, and users can then be launched. This type of 'intelligence gathering' or 'probing action' is often the first thing an attacker will do when attempting to penetrate a particular <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
Types of reconnaissance attacks include call walking and port scanning. Call walking is a type of reconnaissance probe where a malicious user initiates sequential calls to a block of telephone numbers in order to identify what assets are available for further exploitation. Port scanning is similar to call walking in that sequential probes are made against a block of destinations. However, port scanning does not target end-users as call walking does, but instead targets a group of sequential ports in a <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
Depending upon the responses that are received, the attacker then can determine which exploit attempts might or might not work to breach the <a href="../a1/VoIP-Network-platform.asp">network</a>. Using these methods, an attacker can easily identify and gather the domain names and URLs of SIP-enabled devices that populate the <a href="../a1/VoIP-Network-platform.asp">network</a> and launch attacks against those devices.<br />
<br />
<b>Floods and Distributed Floods</b><br />
Flood DoS and DDoS attacks are those attacks whereby a malicious user deliberately sends a tremendously large amount of random messages to one or more VoIP end-points from either a single location (DoS) or from multiple locations (DDoS), as shown in Figure 3. Typically, the flood of incoming messages is well beyond the processing capacity of the target system, thereby quickly exhausting its resources and denying services to its legitimate users.<br />
<br />
In the case of DDoS attacks, the attacker(s) will use multiple sources to launch the assault or a single source masquerading as multiple sources to attack the target system. If the system(s) from which the DDoS attack originates have themselves somehow been compromised, then they are referred to as zombies.<br />
<br />
Oftentimes, however, a flood may be caused by a valid reason (such as a power failure precipitating a flood of SIP end-point registrations or a flood caused by an improperly configured SIP phone).<br />
<br />
<img src="images/Malicious_users.jpg" />
</p>

<p class="virtualpage3">
<b>Protocol Fuzzing</b><br />
Fuzzing is a legitimate method of testing software systems for bugs and is accomplished essentially by providing an application with semi-valid input to see what its reaction will be. Then appropriate fixes can be implemented, if necessary.<br />
<br />
Malicious users, however, employ this same methodology to exploit vulnerabilities in a target system. They do this by sending messages whose content, in most cases, is good enough that the target will assume it's valid. In reality, the message is 'broken' or 'fuzzed' enough that when the target system attempts to parse or process it, various failures result instead. These can include application delays, information leaks, or even catastrophic system crashes.<br />
<br />
<b>Misuse</b><br />
Misuse involves taking over someone's call or making calls on their behalf which is more commonly called spoofing. This is done by deliberately inserting fake data into the source IP address-field portion of the packet to hide the true source of the call. In this way the attacker can 'spoof' a legitimate user and hijack the current session which results in the call either being redirected or terminated, as shown in Figure 4. Spoofing results in misuse/abuse of the system and a denial-of-services (DoS) to the legitimate user.<br />
<br />
<img src="images/Malicious_users2.jpg" /><br />
<br />
<b>Session Anomalies</b><br />
Session anomalies occur when the messages do not come in the correct sequence and therefore neither the end-points nor the call server know how to handle the calls. When hackers or malicious users do this intentionally, it will result in a session abuse for the VoIP system, similar to misuse.<br />
<br />
<b>Stealth Attacks</b><br />
Stealth attacks are those in which one or more specific end-points are deliberately attacked from one (DoS) or more (DDoS) sources, although at a much lower call volume than is characteristic of flood-type attacks. In addition to VoIP spam, detection of stealth attacks is vital for VoIP systems as they have the potential to be far more annoying than what we are familiar with in the data world. VoIP security solutions need to be more sophisticated and use different techniques to protect against stealth and VoIP spam.<br />
<br />
<b>VoIP Spam</b><br />
VoIP spam or Spam-over-Internet <a href="../../a1/VoIP-Network-platform.asp">Telephony</a> (SPIT) is unsolicited and unwanted bulk messages broadcast over VoIP to an enterprise network's end-users. In addition to being annoying and having the potential to significantly impinge upon the availability and productivity of the end-point resource, high-volume bulk calls routed over IP are often very difficult to trace and have the inherent capacity for fraud, unauthorized resource use, and privacy violations.<br />
<br />
<img src="images/Unique_VoIP_threats.jpg" /><br />
<br />
These attacks can be from external sources such as hackers, malicious users and spammers or internal threats from disgruntled employees, infected PCs or email attachments, as shown in
Figure 5. What's required to protect against them is a proactive approach to anticipating and cataloguing the threats and attacks and then to use this expertise as the foundation of a comprehensive solution which protect against them. The VoIP security solution must also have the ability to be updated with vaccines against previously unidentified threats.

</p>

<p class="virtualpage3">
<b>Drawbacks to Today's VoIP Security</b><br />
Although core VoIP assets and related infrastructure can be protected to a certain degree from direct assault through a variety of currently available techniques, such as hardening the underlying IP <a href="../a1/VoIP-Network-platform.asp">network</a> and deploying session border controllers (SBCs), none can protect against the increasing sophistication of attacks against the numerous vulnerabilities inherent in VoIP and related IP communications applications.<br />
<br />
Implementing a comprehensive security solution to deal with both internal and external threats from DoS, DDoS, stealth and spam is a formidable challenge. As mentioned at the outset, the biggest mistake an enterprise can make with securing its VoIP infrastructure is to assume that encryption and authentication are enough to protect the <a href="../a1/VoIP-Network-platform.asp">network</a> and end-users against attacks. This is not to say that authentication and encryption are not important, but they do not protect against zombie and hacker attacks.<br />
<br />
As well, viruses, worms and other malicious activities frequently utilize end-user equipment to penetrate the <a href="../a1/VoIP-Network-platform.asp">network</a>, even when perimeter security mechanisms like firewalls and session border controllers are employed. Complicating the matter further, new and emerging technologies such as IM now represent an ever larger emerging threat to <a href="../../a1/VoIP-Network-platform.asp">networks</a> that completely bypass perimeter defense devices. This has led enterprises to look for alternative security solutions.<br />
<br />
Many of the security products which are currently available primarily focus on remediating threats by employing various disparate technologies such as firewalls, IDS/IPS, and other security devices that are upgraded to support VoIP in addition to their main data protection responsibilities. An example of how a typical VoIP security solution is deployed using these equipment elements to mitigate the inherent vulnerabilities of an IP <a href="../a1/VoIP-Network-platform.asp">network</a> is shown in Figure 6.<br />
<br />
<img src="images/Typical_multi-produc.jpg" /><br />
<br />
At best these solutions protect against OS, IP and TCP layer vulnerabilities and attacks such as TCP syn flood, exhaustion of resources with multiple TCP, UDP DoS attacks, HTTP attacks, TCP Fin/Rst close socket attacks and others.<br />
<br />
These traditional solutions are not at all effective for application-level vulnerabilities in that they cannot provide the needed functionality to effectively detect and protect against VoIP-specific attacks such as floods, protocol fuzzing, stealth, and VoIP spam. At the same time, they cannot protect against vulnerabilities that may be found in encrypted <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> as they are unable to decrypt and analyze the <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> in real-time.<br />
<br />
As well, because this solution represents a layered-approach to <a href="../a1/VoIP-Network-platform.asp">network</a> security, in addition to the extra hardware (application-aware firewall, IDS/IPS, and DoS protection systems) required to secure the <a href="../a1/VoIP-Network-platform.asp">network</a>, additional software must also be installed at different points to allow the hardware components to function properly and to coordinate security <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> and reporting functions.<br />
<br />
Not only do these additional levels of complexity add more points of potential vulnerability, it's easy to see that they do not integrate well with a VoIP <a href="../a1/VoIP-Network-platform.asp">network</a> due to the fact that the delay introduced by every device collectively exceeds the security budget (2 ms for signaling and 100 �s for media) allowed to still ensure toll <a href="../a1/VoIP-Network-platform.asp">quality</a> transmission. As well, many of these devices use a store and forward method to examine the <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> which is just not feasible in the real-time world of IP communications applications.<br />
<br />
To quickly summarize the points above, existing solutions of this type are decidedly deficient in a number of critical ways:<br />
<br />
they cannot function in real-time;<br />
. they cannot process encrypted <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>;<br />
. they do not have the capacity to detect attacks on end users;<br />
. they result in a higher TCO as you need to upgrade multiple boxes; and<br />
. they cannot keep in sync with new IP features or applications offered by the VoIP infrastructure vendors.<br />
<br />
Existing security measures for IP <a href="../../a1/VoIP-Network-platform.asp">networks</a> are at best only effective for traditional types of <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> (web access, e-mail, etc.). However, as VoIP becomes increasingly more prevalent and feature-rich, the need for more effective and robust security solutions becomes obvious.<br />
<br />

</p>

<p class="virtualpage3">
<b>Comprehensive VoIP Security</b><br />
Instead of deploying ineffective 'point' solutions, a complete security solution is required that seamlessly incorporates all existing approaches into a single, comprehensive system, as shown in Figure 7.<br />
<br />
<img src="images/Single_comprehensive.jpg" /><br />
<br />
When deployed in the enterprise, this single, comprehensive device replaces the 3 or 4 point solutions at each location in the <a href="../a1/VoIP-Network-platform.asp">network</a>, as shown in Figure 8. In most cases a firewall will still be deployed to protect against layer 3 and 4 attacks but not the long list of VoIP specific application level ones that were discussed above. You can immediately see the operational simplicity and obvious cost-effectiveness compared to the solution in Figure 6.<br />
<br />
<img src="images/Simplified_comprehensive.jpg" />
<br />
The ideal comprehensive VoIP security solution would incorporate the best practices of data security, from firewall, IDS/IPS, DoS prevention, <a href="../a1/VoIP-Network-platform.asp">network</a> level correlation and spam filtering, while implementing sophisticated techniques to ensure unique VoIP threats are proactively recognized, detected, and eliminated. This single solution for securing IP Communications applications would also include the following features:<br />
<br />
<b>Real-time performance</b><br />
All of this functionality needs to be incorporated into a single device that is built from the ground up using specialized hardware for real-time performance. The appliance must be able to decrypt packets at wire-speed so that the <a href="../a1/VoIP-Network-platform.asp">network</a> can be protected against threats that exist even in encrypted <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. And it must securely store and manage these encryption keys in a separate, tamper-proof, hardware module.<br />
<br />
<b>Not a point of failure</b><br />
It's also preferable that the device functions as a "bump-on-a-wire" so that no configuration changes are required to either the call manager, the VoIP phones or to any other element in the IP <a href="../a1/VoIP-Network-platform.asp">network</a>. Another high-availability feature is fail-safe port bypass functionality which ensures the device is never an additional point-of-failure in the <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />

</p>

<p class="virtualpage3">
<b>Sophisticated behavior learning and verification</b><br />
An ability to continuously learn call patterns and end-point fingerprints, in addition to being able to constantly analyze raw event data based upon specific user-definable criteria and take automatic action, would give the security solution the ability to evolve and adapt on its own to effectively counter any new or existing threat. This would vastly increase its level of effectiveness in ensuring that vulnerabilities are mitigated before any threat can proliferate.<br />
<br />
This level of sophistication is really the only way to identify both stealth attacks and VoIP spam which are vital for any VoIP security system. These types of attacks and service abuse are difficult to detect as the real-time nature of VoIP does not allow the security system the luxury of storing the call while it's analyzed before sending it on as is the case with email.<br />
<br />
The VoIP security system needs to identify and verify these anomalies in real-time before passing on the call. Once a potential anomaly is detected, it should be scrutinized further using various verification techniques to determine if it is in fact an attack which should be dropped or Spam that should be sent to a specific bulk voice mailbox.<br />
<br />
<b>Detection of VoIP spam</b><br />
Machine-generated calls are a popular tool for mass marketing concerns, although the recipients of their messages more often than not find the calls to be highly intrusive and annoying. In addition, machine-generated calls are oftentimes used as automated attack tools by malicious users to overwhelm a system and deprive its legitimate users of services. Machine-generated calls can be detected by performing sophisticated VoIP Turing tests in the suspected <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, as shown in Figure 9. However, when combined with behavior learning and verification, the VoIP Turing test can be used selectively rather than before every call which minimizes its intrusiveness.<br />
<br />
<img src="images/VoIP_Turing.jpg" /><br />
<br />
With a VoIP Turing test, the caller is challenged to respond to a question (i.e. What is the number between 1 and 3?) which the machine cannot do. This test is very similar to the Turing tests that you may have seen on the web when you buy tickets or register for email addresses. Many times you are asked to enter some random numbers or letters that have been smudged like you see here.<br />
<br />
<img src="images/snum.jpg" /><br />
<br />
By entering these letters, the web site doing the challenge is assured you are a human and not a machine trying to buy blocks of tickets or register hundreds of email addresses
</p>

<p class="virtualpage3">
<b><a href="../a1/VoIP-Network-platform.asp">Network</a> level intelligence</b><br />
A <a href="../a1/VoIP-Network-platform.asp">network</a> level intelligence node needs to collect and correlate multiple events and activities from different nodes and end-points in the <a href="../a1/VoIP-Network-platform.asp">network</a> to accurately detect attacks which otherwise might have escaped unnoticed if reported only by a single point in the <a href="../a1/VoIP-Network-platform.asp">network</a>. This capability can inspect the sequence and content of messages to detect protocol anomalies and any instances of end-point scanning.<br />
<br />
The primary purpose of the intelligence node is to receive the variously formatted event and alarm reports from the different security components in the <a href="../a1/VoIP-Network-platform.asp">network</a> and to store, normalize, aggregate and correlate that information into a comprehensive format. It then passes the attack information back to the security nodes which take the action needed to protect the <a href="../a1/VoIP-Network-platform.asp">network</a> and end users, as shown in Figure 10. This allows distributed attacks to be effectively detected and mitigated.<br />
<br />
<img src="images/Network_level.jpg" /><br />
<br />
The primary purpose of the intelligence node is to receive the variously formatted event and alarm reports from the different security components in the <a href="../a1/VoIP-Network-platform.asp">network</a> and to store, normalize, aggregate and correlate that information into a comprehensive format. It then passes the attack information back to the security nodes which take the action needed to protect the <a href="../a1/VoIP-Network-platform.asp">network</a> and end users, as shown in Figure 10. This allows distributed attacks to be effectively detected and mitigated.<br />
<br />
<b>Conclusion</b><br />
Currently, VoIP security solutions are merely an extension of existing data security products and fail to adequately address the increasing complexity of VoIP <a href="../../a1/VoIP-Network-platform.asp">networks</a>. These traditional products are simply not equipped to address the real-time, mission-critical nature of IP communications applications and provide, at best, a piecemeal approach where an entire <a href="../a1/VoIP-Network-platform.asp">network</a> is not secured, leaving significant parts of it exposed and vulnerable to attack.<br />
<br />
Unlike data communications, VoIP is a real-time service and requires security infrastructure to provide automated, immediate security responses to preserve the high availability and <a href="../a1/VoIP-Network-platform.asp">quality</a>-of-service (QoS) expected by telephony users. In light of these considerations, any effective and comprehensive VoIP security system must offer:<br />
<br />
. comprehensive protection with real-time performance<br />
. easy deployment and not be a point-of-failure<br />
. automatic user behavior learning<br />
. <a href="../a1/VoIP-Network-platform.asp">network</a> level intelligence<br />
. effectively handle VoIP spam; and<br />
. interoperability with major VoIP infrastructure vendors.<br />
<br />
At the same time, each of these features must be provided to the <a href="../a1/VoIP-Network-platform.asp">network</a> in a manner that does not exceed the allowable security budget (2 ms for signaling and 100 �s for media) that ensures a high QoS to the VoIP and multimedia user.<br />
<br />
In the end, the only way to provide the required level of protection is to incorporate a variety of sophisticated VoIP-specific security techniques and methodologies that include anomaly detection, filtering, behavior learning, and verification into a single, comprehensive security device. Together, these practices proactively protect the enterprise <a href="../a1/VoIP-Network-platform.asp">network</a> from VoIP attacks, misuse and service abuse which <a href="../../a1/VoIP-Network-platform.asp">networks</a> and end-users face today and in the future.<br />
<br />

<b>About Sipera Systems</b><br />
Sipera Systems, Inc., the leader in pure security for VoIP, <a href="../a1/productos-cr2.asp">mobile</a> and multimedia communications, enables enterprises and operators to protect end users and <a href="../a1/VoIP-Network-platform.asp">network</a> infrastructures from potentially catastrophic attacks, misuse, and abuse of real-time, session-based protocols. Comprised of top vulnerability research experts, the Sipera VIPERT lab concentrates all of its efforts towards identifying SIP, UMA and IMS vulnerabilities. This expertise forms the foundation of Sipera IPCST products which protect IP communications applications and the Sipera LAVAT tools which verify <a href="../../a1/VoIP-Network-platform.asp">networks</a> readiness to resist attacks. Founded in 2003, Sipera is headquartered in Richardson, TX. Visit http://www.sipera.com.</p>
</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Sipera_Enterprise_VoIP_Security_WP.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/3com-solutions-strategies.as"></a>  <a href="../doc/Sipera_Enterprise_VoIP_Security_WP.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
