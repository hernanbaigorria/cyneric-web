<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>3Com�
Solutions:
Strategies for Successful IP <a href="../../a1/VoIP-Network-platform.asp">Telephony</a> Implementations<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">


<b><a href="#">The Problem With Legacy</a></b> <span class="credits"></span><br>
Dial tone is probably the most critical enterprise service of all IT services. Right up there with payroll
service, dial tone or the lack of it, can inspire 1,000 times more passion than poor PC availability,
e-mail spam, or elevator power outages. That's because people have grown accustomed to high <a href="../a1/VoIP-Network-platform.asp">quality</a> audio in their personal and professional lives, and they continue to demand it from the technology their company might
employ. <br /> <br />
For whatever reason, telecommunications departments have tended to have longer product life cycles and more heterogeneous environments than any application or IT unit in the company.
More than anything else, that may have to do with the heritage of limited innovation, oligarchystyle
pricing, and high maintenance costs perpetuated by telephone company repairmen in little
white trucks. In fact, the multivendor environment is probably due not just to the spate of mergers
and acquisitions in the customer environment over the past decade, but also to the geographic
strength of different vendor sales and service organizations, timing of projects, and the natural tendency
to keep vendors "honest" by splitting the business.
<br /> <br />So it should come as no surprise that IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>remains a  potentially perplexing technology
for many companies, possibly tempering enthusiasm for <a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a> deployment of IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a>products. Yet, in the face of what has always been considered a weak business case (how much can you really save by consolidating wiring infrastructure?), there is a compelling demand for IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a>products. A compilation of four leading analyst
reports indicates that 2005 will be the year of "inflection"-when the rate of market growth changes. It is the year in which more IP lines will be purchased than digital, signaling the market's preference for IP technology.
<br />
<br />
In the face of this anticipated surge in market demand, a reevaluation of a company's IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> strategy is appropriate since the technology risk and the company's risk-tolerance have changed. As the advantages of IP
telephony become a business requirement, organizations face strategic decisions. They should understand the tradeoffs of each of the following IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> deployment options: 
1. Hybrid
2. Greenfield
3. Forklift
4. Overlay
5. A Combination of Strategies</p>

<p class="virtualpage3">
<b><br />Hybrid</b> <span class="credits"></span><br>
As a response to the introduction of the IP-PBX by 3Com in 1998, legacy PBX vendors introduced the "Hybrid PBX." Logically, many enterprises looked to their portfolio of legacy vendors for insights into what IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> could do for their business and considered various deployment options.<br />
<br />
The classic response-the Hybrid-involved two components: 
an IP phone and an IP adjunct card that provided access to the PBX switch fabric. As a typical line card, the IP card had Digital Signal Processors that could convert IP packets to circuits and interfaced directly with the switch fabric. The card would also transmit the vendors' proprietary signaling and TDM-translated features into an IP packet destined
for the appropriate IP phone. In this way users would get similar, if not identical features, on the IP set, and the economic link between the PBX switch fabric and the handset would be protected.<br />
<br />
With the Hybrid strategy, users get <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> service on an IP phone, but not much else. Similar to the sizes of PBX line cards that control access concentration and manage call blocking, these IP cards can only support a limited
number of IP phones. If too many calls access the switch fabric or access the card bus, contention may occur and cause users to hear fast busy tones, affecting their satisfaction and the service availability.<br />
<br />
Furthermore, most vendors offer messaging services separately for these users, directly over IP, reducing interoperability with the legacy PBX. Note that since calls always pass through the PBX switch fabric, audio <a href="../a1/VoIP-Network-platform.asp">quality</a> is
limited to 8 kHz sampling and 64 kbps maximum bandwidth.</p>




<p class="virtualpage3">
<b><br />Greenfield</b> <span class="credits"></span><br>
The "Greenfield" strategy is the deployment of a new system in a new facility-often a new building on a campus, an office relocation, or a refurbishment. It takes its name from the original "field of green grass" that (theoretically)
existed before construction began.<br />
<br />
With this strategy the opportunity is to create an experimental "island" of functionality to assess benefits, interoperability functionality, cost control, and user acceptance of the new technology. In this setting, the company classically conducts a Request for Proposal (RFP) process for acquiring IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> products and services and reviews proposals from a variety of vendors and their partners.<br />
<br />
Companies typically adopt this strategy when the digital PBX infrastructure is complex or large, making <a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a> change expensive, disruptive, and difficult.
Also, it is a popular choice when much of the gear is not completely depreciated or when the IT management team is skeptical of the benefits.<br />
<br />
Greenfield strategies once confronted by the limitations associated with the Hybrid approach. 
This has historically been a frequent point of success for the 3Com� NBX� IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> solution.
The NBX product sports features such as H.323-based call control, dual-port Ethernet phones (keeping Ethernet portcount
requirements to a minimum), included voice mail, and simple user and administrator functionality previously unavailable in business <a href="../../a1/VoIP-Network-platform.asp">telephony</a> products.<br />
<br />
The benefits of lower operating costs-easier moves, adds, and changes-has proved effective in many enterprises that swapped their digital PBXs as budget and depreciation schedules allowed.
</p>

<p class="virtualpage3">
<b>Forklift</b> <span class="credits"></span><br>
Without the opportunity to  design a controlled experiment, many enterprises are, perhaps by default, limited to the Forklift model. Here the implementation involves a complete removal of legacy equipment, hence the "Forklift" analogy, since most PBXs are large boxes requiring power assists to relocate them. No doubt, many vendors consider this their best option for sales. However, the risk of failure can be higher than with other strategies.<br />
<br />
The Forklift often implies a flashcut of the service-one day the old service and the next day the new service. This is not a realistic implementation. Clever project managers first deploy the infrastructure (data <a href="../../a1/VoIP-Network-platform.asp">network</a>, call controller,
digital gateways) and then rollout the training and desktop devices in lumpy groups, perhaps 100 users at a time. IP phones can function on the same desk as the standard PBX phones. As training occurs and user anxiety diminishes,
the legacy phones and PBX can be decommissioned.<br />
<br />
Of course, in a global multisite enterprise-wide model, economics and timetable will drive implementation parameters. The project team may design a cost-optimized, <a href="../a1/VoIP-Network-platform.asp">quality</a> process that they can easily replicate. Often, what is appropriate for dozens or hundreds of sites is to divide the labor into specific tasks. The organization employs specialized
third parties to perform each implementation step, including site preparation, user training, staging and testing configurations, shipping assembled product, and installing and commissioning the new system as well as decommissioning the legacy one.

</p>

<p class="virtualpage3">
<b>Overlay</b> <span class="credits"></span><br>
Some enterprises may choose to
unify applications first-centralize
messaging and other convergence
applications for presence
and desktop video conferencing.
Then they may deploy IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> service more gradually as user
demand and user acceptance warrants.
In these "Overlay" implementations,
enterprises deploy
the applications globally to quickly
extract application benefits.<br />
<br />
<a href="../../a1/VoIP-Network-platform.asp">Network</a>-wise, they deploy call
controllers that use Session
Initiation Protocol (SIP) to control
the Wide Area <a href="../../a1/VoIP-Network-platform.asp">Network</a> (WAN).
Digital gateways cap PBX investments
and new users with IP
phones are deployed on the call
controllers. Gateway-to-gateway
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> is carefully managed across
the IP WAN using an enterprise
management system. PBX
Interworking Devices (PIDs) are
used to turn on the message waiting
lamps of users on digital
PBXs, "capping" the legacy
investment, and enabling new IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> application delivery for
enhanced business performance.<br />
<br />
The term "Overlay" is derived
from the way a tablecloth covers
the table surface, providing a
smooth and clean basis for doing
work without regard to the wood
below the cloth. In the same way,
the Overlay strategy covers the
legacy PBX infrastructure with IP
and enables new applications
without serious regard to the
legacy system.<br />
<br />
Companies consider this option
because they can't control other
factors such as depreciation
schedules on existing equipment,
higher investment priorities, or a
skills gap in converged <a href="../../a1/VoIP-Network-platform.asp">network</a> implementation-each of which
pose serious challenges to an IP
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> project. The Overlap
option allows a company to
quickly extract value from the
applications enabled by an IP
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> infrastructure without
necessarily deploying IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> servers. Benefits are made
available through user PCs. As the
pool of new users on the IP
<a href="../../a1/VoIP-Network-platform.asp">telephony</a> system grows, maintenance
costs on the PBX can be
reduced.<br />
<br />
Furthermore, it is increasingly
practical to deploy softphones-
telephone dialing and audio control
PC software-to all users,
especially those on digital handsets.
These applications usually
reduce the use of the digital set
completely. This behavior is possible
since processors have grown in
performance and PC operating
systems have improved their stability,
and are now able to support
interrupt-intolerant applications
like <a href="../../a1/VoIP-Network-platform.asp">telephony</a> Call logs can even
provide the trigger moment when
the use of the digital PBX falls
below the economic point of use.</p>

<p class="virtualpage3">
<b>A Combination Of Strategies</b> <span class="credits"></span><br>
In complex environments, some of
these strategies are implemented
at the same time, often because of
a decentralized organization of
telecom assets and responsibilities.
One division may prefer
Forklift and another the Hybrid,
while the corporation may insist
on Overlay to deploy common
convergence applications. Each
decision-making unit has to consider
the risk, cost, and applications
impact for their operation
and their business. It is within
this context, rather than by
strategic design, that an enterprise
might legitimately deploy a
combination of these strategies.</p>



<p class="virtualpage3">
<b>Conclusion</b> <span class="credits"></span><br>
There are as many ways to implement
IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> as there are
ways to slice bread. The following
table indicates strategic alternatives
and associated risks, applications
impact, and cost.
IT architects and <a href="../../a1/VoIP-Network-platform.asp">network</a> designers
need to proceed with caution
and work with companies that
understand the fundamentals of
not only <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>, but more
importantly, of the potential and
value of IP <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a>. The need
for care in selection and implementation
comes from the very
high expectations of users and
customers. It is the author's hope
that this presentation of strategic
options clearly demonstrates the
broad array of opportunities for
enhancing investment returns and
delivering business benefits now.
<br />
<br />
<img src="images/Comparisons_of_strategies.jpg" />
</p>
<p class="virtualpage3">
<b><a href="#"></a></b> <span class="credits"></span><br>
3Com has been a major contributor to the convergence industry since
the introduction in 1998 of the world's first IP-PBX, the NBX solution.
In 1999 the company developed an architecture for a distributed
softswitch for AT&T and brought the first commercially- deployed
carrier softswitch to market. The 3Com VCXT, introduced in 2003, is the
world's first convergence applications suite.
Transforming business through innovation is not new to 3Com. The
company holds over 900 patents and is the market leader in IP
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> for small to medium enterprises. 3Com operates in over 45
countries and has 1,900 employees. We are changing the way business
speaks.</p>


<p class="virtualpage3">
<b>Contact Information</b> <span class="credits"></span><br>
For additional information about the 3Com Strategies for Successful IP
<a href="../../a1/VoIP-Network-platform.asp">Telephony</a> Implementations contact an authorized 3Com reseller or
3Com Global Services
<br />
North America
Telephone: TippingPoint 888 878 3477
Email: NAquotedesk@tippingpoint.com
Asia Pacific
Email: APACquotedesk@tippingpoint.com
Europe, Middle East,and Africa
Email: EMEAquotedesk@tippingpoint.com
Latin America
Email: LATAMquotedesk@tippingpoint.com
<br />
<br />
Visit www.3com.com for more information about 3Com secure converged <a href="../../a1/VoIP-Network-platform.asp">network</a> solutions.
3Com Corporation, Corporate Headquarters, 350 Campus Drive, Marlborough, MA 01752-3064
3Com is publicly traded on NASDAQ under the symbol COMS.
Copyright � 2005 3Com Corporation. All rights reserved. 3Com and the 3Com logo are registered trademarks of 3Com Corporation. All other company and product
names may be trademarks of their respective companies. While every effort is made to ensure the information given is accurate, 3Com does not accept liability for
any errors or mistakes which may arise. All specifications are subject to change without notice. 503140-003 11/05</p>
</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 3, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../doc/3Com_Strategies_for_Successful_IP_Telephony_Deployment.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
