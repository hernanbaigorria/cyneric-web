<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">

<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> OSP Module
User Guide<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">
<b><a href="#">Introduction</a></b> <span class="credits"></span><br>
Businesses are in a constant struggle to control overhead.Voice over IP (VoIP), along with
the IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> (IPT) capabilities it enables, is a technology that offers small- and
medium-sized (SMBs) businesses a choice of solutions that can lower telecommunications
costs and, in many cases, enhance productivity. Additionally, using VoIP will allow these
businesses to take advantage of future application development.<br />
<br />
A straightforward way of controlling telecom costs is through <a href="../../a1/VoIP-Network-platform.asp">network</a> convergencehaving
a single point of connection for delivering services that customers need at a
particular location. This means, of course, that a customer's telephone service provider is
also the customer's provider of data services, such as Internet access, email, and Web
hosting. Service providers understand the importance of being a one-stop shop, which is
why they are expending large amounts of capital to build multi-service <a href="../../a1/VoIP-Network-platform.asp">network</a> s that
support data, voice, and video services. Initially developed for data communications, IP is
proving to be a highly flexible technology for multi-service delivery.<br />
<br />
Looked at from the side of a customer, earlier concerns about VoIP are now less valid:
thanks to advances made in improving all aspects of <a href="../../a1/VoIP-Network-platform.asp">network</a> convergence-e.g. <a href="../a1/VoIP-Network-platform.asp">quality</a> of
service (QoS), reliability, availability--businesses today have far less reason to be
concerned about trusting their voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> to an IP <a href="../../a1/VoIP-Network-platform.asp">network</a> . Whether customers have
invested in a key telephone system (KTS), a traditional PBX or an IP PBX, or are using
Centrex services from the local phone company, they will find that by switching to a VoIP
solution they are able in most cases to reduce telecom costs even as they increase their
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> capabilities for improved productivity.<br />
<br />
<b>A VoIP Solution To Meet Every Need</b><br />
It used to be that, until fairly recently, voice services were the exclusive domain of the
incumbent local exchange carrier (ILEC). Customers had no choice. But new telecom
regulations combined with rapid advances in technology have changed the competitive
landscape. Today, ILECs as well as a variety of competing service providers are delivering
multiple services over a converged <a href="../../a1/VoIP-Network-platform.asp">network</a>  and, in the process, are rapidly changing the
dynamics of the telecom market. For customers this often means better prices and more
choices. Although ILECs and cable companies are largely marketing VoIP services to
<a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">residential</a> broadband customers, their embrace of the technology has helped allay
concerns about VoIP because of their reputation for providing high-<a href="../a1/VoIP-Network-platform.asp">quality</a> voice and video
services. Businesses, for their part, can take advantage of a fast maturing market that
offers them multiple sources for VoIP services, including ILECs, competitive LECs
(CLECs), Internet Service Providers (ISPs), value-added resellers (VARs) as well as a
number of emerging players. SMBs can now partner with a service provider that offers
them the right VoIP solution for the price and also gives them the option of taking
advantage of advanced applications to meet their evolving needs for increased
productivity.<br />
<br />
The benefits of competitive prices and advanced capabilities make VoIP the right choice
for many SMBs. And once a customer decides to adopt VoIP technology, practically
speaking the choice comes down to two solutions-- hosted VoIP and VoIP access service.


</p>

<p class="virtualpage3">
<b><br />Hosted VoIP</b> <span class="credits"></span><br>
Hosted VoIP, or hosted IP PBX/IP Centrex as it is also called, is a fully outsourced
solution. All of the pieces of the VoIP infrastructure (e.g., softswitch, media gateway,
application server, etc.) are located in the service provider's data center. This solution,
however, should not be confused with the managed IP PBX model. Although in both
scenarios the IP PBX is managed by the service provider, in the managed IP PBX scenario
the equipment could reside either at a customer's location or in the service provider's
data center. Additionally, in the managed IP model the IP PBX is dedicated for the use of a
specific customer. By contrast, in a hosted VoIP solution the equipment supports multiple
customers, although from a customer perspective it appears to be a dedicated IP PBX. Put
another way, hosted VoIP is a partitioned, multi-customer solution, whereas managed IP
PBX is a non-partitioned, single-customer solution.<br />
<br />
A big advantage of opting for a hosted VoIP service is that upfront investment is
minimized because the service is priced on a flat-fee basis, typically running around $60-
$70 per month per station, or per user. In addition to an access line and a VoIP dial tone,
the fee typically would include a-la-carte selection of advanced features and either an
unlimited number or a favorably priced bucket of local and long-distance minutes. For a
customer with multiple sites, hosted VoIP allows all locations to have the same <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> capabilities, an abbreviated dialing plan, hunt groups, and uniform voicemail and auto
attendant appearance to outside callers. Another attractive feature of hosted VoIP is that
the service often provides a Web-based service management interface for the customer's
IT staff to expeditiously handle user moves, adds, changes and perform other
administrative tasks. Users, for their part, may have the convenience of a desktop console
or dashboard from which to configure their features and settings.<br />
<br />
As the table below shows, the next several years will represent a nascent period for
hosted VoIP services.<br />
<br />
<img src="images/below_shows1.jpg" />
</p>
<p class="virtualpage3">
<b>VoIP Access Service</b> <span class="credits"></span><br>
As with hosted VoIP service,VoIP access service may offer a package of local and longdistance
calling minutes in addition to a VoIP dial tone. Because many businesses currently
have their own KTS or PBX/IP PBX and will want to protect their investment in this
equipment, a VoIP access service can serve as an introduction to converged <a href="../../a1/VoIP-Network-platform.asp">network</a> 
services-a single connection for customer data and voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>. Because a single
connection typically translates into reduced cost and convenience,VoIP access services
are expected to have fairly rapid growth in the near term, as the following market data
indicates.<br />
<br />
<img src="images/below_shows2.jpg" /><br /><br />
At a minimum,VoIP access service, also called VoIP trunking, offers, a number of business
trunks (i.e. lines) to customers. Each trunk represents a concurrent call or voice channel
serving a customer's key system, PBX/IP PBX. There is not, however, a one-to-one
relationship between the number of access lines and the number of customer stations;
instead, to ensure efficient usage of the access lines, a customer typically will have a ratio
of 3- or 5-to-1 stations for each access line. A key benefit of VoIP access service is that,
depending upon what VoIP capabilities a service provider has in the data center, the
service could offer advanced features beyond those supported by a customer's <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> equipment. An abbreviated dialing plan, a VoIP VPN and a hosted voice mail solution
represent typical value-added services that service providers can bundle with a VoIP
access offering.</p>

<p class="virtualpage3">
<b><br />The Right VoIP Solution</b> <span class="credits"></span><br>
Because a converged <a href="../../a1/VoIP-Network-platform.asp">network</a>  underpins both hosted VoIP and VoIP access services, this in
itself translates into lower <a href="../../a1/VoIP-Network-platform.asp">network</a>  costs while also offering customers the option of
taking advantage of the many advanced features enabled by VoIP. In fact,VoIP technology
can support very sophisticated services -- for example, contact center capabilities such as
advanced routing, account codes, and authorization codes -- thus making VoIP on par with
customer premises-based solutions.Whether a customer chooses a hosted VoIP or a VoIP
access solution, each offers benefit of cost, convenience, and flexibility.<br />
<br />
<br />
<b>Advantages of hosted VoIP</b><br />
As a fully outsourced solution, hosted VoIP offers customers a number of advantages, not
the least of which is that a customer has the full benefit of the service provider's
investment in a VoIP infrastructure:<br />
<br />
. No need for customers to train a dedicated staff, which would be especially
important for many SMBs<br />

. As the customer's business grows, it is much easier to add stations or provide
services to geographically dispersed locations and/or telecommuters. All
&nbsp;&nbsp;customer locations are part of the same "virtual" IP PBX<br />

. Customers can save on inter-office communications and also on local/long
distance calls because service providers typically offer a bucket of minutes<br />

. Service providers can offer a number of advanced features that can include
anything from voice mail to unified messaging and find-me/follow-me<br />
&nbsp;&nbsp;services<br />

. Features may be offered bundled and/or a la carte, so customers pay only for
those they need<br />

. Web-based service management interfaces make certain tasks (e.g., moves, adds,
and changes) easier for IT staff and users.<br />

. Service providers have a vested interest in offering highly reliable services and
regularly enhancing their solutions to ensure customer satisfaction<br /><br />


<b>Advantages of VoIP Access service</b><br />

Customers who self-manage their CPE will find several advantages to using VoIP access
services, including:<br />
<br />
. They will not have to deal with multiple service providers for their WAN, local
ISDN PRI (integrated services digital <a href="../../a1/VoIP-Network-platform.asp">network</a>  primary rate interface), <br />
&nbsp;&nbsp;and longdistance requirements. Instead, they will have one service provider for their
voice and data requirements<br />

. PRI will not be needed at each customer location for PSTN connectivity
because the service provider will support connectivity to the PSTN<br />

. If the service provider has the appropriate VoIP infrastructure, the customer will
be able to augment its CPE capabilities with many advanced features <br />
&nbsp;&nbsp;(e.g.,
abbreviated intra-company dialing, hunt groups, and find-me/follow me)<br />

. Buckets of local and long-distance minutes typically will reduce costs

</p>

<p class="virtualpage3">
<b>The Saving Begin With Integrated Access</b> <span class="credits"></span><br>
Each of the two described solutions benefits customers. However, one is not inherently
superior. SMBs that have invested in CPE tend to have a trusted relationship with the VAR
that sold them their phone system. If these customers have considerable funds tied up in
the phone system, it is unlikely they will want to consider a hosted VoIP solution. For
them a VoIP access service may be the right choice. Even so, there may be situations in
which these customers might benefit from a hosted VoIP solution. For instance, if business
is expanding rapidly into a number of geographically dispersed locations, it may actually
improve voice service and save money on equipment and staff by opting for hosted VoIP
service.<br />
<br />
For customers who have their own <a href="../../a1/VoIP-Network-platform.asp">telephony</a> equipment, a VoIP access service offers
immediate benefits because it obviates the need for separate connectivity for their data
and voice requirements-typically a T1 for Internet access and an ISDN PRI for their IP
PBX. (As noted earlier, the convenience and economics of a single <a href="../../a1/VoIP-Network-platform.asp">network</a>  connection
for voice/data also apply to hosted VoIP.) Moreover, a customer would do well not to
underestimate the difficulty involved in pricing T1 and PRI services. For one thing, prices
for T1s and PRIs can vary greatly from service provider to service provider and from
region to region-T1 Internet access, for instance, can typically range from about $300 a
month to twice that amount, a PRI will be closer to $500 per month. Customers will find
that looking for the best "deal," particularly in an area whether there are multiple service
providers offering these services, is no small task. And the effort can quickly become even
more frustrating if a customer needs T1s and PRIs at geographically dispersed sites and
has to use a different service provider for each location.<br />
<br />
VoIP technology, on the other hand, offers the convenience of a single service provider
for voice/data as well as the economics resulting from <a href="../../a1/VoIP-Network-platform.asp">network</a>  convergence. As a result,
Frost & Sullivan Whitepaper 7
VoIP technology will not only save customers money but will represent a big step towards
an end-to-end IP <a href="../../a1/VoIP-Network-platform.asp">network</a>  that will serve as a platform for deploying innovative
applications going forward.<br />
<br />
<b><br />VoIP is Going Business Grade...And Into the Future</b> <span class="credits"></span><br>
VoIP services can no longer be viewed as not-good-enough-for-business;VoIP is the
technology for today and the future. For SMBs,VoIP offers the benefits of convergence,
advanced features, control, scalability, and flexibility. Leading service providers currently
are hard at work to turn the vision of wireline/wireless convergence into reality. The goal
is to erase any distinction between wireline/wireless <a href="../../a1/VoIP-Network-platform.asp">network</a> s so that the same services
are available to a person regardless of whether the person has a wired or wireless
connection. All <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> in this vision of convergence will travel across an IP <a href="../../a1/VoIP-Network-platform.asp">network</a> .<br />
<br />
SMBs, however, do not have to wait for that day. After evaluating their voice and data
needs, they can decide which VoIP service model reduces telecom costs and enhances
productivity.<br />
<br />

</p>



<p class="virtualpage3">
<b>Covad�s value proposition</b> <span class="credits"></span><br>
As a leading service provider of voice and data services to SMBs, Covad's VoIP offerings
are available as a hosted VoIP or as a trunking service.With its hosted VoIP service, called
vPBX, Covad delivers business voice services over either one or two T1 lines (the two
lines are for increased bandwidth and load-balancing) or, for the more price conscious
customer, a single digital subscriber line (DSL). The vPBX service is particularly suitable
for a site with 20 to about 250 stations.<br />
<br />
As a fully managed solution, Covad's vPBX service offers a number of advantages,
including:<br />
<br />
. Savings of as much as 20 percent in total telecom costs*<br />
. <a href="../../a1/VoIP-NOC-Managment.asp">24/7</a> customer support<br />
. No changing of toll-free and business phone numbers, plus unlimited inter-office
calls<br />
. Easily moving, adding or changing stations as needed<br />
. Routing calls to employee cell, home or any other phone to reduce phone tag<br />
. Increasing productivity of sales/support staff through multiple call routing
schemes<br />
<br />
*Up to 20% total cost of ownership savings based on case studies by Covad comparing total cost of
ownership of Covad's VoIP solutions versus KTS solutions<br />
<br />
PBXi, which is Covad's VoIP trunking service for customers who have invested in a
telephone system, is offered via analog or PRI connections for a customer that has either
an analog KTS or a digital PBX. Covad offers its analog service as eight or 16 trunks.<br />
<br />
Because oversubscription is common for improved usage of available lines, Covad's 8-
trunk PBXi service should be sufficient to support up to 30 stations, while the 16-trunk
PBXi service supports up to approximately 50 stations. In its digital version, PBXi also
supports up to about 100 stations and offers the additional benefit of direct inward
dialing (DID).<br />
<br />
Following are some of the benefits of Covad's PBXi service:<br /><br />

. Customer keeps existing equipment (KTS/PBX/IP PBX)<br />
. Reduces cost and eases manageability by consolidating voice/data on one
<a href="../../a1/VoIP-Network-platform.asp">network</a> <br />
. Enables evolution to new technology when ready<br />
. Predictable monthly fee for phone service, Internet access, email, and Web
hosting<br />
. Peace of mind with service level agreements on service performance<br />
<br />
The broad product portfolio represented by Covad's vPBX and PBXi services allows
Covad to offer VoIP services to a wide range of customers, regardless of whether a
customer has a few or several hundred employees at a single location or has a number of
sites each with a small number of employees. Customers protect their investment
regardless of whether they deployed an analog, traditional or IP-based phone system or a
mix of systems. Customers can, moreover, outsource all of their voice needs to Covad or,
alternatively, use Covad's vPBX service at some locations and PBXi service at other
locations for a hybrid solution. The voice optimization technology deployed by Covad
gives priority to voice packets over Covad's <a href="../../a1/VoIP-Network-platform.asp">network</a> , assuring a high <a href="../a1/VoIP-Network-platform.asp">quality</a> for customer
phone calls.<br />
<br />
Selecting either Covad's vPBX or PBXi service can enhance customer productivity and/or
reduce telecom costs. Additionally, the generous buckets of minutes that come with the
vPBX and PBXi services (up to 101K minutes) in effect amount to free domestic calling<br />
<br />
Frost & Sullivan believes that Covad, a winner of Frost & Sullivan 2005 Market Leadership
Award, is a noteworthy VoIP service provider because its product portfolio and
geographic coverage give it a broad addressable market.

</p>
</div>
</td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 2, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Increasing_Business_Value_with_VoIP.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="html/Increasing_business_value_wtih_voip.asp"></a> <a href="../doc/Increasing_Business_Value_with_VoIP.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
