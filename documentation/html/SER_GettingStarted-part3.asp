<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
SER - Getting Started, Part 3 <br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">


<p class="virtualpage3">
NOTE: The Contact: header will contain an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> "*" character when a SIP client is requesting to be "unregistered"
from the SIP proxy. In fact, many SIP clients can and/or will send a REGISTER message with a
"Contact: *" header when they are rebooted.<br />
<br />
31. If a client is indeed NATed, then we must inform the SER registrar module that it needs to store the actual
IP address and port that the SIP message came from. This information is then used by subsequent calls to
lookup(location) in order to find the public IP address of a SIP client that is behind a NAT device.<br />
<br />
32. Fix_nated_register() is used specifically for processing REGISTER messages from NATed clients. This is
very different from fix_nated_contact() because the former will not alter the URI in the Contact: header
whereas the latter will.<br />
<br />
Fix_nated_register() will only append parameters to the Contact: header URI, which follows RFC3261
Section 10.3 on handling REGISTER messages. If we had used fix_nated_contact() here then you will likely
have compatibility problems where SIP UAs do not honor the 200 OK response that SER replies with upon
successful REGISTRATION. This would then cause the SIP client to loose its registration.<br />
33. Force_rport() adds the received IP port to the top most via header in the SIP message. This enables subsequent
SIP messages to return to the proper port later on in a SIP transaction.<br />
<br />
34. Invite messages have slightly different NAT testing requirements than REGISTER messages. Here we only
test to see if the SIP message has an RFC1918 IP address in the Contact: header and whether or not the
SIP UA contacted SER on a different IP address or port from what is specified in the via header.<br />
<br />
If it is determined that the SIP client is NATed, then we set flag 7 for later reference.<br />
<br />
NOTE: This client_nat_test() only determines if the message sender is behind a NAT device. At this point
in the ser.cfg file we have not determined if the message recipient is NATed or not.<br />
<br />
36. Add the received port to the VIA header.<br /><br />

37. Now we rewrite the messages Contact:header to contain the IP address and port of the public side of the
NATed SIP client.<br /><br />
38. Enable mediaproxy if needed before sending the message to its destination.<br />
<br />
39. Now we find the contact record for the party being called.<br />
<br />
NOTE: A subtle, but very important, detail here is that flag 6 will be set by the call to lookup(location) in
route[2] if the party being called is found in the MySQL location table and is NATed. The reason flag 6 will
be set is because we specified this as the registrar modules nat_flag parameter.<br />
40. Enable mediaproxy if needed before sending the message to its destination.<br />
<br />
Now that we have taken care of all the NAT related items, we can safely send the INVITE message to its
destination.<br />
<br />
41. Route[4] is a convenience route block which enabled mediaproxy if either the message sender (flag 7) or the
message recipient (flag 6) are NATed.<br />
<br />
42. Flag 8 is used to make sure the mediaproxy doesn't get invoked more than once for our call. If mediaproxy
were to erroneously be called more than once then the SIP message would end up with a corrupted SDP
payload because the call to use_media_proxy() would alter the message incorrectly.<br />
<br />
43. If flag 8 is not set then set it to prevent calling route[4] again.
Here we introduce a reply_route handler. A reply_route is defined just like any other block in SER. The only
difference is that it is called onreply_route.
<br />
<br />
Any message that is passed to this block will be returning to the original sender. You can think of these
messages as the response to the original request that the caller made. The types of messages that will appear
here will have an integer response code, much like HTTP response codes. Examples here would be 200, 401,
403, and 404.<br />
<br />
44. In this ser.cfg we are only interested in response codes of 180, 183, and all 2xx messages for NATed clients.
We can check the status as shown with a regular expression. If any of these response codes are found then
this statement will be TRUE.
<br />
<br />
An important thing to note is that we can check flags set in the other route blocks because their scope is still
valid. So our caller and callee NAT flags are still accessible. If flag 6 is set then the caller is NATed, and if
flag 7 is set then the callee is NATed.<br />
45. We can only call use_media_proxy() for SIP messages that have a valid Contact parameter in the SDP
payload. So here we test to make sure the c= parameter is valid by simply checking the SDP payload length.
We assume that if we have an SDP payload then we will have a c= parameter and can call use_media_proxy().<br />
<br />
46. If we were to simply call use_media_proxy() then we would likely see errors in syslog.
Finally we rewrite the messages <Contact:> header to contain the IP address and port of the public side of
the NATed SIP client.


</p>
<p class="virtualpage3">
<b>Using the Mediaproxy Transparent NAT Traversal ser.cfg</b><br />
We have now made SER 100% NAT aware while keeping all of our SIP clients unaware of NAT. To test this new
NAT functionality make sure you have an instance of mediaproxy running on the same physical server as SER.<br />
<br />
Once mediaproxy is running you can start SER and register your SIP clients as normal. It should not matter
whether the SIP client is behind a NAT device or not.<br />
<br />
To see if a calls RTP streams have been routed to mediaproxy you can execute the Python script located at
/usr/local/mediaproxy/sessions.py (assuming you installed mediaproxy at that location.)
<br />
<br />
You can also install the web <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> tool located at /usr/local/mediaproxy/web/ if you have an Apache web
server running on your SIP proxy. A screen shot of this web <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> tool is available at http://www.ag-projects.
com/docs/MediaSessions.pdf
<b>Handling of NAT using RTPproxy</b><br />
What this ser.cfg will do:<br />
<br />
1. Introduce specialized processing which is necessary when dealing with SIP clients that are behind a NAT
device such as a DSL router or corporate firewall<br />
<br />
RTPproxy is one of two NAT traversal solutions for SER, the other is mediaproxy just descussed in the previous
section. Both RTPproxy and mediaproxy are known as far-end NAT traversal solutions which means that they
handle all aspects of NAT at the SIP proxy location rather than at the SIP client location.<br />
<br />
There are many advantages to handling NAT issues at the SIP proxy server, the primary benefit being that SIP
client configuration is much simpler. Some of the important features of RTPproxy are:<br />
<br />
1. Is called by nathelper, the most configurable NAT helper modules (mediaproxy module is the other one)<br />
2. Can handle almost twice as many calls as mediaproxy on the same hardware<br />
3. Developed in C and is thus easily extendable for most programmers<br />
4. RTPproxy can be installed on a remote server which is not running SER<br />
<br />
RTPproxy is a standalone software and is thus not a part of the sip_router CVS and distribution. However, it can
be found in the same CVS repository as SER (on the same level as sip_router). Just replace the sip_router directory
name with rtpproxy (or use the ONsip.org Getting Started source package). The SER distribution only includes
the glue which gives SER the ability to communicate with a running instance of RTPproxy. This glue is known
as the nathelper module.<br />
<br />
NOTE: In order for RTPproxy to function properly it must be configured to listen on a public IP address. Also, in
most real world configurations, RTPproxy will not be installed on the SER server, but on a remote machine. Refer
to the appendix for information on installing rtpproxy.<br />
<br />
debug=3<br />
fork=yes<br />
log_stderror=no<br /><br />

listen=192.0.2.13 # INSERT YOUR IP ADDRESS HERE<br />
port=5060<br />
children=4<br />
<br />
dns=no<br />
rev_dns=no<br />
fifo="/tmp/ser_fifo"<br />
fifo_db_url="mysql://ser:heslo@localhost/ser"<br />
<br />

loadmodule "/usr/local/lib/ser/modules/mysql.so"<br />
loadmodule "/usr/local/lib/ser/modules/sl.so"<br />
loadmodule "/usr/local/lib/ser/modules/tm.so"<br />
loadmodule "/usr/local/lib/ser/modules/rr.so"<br />
loadmodule "/usr/local/lib/ser/modules/maxfwd.so"<br />
loadmodule "/usr/local/lib/ser/modules/usrloc.so"<br />
loadmodule "/usr/local/lib/ser/modules/registrar.so"<br />
loadmodule "/usr/local/lib/ser/modules/auth.so"<br />
loadmodule "/usr/local/lib/ser/modules/auth_db.so"<br />
loadmodule "/usr/local/lib/ser/modules/uri.so"<br />
loadmodule "/usr/local/lib/ser/modules/uri_db.so"<br />
loadmodule "/usr/local/lib/ser/modules/nathelper.so"<br />
loadmodule "/usr/local/lib/ser/modules/textops.so"<br />
<br />
modparam("auth_db|uri_db|usrloc", "db_url", "mysql://ser:heslo@localhost/ser")<br />
modparam("auth_db", "calculate_ha1", 1)<br />
modparam("auth_db", "password_column", "password")<br />
<br />
modparam("nathelper", "natping_interval", 30)<br />
modparam("nathelper", "ping_nated_only", 1)<br />
modparam("nathelper", "rtpproxy_sock", "unix:/var/run/rtpproxy.sock")<br /><br />

modparam("usrloc", "db_mode", 2)<br />
<br />

modparam("registrar", "nat_flag", 6)<br />
<br />

modparam("rr", "enable_full_lr", 1)<br />
<br />

route {




</p>

<p class="virtualpage3">
# -----------------------------------------------------------------<br />
# Sanity Check Section<br />
# -----------------------------------------------------------------<br />
if (!mf_process_maxfwd_header("10")) {<br />
sl_send_reply("483", "Too Many Hops");<br />
break;<br />
};<br />
if (msg:len > max_len) {<br />
sl_send_reply("513", "Message Overflow");<br />

};<br />
# -----------------------------------------------------------------<br />
# Record Route Section<br />
# -----------------------------------------------------------------<br />
if (method!="REGISTER") {<br />
record_route();<br />
};<br />
if (method=="BYE" || method=="CANCEL") {<br />
unforce_rtp_proxy();<br />
}<br />
# -----------------------------------------------------------------<br />
# Loose Route Section<br />
# -----------------------------------------------------------------<br />
if (loose_route()) { 11<br />
if ((method=="INVITE" || method=="REFER") && !has_totag()) {<br />
sl_send_reply("403", "Forbidden");<br />
break;<br />
};<br />
if (method=="INVITE") {<br />
<br />
if (!proxy_authorize("","subscriber")) {<br />
proxy_challenge("","0");<br />
break;<br />
} else if (!check_from()) {<br />
sl_send_reply("403", "Use From=ID");<br />
break;<br />
};<br />
<br />
consume_credentials();<br />
if (nat_uac_test("19")) {<br />
setflag(6);<br />
force_rport();<br />
fix_nated_contact();<br />
};
<br />

force_rtp_proxy("l");<br />
};<br />
route(1);<br />
break;<br />
};<br />
# -----------------------------------------------------------------<br />
# Call Type Processing Section<br />
# -----------------------------------------------------------------<br />
if (uri!=myself) {<br />
route(4);12<br />
route(1);<br />
break;<br />
};<br />
if (method=="ACK") {<br />
<br />
route(1);<br />
break;<br />
} if (method=="CANCEL") { 13<br />
route(1);<br />
break;<br />
} else if (method=="INVITE") {<br />
route(3);<br />
break;<br />
} else if (method=="REGISTER") {<br />
route(2);<br />
break;<br />
};<br />
lookup("aliases");<br />
if (uri!=myself) {<br />
route(4); 14<br />
route(1);<br />
break;<br />
};<br />
if (!lookup("location")) {<br />
sl_send_reply("404", "User Not Found");<br />
break;<br />
};<br />
route(1);<br />
}<br />
route[1] {<br />
# -----------------------------------------------------------------<br />
# Default Message Handler<br />
# -----------------------------------------------------------------<br />
t_on_reply("1"); 15<br />
if (!t_relay()) { 16<br />
if (method=="INVITE" && isflagset(6)) {<br />
unforce_rtp_proxy();<br />
};<br />
sl_reply_error();<br />
};<br />
}<br />
route[2] {<br />
# -----------------------------------------------------------------<br />
# REGISTER Message Handler<br />
# ----------------------------------------------------------------<br />
17if (!search("^Contact:[ ]*\*") && nat_uac_test("19")) {<br />
setflag(6);<br />
fix_nated_register();<br />
force_rport();<br />
};<br />
sl_send_reply("100", "Trying");<br />




</p>

<p class="virtualpage3">
if (!www_authorize("","subscriber")) {<br />
www_challenge("","0");<br />
break;<br />
};<br />
if (!check_to()) {<br />
sl_send_reply("401", "Unauthorized");<br />
break;<br />
};<br />
consume_credentials();<br />
if (!save("location")) {<br />
sl_reply_error();<br />
};<br />
}<br />
route[3] {<br />
# -----------------------------------------------------------------<br />
# INVITE Message Handler<br />
# -----------------------------------------------------------------<br />
if (!proxy_authorize("","subscriber")) {<br />
proxy_challenge("","0");<br />
break;<br />
} else if (!check_from()) {<br />
sl_send_reply("403", "Use From=ID");<br />
break;<br />
};<br />
consume_credentials();<br />
if (nat_uac_test("19")) { 18<br />
setflag(6);<br />
}<br />
lookup("aliases");<br />
if (uri!=myself) {<br />
route(4); 19<br />
route(1);<br />
break;<br />
};<br />
if (!lookup("location")) {<br />
sl_send_reply("404", "User Not Found");<br />
break;<br />
};<br />
route(4); 20<br />
route(1); 21<br />
}<br />
22route[4] {<br />
# -----------------------------------------------------------------<br />
# NAT Traversal Section<br />
# -----------------------------------------------------------------<br />
if (isflagset(6)) {<br />
force_rport();<br />
fix_nated_contact();<br />
force_rtp_proxy();<br />
}<br />
}<br />
23onreply_route[1] {<br />
if (isflagset(6) && status=~"(180)|(183)|2[0-9][0-9]") { 24<br />
if (!search("^Content-Length:[ ]*0")) { 25<br />
force_rtp_proxy();<br />
};<br />
};<br />
26if (nat_uac_test("1")) {<br />
fix_nated_contact();<br />
};<br />
}<br />
<br />
<b>RTPproxy Transparent NAT Traversal ser.cfg Analysis</b><br />
1. Up until now we have run SER as a foreground process. From this point forward we will run SER as a daemon.
The fork directive tells the SER daemon to run in the background. This is a requirement for using the init.d
start script shown in the appendix.<br />
<br />
2. Since we are running SER as a background process we must set the log_stderror directive equal to no in order
to not keep SER in the foreground.<br />
<br />
3. The uri module is introduced here to access the has_totag() function. This function is necessary for processing
re-INVITEs and is described below.<br />
<br />
4. Here we load the nathelper module. Nathelper has functionality for rewriting SIP messages. It also communicates
with rtpproxy, which is a standalone program. Make sure that you have rtpproxy started before you
start SER. It can be found in the same cvs repository as SER or you can use the SER package provided on
http://www.onsip.org/ under Downloads. Start rtpproxy without parameters and make sure it is running using
the 'ps -ax | grep rtpproxy' command. If SER cannot communicate with rtpproxy, you will get errors in
/var/log/messages when staring SER."<br />
<br />
5. The textops module provide utility functions for manipulating text, searching for substrings, and checking
for the existence of specific header fields.<br />
<br />
6. Unlike mediaproxy, rtpproxy does not have a built-in ping keep-alive capability (i.e. sending packets regularly
to the clients to make sure the NAT binding is kept in place and thus incoming SIP messages or RTP streams
can come in). However, this functionality is instead built into nathelper. <br />
<br />
The natping_interval is a very crucial setting which controls how often our ser proxy will ping registered SIP
UAs. Most NAT devices will only hold port bindings open for a minute or two, so we specify 30 seconds
here.<br />
<br />
This causes ser to send a 4-byte UDP packet to each SIP UA every 30 seconds. This is all that is required to
keep NATed clients alive.<br />
<br />
NOTE: Some NAT devices have been reported to not allow incoming keep-alives. Thus, many UAs have
their own implementations of keep-alive. If you experience one-way audio problems after a while, you may
have run into this problem. The only solution is to turn on user client keep-alives.<br />
7. Nathelper can ping all UAs or only the ones that have been flagged with a special nat flag (see further below).
We only need to ping NATed clients as we assume all other clients have public addresses and keep-alive is
not needed.<br />
8. Ser and rtpproxy communicate via a standard Unix socket, with the default socket path specified here. <br />
<br />
NOTE: If you change the socket path here, you must be sure to start rtpproxy with the parameter: s
unix:socketpath<br />
<br />






<br />




</p>

<p class="virtualpage3">
9. You can also start rtpproxy with f to let it run without forking. It will then show you what is happening.
When SIP clients attempt to REGISTER with our SIP proxy we need a way to tell the registrar module to
store NAT information for the UA. We do this by using flag 6, which has been arbitrarily chosen (but defined
earlier in the moduel parameter section). We could have specified another integer here, but 6 has become the
accepted default for nat_flag.<br />
<br />
10. When nat_flag is set before calling the save() function to store contact information, then ser will also preserve
the NAT contact information as well as set the flags column in the MySQL location table. By doing so, we
can call lookup(location) when processing messages and flag 6 will be set for NATed clients.
When we decide that a particular call must be proxied through our proxy server, we must also ensure that
the call is torn down when a call is hung up (BYE) or cancelled (CANCEL).<br />
<br />
11. Our NAT traversal requirements must handle re-INVITE messages in a special manner to prevent RTP media
streams from dropping off during a re-INVITE. So we do special re-INVITE NAT processing here. The
has_totag() function will return true if the SIP message has a To header field. If it has, the message be will
be in-dialog.<br />
<br />
force_rtp_proxy() has an l (lookup) parameter. When this parameter is specified, rtpproxy will only proxy
the call if a session already exists. The reason for doing this is that rtpproxy cannot really know that this is
a re-INVITE and we want to avoid unnecessary proxying calls that do not need it. All re-INVITEs will call
force_rtp_proxy(l), but only existing proxied sessions will be re-proxied. We cannot put the force_rtp_proxy()
call inside the nat_uac_test check, as this will prevent proxying to happen if the re-INVITE is sent from a
non-NATed UA to a NATed UA (because nat_uac_test() will be false).<br />
<br />
The other things we do here are: if NAT is detected for the UA sending the INVITE, set the NAT flag, so
we know this is a NATed call (setflag(6)), add the received port to top-most via header (force_rport), as well
as rewrite the Contact header with the public IP address of the user client (fix_nated_contact).<br />
11. In the event that our message is no longer to be handled by our SIP router, we call our NAT handling route
block to enable rtpproxy if needed before sending the message to its destination.<br />
<br />
12. We now explicitly handle CANCEL messages. CANCEL messages can be safely processed with a simple
call to t_relay() because SER will automatically match the CANCEL message to the original INVITE message.
So here we just route the message to the default message handler.<br />
<br />
14. Enable rtpproxy if needed before sending the message to its destination.<br />
<br />
15. When dealing with NATed clients, we must correctly handle response messages that are destined back to the
client. These response messages are accessible in ser by using a reply_route block. <br />
<br />
Ser allows you to specify multiple reply_route blocks, which can perform many tasks. Here we specify that
any reply messages must be passed to reply_route["1"], which is defined at the end of the ser.cfg file.<br />
<br />
In order to invoke a reply_route, you simply need to set the handler prior to calling t_relay(), as we have
done here.<br />
<br />
16. If something goes wrong and we have called force_rtp_proxy (so that rtpproxy set up a new proxy call), we
need to tell rtpproxy to tear it down again using unforce_rtp_proxy().
<br />
<br />
17. When SIP clients attempt to REGISTER with our SIP proxy we need a way to tell the registrar module to
store NAT information this particular UA. We do this by using flag 6, which has been arbitrarily chosen (but
defined earlier in the module parameter section). We could have specified another integer here, but 6 seems
to be the accepted standard for nat_flag.<br />
<br />
When nat_flag is set before calling the save() function to store contact information, then ser will also preserve
the NAT contact information as well as set the flags column in the MySQL location table. By doing so, we
can later call lookup(location) when processing messages and flag 6 will be set for NATed clients.<br />
<br />
In order to determine whether a SIP client is NATed we use the nathelper function nat_uac_test(), which
accepts an integer as a parameter. This parameter specifies which part of the SIP message to examine for
determining the NAT status of the client. 19 indicates a common subset of tests, and are the recommended
tests.<br />
<br />
NOTE: These are the tests you can run using nat_uac_test (in the order they are employed):<br />
<br />
1. (16) Is received port (source port of the packet) different from the port specified in the top-most Via
header? (this can be used to detect some broken STUN implementations)<br />
2. (2) Is the received IP address (source IP of the packet) different from the IP address in the top-most Via
header?<br />
3. (1) Does the Contact header contain an RFC1918 address? RFC1918 addresses are 10.0.0.0/24,
172.16.0.0.0/20, and 192.168.0.0/16<br />
4. (8) Does the SDP payload of an INVITE or OK message contain an RFC1918 address?<br />
5. (4) Does the top-most Via header contain an RFC1918 address?<br />
<br />
The numbers in paranthesis indicate the number to be used to invoke the test in nat_uac_test(). To invoke
more than one test, add up the numbers of the tests you want to run. So, as we use nat_uac_test(19) in our
script, it means that test #1 (16) + #2 (2) + #3 (1) = 19, are run.<br />
<br />
WARNING: Before changing the tests, you should know what you are doing. You should scrutinize the SIP
message coming from your UAs to determine which tests will be true.<br />
<br />
You will notice that we first call the search() function to find out if the Contact: header contains an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
or not. We only test for NAT if the Contact: header is not an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
<br />
NOTE: If the <Contact:> header is an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> rather than a SIP URI, then this indicates that the SIP client
is attempting to unregister with the SIP proxy. The ser save() function has special provisions to remove all
binds for the AOR in this case. It is important to understand that we cannot accurately determine the NAT
status of a client when the contact information is missing<br />


</p>

<p class="virtualpage3">

18. We test to see if the user client is behind NAT, if so, we set the NAT flag (6). This flag is used further below.<br />
19.  Enable rtpproxy if needed before sending the message to its destination.<br />
20.  Enable rtpproxy if needed before sending the message to its destination.<br />
21.  Now that we have taken care of all the NAT related items, we can safely send the INVITE message to its
destination.<br />
22. Route[4] is a convenience route to enable rtpproxy.<br /><br />
If a client is NATed, we must do the following things: Add the received (public) port to the top-most Via
header (force_rport), rewrite the Contact header with the public IP address and port of the NAT in front of
the user client (fix_nated_contact), and set up the proxying using force_rtp_proxy(). Nathelper will then
communicate to rtpproxy, which will allocate RTP (UDP) ports and the SDP payload of the INVITE will be
rewritting (see the introductory section on NATing for details).<br />
23.Here we introduce a reply_route. A reply_route is defined just like any other block in ser. The only difference
is that it is called onreply_route.<br />
<br />
Any message that is passed to this block will be returning to the original sender. You can think of these
messages as the response to the original request that the caller made. The types of messages that will appear
here will have an integer response code, much like HTTP response codes. Examples here would be 200, 401,
403, and 404.<br />
<br />
24. In this ser.cfg we are only interested in response codes of 180, 183, and all 2xx messages for NATed clients.
We can check the status as shown with a regular expression. If any of these response codes are found then
this statement will be TRUE.<br />
<br />
An important thing to note is that we can check flags set in the other route blocks because their scope is still
valid. So our caller and callee NAT flags are still accessible.<br />
We can only call force_rtp_proxy() for SIP messages that have a valid contact parameter in the SDP payload.
So here we test to make sure the c= parameter is valid by simply checking the SDP payload length. We assume
that if we have an SDP payload then we will have a c= parameter and can call force_rtp_proxy ().<br />
26. Before our reply_route processing is complete we have one more NAT test to try. Here we make sure the
Contact: header does not contain an RFC1918 IP address. If it does, then we replace it with the public IP
address and port of the NAT in front of the user client.<br />
<br />
<b>Chapter 9. PSTN Gateway Connectivity</b><br /><br />

What this ser.cfg does:<br /><br />
<br />
1. Adds the ability to connect to the PSTN for inbound and outbound calling<br />
<br />
Up until now we have focused on SIP only calls, which means you could only send and receive calls between IP
phones on your SIP proxy. But most people will need to call regular land lines as well as receive calls from them.<br />
<br />
This is accomplished by means of a PSTN Gateway. SER itself does not provide PSTN access. You must install
external equipment for this. A very popular and cost effective solution is a <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> AS5300. The <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> AS5300 is
a multi-function chassis which allows you to plug in many different feature cards. In order for the <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> AS5300
to operate as a PSTN gateway you must have a T1/E1 interface board and a Voice Card. The T1/E1 card is used
to connect the AS5300 to the actual PSTN by means of a PRI from your local telephone company. The voice card
contains DSP (digital signal processing) chips that convert audio streams between the SIP world and the PSTN
world.<br />
<br />
NOTE: Some users have asked how do I get a real telephone number for my SIP phone and the answer to that
question is to get a PSTN gateway. When your local telephone company installs your PRI they will be able to allocate
DIDs (direct inward dial, or telephone numbers) to you. You can then assign the DIDs as usernames in the
MySQL subscriber table and when the DID is called from the PSTN it will cause your PSTN gateway to generate
an INVITE message and send it to SER which will ring the SIP phone just like a normal SIP-to-SIP call.<br />
<br />
When dealing with PSTN calls, you can actually think of the SIP client as one end of the call and the PSTN gateway
as the other end. Many people get confused on this minor detail because they fail to understand that the actual
PSTN gateway is indeed a SIP client itself.<br />
<br />
<img src="images/Reference_Design_PSTN.jpg" /><br />
<br />



</p>

<p class="virtualpage3">
In order to call a PSTN phone from your SIP phone SER receives the INVITE message as usual. If the R-URI
(request URI) is determined to be a PSTN number then SER changes the host IP of the R-URI to be that of the
PSTN gateway and then it relays the message to the PSTN gateway. After this, the call behaves just like a normal
SIP-to-SIP call.<br />
<br />
When receiving a call from the PSTN, the PSTN gateway will send a new INVITE message to SER and it will be
processed just like any normal SIP-to-SIP call.<br />
<br />
So now that you have a brief understanding of how SIP-to-PSTN and PSTN-to-SIP calling works, lets take a look
at the PSTN-enabled ser.cfg configuration file.<br />
<br />
NOTE: This ser.cfg file does not take in to consideration user ACLs. This means that all registered user accounts
will have PSTN access. We will introduce ACL control in a future example.<br />
<br />
debug=3<br />
fork=yes<br />
log_stderror=no<br /><br />


listen=192.0.2.13 # INSERT YOUR IP ADDRESS HERE<br />
port=5060<br />
children=4<br /><br />

dns=no<br />
rev_dns=no<br />
fifo="/tmp/ser_fifo"<br />
fifo_db_url="mysql://ser:heslo@localhost/ser"<br />
<br />

loadmodule "/usr/local/lib/ser/modules/mysql.so"<br />
loadmodule "/usr/local/lib/ser/modules/sl.so"<br />
loadmodule "/usr/local/lib/ser/modules/tm.so"<br />
loadmodule "/usr/local/lib/ser/modules/rr.so"<br />
loadmodule "/usr/local/lib/ser/modules/maxfwd.so"<br />
loadmodule "/usr/local/lib/ser/modules/usrloc.so"<br />
loadmodule "/usr/local/lib/ser/modules/registrar.so"<br />
loadmodule "/usr/local/lib/ser/modules/auth.so"<br />
loadmodule "/usr/local/lib/ser/modules/auth_db.so"<br />
loadmodule "/usr/local/lib/ser/modules/uri.so"<br />
loadmodule "/usr/local/lib/ser/modules/uri_db.so"<br />
loadmodule "/usr/local/lib/ser/modules/domain.so"<br />
loadmodule "/usr/local/lib/ser/modules/mediaproxy.so"<br />
loadmodule "/usr/local/lib/ser/modules/nathelper.so"<br />
loadmodule "/usr/local/lib/ser/modules/textops.so"<br />
loadmodule "/usr/local/lib/ser/modules/avpops.so"<br />
loadmodule "/usr/local/lib/ser/modules/permissions.so"<br /><br />

modparam("auth_db|permissions|uri_db|usrloc",<br />
"db_url", "mysql://ser:heslo@localhost/ser")<br />
modparam("auth_db", "calculate_ha1", 1)<br />
modparam("auth_db", "password_column", "password")<br />
<br />
modparam("nathelper", "rtpproxy_disable", 1)<br />
modparam("nathelper", "natping_interval", 0)<br />
<br />
modparam("mediaproxy","natping_interval", 30)<br />
modparam("mediaproxy","mediaproxy_socket", "/var/run/mediaproxy.sock")<br />
modparam("mediaproxy","sip_asymmetrics","/usr/local/etc/ser/sip-clients")<br />
modparam("mediaproxy","rtp_asymmetrics","/usr/local/etc/ser/rtp-clients")<br /><br />

modparam("usrloc", "db_mode", 2)<br />
<br />

modparam("registrar", "nat_flag", 6)<br />
<br />

modparam("rr", "enable_full_lr", 1)<br />
<br />

modparam("tm", "fr_inv_timer", 27)<br />
modparam("tm", "fr_inv_timer_avp", "inv_timeout")<br />
<br />

modparam("permissions", "db_mode", 1)<br />
modparam("permissions", "trusted_table", "trusted")<br />
<br />

route {<br />
<br />
# -----------------------------------------------------------------<br />
# Sanity Check Section
# -----------------------------------------------------------------<br />
if (!mf_process_maxfwd_header("10")) {<br />
sl_send_reply("483", "Too Many Hops");<br />
break;<br />
};<br />
if (msg:len > max_len) {<br />
sl_send_reply("513", "Message Overflow");<br />
break;<br />
};<br />
# -----------------------------------------------------------------<br />
# Record Route Section<br />
# -----------------------------------------------------------------<br />
if (method=="INVITE" && client_nat_test("3")) {<br />
# INSERT YOUR IP ADDRESS HERE<br />
record_route_preset("192.0.2.13:5060;nat=yes");<br />
} else if (method!="REGISTER") {<br />
record_route();<br />
};<br />

# -----------------------------------------------------------------<br />
# Call Tear Down Section<br />
# -----------------------------------------------------------------<br />
if (method=="BYE" || method=="CANCEL") {<br />
end_media_session();<br />
};<br />
# -----------------------------------------------------------------<br />
# Loose Route Section<br />
# -----------------------------------------------------------------<br />
if (loose_route()) {<br />
if ((method=="INVITE" || method=="REFER") && !has_totag()) {<br />
sl_send_reply("403", "Forbidden");<br />
break;<br />
};<br />
if (method=="INVITE") {<br />
if (!allow_trusted()) {<br />
if (!proxy_authorize("","subscriber")) {<br />
proxy_challenge("","0");<br />
break;<br />
} else if (!check_from()) {<br />
sl_send_reply("403", "Use From=ID");<br />
break;<br />
};<br />
consume_credentials();<br />
};<br />
if (client_nat_test("3")||search("^Route:.*;nat=yes")){<br />
setflag(6);<br />
use_media_proxy();<br />
};<br />
};<br />
route(1);<br />
break;<br />
};<br />

</p>
<p class="virtualpage3">
# -----------------------------------------------------------------<br />
# Call Type Processing Section<br />
# -----------------------------------------------------------------<br />
if (!is_uri_host_local()) {<br />
if (is_from_local() || allow_trusted()) {<br />
route(4);11<br />
route(1);<br />
} else {<br />
sl_send_reply("403", "Forbidden");12<br />
};<br />
break;13<br />
};<br />
if (method=="ACK") {<br />
route(1);<br />
break;<br />
} if (method=="CANCEL") {<br />
route(1);<br />
break;<br />
} else if (method=="INVITE") {<br />
route(3);<br />
break;<br />
} else if (method=="REGISTER") {<br />
route(2);<br />
break;<br />
};<br />
lookup("aliases");<br />
if (!is_uri_host_local()) { 14<br />
route(4);<br />
route(1);<br />
break;<br />
};<br />
if (!lookup("location")) {<br />
sl_send_reply("404", "User Not Found");<br />
break;<br />
};<br />
route(1);<br />
}<br />
route[1] {<br />
# -----------------------------------------------------------------<br />
# Default Message Handler<br />
# -----------------------------------------------------------------<br />
t_on_reply("1");<br />
if (!t_relay()) {<br />
if (method=="INVITE" || method=="ACK") {<br />
end_media_session();<br />
};<br />
sl_reply_error();<br />
};<br />
}<br />
route[2] {<br />
# -----------------------------------------------------------------<br />
# REGISTER Message Handler<br />
# -----------------------------------------------------------------<br />
sl_send_reply("100", "Trying");<br />
if (!search("^Contact:[ ]*\*") && client_nat_test("7")) {<br />
setflag(6);<br />
fix_nated_register();<br />
force_rport();<br />
};<br />
if (!www_authorize("","subscriber")) {<br />
www_challenge("","0");<br />
break;<br />
};<br />
if (!check_to()) {<br />
sl_send_reply("401", "Unauthorized");<br />
break;<br />
};<br />
consume_credentials();<br />
if (!save("location")) {<br />
sl_reply_error();<br />
};<br />
}<br />
route[3] {<br />
# -----------------------------------------------------------------<br />
# INVITE Message Handler<br />
# -----------------------------------------------------------------<br />
if (client_nat_test("3")) {<br />
setflag(7);<br />
force_rport();<br />
fix_nated_contact();<br />
};<br />
if (!allow_trusted()) { 15<br />
if (!proxy_authorize("","subscriber")) {<br />
proxy_challenge("","0");<br />
break;<br />
} else if (!check_from()) {<br />
sl_send_reply("403", "Use From=ID");<br />
break;<br />
};<br />
consume_credentials();<br />
};<br />
if (uri=~"^sip:1[0-9]{10}@") { 16<br />
strip(1);<br />
};<br />
lookup("aliases");<br />
if (!is_uri_host_local()) { 17<br />
route(4);<br />
route(1);<br />
break;<br />
};<br />
if (uri=~"^sip:011[0-9]*@") { # International PSTN 18<br />
route(4);<br />
route(5);<br />
break;<br />
};<br />
if (!lookup("location")) {<br />
if (uri=~"^sip:[0-9]{10}@") { # Domestic PSTN 19<br />
route(4);<br />
route(5);<br />
break;<br />
};<br />
sl_send_reply("404", "User Not Found");<br />
break;<br />
};<br />
route(4);<br />
route(1);<br />
}<br />
route[4] {<br />
# -----------------------------------------------------------------<br />
# NAT Traversal Section<br />
# -----------------------------------------------------------------<br />
if (isflagset(6) || isflagset(7)) {<br />
if (!isflagset(8)) {<br />
setflag(8);<br />
use_media_proxy();<br />
};<br />
};<br />
}<br />
20route[5] {<br />
# -----------------------------------------------------------------<br />
# PSTN Handler<br />
# -----------------------------------------------------------------<br />
rewritehost("192.0.2.245");21 # INSERT YOUR PSTN GATEWAY IP ADDRESS<br />
avp_write("i:45", "inv_timeout");22<br />
route(1);<br />
}<br />
onreply_route[1] {<br />
if ((isflagset(6) || isflagset(7)) && <br />
(status=~"(180)|(183)|2[0-9][0-9]")) {<br />
if (!search("^Content-Length:[ ]*0")) {<br />
use_media_proxy();<br />
};<br />
};<br />
if (client_nat_test("1")) {<br />
fix_nated_contact();<br />
};<br />
}<br />
</p>

<p class="virtualpage3">
<b>PSTN Gateway Connectivity ser.cfg Analysis</b><br />
1. The avpops module is a very power library for manipulating SIP message headers, storing temporary or
persistent message data, and comparing data items. Avpops is also used to dynamically alter certain parts of
SER itself. We use avpops in this example to dynamically change the amount of time SER will wait for an
INVITE message to be forwarded to its destination.<br />
<br />
The reason we need to do this is that a SIP-to-SIP call generally connects much faster than a SIP-to-PSTN
call. Therefore we will allow SER to wait a while longer when calling a PSTN telephone<br />
2. The permissions module gives SER access to the trusted MySQL table. The trusted table is acts as a repository
of privileged devices. We must add our PSTN gateway device to the trusted table to prevent SER from
challenging the PSTN gateway for authentication credentials.<br />
<br />
3. The permissions module needs to access MySQL so we add it to the db_url parameter list.<br />
<br />
4. The default INVITE timer will be allowed 27 seconds to connect. If we are going to call a PSTN destination
then we will set the inv_timeout AVP to a higher value in order to allow additional time for the call to connect.
This is show in the PSTN route handler.<br />
<br />
5. The permissions module needs to know that it should connect to the MySQL database to find its data.<br />
<br />
6. The trusted_table parameter informs the permissions module as to which MySQL table it will read from. In
this case the table called trusted will be used.<br />
<br />
7. Now that we can receive INVITE messages from registered SIP clients as well as our PSTN gateway, we
need to challenge INVITE messages in a more specialized fashion. The allow_trusted() function is provided
by the permissions module. It will access the MySQL table called "trusted" to get a list of privileged IP addresses.
When the PSTN gateway sends an INVITE to SER, the allow_trusted() should return TRUE to
prevent SER from replying with a 401 Unauthorized message.<br />
<br />
As a general rule, the PSTN gateway should always be allowed to send messages to SER without being
challenged for credentials, however, all INVITE messages being sent to the PSTN gateway should be challenged
to prevent toll fraud for which the PSTN gateway owner is responsible for.<br />
<br />
NOTE: It is very important to understand the significance of using proxy_authorize() and allow_trusted() in
the loose_route() code block. A re-INVITE should only be processed by SER if it originated from a SIP client
that can be authenticated or trusted by SER. Authentication is verified with the call to proxy_authorize(),
which will check the MySQL subscriber table whereas trust relationships are tested with the call to allow_trusted(),
which will check the MySQL trusted table. A PSTN gateway which cannot authenticate against SER
must be added to the MySQL trusted table, otherwise re-INVITE and REFER messages from the PSTN
gateway will not be routed by SER.<br />
8. Our previous configuration files had a potential open relay problem where by SIP messages targeted at a
domain that is not locally served could be relayed without being challenged. This was exposed in the following
code<br />
<br />
if (uri!=myself) {<br />
route(4);<br />
route(1);<br />
break;<br />
}; <br />
<br />
9. Now that our SIP proxy has PSTN access we need to close this potential open relay to prevent toll fraud.
In previous ser.cfg examples we used the test condition (uri != myself) to determine if the SIP message was
directed at our SIP router or not. From this point forward will will replace this test condition with a call to
is_uri_host_local(). which will determine if the domain portion of the Request URI is a domain that our SIP
proxy is responsible for. It does so by querying the MySQL table called domain. In order for this function
to return TRUE for messages that our SIP proxy is responsible for, we must add our domain to the MySQL
domain table. If your SIP proxy handles multiple domains, then you must add all domains to this table.<br />
<br />
10. NOTE: The is_uri_host_local() function is exposed in the domain.so module.
If the SIP message is not targeted at our domain, we must now check to see if the message sender belongs
to our domain. We should only forward messages to external domains if the message sender belongs to our
domain.<br />
<br />
The is_from_local() function will query the MySQL domain table for the host name that appears in the
From: header of the SIP message.<br />
<br />
NOTE: The is_from_local() function is exposed in the domain.so module.<br />
<br />
Another thing that we must allow is messages from our PSTN gateway. Since our PSTN gateway is a trusted
device, it must be allowed to forward messages to external domains. This is accomplished with the call to
allow_trusted().<br />
<br />
11. If the message is permitted to leave our SIP proxy then we must take care of NAT traversal prior to relaying
the message.<br />
<br />
12. If the message is not permitted to leave our SIP proxy then we reply with a 403 message to inform the message
sender that we are not an open relay.<br />
<br />
13. Now that we have handled the message we simply stop processing.<br />
<br />
14. Determine if the domain portion of the Request URI is a domain that our SIP proxy is responsible for.<br />
<br />
15. INVITE message must be challenged for credentials here just as they were in the loose_route() section of
the main route code block.<br />
<br />
NOTE: It is important to understand that INVITE messages can potentially pass through the SIP router in
either route[3] or in the loose_route() section of the main route. Generally, original INVITE messages will
be passed to route[3], whereas re-INVITE messages will get picked up by the loose_route() function.
Therefore it is very important to secure both sections with the appropriate calls as demonstrated here.<br />
<br />
NOTE: Failure to properly secure your SIP route could result in toll charges that you are responsible for,
assuming your SIP router has access to the PSTN.<br />
16. Many PSTN gateways do need or permit long distance prefixes when dialing. In North America we dial 1 +
a 10-digit number to place a call outside of our local calling area. So we remove the leading 1 from the RURI
before sending the message to the PSTN gateway.<br />
<br />
17. Determine if the domain portion of the Request URI is a domain that our SIP proxy is responsible for.<br />
<br />
18. This ser.cfg assumes that international numbers are prefixed with 011 and that domestic numbers are 10 numeric
digits.<br />
<br />
Here we use regular expression matching to check the R-URI. If it starts with 011 and is followed by numeric
digits only, then we have a match for an international PSTN destination. If a match is found then we route
the message to the PSTN handler, route[4].<br />
19. If the R-URI match 10 numeric digits then we know we have a domestic number and we route the INVITE
message to the PSTN hander, route[4].<br />
<br />
NOTE: We know we have a PSTN number at this point because the call to lookup(location) on the previous
line returned FALSE.<br />
20. Here we introduce route block 5 for handling PSTN destinations.<br />
<br />
21. Rewritehost() is used to alter the R-URI so that the message will be relayed to our PSTN gateway. You must
pass the IP address of your PSTN gateway to this function. SER will then send the INVITE message to your
PSTN gateway properly when you call t_relay().<br />
<br />
22. Avp_write() alters the amount of time allowed for a PSTN call to connect. If you recall we specified
inv_timeout as the loadparam value for fr_inv_timeout.<br />
<br />
This line specifies that we want to allow 45 seconds before timing out.<br />
<br />

</p>


</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/SER-GettingStarted.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../doc/SER-GettingStarted.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
