<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
SER - Getting Started, Part 4 <br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">




<p class="virtualpage3">
<b>Using the PSTN Gateway Connectivity ser.cfg Example</b><br />
<br />
Before you can use the PSTN enabled ser.cfg you we must take care of a few remaining items. The first thing to
do is copy the following files which the permissions module will look for upon server startup.<br />
<br />
cp [ser-source]/modules/permissions/config/permissions.allow /usr/local/etc/ser<br />
<br />
cp [ser-source]/modules/permissions/config/permissions.deny /usr/local/etc/ser<br />
<br />
The last thing to take care of before you can use the PSTN gateway example, namely add a record to the MySQL
trusted table. To do so you can issue the following SQL command by opening up your MySQL terminal.<br />
<br />
[#] mysql u ser p<br />
<br />
-- enter your mysql password --<br />
<br />
mysql> use ser;<br />
<br />
mysql> insert into trusted values (192.0.2.245, any, ^sip:.*$);<br />
<br />
NOTE: The above INSERT SQL statement tells SER to allow any protocol (udp or tcp) from IP 192.0.2.245 (the
IP of the PSTN gateway) to send any message to SER without being challenged for credentials.<br />
<br />
NOTE: You must restart SER when altering the trusted table because the entries in this table are read only during
server start up. Alternatively, you can use the FIFO command: serctl fifo trusted_reload<br />
<br />

Now that your SIP router is PSTN Enabled you can start dialing domestic and international destinations. A word
of caution is that many PSTN gateways perform their own accounting. You must be certain that SER is properly
record-routing all messages to guarantee that BYE messages are sent to the PSTN gateway.<br />
<br />

If a BYE message is not delivered to a PSTN gateway then you run the risk of additional toll charges because the
PSTN circuit could be left open.<br />
<br />
<b>Chapter 10. Call Forwarding ser.cfg</b><br />
<br />
What this ser.cfg will do:<br />
<br />
1. Introduce forwarding concepts known as serial forking and redirection<br />
2. Implement blind call forwarding<br />
3. Implement forward on busy<br />
4. Implement forward on no answer<br /><br />

Call forwarding can be achieved using one of two methods, namely serial forking and redirection. Both methods
have advantages. Serial forking, as the name implies creates a new leg of the call (after the first fails) and sends a
new INVITE message to the forwarded destination. On the other hand, redirection sends a reply message back to
the caller and gives them the forwarded location. The caller then creates a brand new INVITE message with the
forwarded destination in the R-URI and places the call.<br />
<br />
Redirection must be used with caution if a PSTN gateway is accessible. The reason is that it is conceivable that
SER could send a redirection response back to a PSTN gateway and the PSTN gateway could then send a new
INVITE message to the forwarded destination and bypass SER, which could be an international call for which
SER is unaware. This would result in toll charges that are not billable. The reason that SER could be unaware of
the new call is that redirection is accomplished by replying with a 302 Temporarily Moved message. In the IP
world, the intention is that the IP phone shall decide what to do, possibly even ask the user should I go ahead and
make the call to the new destination? This way the user will acknowledge potential charges. However, there is no
standard way 302 messages are handling in the interface between PSTN and IP and non-billable calls is a possibility.<br />
<br />
Serial forking does not have this possible security exposure because SER will record route all messages, which
means that even if the forwarded destination is a PSTN phone, SER will be able to account for the call and the toll
charges, if any, would be billable to the subscriber.<br />
<br />
This section implements three types of call forwarding:<br />
<br />
1. Blind Call Forwarding All INVITE messages sent to the phone will be intercepted at the SIP router. The SIP
router will create a new call leg and send the INVITE message to the forwarded destination. This means that
the phone with blind call forwarding set will not even ring. This also means that it doesnt matter if the phone
is registered with SER.<br />
<br />

2. Forward On Busy If a phone replies to an INVITE message with a 486 Busy, SER will intercept the 486 response
and create a new call leg, which will send an INVITE message to the forwarded destination.<br />
<br />

3. Forward No Answer If a phone replies to an INVITE message with a 408 Request Timeout, SER will intercept
the 408 message and create a new call leg, which will send an INVITE message to the forwarded destination.
<br />
<br />
Blind call forwarding is handled somewhat differently than forward no answer and forward on busy because blind
call forwarding is altering an INVITE message for which the destination set has yet to be processed. This means
that we can simply change the R-URI and relay the message.<br />
<br />
The other two types of call forwarding require much more effort to implement because we must catch the error
replies, alter the R-URI, and then append the forwarded contact location to the destination set and finally relay the
message.<br />
<br />
All call forwarding preferences are stored in the MySQL usr_preferences table. We will use the AVPOPS module
to read a subscribers call forwarding settings. AVPOPS will also be used to alter the call sequence and perform
serial forking that we require.<br />
<br />
PLEASE NOTE! This configuration cannot recursive forwarding, i.e. forwarding to somebody who has forwarded.
Adding support for this is complex, but is possible and is left as an exercise for the reader ;-)<br />
<br />
In addition we have included an appendix describing the Call processing Language that allows more comprehensive
control of a call.<br />
<br />
debug=3<br />
fork=yes<br />
log_stderror=no<br />
listen=192.0.2.13 # INSERT YOUR IP ADDRESS HERE<br />
port=5060<br />
children=4<br />
dns=no<br />
rev_dns=no<br />
fifo="/tmp/ser_fifo"<br />
fifo_db_url="mysql://ser:heslo@localhost/ser"<br />
loadmodule "/usr/local/lib/ser/modules/mysql.so"<br />
loadmodule "/usr/local/lib/ser/modules/sl.so"<br />
loadmodule "/usr/local/lib/ser/modules/tm.so"<br />
loadmodule "/usr/local/lib/ser/modules/rr.so"<br />
loadmodule "/usr/local/lib/ser/modules/maxfwd.so"<br />
loadmodule "/usr/local/lib/ser/modules/usrloc.so"<br />
loadmodule "/usr/local/lib/ser/modules/registrar.so"<br />
loadmodule "/usr/local/lib/ser/modules/auth.so"<br />
loadmodule "/usr/local/lib/ser/modules/auth_db.so"<br />
loadmodule "/usr/local/lib/ser/modules/uri.so"<br />
loadmodule "/usr/local/lib/ser/modules/uri_db.so"<br />
loadmodule "/usr/local/lib/ser/modules/domain.so"<br />
loadmodule "/usr/local/lib/ser/modules/mediaproxy.so"<br />
loadmodule "/usr/local/lib/ser/modules/nathelper.so"<br />
loadmodule "/usr/local/lib/ser/modules/textops.so"<br />
loadmodule "/usr/local/lib/ser/modules/avpops.so"<br />
loadmodule "/usr/local/lib/ser/modules/permissions.so"<br />

</p>
<p class="virtualpage3">
modparam("auth_db|permissions|uri_db|usrloc",<br />
"db_url", "mysql://ser:heslo@localhost/ser")<br />

modparam("auth_db", "calculate_ha1", 1)<br />
modparam("auth_db", "password_column", "password")<br />
<br />

modparam("nathelper", "rtpproxy_disable", 1)<br />
modparam("nathelper", "natping_interval", 0)<br />
<br />

modparam("mediaproxy","natping_interval", 30)<br />
modparam("mediaproxy","mediaproxy_socket", "/var/run/mediaproxy.sock")<br />
modparam("mediaproxy","sip_asymmetrics","/usr/local/etc/ser/sip-clients")<br />
modparam("mediaproxy","rtp_asymmetrics","/usr/local/etc/ser/rtp-clients")<br />
<br />

modparam("usrloc", "db_mode", 2)<br />
<br />

modparam("registrar", "nat_flag", 6)<br />
<br />

modparam("rr", "enable_full_lr", 1)<br />
<br />

modparam("tm", "fr_inv_timer", 27)<br />
modparam("tm", "fr_inv_timer_avp", "inv_timeout")<br />
<br />

modparam("permissions", "db_mode", 1)<br />
modparam("permissions", "trusted_table", "trusted")<br />
modparam("avpops", "avp_table", "usr_preferences")<br />
route {<br />
# -----------------------------------------------------------------<br />
# Sanity Check Section<br />
# -----------------------------------------------------------------<br />
if (!mf_process_maxfwd_header("10")) {<br />
sl_send_reply("483", "Too Many Hops");<br />
break;<br />
};<br />
if (msg:len > max_len) {<br />
sl_send_reply("513", "Message Overflow");<br />
break;<br />
};<br />
# -----------------------------------------------------------------<br />
# Record Route Section<br />
# -----------------------------------------------------------------<br />
if (method=="INVITE" && client_nat_test("3")) {<br />
# INSERT YOUR IP ADDRESS HERE<br />
record_route_preset("192.0.2.13:5060;nat=yes");<br />
} else if (method!="REGISTER") {<br />
record_route();<br />
};<br />
# -----------------------------------------------------------------<br />
# Call Tear Down Section<br />
# -----------------------------------------------------------------<br />
if (method=="BYE" || method=="CANCEL") {<br />
end_media_session();<br />
};<br /><br />
# -----------------------------------------------------------------<br />
# Loose Route Section<br />
# -----------------------------------------------------------------<br />
if (loose_route()) {<br />
if (!has_totag()) {<br />
sl_send_reply("403", "Forbidden");<br />
break;<br />
};<br />
if (method=="INVITE") {<br />
if ((method=="INVITE" || method=="REFER") && !has_totag()) {<br />
if (!proxy_authorize("","subscriber")) {<br />
proxy_challenge("","0");<br />
break;<br />
} else if (!check_from()) {<br />
sl_send_reply("403", "Use From=ID");<br />
break;<br />
};<br />
consume_credentials();<br />
};<br />
if (client_nat_test("3")||search("^Route:.*;nat=yes")){<br />
setflag(6);<br />
use_media_proxy();<br />
};<br />
};<br />
route(1);<br />
break;<br />
};<br />
# ----------------------------------------------------------<br />
# Call Type Processing Section<br />
# ----------------------------------------------------------<br />
if (!is_uri_host_local()) {<br />
if (is_from_local() || allow_trusted()) {<br />
route(4);<br />
route(1);<br />
} else {<br />
sl_send_reply("403", "Forbidden");<br />
};<br />
break;<br />
};<br />
if (method=="ACK") {<br />
route(1);<br />
break;<br />
} if (method=="CANCEL") {<br />
route(1);<br />
break;<br />
} else if (method=="INVITE") {<br />
route(3);<br />
break;<br />
} else if (method=="REGISTER") {<br />
route(2);<br />
break;<br />
};<br /><br />

lookup("aliases");<br />
if (!is_uri_host_local()) {<br />
route(4);<br />
route(1);<br />
break;<br />
};<br />
if (!lookup("location")) {<br />
sl_send_reply("404", "User Not Found");<br />
break;<br />
};<br />
route(1);<br />
}<br />
route[1] {<br />
# ----------------------------------------------------------<br />
# Default Message Handler<br />
# ----------------------------------------------------------<br />



</p>
<p class="virtualpage3">
if (!check_to()) {<br />
sl_send_reply("401", "Unuthorized");<br />
break;<br />
};<br />
consume_credentials();<br />
if (!save("location")) {<br />
sl_reply_error();<br />
};<br />
}<br />
route[3] {<br />
# ----------------------------------------------------------<br />
# INVITE Message Handler<br />
# ----------------------------------------------------------<br />
if (client_nat_test("3")) {<br />
setflag(7);<br />
force_rport();<br />
fix_nated_contact();<br />
};<br />
if (!allow_trusted()) {<br />
if (!proxy_authorize("","subscriber")) {<br />
proxy_challenge("","0");<br />
break;<br />
} else if (!check_from()) {<br />
sl_send_reply("403", "Use From=ID");<br />
break;<br />
};<br />
consume_credentials();<br />
};<br />
if (uri=~"^sip:1[0-9]{10}@") {<br />
strip(1);<br />
};<br />
lookup("aliases");<br />
if (!is_uri_host_local()) {<br />
route(4);<br />
route(1);<br />
break;<br />
};<br />
if (uri=~"^sip:011[0-9]*@") {<br />
route(4);<br />
route(5);<br />
break;<br />
};<br />
if (avp_db_load("$ruri/username", "s:callfwd")) {<br />
setflag(22);<br />
avp_pushto("$ruri", "s:callfwd");<br />
route(6);<br />
break;<br />
};<br />
if (!lookup("location")) {<br />
if (uri=~"^sip:[0-9]{10}@") {<br />
route(4);<br />
route(5);<br />
break;<br />
};<br />
sl_send_reply("404", "User Not Found");<br />
break;<br />
};<br />
if (avp_db_load("$ruri/username", "s:fwdbusy")) {<br />
if (!avp_check("s:fwdbusy", "eq/$ruri/i")) {<br />
setflag(26);<br />
};<br />
};<br />
if (avp_db_load("$ruri/username", "s:fwdnoanswer")) {<br />
if (!avp_check("s:fwdnoanswer", "eq/$ruri/i")) {<br />
setflag(27);<br />
};<br />
};<br />

t_on_failure("1");<br />
route(4);<br />
route(1);<br />
}<br />
route[4] {<br />
# ----------------------------------------------------------<br />
# NAT Traversal Section<br />
# ----------------------------------------------------------<br />
if (isflagset(6) || isflagset(7)) {<br />
if (!isflagset(8)) { 11<br />
setflag(8);<br />
use_media_proxy();<br />
};<br />
};<br />
}<br /><br />
route[5] {<br />
# ----------------------------------------------------------<br />
# PSTN Handler<br />
# ----------------------------------------------------------<br />
rewritehost("192.0.2.245"); # INSERT YOUR PSTN GATEWAY IP ADDRESS<br />
avp_write("i:45", "inv_timeout");<br />
t_on_failure("1");12<br />
route(1);<br />
}<br />
13route[6] {<br />
# ----------------------------------------------------------<br />
# Call Forwarding Handler<br />
#<br />
# This must be done as a route block because sl_send_reply() cannot be<br />
# called from the failure_route block<br />
# ----------------------------------------------------------<br />
if (uri=~"^sip:1[0-9]{10}@") { 14<br />
strip(1);<br />
};<br />
lookup("aliases");15<br />
if (!is_uri_host_local()) { 16<br />
if (!isflagset(22)) {<br />
append_branch();<br />
};<br />
route(4);<br />
route(1);<br />
break;<br />
};<br />
if (uri=~"^sip:011[0-9]*@") {<br />
route(4); 17<br />
route(5);<br />
break;<br />
};<br />
if (!lookup("location")) { 18<br />
if (uri=~"^sip:[0-9]{10}@") {<br />
route(4);<br />
route(1);<br />
break;<br />
};<br />
sl_send_reply("404", "User Not Found");<br />
};<br />
route(4);<br />
route(1);<br />
}<br /><br />
onreply_route[1] {<br />
if ((isflagset(6) || isflagset(7)) &&<br />
(status=~"(180)|(183)|2[0-9][0-9]")) {<br />
if (!search("^Content-Length:[ ]*0")) {<br />
use_media_proxy();<br />
};<br />
};<br />
if (client_nat_test("1")) {<br />
fix_nated_contact();<br />
};<br />
}<br />
19failure_route[1] {<br />
if (t_check_status("487")) { 20<br />
break;<br />
};<br />
if (isflagset(26) && t_check_status("486")) { 21<br />
if (avp_pushto("$ruri", "s:fwdbusy")) {22<br />
avp_delete("s:fwdbusy");23<br />
resetflag(26);<br />
route(6);24<br />
break;<br />
};<br />
};<br /><br />
if (isflagset(27) && t_check_status("408")) { 25<br />
if (avp_pushto("$ruri", "s:fwdnoanswer")) { 26<br />
avp_delete("s:fwdnoanswer");27<br />
resetflag(27);<br />
route(6); 28<br />
break;<br />
};<br />
};<br />
end_media_session();29<br />
}<br />
</p>
<p class="virtualpage3">
<b>Call Forwarding ser.cfg Analysis</b><br />
<br />
1. Call forwarding is dependent on the avpops module. This module needs to access the MySQL database in
order to read a subscribers call forwarding preferences from the usr_preferences table. So, here we specify
the MySQL database and table for call forwarding preferences.<br />
<br />
2. Here is where we implement the blind call forwarding functionality. When processing an INVITE message
we need to lookup any row in the MySQL usr_preferences table to see if we can find a record that has the
name portion of the R-URI and an attribute of callfwd. If the avp_db_load() function returns TRUE then the
AVP named s:callfwd will have the blind call forwarding destination set.<br />
<br />
3. If we are about to serial fork the call because of blind call forwarding, then we set flag 22 for future reference
in route[6]. This is important because this flag determines whether or not we need to call the append_branch()
function which actually forks the call. A subtle item to understand here is that blind call forwarding can only
happen when processing the original INVITE. This means that the destination set of the call has not been
processed yet, so we can safely alter the R-URI and the call to append_branch() should not be done.<br />
<br />
Forward on busy and forward no answer however, only occur after the original INVITE has failed, which
means the destination set has been processed. In order to fork the call at this point the append_branch()
function must be called.Therefore, flag 22 will be set only for blind call forwarding.<br />
4. Since avp_db_load() found a blind call forwarding record in MySQL we need to write that new destination
to the R-URI in order to send the INVITE message to the correct destination. Avp_pushto() does just this.<br />
<br />
This statement copies the value in AVP s:callfwd to the R-URI of the actual message.<br />
5. Send the processing to route(6) which handles all call forwarding.<br />
<br />
6. This section loads the subscribers preferences for forward on busy and forward no answer. Load the forward
on busy setting from the MySQL usr_preferences table. Here we look for records in the database table where
the attribute column value is fwdbusy and the username part (before @) of the R-URI can be found in the
tables username column. avp_db_load() returns true if one or more records were found.<br />
<br />
7. We do not allow users to forward to their now SIP phone, which would cause looping problems for SER. So
we use avp_check() to make sure the forward on busy destination is not the same as the R-URI destination.<br />
<br />
If forward on busy is set then we need to remember this for future processing in the failure_route of this
configuration script. If the subscriber in the R-URI has forward on busy enabled, then flag 26 will be set,
and we will be able to detect this in the failure_route.<br />
8. Load the forward no answer setting from the MySQL usr_preferences table. Here we look for records where
the username port of the R-URI has an attribute column value of fwdnoanswer.<br />
<br />
9. We do not allow users to forward to their own SIP phone, which would cause looping problems for SER. So
we use avp_check() to make sure the forward no answer destination is not the same as the R-URI destination.<br />
<br />
If forward on busy is set then we need to remember this for future processing in the failure_route of this
configuration script. If the subscriber in the R-URI has forward no answer enable then flag 27 will be set,
and we will be able to detect this in the failure_route.<br />
10.t_on_failure() informs SER that we want to perform special handling when a failure condition occurs. Failure
conditions in this context refer to 4xx and 5xx response codes. For example, 486 is the response code for a
busy answer.<br />
<br />
By setting t_on_failure(1) before calling t_relay(), SER will pass control to the code block defined as failure_
route[1] which appears at the end of the configuration scr<br />
<br />
11. We must take special precautions not to call use_media_proxy() more than once because by doing so the c=
field in the SDP will get corrupted. This can happen if the original INVITE was NATed in route(4), get called
during the call set up, and a 486 Busy is received. This will trigger the forward on busy functionality. Once the call is forked, use_media_proxy() must not be called because it was already called during the original
INVITE processing logic.<br />
<br />
12. Enable the failure route for all PSTN calls.<br />
<br />
13. Here we introduce the call forwarding handler. This handler will create the new call leg for all three types
of call forwarding.<br />
<br />
14. In North America we dial 1+ ten digits to call a long distance number. If the R-URI contains this prefix we
strip it off before processing.<br />
<br />
15. Find any aliases. The call forwarding number in the MySQL usr_preferences table could very well be an
alias in our database.<br />
<br />
16. If the R-URI is not served by our domain then call append_branch() for forward on busy and forward no
answer before enabling the NAT processing code in route(4) and relaying the cal from route(1).<br />
<br />
17. If the R-URI is an international PSTN destination then call the NAT processing code in route(4) and relay
to the PSTN via route(5).
<br /><br />

18. Find the R-URI in the user location cache. If a user is not found, then determine if it is a domestic PSTN
destination. If so, enable the NAT processing logic in route(4) and relay the call to the PSTN via route(5).<br />
<br />
Enable NAT traversal code and relay the message. Messages hitting this line are INVITE messages being
forwarded to another SIP phone that is registered on this SIP router.<br />
19. Here we introduce the failure route. If we call t_on_failure() before t_relay(), then SER will execute this code
block when a 4xx or 5xx reply message is received.<br />
<br />
20. If we entered the failure route because the caller cancelled the call (ie, response code 487), then we simply
stop processing.<br />
<br />
21. If forward on busy (flag 26) is set and the reply message is a 486, then enter the forward on busy code block.<br />
<br />
22. Here we retrieve the previously loaded forward on busy destination URI. The AVP named s:fwdbusy is
copied to the R-URI.<br />
<br />
23. Since we've now copied the forward on busy URI to the R-URI we can safely delete the AVP. We could
omit this line altogether and SER would free the AVP when the transaction is freed.<br />
<br />
We now clear the forward on busy flag to prevent accidentally entering this code block on future 486 messages.<br />
24. Here we pass control to the call forwarding route block and exit.<br />
<br />
25. If forward no answer (flag 27) is set and the reply message is a 408 then enter the forward no answer code
block.<br />
<br />
26. Here we retrieve the previously loaded forward no answer destination URI. The AVP named s:fwdnoanswer
is copied to the R-URI.<br />
<br />
27. Since we've now copied the forward no answer URI to the R-URI we can safely delete the AVP. We could
omit this line altogether and SER would free the AVP when the transaction is freed.<br />
<br />
We now clear the forward no answer flag to prevent accidentally entering this code block on future 408
messages.<br />
28. Here we pass control to the call forwarding route block and exit.<br /><br />

29. Disable mediaproxy if it was enabled during the call set up.<br />
<br />




</p>
<p class="virtualpage3">

<b>Using the Call Forwarding ser.cfg Example</b><br />
In order to use the new call forwarding SER configuration you need to populate the usr_preferences table in
MySQL. Below are a few example rows which show each of the call forwarding types.<br />
<br />
Example usr_preferences Call Forwarding Records<br />

<br />
+------------+-------------+--------------------------------+<br />
| username | attribute | value |<br />
+------------+-------------+--------------------------------+<br />
| 5025552001 | callfwd | sip:5025554706@sip.example.com |<br />
| 9145551451 | fwdbusy | sip:9145550257@sip.example.com |<br />
| 5615553320 | fwdnoanswer | sip:9545553040@sip.example.com |<br />
<br />
<b>Chapter 11. Appendix - How to download
and configure the latest version of SER</b><br />
<br />
This appendix will show you how to download all the relevant source code, compile and install all the binaries to
make a working system. You will also be shown how to modify the source to support MySQL.<br />
<br />
<b>Downloading the Latest SER Source Code</b><br />
As an alternative, you can get the source from the CVS. The source code to SER is kept under CVS control on the
website cvs.berlios.de.<br />
<br />
To load the source you will either need the CVS package or you will have to browse the ftp.berlios.de
[ftp://ftp.berlios.de/] website to find the source code.<br />
<br />
Most linux distributions have cvs loaded, in which case you can login to your usual account and enter the following
command :-<br />
<br />
cd src<br />
<br />
export CVSROOT=:pserver:anonymous@cvs.berlios.de:/cvsroot/ser<br />
<br />
cvs login
<br />
<br />
[ you will be prompted for a password, please just enter RETURN ]<br />
<br />
cvs co -r rel_0_9_0 sip_router<br />
<br />
This will create a directory sip_router in the current directory with all the source code, but not RTPproxy. To get
RPTproxy, you run the command cvs co -r rel_0_9_0 rtpproxy
<b>Making the SER Binaries and installing</b><br />
Once the source code has been loaded ( either via cvs or by extraction from a zip file ) we are now ready to compile
and install the binaries. The default makefiles does not compile a number of module ( including the MySQL
module ) but for the first hello world scenario we dont need it.<br />
<br />
To make the code enter the following commands :-<br />
<br />
cd sip_router<br />
<br />
make all<br />
<br />
All the modules will be compiled and ready for installation. To perform this task you need root privileges, so enter
the following command :-<br />
<br />
su<br />
<br />
[ Enter your root password ]<br />
<br />
make install<br />
<br />
This will install the binaries by default in the directory /usr/local/etc/ser<br />
<br />
Your installation is now ready to be executed. However the next sub-section describes how to make your sip
server start whenever your machine is rebooted.<br />
<br />
To check that everything is OK, enter the ser -V and you should get the following output :-<br />
<br />
version: ser 0.9.3 (i386/linux)<br />
flags: STATS: Off, USE_IPV6, USE_TCP, DISABLE_NAGLE, USE_MCAST, DNS_IP_HACK, SHM_MEM, SHM_ADAPTIVE_WAIT_LOOPS=1024, MAX_RECV_BUFFER_SIZE 262144, MAX_LISTEN 16, MAX_URI_SIZE 1024, @(#) $Id: main.c,v 1.197 2004/12/03 19:09:31 andrei Exp $<br />
main.c compiled on 18:17:55 Mar 6 2005 with gcc 3.3<br />
<br />
You will now have a working system. The binaries are loaded into the directory /usr/local/sbin, and the configuration
files ( including ser.cfg ) loaded into the directory /usr/local/etc/ser.<br />
<br />
If you take the hello world ser.cfg file and replace the one in the configuration directory you can start the sip
server by having root privileges and entering the command :-<br />
<br />

# ser<br />
<br />

To check everything is OK, run the ps command to see if there are a number of ser processes running.
You now have a working system.<br />
<br />
You can get the latest version from http://mediaproxy.ag-projects.com/.<br />
<br />
The package has thorough INSTALL and README files. PLEASE NOTE that the configuration files in this
document does not use proxydispatcher.py. That is, the mediaproxy SER module calls the mediaproxy.py proxy
daemon directly. If you want to install mediaproxy on another server, you will have to start proxydispatcher.py
on the server running SER and mediaproxy.py on the dedicated RTP proxy server. Change the following line in
ser.cfg:<br />
<br />
modparam("mediaproxy","mediaproxy_socket", "/var/run/mediaproxy.sock")<br />
<br />
to<br />
<br />

modparam("mediaproxy","mediaproxy_socket", "/var/run/proxydispatcher.sock")<br />
<br />

Please refer to the INSTALL and README files if you want to run multiple mediaproxy RTP proxy servers.<br />
<br />



</p>

<p class="virtualpage3">
<b>Configuring the system</b><br />
Generally, the system has to be configured so that the installed programmes are executed at start-up. Normally,
this is completed by providing files in /etc/init.d. We leave it to the user to configure the services of their own
system; however we have provided example scripts to start the SER and mediaproxy processes. Please note, if you
are not supporting RTP proxying, then you do not need to run the mediaproxy process.<br />
<br />

To install the scripts, please create the file(s) in the /etc/init.d directory and then run the commands :-<br />
<br />

# chkconfig -add ser<br />
<br />

# chkconfig -add mediaproxy<br />
<br />
<b>Init.d/ser</b><br />
#!/bin/bash<br />

#<br />
# Startup script for SER<br />
#<br />
# chkconfig: 345 91 15<br />
# description: Ser is a fast SIP Proxy.<br />
#<br />
# processname: ser<br />
# pidfile: /var/run/ser.pid<br />
# config: /etc/ser/ser.cfg<br />
# Source function library.<br />
. /etc/rc.d/init.d/functions<br />
ser=/usr/local/sbin/ser<br />
prog=ser<br />
OPTIONS="-d -d -d -d -d -d -d -d -d"<br />
RETVAL=0<br />
start() {<br />
echo -n $"Starting $prog: "<br />
daemon $ser $OPTIONS<br />
RETVAL=$?<br />
echo<br />
[ $RETVAL = 0 ] && touch /var/lock/subsys/ser<br />
return $RETVAL<br />
}<br />

stop() {<br />
echo -n $"Stopping $prog: "<br />
killproc $ser<br />
RETVAL=$?<br />
echo<br />
[ $RETVAL = 0 ] && rm -f /var/lock/subsys/ser /var/run/ser.pid<br />
}<br />
reload() {<br />
echo -n $"Reloading $prog: "<br />
killproc $ser -HUP<br />
RETVAL=$?<br />
echo<br />
}<br />
# See how we were called.<br />
case "$1" in<br />
start)<br />
start<br />
;;<br />
stop)<br />
stop<br />
;;<br />
status)<br />
status $ser<br />
RETVAL=$?<br />
;;<br />
restart)<br />
stop<br />
sleep 3<br />
start<br />
;;<br />
condrestart)<br />
if [ -f /var/run/ser.pid ] ; then<br />
stop<br />
start<br />
fi<br />
;;<br />
*)<br />
echo $"Usage: $prog {start|stop|restart|condrestart|status|help}"<br />
exit 1<br />
esac<br />
exit $RETVAL<br />
<br />
<b>Init.d/mediaproxy</b><br />
The mediaproxy programme is used to support NAT configurations.<br />
<br />
#!/bin/sh<br />
#<br />
# chkconfig: 2345 90 20<br />
# description: VoIP RTP Proxy Server<br />
#<br />
# processname: mediaproxy<br />
# pidfile: /var/run/mediaproxy.pid<br />
# source function library<br />
. /etc/rc.d/init.d/functions<br />
<br />
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin<br />
<br />
INSTALL_DIR="/usr/local"<br />
RUNTIME_DIR="/var/run"<br /><br />

PROXY="$INSTALL_DIR/mediaproxy/mediaproxy.py"<br />
DISPATCHER="$INSTALL_DIR/mediaproxy/proxydispatcher.py"<br />
PROXY_PID="$RUNTIME_DIR/mediaproxy.pid"<br />
DISPATCHER_PID="$RUNTIME_DIR/proxydispatcher.pid"<br />
<br />
# Options for mediaproxy and dispatcher. Do not include --pid pidfile<br />
# --pid pidfile will be added automatically if needed.<br />
PROXY_OPTIONS="--ip=10.3.0.221 --listen=127.0.0.1"<br />
DISPATCHER_OPTIONS="domain://sip.dialez.com"<br />
<br />

NAME="mediaproxy"<br />
DESC="SER MediaProxy server"<br />
<br />
test -f $PROXY || exit 0<br />
test -f $DISPATCHER || exit 0<br /><br />

if [ "$PROXY_PID" != "/var/run/mediaproxy.pid" ]; then<br />
PROXY_OPTIONS="--pid $PROXY_PID $PROXY_OPTIONS"<br />
<br />

fi<br />
<br />
if [ "$DISPATCHER_PID" != "/var/run/proxydispatcher.pid" ]; then<br />
DISPATCHER_OPTIONS="--pid $DISPATCHER_PID $DISPATCHER_OPTIONS"<br />
fi<br />

start() {<br />
echo -n "Starting $DESC: $NAME"<br />
$PROXY $PROXY_OPTIONS<br />
$DISPATCHER $DISPATCHER_OPTIONS<br />
echo "."<br />
}<br />
stop () {<br />
echo -n "Stopping $DESC: $NAME"<br />
kill `cat $PROXY_PID`<br />
kill `cat $DISPATCHER_PID`<br />
echo "."<br />
}<br />
case "$1" in<br />
start)<br />
start<br />
;;<br />
stop)<br />
stop<br />
;;<br />
restart|force-reload)<br />
stop<br />
#sleep 1<br />
start<br />
;;<br />
*)<br />
echo "Usage: /etc/init.d/$NAME {start|stop|restart|force-reload}" >&2 <br />
exit 1<br />
;;<br />
esac<br />
exit 0<br />



</p>
<p class="virtualpage3">
<b>Supporting MySQL</b><br />
<br />
A database is needed to support a number of functions within ser. Namely authentication and accounting. In our
configuration we have used MySQL, other databases can be supported but will require a different setup.<br />
<br />
We are assuming that you have a working MySQL database with a root password. Of course, you will also need
the development MySQL package loaded as it contains the header files for the source code.<br />
<br />
To support MySQL, three steps are needed :-<br />
<br />
1. Modify the source, rebuild and reinstall the binaries<br />
2. Modify MySQL to support the database and tables needed<br />
3. Modify the ser.cfg to use the database<br />
<br />
All the source has already been loaded, what we need to change is the Makefile so that we now include the MySQL
module. Edit the file src/sip_router/Makefile and at the line starting exclude_modules= remove the reference to
mysql. Save the file and then recompile the code with the command make all. Reinstall the binaries by taking root
privileges and executing make install.<br />
<br />
MySQL needs to be modified to support the SER database and tables. Again take on root privileges and execute
make dbinstall. You will be asked for the root MySQL password and also the realm that you want. This is configurable,
but normally you use the IP address of the sip server.<br />
<br />
Now replace the ser.cfg file with the one that supports MySQL and restart the sip server.<br />
<br />
<b>Debugging Tips</b><br />
<br />
There a number of techniques available to debug SER, the main types fall into two categories :-<br />
<br />
1. Capture the SIP messages<br />
2. Generate debug information<br />
These types will now be described.<br />
<br />
<b>Capture of SIP Messages</b><br />
A very useful technique is to look at the actual SIP messages that are produced by SER as seen by other clients
on the<a href="../../a1/VoIP-Network-platform.asp"> network</a>. Two tools are commonly used, ethereal ( or the text based equivalent tethereal ) and ngrep. Assuming
that you have root access to the sip server, then the syntax for running ethereal is :-<br />
<br />
# ethereal port 5060<br />
<br />

This will display all packets being sent and received from port 5060 which is the common port used by sip. Added
-w file.cap will store the data for future viewing.<br />
<br />
<b>Generate Debug Information</b><br />
<br />
SER has the ability to generate a vast amount of information that can be used for debugging. To capture this data,
either the stderr file can be redirected, or the program run in foreground. This later technique is achieved by
modifying the line fork=no in ser.cfg.<br />
<br />
Once the data is being displayed /captured, then the level of detail can be varied by modifying the line debug=4
in ser.cfg higher the number more detail is generated.<br />
<br />
Finally the user can add more information by inserting into ser.cfg the command xlog at appropriate points then
debug information is required. Please refer to the xlog module README for details.<br />
<br />
<b>Chapter 12. Appendix The Call Processing
Language (CPL)</b><br />
<br />
The Call Processing Language (CPL) is a language designed to describe and control Internet <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> services
and provide call processing functionality. It is designed for end user service creation and is purposely limited its
capabilities. It works on top of SIP or H323 and is a safe language for non-experienced users as it can only access
limited resources, cannot call external programs and does not provide loops or recursion. To highlight the applicability
of this language, suggestions are given below regarding possible uses of CPL scripts, including call routing,
screening and logging services:<br />
<br />
1. Call Forward on Busy/No Answer<br />
<br />
2. Call Screening and Rule Based Call Processing<br />
<br />
3. Intelligent User Location<br />
<br />
4. Administrator Service Definition<br />
<br />
[Enables administrators to devise policies for site wide <a href="../../a1/VoIP-Network-platform.asp">telephony</a> use]<br />
<br />
1. Web Middleware<br />
<br />
2. Sequential Call Forking (Note the SER LCR and AVPOPS module can also be utilised)<br />
<br />
Ser.cfg<br />
Before CPL scripts can become operational on the SER server, the ser.cfg must be modified to interpret the scripts
correctly.<br />
The cpl-c module must first be loaded by ser:<br />
loadmodule "/opt/ser/lib/ser/modules/cpl-c.so"<br />
Next the modules must be configured:<br />
<br />
modparam("cpl-c", "cpl_db", "mysql://root:password@localhost/ser")<br /><br />
In this case "username" and "password" represent the username and password for the mysql database named ser.
The entry at "localhost" should match with the server name on which the mysql database is running. ser and heslo
are the default username and password.<br /><br />
modparam("cpl-c", "cpl_table", "cpl")<br /><br />
This refers to the "cpl" table which is the default table for the cpl-scripts in the database.<br />
<br />

modparam("cpl-c", "cpl_dtd_file", "/tmp/ser-0.9.0/modules/cpl-c/cpl-06.dtd")<br />
<br />
Pointers to the location of the CPL XML DTD file must be given (the XML DTD is necessary for parsing CPL
scripts and is described in further detail later in this section).<br />
<br />
modparam("cpl-c", "lookup_domain", "location")<br />
<br />
This parameter should be set to "location" to let the lookup-node work correctly.<br />
<br />
All the above parameters are mandatory. Two more parameters exist which are optional: A debugging parameter
pointing to the existence of a log file and a parameter that specifies the maximum of recursive executions in CPL.<br />
<br />
If an invite is incoming and a cpl script exists for the recipient, the default call processing is forfeited in favour of
the CPL script logic.

</p>
<p class="virtualpage3">
#--------------------Call Type Processing Section---------------------#<br />
# if the request is for other domain use UsrLoc<br />
# (in case, it does not work, use the following command<br />
# with proper names and addresses in it)<br />
if (uri==myself) {<br />
if (method == "INVITE"){<br />
if(!cpl_run_script("incoming", "is_stateless"))<br />
{<br />
# script execution failed<br />
t_reply("500", "CPL script execution failed");<br />
};<br />
route(3);<br />
break;<br />
} else if (method == "REGISTER"){<br />
#handle REGISTER messages with CPL script<br />
cpl_process_register();<br />
route(2);<br />
break;<br />
};<br />
<br />
Once the ser.cfg has been modified and SER has been restarted, issue the pstree command and two child processes
should be observed:<br />
<br />
e.g<br />
<br />

|---rtpproxy<br />
<br />

|---ser-+-25&[ser]<br />
<br />

---2*[ser---ser]<br />
<br />

Uploading a CPL Scrip<br />
<br />

A CPL script can be uploaded onto SER using either the SIP REGISTER message or manually via SERs FIFO
facility.<br />
<br />

SIP REGISTER Message<br />
<br />

A SIP REGISTER message (the message used by SIP to register a user on the <a href="../../a1/VoIP-Network-platform.asp">network</a>) contains the CPL script
in the body/payload. When the REGISTER message reaches SER, the CPL script is retrieved from the payload
and stored in the cpl table of the ser database. Thus, the CPL script resides in the SER database and will be executed
when a call arrives addressed to that user. Then the script will drive the action to be taken with that call according
to the requirements specified by the user in the script<br />
<br />

CPLED, a free graphical tool, can be used for the above purpose. It includes a script transport feature whereby
scripts can be downloaded, uploaded or removed via http or the SIP REGISTER method (authentication supported).
For more information refer to http://www.iptel.org/products/cpled/.<br />
<br />

Serctl FIFO Interface<br />
<br />

To use the FIFO interface, a command similar to the following can be used:<br />
<br />

serctl fifo LOAD_CPL user@domain /path/to/cpl/script<br />
<br />

e.g.<br />
<br />

serctl fifo LOAD_CPL 2000@server /opt/ser/etc/ser/cplscript.cpl.xml<br />
<br />

Removing a CPL Script<br />
<br />
If a user wishes to remove a particular CPL script, this can also be done using SERs FIFO facility.<br />
<br />
serctl fifo REMOVE_CPL user@domain<br />
<br />

hat, Where, Which & How?<br />
<br />
What does a CPL script do? - A CPL script runs in a signaling server (not protocol specific, can be SIP or H323)
and controls that systems proxy, redirect or rejection actions for the set-up of a particular call. More specifically
it replaces the user location functionality of a signaling server. It takes the registration information, the specifics
of the call request and other external information and chooses the signaling actions to perform.<br />
<br />
Where are CPL scripts located? - Users can have CPL scripts on any <a href="../../a1/VoIP-Network-platform.asp">network</a> server which their call establishment
requests pass through and with which they have a trust relationship. CPL scripts can reside on SIP/H323 servers,
application servers or intelligent agents.<br />
<br />
Which party generates CPL scripts? - CPL scripts are extremely generic in their ability to be adapted to the requirements
of all parties involved in a call transaction. In the most direct approach, end users can utilise CPL for describing
their services. Third parties can also utilise CPL to create and/or customize services for clients, running
on either servers owned by the user or the users service provider. Service administrators can also use CPL to define
server service policies and it can also act as a back end for a web interface whereby service creation and customization
is provided transparently to the user<br />
<br />
CPL scripts are usually associated with a particular Internet <a href="../../a1/VoIP-Network-platform.asp">telephony</a> address. Each SER user can only have one
CPL script. If a new script is loaded, the previous one is overwritten. If different services are required by the same
user e.g. time based routing, call screening, forward on busy etc, then these must be mixed in the same script during
provisioning.<br />
<br />
How? - Methods for CPL Script Generation<br />
<br />
Manually<br />
<br />
CPL scripts can be easily created by hand due to its uncomplicated syntax and similarity with HTML. Examples
of such scripts are included at the end of this document.<br />
<br />
Automated<br />
<br />
As previously described, web middleware could be utilised to transparently provision the CPL syntax.<br />
<br />
Graphical Tools<br />
<br />
Graphical User Interface (GUI) tools are also available for provisioning CPL scripts. These provide inexperienced
users with powerful access to CPLs functionality through a simple interface.<br />
<br />
An example of this is CPLED, a free graphical CPL editor which was mentioned earlier in the section. It is written
in JAVA and can be used as a standalone application or JAVA applet.<br />
<br />
The XML DTD for CPL<br />
<br />
Syntactically, CPL scripts are represented by XML documents. This is advantageous in that it makes parsing easy
and parsing tools are publicly available. It is simple to understand and like HTML consists of a hierarchical
structure of tags. A complete Document Type Declaration (DTD) describing the XML syntax of the CPL can be
found in the /ser-0.9.0/modules/cpl-c directory and also should be called from the ser.cfg as shown earlier. It should
be consulted if syntax errors are experienced uploading CPL scripts and all CPL scripts should comply with this
document. The XML DTD is also provided in Appendix X.<br />
<br />
CPL Script Examples<br />
<br />
Call Screening a Particular Contact<br />
<br />
This script demonstrates CPLs call screening abilities and rejects all calls received from extension 2000.<br />
<br />

</p>
<p class="virtualpage3"><br />
?xml version=1.0 ?<br />
!DOCTYPE cpl SYSTEM cpl.dtd<br />
cpl<br />
incoming<br />
address-switch field=�origin� subfield=�user<br />
address is=2000<br />
reject status=reject reason=I don�t take calls from extension 2000 /<br />
/address<br />
/address-switch<br />
/incoming<br />
/cpl<br />
<br />
Time-Based Switch<br />
<br />
This script illustrates a time-based CPL script. Incoming calls received between Monday Friday and 9 a.m to 5
p.m. are proxied as normal. However if a call presents at the server outside those hours, it is directed to the users
voicemail.<br />
<br />
?xml version=1.0 ?
!DOCTYPE cpl SYSTEM cpl.dtd
cpl
time-switch
time dtstart=9 dtend=5 wkst=MO|TU|WE|TH|FR
lookup source=registration
success
proxy /
/success
/lookup
/time
otherwise
location url=sip:2000@voicemail.server.com
proxy /
/location
/otherwise
/time-switch
/cpl

<br />
<br />
Forward on No Answer or Busy<br />
<br />
?xml version=1.0 ?<br />
!DOCTYPE cpl SYSTEM cpl.dtd<br />
cpl<br />
subaction id=voicemail<br />
location url=sip:2000@voicemail.server.com<br />
proxy /<br />
/location<br />
/subaction<br />
incoming<br />
location url=sip:2000@pc.server.com<br />
proxy timeout=8<br />
busy<br />
sub ref=voicemail/<br />
/busy<br />
noanswer<br />
sub ref=voicemail/<br />
/noanswer<br />
/proxy<br />
/location<br />
/incoming<br />
/cpl<br />
<br />
The above script could perhaps be modified for serial forking along the lines of the following:<br />
<br />
?xml version=1.0 ?<br />
!DOCTYPE cpl SYSTEM cpl.dtd<br />
cpl<br />
subaction id=pda<br />
location url=sip:2000@pda.server.com<br />
proxy /<br />
/location<br />
/subaction<br />
incoming<br />
location url=sip:2000@pc.server.com<br />
proxy timeout=8<br />
noanswer<br />
sub ref=pda/<br />
/noanswer<br />
/proxy<br />
/location<br />
/incoming<br />
/cpl<br />
<br />
Outgoing Call Screening [*]<br />
<br />
This script illustrates how to filter outgoing calls. This script blocks an outgoing calls to premium rate numbers.<br />
<br />
?xml version=1.0 ?<br />
!DOCTYPE cpl SYSTEM cpl.dtd<br />
cpl<br />
subaction id=pda<br />
location url=sip:2000@pda.server.com<br />
proxy /<br />
/location<br />
/subaction<br />
incoming<br />
location url=sip:2000@pc.server.com<br />
proxy timeout=8<br />
noanswer<br />
sub ref=pda/<br />
/noanswer<br />
/proxy<br />
/location<br />
/incoming<br />
/cpl<br />
<br />
Priority and Language Routing [*]<br />
<br />
The following example illustrates a service based a calls priority value and language settings. If the call request
has a priority of urgent or higher, the default script behavior is performed. Otherwise the language field is checked
for the language (Spanish) and if it is present the call is proxied to a Spanish-speaking operator, otherwise to an
English-speaking operator.<br />
<br />
?xml version=1.0 ?<br />
!DOCTYPE cpl SYSTEM cpl.dtd<br />
cpl<br />
incoming<br />
priority-switch<br />
priority greater=urgent /<br />
otherwise<br />
language-switch<br />
language matches=es<br />
location url=sip:Spanish@operator.server.com<br />
 proxy /<br />
/location<br />
language<br />
location url=sip:English@operator.server.com<br />
 proxy /<br />
/location<br />
/language-switch<br />
/otherwise<br />
/priority-switch<br />
/incoming<br />
/cpl<br />
<br />



</p>

<p class="virtualpage3">
<b>A More Complex Example [not verified]</b><br />
In this scenario a user wants all calls directed to the terminal at his desk. If he does not answer after a certain
period of time, calls from this boss are forwarded to his pda and all others are directed to his voicemail.<br />
<br />
?xml version=1.0 ?<br />
!DOCTYPE cpl SYSTEM cpl.dtd<br />
cpl<br />
location url=sip:2000@deskphone.server.com<br />
proxy timeout=8s<br />
busy<br />
location url=sip:2000@voicemail.server.com merge=clear<br />
redirect /<br />
/location<br />
/busy<br />
noanswer<br />
string-switch field=from<br />
string matches=boss@*server.com<br />
location url=sip:2000@pda.server.com merge=clear<br />
proxy /<br />
/location<br />
/string<br />
otherwise<br />
location url=sip:2000@voicemail.server.com merge=clear<br />
redirect /<br />
/otherwise<br />
/string-switch<br />
/noanswer<br />
/proxy<br />
/location<br />
/cpl<br />
<br />
References<br />
<br />

The following documents have been referenced throughout this document and can be used as a source of further
information regarding CPL.


</p>

</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/SER-GettingStarted.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="../doc/SER-GettingStarted.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
