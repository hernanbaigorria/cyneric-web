<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}
.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top"><br />
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="739" border="0" cellspacing="0">
  <tr>
    <td width="263" valign="top" scope="col"><br />
        <!--#include file="../../includes/menudocumentation.asp" --></td>
    <td width="472" valign="top" scope="col"><h1>
Enterprise Telecom Security Threats<br />
            </h1> <div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">

<p class="virtualpage3">


<b><a href="#">Executive Summary</a></b> <span class="credits"></span><br>
Corporations are typically connected to two public,
untrusted <a href="../../a1/VoIP-Network-platform.asp">networks</a>: the Public Switched Telephone
<a href="../a1/VoIP-Network-platform.asp">Network</a> (PSTN); and Wide Area <a href="../../a1/VoIP-Network-platform.asp">Networks</a> (WANs) or the
Internet. Traditional phone stations or peripherals connect
to the PSTN to enable voice calls, modem and fax
communications, and video teleconferencing. Corporate
computers connect to WANs or the Internet for data
communications and access to information. Internet
Protocol (IP) phones and soft phones either connect to the
PSTN via a media gateway, or cross the corporate
perimeter to connect to WANs or the Internet. Although
nearly all corporations secure the Internet connection to
their internal data <a href="../../a1/VoIP-Network-platform.asp">networks</a> with IP firewalls and related
technologies, most corporations have yet to lock down their
voice connection to the untrusted PSTN, or when present,
bring an equal level of security to their IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a> connections.<br />
<br />
An array of vulnerabilities in internal data <a href="../../a1/VoIP-Network-platform.asp">networks</a>, the
traditional phone environment, and other critical corporate
infrastructure are accessible through an enterprise's
unsecured traditional phone <a href="../a1/VoIP-Network-platform.asp">network</a>. Although attacks
against an enterprise's Internet connection receive the
most public attention, attacks against an enterprise
through the traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a> are common. In a
data <a href="../a1/VoIP-Network-platform.asp">network</a> attack perpetrated through the traditional
phone <a href="../a1/VoIP-Network-platform.asp">network</a>, intruders bypass Internet-related defenses
by using the PSTN to access unauthorized and poorly
secured modems. Attackers exploit modems as
interconnection devices between enterprise voice and data
<a href="../../a1/VoIP-Network-platform.asp">networks</a> that enable cross-<a href="../a1/VoIP-Network-platform.asp">network</a> attacks.<br />
<br />
Unauthorized modem use is prevalent in many
corporations, due to the logging and filtering of Local Area
<a href="../a1/VoIP-Network-platform.asp">network</a> (LAN)-based Internet sessions by IP firewalls. The
only means by which an employee can initiate a private
Internet session, is through an un-monitored enterprise
phone line connection to their personal Internet Service
Provider (ISP). Once connected over the PSTN, users can
access any content, engage in any transaction, or upload
and download any information-completely invisible to the
enterprise security or networking team. The vast majority
of unauthorized modems are non-secure and poorly
configured, because access to private Internet usage is the
employee's goal-not security of the corporate LAN. Beyond
these unauthorized modems, nearly all enterprise's have a
collection of authorized modems for uses such as vendor
maintenance port access into key networking infrastructure
and building systems. A typical enterprise, even one with
restricted modem usage policies and procedures, has some
number of unauthorized and/or poorly secured modems-
open "back doors" into their data <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
The traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a> is slowly migrating to Voice
over Internet Protocol (i.e., VoIP, or IP <a href="../../a1/VoIP-Network-platform.asp">telephony</a>), where
voice is a critical service running on the IP <a href="../a1/VoIP-Network-platform.asp">network</a>. At this
time, approximately 1% to 2% of telephone stations are
based on VoIP. This migration will take many years, with
most enterprises slowly rolling out the new technology by
using a hybrid <a href="../a1/VoIP-Network-platform.asp">network</a> consisting of both traditional circuitswitched
and VoIP equipment. During this transition, the
existing circuit-switched voice security threats such as
attacks on authorized and unauthorized modems, toll fraud,
and eavesdropping in the PSTN will continue-and VoIP will
introduce entirely new vulnerabilities of its own.<br />
<br />
Security is required for reliable VoIP deployment.
Unfortunately, securing VoIP is more difficult than securing
traditional, circuit-switched voice <a href="../../a1/VoIP-Network-platform.asp">networks</a>. With voice as a
service on the IP <a href="../a1/VoIP-Network-platform.asp">network</a>, it inherits both the advantages-
and the disadvantages of the IP <a href="../a1/VoIP-Network-platform.asp">network</a>. Now voice is
vulnerable to worms, viruses, and Denial of Service (DoS)-
all threats that did not exist on the circuit-switched
<a href="../a1/VoIP-Network-platform.asp">network</a>. For example, an attacker can use registration
hijacking, proxy impersonation, message tampering, or
session tear down to cause DoS on any VoIP <a href="../a1/VoIP-Network-platform.asp">network</a>
component, as well as Time Division Multiplex (TDM)
systems. An attacker can also exploit common and wellknown
weaknesses in authentication; implementation flaws
in software, protocols, and voice applications; weaknesses
in IP PBX's general-purpose and non-secure operating
systems and supporting services; as well as limitations in
perimeter security devices.<br />
<br />
VoIP is also harder to secure than other, traditional IP
services. VoIP is very susceptible to DoS attacks, is
comprised of complex, rapidly implemented "standards,"
and due to <a href="../a1/VoIP-Network-platform.asp">Network</a> Address Translation (NAT), dynamic
porting, and performance requirements, VoIP poses a
problem for conventional perimeter security devices.<br />
<br />
The vulnerabilities inherent in VoIP services expose the
enterprise to some old threats present on the traditional
voice <a href="../a1/VoIP-Network-platform.asp">network</a> such as toll fraud and eavesdropping, but
they also introduce new threats, such as attacks on IP
PBXs, DoS on signaling and media, DoS on gateways and
TDM systems, protocol attacks, call blocking, data tunneling
and <a href="../a1/VoIP-Network-platform.asp">quality of Service</a> (QoS) theft, and attacks against IP
phones, none of which were present with traditional voice
implementations.<br />
<br />
This document provides background on vulnerabilities in
both traditional circuit-switched and VoIP implementations,
as well as how different VoIP deployment scenarios are
impacted by the threats. Vulnerabilities in a hybrid <a href="../a1/VoIP-Network-platform.asp">network</a>
consisting of both legacy circuit-switched and VoIP
equipment are also discussed.<br />
<br />
<b>Table of Contents</b><br />
1. Introduction<br />
2. Traditional Voice Vulnerabilities and Threats<br />
3. VoIP Vulnerabilities and Threats<br />
4. Hybrid Circuit-Switched/VoIP Vulnerabilities and
Threats<br />
5. Summary<br />
<br />



</p>

<p class="virtualpage3">
<b>1. Introduction</b><br />
The vast majority of enterprises maintain a presence on the
Internet in order to conduct business and provide Internet
access for work-related activities. To secure the connection
to the Internet and protect internal <a href="../../a1/VoIP-Network-platform.asp">networks</a>, enterprises
deploy a variety of security devices, including firewalls,
Virtual Private <a href="../../a1/VoIP-Network-platform.asp">Networks</a> (VPNs), Intrusion
Detection/Prevention (IDP), anti-virus, and content
<a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a>. When properly deployed and configured, these
products help to protect the internal IP <a href="../a1/VoIP-Network-platform.asp">network</a> from
attacks against the enterprise's Internet connection.
However, none of these Internet-related security
technologies protect the internal IP <a href="../a1/VoIP-Network-platform.asp">network</a> from attacks
Enterprise Telecom Security Threats Page 2
against the traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a> connections created
by unauthorized or non-secure modems and poorly
configured voice systems.<br />
<br />
Furthermore, conventional measures taken to secure the
data <a href="../a1/VoIP-Network-platform.asp">network</a> are inadequate for voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> on the IP
<a href="../a1/VoIP-Network-platform.asp">network</a>, which cannot be subject o delays like those
caused by standard firewalls. It is important for companies
to take the necessary steps to provide a secure
environment that addresses the unique vulnerabilities of IP
telephony. [1]
<br />
<br />
Securing the enterprise's Internet connection is the focus of
a great deal of security attention-and a warranted
investment, because a large number of attacks come from
the Internet. However, with so much robust technology
protecting the Internet connection, many attackers search
for pathways of penetration that are less likely to be
detected. Unauthorized and non-secure modems offer an
attacker that easy, unmonitored method of obtaining
access to the internal data <a href="../a1/VoIP-Network-platform.asp">network</a>, so they pose a serious
threat to security.
<br />
<br />

<b>Unauthorized and Non-secure Modems</b><br />

When an attacker accesses an unauthorized or non-secure
modem, the IP <a href="../a1/VoIP-Network-platform.asp">network</a>-based security products cannot see
or detect the intrusion. Typically, no record of the
attacker's access is logged-except perhaps a long call
recorded on the PBX-and even this record exists only if the
accessed modem line routes through the PBX. Logs on the
attacked system may record the access-but they are
easily deleted by the attacker. The attacker can continue to
re-use this newfound vulnerability for future access, or
install another "back door," which allows access through
another means, including the Internet.<br />
<br />
As stated in September 2000 in the SANS Institute
Resources, "Insecure modems, authorized or not, are
vulnerable to such attacks and penetration of these
modems will subvert the protection of firewalls." [3]<br />
<br />
The following sections describe several situations exploiting
unauthorized and non-secure modems within an enterprise.<br />
<br />
<b>War Dialing</b><br />
Normally, an attacker uses "war dialing," the process of
scanning a large set of phone numbers within an enterprise
to pinpoint unauthorized and non-secure modems. The
attacker easily obtains the enterprise's phone numbers
using a phone book, social engineering, business cards, or
even the Internet. Once the enterprise's range of numbers
are obtained, the war dialer dials the numbers and
searches for answering modems, recording the phone
numbers of the answering modems for later attack. Some
war dialers can identify the system controlling the modem
and attempt to guess passwords. Any interested individual
can download free war dialers from the Internet. Some of
the more common freeware war dialers include Tone Loc,
THC, Grim Scanner, Super Dial, Wild Dialer, and Ultimate
War dialer 32. An attacker can even get a war dialer that
runs on a Personal Digital Assistant (PDA) with a wireless
modem.<br />
<br />
When a war dialer finds a modem and guesses the
username/password, the attacker has gained an
unmonitored access point into the enterprise. From this
point, the attacker can damage or manipulate the initial
system, or since the system usually resides on a <a href="../a1/VoIP-Network-platform.asp">network</a>,
the attacker can access any other system in the <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
To further illustrate the problem, in the 1999 Computer
Crime and Security Survey conducted by the Computer
Security Institute (CSI) and the Federal Bureau of
Investigation (FBI), of the almost 600 respondents, 28%
reported modem-related security breaches. [4]<br />
<br />

Peter Shipley amply demonstrated the problem posed by
non-secure modems with a war dialing experiment that he
conducted and later discussed at DEFCON (an annual
"hacker" convention). In the experiment, he instructed his
computer to dial 5.3 million phones in the San Francisco
Bay area looking for computers connected to modems. He
found numerous modems, including those that allowed him
access to environmental controls and other critical systems.
He further stated that 75% of the computer systems
accessible via modem were non-secure enough for an
intruder to penetrate the system.



<p class="virtualpage3">
Figure 1 provides a graphic from Signal magazine which
illustrates the difference between what security managers
perceive to be the greatest threat to their <a href="../a1/VoIP-Network-platform.asp">network</a> and the
reality that is often revealed by a vulnerability assessment
is performed. The actual threat associated with modems is,
in reality, much higher than generally realized. [2]<br />
<br />

<img src="images/Threat_Level_from_Unauthorized_Modem.jpg" /><br />
<br />
Figure 1 - Threat Level from Unauthorized Modem
<br />
<br />
<b>Unauthorized Remote Access</b><br />
internal <a href="../a1/VoIP-Network-platform.asp">network</a>, most enterprises invest in Internet-based
VPNs and managed Remote Access Servers (RAS).
Unfortunately, users often set up their own personal
remote access. The reasons for this breach of security vary,
but include a simple lack of awareness of the RAS, a real or
perceived instability of the RAS, and a desire to work
outside the <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> of the VPN or RAS. Modern Personal
Computers (PCs) make it easy for users to install a modem
and personal remote access software, such as PCAnywhere.
Users also provide backup remote access to a critical server
by installing an external modem on the server's serial port.
Although not always done with malicious intent, installation
of unauthorized and non-secure modems create major
security vulnerabilities for the enterprise. As stated in
Information Security magazine, "one of the security
professional's worst nightmares is the employee who
activates an unauthorized modem on his desktop PC,
installs a remote control program such as PCAnywhere
(without a password), and turns on the modem before
going home at night." [5]<br />
<br />
Figure 2 illustrates this "back door" remote access into the
enterprise LAN. Although the firewall monitors access from
the Internet, the firewall cannot monitor access to the
personal remote access modem.<br />
<br />
When an attacker war dials the enterprise, finds the
unauthorized remote access modem, and obtains a
password, they have full access to the system. From this
point, they can access data on the system itself, access
other systems on the <a href="../a1/VoIP-Network-platform.asp">network</a>, or install a trojan or other
back door application.<br />
<br />
<b>Unauthorized ISP Access</b><br />
Employee use of unauthorized modems for Internet access
is a more common and serious problem. Many users are
highly dependent upon the Internet for non-work-related
activities, including access to entertainment, gaming, stock
trading, Internet auctions, shopping, news, chatting, email
exchange, and access/exchange of objectionable material.
Most enterprises monitor employee access to the Internet,
and many enterprises block Internet access by site or
content.<br />
<br />
This "need" for private Internet access, coupled with
robust, IP-based Internet usage <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">monitoring</a> technologies,
has greatly increased the number of unauthorized modems
used for Internet access. Most users already have personal
ISP accounts that they use when accessing the Internet
from home. To reach the Internet from work, these users
simply install a modem on their work computer and dial a
local or 1-800 ISP. Although this unauthorized connection
may be slower than the enterprise's high-speed Internet
connection, the advantage of a completely unmonitored
connection outweighs the inconvenience.<br />
<br />
As stated in the SANS Institute Resources, "Unauthorized
or uncontrolled desktop modems are a bigger threat to a
business's security today mainly due to the changes in
computer <a href="../../a1/VoIP-Network-platform.asp">networks</a>." [6]<br />
<br />
<img src="images/Unauthorized_Remote_Access_Modems.jpg" /><br />
<br />
Figure 2 - Unauthorized Remote Access Modems
</p>

<p class="virtualpage3">
When a user accesses the Internet through an ISP, they
intentionally or inadvertently create a completely
unmonitored connection that can:<br />
<br />
�  Provide an attacker with unprotected access for a crossnetwork
attack on the internal data <a href="../a1/VoIP-Network-platform.asp">network</a>. The
connection is found, and vulnerabilities are exploited
using IP port scanning.<br />
�  Transfer proprietary or classified data to their ISP.<br />
�  Access a non-enterprise email account and download
worms/viruses and other software with malicious
content.<br />
�  Access objectionable material and place it on the
enterprise <a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
�  Tie up expensive voice channel bandwidth during the
session.<br />
�  Spend unproductive time using the Internet for non-work
related activities.<br /><br />
Employee abuse of Internet access privileges is quantified
in the 2004 CSI/FBI Computer Crime and Security Survey.
Of the almost 500 respondents (primarily financial
institutions, large corporations and government agencies),
59% detected employee abuse of Internet access
privileges, for an estimated loss of $10,601,055. [8]<br />
<br />
One government security administrator found that
unauthorized ISP access consumed 28.5% of the local
access voice circuits during peak hours. In this case,
unauthorized ISP access consumed almost three full T1
circuits (72 total voice channels). Security concerns aside,
this is a tremendous waste of expensive voice bandwidth.
Typically, voice T1 circuits cost $1,000 per month.
Elimination of this sort of unauthorized activity saved the
site approximately $36,000 per year.<br />
<br />
Another negative effect of unauthorized ISP access occurs
when an enterprise tries to rid itself of a virus or worm.
Users reintroduce the virus via their personal accounts over
their modem connections, so the virus/worm continues to
reappear despite upgrades to their <a href="../a1/VoIP-Network-platform.asp">network</a>-based mail
scanning software and laborious PC and server purgings.<br />
<br />
Truly serious conditions occur in highly secure areas where
Internet access is prohibited, but the restriction cannot be
effectively enforced. Under these circumstances, users gain
unauthorized access to the Internet by connecting modems
to telephone lines. In doing so, the user places a highly
secure system and its <a href="../a1/VoIP-Network-platform.asp">network</a> on the Internet. The user
can upload sensitive or classified material to their ISP or
inadvertently provide an Internet-based attacker with
access to their system.<br />
<br />

Figure 3 illustrates the use of unauthorized modems for
ISP access. By accessing the Internet with an unauthorized
modem, the user circumvents the IP firewall and places
their system and the enterprise's <a href="../a1/VoIP-Network-platform.asp">network</a> directly onto the
Internet.<br />
<br />
<img src="images/Unauthorized_ISP_Access.jpg" /><br />
<br />
Figure 3 - Unauthorized ISP Access<br />
<br />

<b>Unsecured Authorized Modems</b><br />
All enterprises have some number of authorized modems.
Critical systems such as large computing, voice, and
infrastructure systems often use modems to provide
remote access for technicians or data acquisition/control for
a Supervisory Control and Data Access (SCADA) system.<br />
<br />
Although many enterprises protect their critical system
modems, most do not. Large enterprises often have
hundreds of authorized modems, and it is difficult to
efficiently enforce a corporate policy across a large number
of modems. Although strong passwords, dialback capability,
and other techniques are used to try to protect authorized
modems, these are individually managed point solutions
that are difficult and expensive to manage on a large scale,
and inevitably lead to some number of poorly protected
modems.<br />
<br />
Non-secure, authorized modems are an issue not only
because of their large numbers, but also because of the
criticality of the systems using the modems. The modems
are present to provide maintenance or access/control for
what is typically a complex and critical system. Therefore,
any unauthorized access can have serious results. An
attacker can disable a PBX or use it for toll fraud, steal data
from servers, manipulate environmental systems, affect
critical infrastructure, etc. The potential for damage and
misuse is an issue even if the attacker cannot use the
system as a jumping point to another system or a cross<a href="../a1/VoIP-Network-platform.asp">network</a>
attack.<br />
<br />
Figure 4 illustrates an attacker accessing non-secure
authorized modems. As shown in the figure, non-secure
authorized modems can provide access to data networks,
voice <a href="../../a1/VoIP-Network-platform.asp">networks</a>, and other building systems.<br />
<br />
<img src="images/Non_Secure_Authorized_Modems.jpg" /><br />
<br />

Figure 4 - Non-Secure Authorized Modems


</p>

<p class="virtualpage3">
SCADA systems are especially vulnerable. For example,
utilities use modems for access and control of their remote
stations. Access to these systems can result in disruption of
electrical power, gas resources, water supply, and other
critical services. This is a very serious issue, and attackers
are likely to exploit it heavily in the future as heightened
security prevents other means of attack.<br />
<br />
The U.S. Department of Energy conducted a demonstration
at several SCADA sites in April 2004. Detailed data is
sensitive, but the security analysis summary states that
unprotected remote access modems offer almost no
defense again attacks and are easily identifiable by novice
attackers using tools freely available on the Internet. It
goes on to say that attacks against remote access modems
could easily lead to exposure or compromise of sensitive
information, and eventually, complete access to the
automation system resources.<br />
<br />
<b>Attacks Against Traditional Voice Systems
and Services</b><br />
<br />
<b>Toll Fraud</b><br />
Although modems are not directly involved, the voice
<a href="../a1/VoIP-Network-platform.asp">network</a> provides an avenue of attack against critical voice
systems. Attackers can place a standard call and use Dual
Tone Multi-Frequency (DTMF) tones to access and
manipulate PBXs, Interactive Voice Response (IVRs),
Automatic Call Distribution (ACDs), and other systems in
order to commit theft of long-distance services, or create
other issues. By war dialing, attackers find lines and codes
that provide a second dial tone, which they use to commit
toll fraud.<br />
<br />
Some enterprises enable Direct Inward Service Access
(DISA), which allows a caller to gain an outbound dial tone.
Passwords can be enabled with DISA, but they are often
weak and once determined, can be "sold" to facilitate toll
fraud. It is also possible to war dial an enterprise and scan
for DISA tones, which can then be easily exploited.<br />
<br />
This is not a new problem, and it continues to impact
business operations for enterprises. Other attacks include
use of DTMF to guess passwords and access voice mail and
other voice systems. Consider the example with Hewlett
Packard, where an extremely sensitive voice mail was
extracted from an executive's voice mailbox.<br />
<br />
In 2003, the Communications Fraud Control Association
(CFCA) estimate d annual telecom fraud losses worldwide to
be in the range of $35-$40 billion U.S. dollars-up from the
previously estimated $12 billion. The estimate was based
on the results of a comprehensive opinion survey of
telecom companies in 26 countries. Of the 20 to 30
different fraud types identified by the respondents,
PBX/PABX/Voice Mail/CPE fraud (i.e., theft of long distance
services by an unrelated third party by penetration or
manipulation of Customer Premise Equipment) was ranked
#2 and increasing. [9]<br />
<br />
Figure 5 illustrates an attacker accessing and exploiting
the various voice systems and services that are vulnerable
to attack.<br />
<br />
<img src="images/Voice_System_Attacks.jpg" /><br />
<br />
Figure 5 - Voice System Attacks

</p>

<p class="virtualpage3">
<b>Eavesdropping</b> <span class="credits"></span><br>
The vast majority of voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> is not encrypted in any
way, so it is possible to intercept and eavesdrop on
conversations in the PSTN. Furthermore, this vulnerability
is increasing in severity with the rapid transition of the
PSTN from a circuit-switched <a href="../a1/VoIP-Network-platform.asp">network</a> to a packet-based
<a href="../a1/VoIP-Network-platform.asp">network</a>.<br />
<br />
Clearly, the U.S. Government recognizes this vulnerability,
because it has countered the threat by building dedicated
voice networks and using approximately 500,000 secure
phones to encrypt voice calls. These secure phones are
point-to-point encryption devices called Secure Telephone
Units (STUs) and Secure Telephone Equipment (STEs). The
STUs and STEs are effective when manually activated by
the user, but fall short as an effective overall solution, since
each person conducting a classified or sensitive
conversation must have access to a device.<br />
<br />
The same eavesdropping vulnerability that the U.S.
Government recognizes also exists for commercial
enterprises. Commercial secure phones are used to encrypt
conversations for enterprises, and executives use these
phones for international calls, secure faxes, and secure
video conferencing.<br />
<br />
<b>3. VoIP Vulnerabilities and Threats</b><br />
VoIP is the future for voice communications, but successful
VoIP deployment requires strong security. If the VoIP
<a href="../a1/VoIP-Network-platform.asp">network</a> is not secure, the expected levels of <a href="../a1/VoIP-Network-platform.asp">quality</a> and
reliability cannot be maintained. The challenges inherent in
securing services on a shared IP <a href="../a1/VoIP-Network-platform.asp">network</a> make securing a
VoIP <a href="../a1/VoIP-Network-platform.asp">network</a> much more complex and arduous than
securing a traditional circuit-switched voice <a href="../a1/VoIP-Network-platform.asp">network</a>. For
instance, VoIP is vulnerable to traditional IP attacks-
worms, viruses, and DoS-and is only as secure as the
weakest link on the <a href="../a1/VoIP-Network-platform.asp">network</a>. VoIP services are provided via
IP PBXs running on non-secure operating systems with
non-secure supporting services (such as databases and
web servers). These operating systems and services are
susceptible to the same attacks that regularly knock out
other types of servers, making the IP PBXs more vulnerable
than traditional PBXs.<br />
<br />
Securing VoIP is also more complex and arduous because it
involves more components and software than a traditional
circuit-switched voice <a href="../a1/VoIP-Network-platform.asp">network</a>. In fact, VoIP has the
potential to double the number of IP devices on the
<a href="../a1/VoIP-Network-platform.asp">network</a>, thereby doubling the number of access points into
the <a href="../a1/VoIP-Network-platform.asp">network</a>. VoIP components include IP PBXs, supporting
servers, media gateways, switches, routers, firewalls,
cabling, and IP phones/softphones. More components mean
a greater potential for vulnerability-and VoIP components
often use general-purpose operating systems, which tend
to have more vulnerabilities than purpose-built operating
systems. If not properly secured, each endpoint, IP phone,
and softphone is susceptible to attacks such as registration
hijacking, toll fraud, eavesdropping, and malicious code-
from both outside and inside an enterprise.<br />
<br />
VoIP is also a greater challenge to secure than other, more
traditional IP services. There are several characteristics and
requirements unique to VoIP that make providing security
much more difficult. For example, VoIP has unique realtime
and reliability requirements that make it highly
susceptible to DoS attacks, yet intolerant of any security
feature that adds latency. Any delay of the media by as
much as .5 of a second makes a conversation unusable.<br />
<br />
Unfortunately, traditional data security devices are not
designed to adequately address the real-time requirements
of voice communications. Most firewalls slow data transfer,
impeding the flow of <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> and adding an unacceptable
latency to RTP packets. VoIP requires up to six additional
ports to be opened for the duration of each call: two wellknown
signaling ports (one for each direction), as well as
two ports for the media, and, optionally, two more ports for
RTCP, which monitors performance. Conventional firewalls
were not designed to handle this type of complex <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>.
Nor are they designed to monitor VoIP signaling for attacks
against the voice <a href="../a1/VoIP-Network-platform.asp">network</a>. Without inspection at the
application layer, firewalls have to open these several ports
per call without determining whether the packets are
legitimate, leaving the IP PBXs and IP phones vulnerable to
external attacks such as DoS and toll fraud.<br />
<br />
Figure 6 illustrates that traditional data firewalls control
which services pass through the corporate perimeter, but
by failing to monitor VoIP <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> at the application level,
leave a security gap through which IP PBXs, IP phones, and
other components are exposed to attack.<br />
<br />
<img src="images/Security_Gap_Left_by_Traditional_Data_Firewall.jpg" /><br />
<br />
Figure 6 -Security Gap Left by Traditional Data Firewall
</p>



<p class="virtualpage3">
Based on interviews with companies that have installed IPT
[IP Telephony], Forrester found that most companies fail to
consider the unique vulnerabilities caused by integrating
voice into a converged <a href="../a1/VoIP-Network-platform.asp">network</a> prior to deployment.
Although most companies take major steps to regularly
upgrade their data <a href="../../a1/VoIP-Network-platform.asp">networks</a> to prevent attacks, many fail
to recognize the need to add additional security measures
when adding voice traffic to the data <a href="../../a1/VoIP-Network-platform.asp">networks</a>. Only 25%
of the companies interviewed upgraded or replaced firewalls and just 22% changed to secure gateways.
Companies that do not address security requirements.risk
exposing their data <a href="../../a1/VoIP-Network-platform.asp">networks</a> to malicious attacks from
external or internal sources. [1]<br />
<br />
VoIP is also more difficult to secure than other, more
traditional IP services because VoIP consists of many
complex "standards" that are both dynamic and, due to a
"rush to market," often poorly implemented. There are
many VoIP "standards," including the Session Initiation
Protocol (SIP), H.323, MGCP, H.248, and vendorproprietary
protocols/versions. H.323 and SIP are two
leading protocols used for VoIP. H.323 is the most
commonly used today, but SIP is considered the future
protocol for VoIP; the universal protocol to integrate the
voice <a href="../a1/VoIP-Network-platform.asp">network</a> and provide the foundation for new
applications.<br />
<br />
However, it is very difficult to secure a SIP system with the
current state of SIP implementations. As an emerging and
relatively immature standard, SIP does not have clearly
defined security requirements. The lack of firm
specifications on SIP standards allows vendors to determine
how much security to build into the system. Unfortunately,
SIP does not offer any built-in security. Most SIP
development has focused on feature sets and
interoperability, with limited attention paid to security.
Even if one vendor's components do support security, all
other participating components must also support security
in order for it to be used.<br />
<br />
VoIP standards are complex, and many implementations
have flaws (i.e., programming mistakes), which lead to
vulnerabilities. For instance, an implementation flaw such
as not properly checking the size of a protocol request can
be exploited by an attacker to achieve unauthorized remote
access or a DoS attack.<br />
<br />
Vulnerabilities are also a result of vendor "rush to market."
Protocols can be either implemented by the vendor (i.e., it
is up to the vendor to focus on building a secure
implementation), or purchased from a "stack" vendor (in
which case vulnerabilities are shared among any system
using the stack).<br />
<br />
An example of vulnerabilities resulting from an
implementation flaw that was shared among multiple
vendors-and even a vendor's multiple products-is a
security flaw in a widely replicated implementation of the
H.323 protocol that was discovered by researchers at the
University of Oulu in Finland. In January 2004, Microsoft
conceded that users of the NetMeeting software were
probably vulnerable to the buffer overflow bugs found in
implementations of the H.323 VoIP protocol, which could
allow a remote attacker to take control of affected systems.
(The flaw was previously addressed in Microsoft's Internet
Security and Acceleration server software.) Microsoft was
only one of a large number of vendors impacted by the
H.323 implementation flaw. [10]
<br />
<br />

Figure 7 illustrates the basic layers of software used in an
IP PBX-any of which can have vulnerabilities.
<br />
<br />
<img src="images/Typical_IPPBX_Software_Stack.jpg" /><br />
<br />
Figure 7 - Typical IP PBX Software Stack
</p>


<p class="virtualpage3">
The above challenges entailed in securing the VoIP <a href="../a1/VoIP-Network-platform.asp">network</a>
leave VoIP vulnerable to several threats, including
eavesdropping, DoS on signaling and media streams, DoS
on gateway and existing TDM systems, attacks exploiting
weaknesses in the various protocols, data tunneling,
attacks exploiting weaknesses in IP PBX implementation,
unauthorized access to IP PBXs, and attacks against IP
phones/softphones. Additionally , VoIP is vulnerable to more
severe attacks against various voice services, such as toll
fraud, voice mail attacks, and call manipulation. The
remainder of this section focuses on the vulnerabilities
found in the majority of vendor implementations, attacks
that exploit the vulnerabilities, and the level of threat
impacting common VoIP deployment scenarios.<br />
<br />
<b>SIP Vulnerabilities</b><br />
Some SIP/VoIP vulnerabilities result from the underlying
computer system platform, implementation bugs, and the
IP <a href="../a1/VoIP-Network-platform.asp">network</a> (media attacks). SIP systems can be attacked
directly or indirectly by viruses/worms, which affect the
underlying operating system, the database, web server, or
other supporting software. A SIP system can be attacked
through an implementation flaw in any software
component, including the SIP "stack" or application. This
section focuses on inherent SIP vulnerabilities present for
the majority of vendor implementations, such as
registration hijacking, proxy impersonation, SIP message
tampering, session tear down, DoS, and NAT issues.<br />
<br />
<b>Registration Hijacking</b><br />
A rogue device can register itself and receive incoming calls
intended for a legitimate phone. Registration hijacking
describes when an attacker impersonates a valid User Agent (UA) to a registration server, and then registers
itself, causing all requests intended for the legitimate UA to
be directed to the attacker instead.<br />
<br />
Registration hijacking can result in toll fraud, where toll
calls are relayed to a media gateway. An automated attack
could generate many calls, resulting in huge charges or a
DoS condition against a gateway or PBX. Attackers can also
redirect all incoming (phone) or outgoing (gateway) calls
via their UA to eavesdrop or monitor calls.<br />
<br />
Registration is commonly performed using User Datagram
Protocol (UDP), which is non-secure. Authentication is often
not required, in fact, registration servers are only
"RECOMMENDED" to challenge UAs for authentication. Even
when authentication is used, it is normally weak (a
username and password, sent in the clear). Strong
authentication can only be used if all participating
components support it.<br />
<br />
Compromised SIP phones can be used to recover
passwords for use in registration hijacking or guessing
passwords. Compromised soft phones may also be used to
find passwords. If passwords are either set to a default or
generated mechanically (e.g., "company-1234" for
extension 1234), an attacker may be able to guess other
UA passwords. Dictionary-style attacks are also able to
recover strong passwords. Additionally, an internal or
external attacker can "fish" for registerable UA addresses to
build a directory.<br />
<br />
<b>Proxy Impersonation</b><br />
A phone or proxy server can be tricked into communicating
with a rogue proxy server. Outgoing calls from a UA are
sent either directly or via intervening proxy/proxies to a
proxy in the domain of the called party. Proxy
impersonation describes the situation where an attacker's
proxy intercepts these calls. Proxy impersonation can occur
through packet interception, domain registration hijacking,
and Domain Name Server (DNS) impersonation, allowing
an attacker to eavesdrop, track, redirect, and block calls.<br />
<br />
By impersonating a proxy once, an attacker can forge "301
Moved Permanently" responses, which causes the caller
and intervening proxies to use an attacker's proxy instead
of the originally intended proxy. By impersonating an
organization's proxy, attackers can redirect incoming SIP
calls via their UAs to eavesdrop and track calls, or perform
selective and <a href="../../a1/VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp">wholesale</a> DoS.<br />
<br />


<b>Message Tampering</b><br />
Message tampering occurs when an attacker intercepts and
modifies packets between proxies. UAs may secure SIP
messages using Secure Multipurpose Internet Mail
Extension (S/MIME) to authenticate and optionally encrypt
their text, with only those portions needed to deliver the
packet remaining in the clear. Even with S/MIME, attackers
may tamper with routing information to confuse proxies
(e.g., they may include broadcast addresses or loops in an
attempt to overload servers).<br />
<br />
Message tampering can cause users to be billed incorrectly
for calls, and can mask toll fraud attacks. By modifying
packets, attackers can redirect all incoming calls via their
UAs to eavesdrop on the calls. Attackers can also block
calls, or send unexpected calls through media gateways,
which can tie up TDM trunking or switches. Additionally, attackers can use message tampering to redirect future SIP
calls via impersonating proxies, which can continue to track
calls after the attack is completed or defeated.<br />
<br />
<b>Session Tear Down</b><br />
Once a call is established, either party can modify or end it.
If attackers observe the initial messages, they can forge a
BYE or re-INVITE message to shut down or alter the
session. The latter case allows the attacker to redirect
media, which allows eavesdropping. In general, teardown
messages from an attacker are impossible to prevent,
because the necessary fields must be sent in the clear to
allow routing.<br />
<br />
Loss of service is the primary affect of session teardown
and is a likely result of incorrectly implemented attempts to
reestablish sessions to implement eavesdropping.
Redirecting media to broadcast addresses could cause a
DoS attack. Redirecting media sessions to a media gateway
could cause a DoS attack on the gateway.
</p>

<p class="virtualpage3">

<b>DoS</b><br />
Denial of service occurs when an attacker sends a wellcrafted
packet to the target system, causing it to fail,
resulting in a loss of function for an IP PBX or other VoIP
component. DoS also occurs when an attacker causes a
flood of packets to be sent, overwhelming the target and
preventing it from handling legitimate requests.<br />
<br />
DoS against a SIP system is simpler to achieve than DoS
against other data systems, due to the QoS requirements
of VoIP. DoS against a SIP system can occur through
registration hijacking, proxy impersonation, session tear
down, message tampering, or these additional means:<br />
<br />
�  Weak authentication on proxies forces them to handle
signaling packet floods.<br />
�  Using DNS to resolve remote hosts is proposed for SIP
address resolution over the Internet. If the DNS server
has incorrect data or has failed, a DoS condition results.<br />
�  DoS can result in targeted or large-scale loss of function
for the SIP system. A compromised PC can forge "Vias"
and create redirection to a non-existent host or a circuit
"Via" path. Using broadcast addresses causes <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>
amplification.<br />
�  SIP requires firewall ports to be opened for UDP streams,
allowing possible flooding of UAs. Weak authentication
mechanisms force UAs (including media gateways) to
handle UDP flooding. Firewalls must make fast,
intelligent decisions about which UDP ports are in use by
valid SIP calls.<br />
�  Firewalls determining which UDP ports are in use by SIP
<a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> and which ports should be denied could be slowed
if UDP streams are sent to random ports, consuming all
processing resources by forcing the firewall to determine
the state of all UDP ports.<br />
<br />
The 2003 CSI/FBI Computer Crime and Security Survey
reported that DoS had risen to be the second most
expensive computer crime among survey respondents . [7]
In the 2004 survey, DoS emerged as the most expensive
computer crime (replacing theft of proprietary information,
which had been the leading loss for 5 consecutiv e years).
The combined DoS losses for both years exceeded $91
million. [8]<br />
<br />
Considering the currently small percentage of VoIP
deployments, DoS attacks against VoIP systems cannot yet
be a significant contributor to these statistics. However,
considering DoS against a SIP system is simpler to achieve
than DoS against other data systems, the 2003 and 2004
surveys paint an ominous picture of future DOS attacks on
non-secure voice services.<br />
<br />
<b>NAT Transversal Issues</b><br />
NAT is an Internet standard that enables a LAN to use one
set of IP addresses for internal <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> and a second set of
addresses for external <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>, allowing the enterprise to
shield internal addresses from the public Internet. NAT
translates the internal local addresses to globally unique IP
addresses before sending packets to the outside <a href="../a1/VoIP-Network-platform.asp">network</a>.
NAT is common in the current IPv4 Internet, and is
essential in dealing with the limited address space problem
present with IPv4.<br />
<br />
SIP ignores the existence of NAT and assumes end-to-end
delivery of packets. The Session Description Protocol (SDP)
layer of SIP communicates the IP addresses of media
endpoints between UAs. If NAT modifies the IP addresses,
connections cannot be made, so Back-to-Back User Agents
(B2BUAs) straddle a NAT at the perimeter and modify SDP
to enable the connection.<br />
<br />
To be able to modify IP addresses, B2BUAs require SDP to
be unencrypted and unauthenticated. When B2BUAs are
used, it is impossible to use S/MIME to secure signaling, so
a large range of attacks are possible, including registration
hijacking, proxy impersonation, session tear down,
message tampering, and DoS, plus attacking the B2BUA to
gather the signaling data it processes.<br />
<br />
<b>IP PBX Vulnerabilities</b><br />
IP-enabled PBXs are traditional systems using a TDM-based
switching fabric. IP PBXs (also described as client/server,
call managers, media servers, etc.) are server platforms
running Windows, Linux, or some variant of Unix, which
provide voice services solely on the IP <a href="../a1/VoIP-Network-platform.asp">network</a> and require
a media gateway for communication to the PSTN.
Additional servers provide redundancy or other applications
such as voice mail and presence.<br />
<br />
The most critical vulnerabilities are those present on IP
PBXs-which are the heart of any VoIP <a href="../a1/VoIP-Network-platform.asp">network</a>. Due to
both their critical role in providing voice service and the
complexity of the software running on them, IP PBXs are
the primary target for attackers. An IP PBX's multiple
vulnerabilities are discussed below.<br />
<br />
<b>Non-Secure Operating System/Support Services</b><br />
IP PBXs run commercial operating systems including
Windows 2000, Linux, and variants of Unix, each of which
carry well-known vulnerabilities typically accessed through
<a href="../a1/VoIP-Network-platform.asp">network</a> services. The number of vulnerabilities depends on
the operating system, its version, and which <a href="../a1/VoIP-Network-platform.asp">network</a>
services are present/enabled. For example, the <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> Call
Manager runs on Windows 2000 and has a history of
vulnerabilities.<br />
<br />
Most IP PBXs run supporting services, such as a web server
or database. These services are used over the <a href="../a1/VoIP-Network-platform.asp">network</a>, and
like the operating system, can be exploited. For instance,
the <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> Call Manager uses IIS and SQL Server, which
have known vulnerabilities. The "Code Red" virus is an
example of an attack that targets IIS, and the SQL
Slammer worm is an example of an attack that targets SQL
Server.<br />
<br />
Operating system and support service vulnerabilities can be
exploited to cause a failure, or allow unauthorized access,
enabling configuration modification and access to sensitive
data, including voice mail messages and CDR.
</p>


<p class="virtualpage3">
<b>Unauthorized Administrative Access</b><br />
IP PBXs are managed through a web-based interface or
<a href="../a1/VoIP-Network-platform.asp">network</a> client. If the authentication method is
compromised, an attacker has administrative access to the
IP PBX. If a web server or client uses a weak protocol to
exchange authentication information, it is possible to collect
and exploit it.<br />
<br />
<b>Voice Application Implementation Attack</b><br />
Vulnerabilities also exist in the actual voice applications,
which accept signaling requests from the <a href="../a1/VoIP-Network-platform.asp">network</a> (in some
cases an external <a href="../a1/VoIP-Network-platform.asp">network</a>). Because the voice applications
are feature-rich, there will be the similar types of
implementation vulnerabilities. This will continue as
standards evolve and grow in scope. Vendors will be
constantly working to keep up with these standards and
their implementations will include vulnerabilities. Large
vendors are likely to have their own implementations and
the smaller ones will use protocol stacks from other
vendors as building blocks. When a vulnerability is found, it
will impact many users and possibly multiple vendor
implementations.<br />
<br />
Voice application vulnerabilities can be exploited to cause a
DoS, provide unauthorized access, or perform unauthorized
actions such as terminating a call, allowing a toll call, or
impacting a user's phone services. If unauthorized access is
obtained, access to configuration and data is also possible.<br />
<br />
<b>Voice Application Manipulation</b><br />
Voice applications will also be vulnerable to signaling
manipulation or attacks against weak authentication. For
example, unauthorized users will access toll lines, listen to
voice mail, and manipulate presence. Most of the
authentication information from IP phones is either weak
and/or sent in the clear. If attackers can authenticate
themselves as a privileged (or important) user, they will
have access to that user's service.<br />
<br />
<b><a href="../a1/VoIP-Network-platform.asp">Network</a> and Media Vulnerabilities</b><br />
VoIP transport is provided across the LAN, WAN, and
Internet. VoIP administration, call control signaling, and the
voice media travel over the same physical <a href="../a1/VoIP-Network-platform.asp">network</a> as other
IP services, consisting of switches, routers, firewalls, and
cabling. IP phones exchange signaling with IP PBXs to
establish calls, but call media is exchanged directly
between IP Phones. Although its components are less
vulnerable than IP PBXs, as described below, the <a href="../a1/VoIP-Network-platform.asp">network</a>
itself can be exploited to impact voice services.<br />
<br />
<b>DoS on Signaling</b><br />
VoIP <a href="../a1/VoIP-Network-platform.asp">network</a> components that accept signaling are
vulnerable to DoS. If an attacker generates a flood of
certain signaling requests (e.g., call setup requests) to a IP
PBX, media gateway, or IP Phone, the targeted device will
Enterprise Telecom Security Threats Page 11
be unable to efficiently process legitimate requests. The
actual level of vulnerability depends on device set up (i.e.,
authentication scheme, whether Virtual LANs (VLANs) are
used, etc.). If IP softphones are allowed, a rogue
application could exploit this vulnerability.<br />
<br />
Signaling DoS attacks on media gateways can consume all
available TDM bandwidth, preventing other outbound and
inbound calls and affecting other sites that use TDM. For
example, certain entertainment providers who set up TDM
1-800 numbers for voting were impacted by individuals
generating hundreds or thousands of VoIP requests through
a broadband service provider, which in turn converted them
to TDM and sent them on.<br />
<br />
<b>DoS on Media Sessions</b><br />
DoS on media is a serious problem. VoIP media, which is
normally carried with RTP, is vulnerable to any attack that
congests the <a href="../a1/VoIP-Network-platform.asp">network</a> or slows the ability of an end device
(phone or gateway) to process the packets in <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">real time</a>. An
attacker with access to the portion of the <a href="../a1/VoIP-Network-platform.asp">network</a> where
media is present simply needs to inject large numbers of
either RTP packets or high QoS packets, which will contend
with the legitimate RTP packets.<br />
<br />
VoIP media sessions are very sensitive to latency and jitter.
They are sent point-to-point and, other than media
gateways, do not typically go through IP PBXs. VoIP
requires QoS on the <a href="../a1/VoIP-Network-platform.asp">network</a>, which requires a switched
connection and a scheme for prioritizing voice <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a>.
VLANs help provide QoS and aid security.<br />
<br />
An attacker with access to the <a href="../a1/VoIP-Network-platform.asp">network</a> where the media
sessions are active, and who can generate packets with
high QoS labeling, will impact the media sessions. The most
vulnerable parts of the <a href="../a1/VoIP-Network-platform.asp">network</a> are shared segments, such
as the WAN or untrusted long-haul <a href="../a1/VoIP-Network-platform.asp">network</a>. The amount of
high QoS <a href="../a1/VoIP-Network-Real-Time-alert-monitoring.asp">traffic</a> allowable on a shared WAN is finite and
vulnerable to DoS.<br />
<br />
<b>Eavesdropping</b><br />
Users expect voice calls to be private, as opposed to email
or Instant Messaging (IM), where there is not usually an
expectation of privacy. Although some VoIP calls are
encrypted, most are not. Additionally, encryption without
strong authentication cannot guarantee privacy, because
participants can't be sure an attacker is not accessing the
media by performing a Main-In-The-Middle (MITM) attack.
If an attacker gains access to unencrypted media, simple
tools such as VOMIT, which converts an IP phone
conversation into a .WAV file, can be used to listen to or
distribute audio.<br />
<br />
If an attacker has access to the portion of the <a href="../a1/VoIP-Network-platform.asp">network</a>
where a call is being transported, the call can be recorded
using a packet sniffer or MITM application. A packet sniffer
records packets and saves them to a file. A tool such as
VOMIT converts the packets into a .WAV file that can be
distributed and played. A packet sniffer can collect media
as well as signaling, which can include tones, such as DTMF
tones used for account codes. The majority of VoIP
installations use switched Ethernet <a href="../a1/VoIP-Network-platform.asp">networks</a>, often with
VLANs, making it more difficult to sniff packets.<br />
<br />
VoIP calls taking place within the LAN are as vulnerable to
eavesdropping as other communication services, such as
email and IM. Traditional voice calls are "perceived" to be
more secure and more suitable for sensitive
communications because traditional (and VoIP) calls use
real-time protocols, as opposed to store-and-forward
protocols.

</p>

<p class="virtualpage3">
MITM attacks can also occur and defeat encryption on the
IP phone. For example, the MITM application acts as the IP
PBX, interacts with the real IP PBX, and "tricks" the source
and destination IP phones into sending the media to an
application that can record it. In this case, the call signaling
and media can be recorded, monitored, and manipulated.<br />
<br />
<b>Data Tunneling</b><br />
As VoIP connections are set up to an untrusted <a href="../a1/VoIP-Network-platform.asp">network</a>,
additional ports must be opened in the perimeter firewall.
As commonly occurs with port 80 (HTTP), other services
will "tunnel" through the new VoIP ports. These apparent
"voice" calls will be data calls, tunneling through the
firewall and wasting high QoS bandwidth. This same issue
exists for the WAN, where precious high QoS bandwidth is
wasted by data calls. Basically, these are new generation
modems, which must be detected and terminated in order
to maintain QoS.<br />
<br />
<b>IP Phone/Soft Phone Vulnerabilities</b><br />
IP phones are specialized computers designed to provide
voice service over the IP <a href="../a1/VoIP-Network-platform.asp">network</a> and use embedded
application-specific or general-purpose operating systems.
Softphones are applications that run on PCs and provide a
phone interface and functionality. Specific vulnerabilities
are being found on IP Phones at a faster rate than on IP
PBXs, mainly because the phones are fairly cheap and easy
for hackers to set up and experiment with. Similar
vulnerabilities exist with IP softphones because vendors
often use very similar software.<br />
<br />
IP Phones and softphones are the least critical part of the
VoIP <a href="../a1/VoIP-Network-platform.asp">network</a>, but they are also the most common and
least controllable components. This is especially true for IP
softphones, which are applications running on PCs.<br />
<br />
While IP Phones are fairly new, there are many
vulnerabilities present, virtually all of which are common on
other IP devices. This is alarming, because it indicates
security has received a low priority. Phones run generalpurpose
operating systems and must support rich signaling
as a basis for new applications. They are also inexpensive
(check eBay for IP phones) and easy for attackers to
acquire and experiment with. Studies such as the one at
www.sys-security.com, although slightly dated, accurately
illustrates the types of issues present with some phones.<br />
<br />
The specific vulnerabilities discussed in this section have
been or can be fixed, but the user is required to patch the
software on the IP Phone. Similar vulnerabilities exist with
IP softphones because vendors often use very similar
software. IP phone/Soft phone vulnerabilities include:<br />
<br />
<b>Unauthorized Remote Access</b><br />
Multiple IP Phones, including those from <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> (when using
"Skinny") and Pingtel, provide a web server for
administration. The web servers use base64 encoded
username/password pairs, which if retrieved from the
<a href="../a1/VoIP-Network-platform.asp">network</a>, are easily decoded.
Enterprise Telecom Security Threats Page 12
The Pingtel IP Phone ships with no administrator password
for the "admin" account, and the account name cannot be
changed. If a password is not implemented, an attacker can
gain both remote and local administrative access.<br />
<br />
Multiple IP Phones, including those from <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> and Pingtel,
provide a Telnet server. For both types of phones, abuse of
the Telnet feature provides operating system level access.
Telnet abuse leads to full access of the IP Phone operating
system.<br />
<br />
Administrative access to the IP Phone allows attacks
beyond DoS. For example, when using SIP, a MITM proxy
can be configured, giving the attacker access to the
signaling and media streams, which is a more likely means
of obtaining signaling and media than sniffing.<br />
<br />
The <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> ATA-186 analog telephone adapter interfaces
analog phones to a VoIP <a href="../a1/VoIP-Network-platform.asp">network</a>, and can be configured
via a web interface. One version of this device displays its
configuration screen-with password-if sent a single byte
HTTP POST. Such commands can be used to reconfigure
the device.<br />
<br />
<b>DoS</b><br />
For multiple IP Phones, an authenticated user can change
settings to create a DoS condition for the IP Phone. For
example, <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> IP Phones can be forced to restart by using
common DoS applications, including "jolt," "hping2,"
"trash," etc. <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> IP Phones can also be made to restart if
sent a specifically constructed HyperText Transfer Protocol
(HTTP) request. Any active call will be terminated in this
case.<br />
<br />
For specific Pingtel IP Phones, an authenticated user can
reset the phone, which requires 45 seconds to reset.
Certain values can be set which prevent the IP phone from
rebooting, which requires a local update. If exploited for
hundreds or thousands of phones within an enterprise, a
great deal of effort would be required to recover.<br />
<br />
<b>Unauthorized Local Access</b><br />
IP Phones are very vulnerable if an attacker has physical
access to the phone itself. Most IP Phones authenticate
themselves either only once or periodically, and do not
require authentication for each use (which would be
unacceptable for the average user and for 911 situations).<br />
<br />
IP Phones are highly programmable, so an authenticated
user can transfer, block, forward calls, etc. Additionally, an
attacker with local access can install a hub between the IP
Phone and the switch and eavesdrop on packets.
</p>

<p class="virtualpage3">
The <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> IP Phones locally store their configuration. Most
of this data is accessible via the "Settings" button. By
default, these settings are locked, but can be modified via a
documented, but non-configurable special key combination
"**#".<br />
<br />
The Pingtel IP Phones can be reset to defaults, changing
<a href="../a1/VoIP-Network-platform.asp">network</a> settings without requiring authentication. During
authentication, a potential attacker can view the password,
which is displayed prior to being replaced by an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
<br />
<b>Protocol Implementation Attack</b><br />
As previously discussed, protocols such as SIP have
inherent vulnerabilities. For example, an attacker who gains
visibility into the signaling between two IP Phones can issue
CANCEL or BYE requests, which effectively create DoS
against one or both phones. The attacker can also provide
certain return codes which create a DoS or allow a MITM
attack. These vulnerabilities can only be used if the
attacker has visibility into the signaling, so as to know
when to generate the appropriate requests or responses.<br />
<br />
<b>Unauthorized Firmware Upgrades</b><br />
Virtually all IP phones are programmable and can be
upgraded with new firmware. For instance, <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> phones
are upgraded through Trivial File Transfer Protocol (TFTP),
which is not secure and allows a trojan or rootkit to be
placed onto the IP Phone.<br />
<br />
The Pingtel SIP-based phone downloads applications from a
configured location. The default applications are retrieved
from http://appsrv.pingtel.com when the phone first boots.
By altering the DNS settings to point to a malicious server,
it is possible to cause the phone to download and install a
malicious package from an alternate location. Additionally,
the phone's firmware can be upgraded without
administrative privileges.<br />
<br />
VoIP Deployment Scenarios
The actual level of threat posed to an enterprise VoIP
<a href="../a1/VoIP-Network-platform.asp">network</a> varies based on the network's deployment
configuration, several of which are discussed below.<br />
<br />
<b>Campus/internal VoIP</b><br />

The vast majority of VoIP deployments are campus level,
where IP voice is converted to TDM by a media gateway for
access to the PSTN. VoIP services do not connect to the
Internet or any other untrusted <a href="../a1/VoIP-Network-platform.asp">network</a>. Most of these
environments use switched <a href="../../a1/VoIP-Network-platform.asp">networks</a> and VLANs, which
further segment the VoIP. In these installations, VoIP is
basically an island with a TDM perimeter. Of course, the
VoIP does interact with the LAN, which is connected to the
Internet, so it is possible for an attacker to gain access to
the LAN. Since an attack on the VoIP <a href="../a1/VoIP-Network-platform.asp">network</a> must
originate within the internal <a href="../a1/VoIP-Network-platform.asp">network</a>, the threat is
considered moderate.<br />
<br />
Figure 8 illustrates the risks involved with a
campus/internal VoIP deployment. The internal threat (i.e.,
an internal worker or a worker/vendor with remote access
to the <a href="../a1/VoIP-Network-platform.asp">network</a>) can access and exploit the IP PBXs. Toll
fraud or DoS attacks may be initiated against resources
outside the enterprise, creating liability and cost issues for
the enterprise. The legacy voice <a href="../a1/VoIP-Network-platform.asp">network</a> continues to
provide remote access into the enterprise LAN, as
discussed with reference to Figures 2, 3, and 4.<br />
<br />
<img src="images/Campus_Internal_VoIP_Risks.jpg" /><br />
<br />
Figure 8 - Campus/Internal VoIP Risks
</p>
<p class="virtualpage3">
<b>IP Centrex/Hosted IP</b><br />
According to Frost and Sullivan, the IP Centrex market is
expected to grow to 10 million lines by 2008. [11] In this
scenario, the IP PBXs are located at and managed by a
service provider, moving the responsibility for security to
the service provider-which "should" increase security.
However, VoIP must now traverse between the trusted and
untrusted perimeter of the enterprise and pass through the
enterprise firewall. As discussed previously, traditional data
firewalls are inadequate to meet the unique real-time, reliability, security, and session management requirements
of VoIP. The internal threat found in a campus deployment
still exists with IP Centrex, in addition to a threat from the
service provider's shared/untrusted <a href="../a1/VoIP-Network-platform.asp">network</a>; therefore, the
threat level is considered high.<br />
<br />
Figure 9 illustrates the various risks associated with an IP
Centrex deployment. The LAN is accessible by an attack
through the service provider's shared <a href="../a1/VoIP-Network-platform.asp">network</a>. Attacks
launched from within the enterprise, as well those through
the legacy phone <a href="../a1/VoIP-Network-platform.asp">network</a> continue to be issues.<br />
<br />
<img src="images/IP_Centrex_VoIP_Risks.jpg" /><br />
<br />
Figure 9 - IP Centrex VoIP Risks
<br />
<br />

<b>End-to-End VoIP</b><br />
Long term, VoIP calls will be transported as IP end-to-end.
Service providers will manage the media gateways that
translate between VoIP and legacy TDM-and TDM will be
the island, rather than VoIP being the island. This
deployment can offer enhanced applications and less
expensive calling. Most enterprises, especially large ones,
will manage their own IP PBXs and accept VoIP from an
untrusted <a href="../a1/VoIP-Network-platform.asp">network</a>. In an end-to-end VoIP deployment, the
voice <a href="../a1/VoIP-Network-platform.asp">network</a> can be attacked directly from the untrusted
external voice <a href="../a1/VoIP-Network-platform.asp">network</a>; therefore, the threat level is high.<br />
<br />
Figure 10 illustrates the multiple risks presented whenever
VoIP services are received from an untrusted IP <a href="../a1/VoIP-Network-platform.asp">network</a>.
Internal attacks against the IP PBXs, outbound attacks
against external resources, and inbound attacks directly
from the external voice <a href="../a1/VoIP-Network-platform.asp">network</a> are all capable of
exploiting vulnerabilities.<br />
<br />
<img src="images/End-to-End_VoIP_Risks.jpg" /><br />
<br />

Figure 10 - End-to-End VoIP Risks
<br />

<br />

<b>4. Hybrid Circuit-Switched/VoIP</b><br />
Vulnerabilities and Threats
During the transition from traditional circuit-switched voice
to VoIP, most enterprises will slowly deploy VoIP
technology by using a hybrid <a href="../a1/VoIP-Network-platform.asp">network</a> consisting of both
legacy circuit-switched and VoIP equipment. Even after an
enterprise becomes "all-VoIP," is it likely that TDM-based
services such as data modems, fax machines, dial-in
maintenance ports, alarm systems, and backup emergency
connection to the PSTN will continue to be supported in
parallel with the new VoIP <a href="../a1/VoIP-Network-platform.asp">network</a>. The security issues
with circuit-switched <a href="../../a1/VoIP-Network-platform.asp">networks</a> will persist, new issues with
VoIP will be introduced-and variations on the circuitswitched
and VoIP issues.</p>
<p class="virtualpage3">

<b>5. Summary</b><br />
The traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a>, the emerging VoIP <a href="../../a1/VoIP-Network-platform.asp">network</a>,
and the IP data <a href="../a1/VoIP-Network-platform.asp">network</a> face significant threats through the
unsecured telecom environment. Unauthorized and nonsecure
modems create the majority of the threat from the
traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a>. Although these issues have
existed for years, they have become more acute due to the
rapid growth of the Internet and the maturity of the tools
used to protect the enterprise's connection to the Internet.
Threats from the traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a> will not
disappear with VoIP implementation. Even after the
migration period is over, enterprises will continue with a
hybrid mix of legacy services and IP services.<br />
<br />
VoIP is the future for voice communications. However, it is
clear that the VoIP <a href="../a1/VoIP-Network-platform.asp">network</a> will be less secure than the
traditional voice <a href="../a1/VoIP-Network-platform.asp">network</a>. These vulnerabilities include
those common to any IP service, and those that are
uniquely a result of the complexity and real-time
requirements of the voice service.<br />
<br />
As long as VoIP is primarily implemented as an internal
service, its inherent vulnerabilities represent a risk similar
to other internal IP applications. However, when the
enterprise connects VoIP to the untrusted <a href="../a1/VoIP-Network-platform.asp">network</a>, the
threat will increase significantly-and conventional
perimeter security devices are not designed to adequately
support the unique requirements of voice communications.<br />
<br />
Voice managers need to be able to control security across a
mixture of both circuit-switched and VoIP <a href="../../a1/VoIP-Network-platform.asp">networks</a>. They
need to both secure their entire voice service and adapt as
their <a href="../a1/VoIP-Network-platform.asp">network</a> migrates to VoIP. Voice managers need a
solution that is agnostic to the underlying transport type,
has a robust management infrastructure, and a user
interface that insulates them from the myriad details of the
underlying hardware, transport, and protocols.</p>

</div></td>
  </tr>
</table><script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="281" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="257" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Telecom_Security_Threats.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /> </a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="html/3com-solutions-strategies.as"></a>  <a href="../doc/Telecom_Security_Threats.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
         </tr>
       </table></td>
       </tr>
   </table>
   </td>
</tr>
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
