<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script type="text/javascript" src="virtualpaginate.js">

/***********************************************
* Virtual Pagination script- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="../simpletree.css" />
<style type="text/css">

/*Sample CSS used for the Virtual Pagination Demos. Modify/ remove as desired*/

.virtualpage, .virtualpage2, .virtualpage3{
/*hide the broken up pieces of contents until script is called. Remove if desired*/
display: none;
}

.paginationstyle{ /*Style for demo pagination divs*/
	width: 530px;
	padding: 16px 10px 4px 0;
	background-image: url(images/fondo-pag.gif);
	height: 32px;
	background-repeat: no-repeat;
	text-align: right;
	clear: right;
	position: relative;
	background-position: right;
	margin-top: 10px;
	margin-bottom: 5px;
	margin-left: 0;
}

.paginationstyle select{ /*Style for demo pagination divs' select menu*/
	border: 1px solid navy;
	margin: 0 5px ;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}

.paginationstyle a{ /*Pagination links style*/
padding: 0 5px;
text-decoration: none;
border: 1px solid black;
color: navy;
background-color: white;
}

.paginationstyle a:hover, .paginationstyle a.selected{
color: #000;
background-color: #FEE496;
}

.paginationstyle a.imglinks{ /*Pagination Image links style (class="imglinks") */
border: 0;
padding: 0;
}

.paginationstyle a.imglinks img{
vertical-align: bottom;
border: 0;
}

.paginationstyle a.imglinks a:hover{
background: none;
}

.paginationstyle .flatview a:hover, .paginationstyle .flatview a.selected{ /*Pagination div "flatview" links style*/
color: #000;
background-color: yellow;
}

.style1 {font-style: italic}
</style>



</head>
<body onselectstart="return false"><table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome"><tr><td width="221" align="left" valign="top">
   <table width="742" border="0" align="center" cellpadding="8" cellspacing="0">
          <tr>
            <td width="726" scope="col">
<!-- Pagination DIV for Demo 3 -->




<!-- Initialize Demo 3 -->
<table width="100" border="0" cellspacing="0">
  <tr>
    <td valign="top" scope="col"><!--#include file="../../includes/menudocumentation.asp" --></td>
    <td valign="top" scope="col"><h1>
Security, Wiretapping, and the Internet<br />
            </h1> 
<div id="listingpaginate" class="paginationstyle">
<a href="#" rel="previous" class="imglinks"><img src="images/roundleftig4.gif" width="20" height="20" /></a> 
<select>
</select> <a href="#" rel="next" class="imglinks"><img src="images/roundrightat5.gif" width="20" height="20" /></a></div><div style="width: 520px; border: 1px dashed gray; padding: 10px;">
<p class="virtualpage3">
<b><a href="#">Introduction to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a></a></b> <span class="credits"></span><br>
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is a fully Open Source, hybrid TDM and packet voice PBX and IVR platform.<br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is and has been Open Source under GNU GPL (with an exception permitted
for linking with the OpenH323 project, in order to provide H.323 support). Commercial
licensing is available from Linux Support Services, Inc. (http://www.linux-support.net) for
applications in which the GPL is inappropriate.<br />
<br />
Unlike many modern "soft switches", <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can use both traditional TDM
technology and packet voice (Voice over IP and Voice over Frame Relay) protocols. Calls
switched on TDM interfaces provide lag-less TDM call <a href="../a1/VoIP-Network-platform.asp">quality</a>, while retaining
interoperability with VoIP packetized protocols.<br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> acts as a full featured PBX, supporting virtually all conventional call
features on station interfaces, such as Caller*ID, Call Waiting,Caller*ID on Call Waiting, Call
Forward/Busy, Call Forward/No Answer, Call Forward Variable, Stutter Dialtone, Three-way
Calling, Supervised Transfer, Unsupervised Transfer, ADSI enhancements, Voicemail,
Meet-me Conferencing, Least Cost Routing, VoIP gatewaying, Call Detail Records, etc. At
the same time, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> provides full IVR capability, programmable at several layers, from a
low-level C interface, to high level AGI scripting (analogous to CGI) and extension logic
interfaces. <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> IVR applications run seamlessly from one interface to another, and need
not know anything about the physical interface, protocol, or codec of the call they are
working with, since <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> provides total abstraction for all those concepts.<br />
<br />
<b>Supported Hardware</b><br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> supports a variety of hardware interfaces for bringing in <a href="../../a1/VoIP-Network-platform.asp">telephony</a> channels
to a Linux box. In order of best supported to least supported:<br />
<br />
Zaptel (Zapata <a href="../../a1/VoIP-Network-platform.asp">Telephony</a>) interface<br />
<br />
The zaptel <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> infrastructure was jointly developed by Mark Spencer of Linux
Support Services, Inc. and Jim Dixon of Zapata <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a>. The zaptel interface breaks with
the established conventional wisdom by using the host processor to simulate the TDM bus
which is typically built into other <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">telephony</a> interfaces (e.g. Dialogic and other H.100
vendors). The resulting pseudo-TDM architecture is a system which requires somewhat
more processor, at a substantial savings in hardware cost and increase in flexibility.
Resources such as echo cancelers, HDLC controllers, conferencing DSP's, DAX's and
more are replaced by software equivalents. Just like traditional hard-TDM interfaces,
switching is done in near-realtime, and call qualities are substantially the same. The
pseudo-TDM architecture also can extend the TDM bus across Ethernet <a href="../../a1/VoIP-Network-platform.asp">networks</a>. Zaptel
devices also support data modes on clear channel interfaces, including <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> HDLC, PPP,
and Frame Relay<br />
<br />
A number of Zaptel compatible interfaces are available from Linux Support Services.
At the time of this writing, the following cards are available:<br />
<br />
X100P - Single FXO Analog Interface (PCI)<br />
T100P - Single T1/PRI Interface (PCI)<br />
E100P - Single E1/PRA Interface (PCI, exp. Q3 2002)<br />
T400P - Quad T1/PRI Interface (PCI)<br />
E400P - Quad E1/PRA Interface (PCI)<br />
S100U - Single FXS Interface (USB)<br />

<br />
<i>Linux <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a> Interface</i><br />
<br />
The Linux <a href="../../a1/VoIP-Billing-Platform-Retail-Voip-Billing.asp">Telephony</a>Interface, was developed primarily by Quicknet, Inc. with help from
Alan Cox. This interface is geared toward single analog interfaces and also provides
support for low bit-rate codecs. The following products are known to work with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>:<br />
<br />
Quicknet Internet Phonejack (ISA, FXS)<br />
Quicknet Internet Phonejack PCI (PCI, FXS)<br />
Quicknet Internet Linejack (ISA, FXO or FXS)<br />
Quicknet Internet Phonecard (PCMCIA, FXS)<br />
Creative Labs VoIP Blaster (limited support)<br />
<br />

<i>ISDN4Linux</i><br />
<br />
The ISDN4Linux interface is used primarily in Europe to bring lines into <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> from BRI
interfaces. Any adapter that is supported by ISDN4Linux should work with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
<br />
<i>OSS / ALSA Console Drivers</i><br />
<br />
The OSS and ALSA console drivers allow a single sound card to be used as a "console
phone" for placing and receiving test calls. Using autoanswer/autohangup, the console can
also be used to create an intercom.<br />
<br />
<i>Adtran Voice over Frame Relay</i><br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> supports Adtran's proprietary Voice over Frame Relay protocol. The following
products are known to talk to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> using VoFR. You will need a Sangoma Wanpipe or
other frame relay interface to talk to them:<br />
<br />
Adtran Atlas 800<br />
Adtran Atlas 800+<br />
Adtran Atlas 550<br />
<br />
</p>

<p class="virtualpage3"><b>Supported VoIP Protocols</b><br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> supports three VoIP protocols, two industry standards and one originally developed
specifically for <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, but now used by a number of other hardware and software devices.
In the order of most supported to least supported:<br />
<br />
<i>Inter-<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> Exchange (IAX)</i><br />
<br />
IAX is the defacto standard VoIP protocol for <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> networking. Perhaps its most
impressive feature is its transparent interoperation with NAT and PAT (IP masquerade)
firewalls, including placing, receiving, and transferring calls and registration. In this way,
PBX's and phones can be totally portable. Just plug them in anywhere on the Internet and they'll register with their home PBX, instantly routing extensions to them appropriately. IAX is
extremely low-overhead (four bytes of header, as compared to at least 12 bytes of header for
RTP based protocols like SIP and H.323). IAX control messages are also substantially
smaller. IAX supports internationalization, permitting the requesting PBX or phone to receive
content from the providing PBX in its preferred language if available. IAX also supports
authentication on incoming and outgoing calls, with fine-grained control to limit access to
specific portions of the dialplan. Using IAX dialplan polling, the dialplan for a collection or
cluster of PBX's can be centralized, with each PBX only needing to know its local
extensions, and able to query the central PBX for further assistance as required.
<br />
<br />
<i>Session Initiation Protocol (SIP)</i><br />
<br />
SIP is the IETF standard for VoIP. Its call control syntax resembles SMTP, HTTP, FTP and
other IETF protocols. The back-end runs on <a href="../../a1/VoIP-Network-Real-Time-alert-monitoring.asp">Real Time</a> Protocol (RTP). SIP is widely
regarded as the up-and-coming standard in VoIP due to its relative simplicity in comparison
with H.323 and human-readability. As of the time of this writing, the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> SIP stack has
interoperated with a multiple vendors including SNOM and <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a>.<br />
<br />
<i>H.323</i><br />
<br />
H.323 is the ITU standard for VoIP. Support for H.323 in <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> was contributed by Michael
Mansous of InAccess <a href="../../a1/VoIP-Network-platform.asp">Networks</a> (http://www.inaccessnetworks.com), and is based on the
OpenH323 project (http://www.openh323.org).

<b>Codec and file formats</b><br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> provides seamless and transparent translation between all of the following codecs:<br />
<br />
16-bit Linear        128 kbps<br />
G.711u (?? -law)     64 kbps<br />
G.711a (A-law)       64 kbps<br />
IMA-ADPCM            32 kbps<br />
GSM 6.10             13 kbps<br />
MP3                  (variable, decode only)<br />
LPC-10               2.4 kbps<br />
<br />
In addition, other codecs, such as G.723.1 and G.729 can be passed through transparently.<br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> seamlessly and transparently supports a variety of file formats for audio files. When
a file is played on a channel, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> chooses the "least expensive" format for that device.
Supported formats include:<br />
<br />
"raw"           16-bit linear raw data<br />
"pcm"           8-bit mu-law raw data<br />
"vox"           4-bit IMA-ADPCM raw data<br />
"mp3"           MPEG2 Layer 3<br />
"wav"           16-bit linear WAV file (8000 Hz)<br />
"WAV"           GSM compressed WAV file (8000 Hz)<br />
"gsm"           Raw GSM compressed data<br />
"g723"          Simple G723 format with timestamp<br />
<br />
<b>Why <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is Important</b><br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> represents a highly valuable piece of software for a number of reasons:
Extreme cost reduction - Combined with low-cost Linux <a href="../../a1/VoIP-Network-platform.asp">telephony</a> hardware, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can be
used to create a PBX at a fraction of the price of traditional PBX and key systems, while
providing a level of functionality exceeding that of many of the most expensive systems
available.<br />
<br />
Control - <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> allows the user to take control of their phone system. Once a call is in an
Linux box with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, anything can be done to it. In the same way that Apache gives the
user fine-grained control over virtually every aspect of its operation (and its Open Source
nature gives even more flexibility), the same applies to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> (in fact, it is my hope that
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> will one day become the Apache of the <a href="../../a1/VoIP-Network-platform.asp">telephony</a> world, greatly surpassing the
market share of the proprietary players).<br />
<br />
Rapid deployment and development - <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> allows PBX's and IVR applications to be
rapidly created and deployed. Its powerful CLI and text configuration files allow both rapid
configuration and real-time diagnostics.<br />
<br />
Rich, broad feature base - Because <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is Open Source and is implemented in software,
not only does it provide features such as voicemail, voice menus, IVR and conferencing
which are very expensive for proprietary systems, but it also allows new features to be
added rapidly and with minimal effort.<br />
<br />
Customization - Through its internationalization support, configuration files, and source code,
every aspect of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can be tweaked. For example, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>'s codes for call features
could be changed to match an existing system.<br />
<br />
Dynamic Content Deployment - In the same way that web servers like Apache allow a user to
deploy dynamic content, such as account information, movie show times, etc, on the web,
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> permits you to deploy such dynamic content over the telephone, with the same ease
as CGI.<br />
<br />
Extremely Flexible Dialplan - <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>'s unusually flexible dialplan allows seamless integration
of IVR and PBX functionality. Many of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>s existing features (and desired features of
the future) can be implemented using nothing more than extension logic. <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> supports a
mix of extension lengths.<br />
<br />
</p>
<p class="virtualpage3">
<b><a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> as a Black Box</b><br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> connects any phone, phone
line, or packet voice connection, to
any other interface or service, using
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> applications. <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>'s
codec, file format, channel, CDR,
application, switch and other Apia's
distance the developer/deployer from
the intricacies of the entire system.<br />
<br />
<img src="images/Asterisk_as_a_Black_Box.jpg" /><br />
<br />
<b>Example <a href="../../a1/VoIP-Network-platform.asp">Networks</a></b><br />
It may be helpful to see some sample diagrams of real-world Asterisk installations to see just
how you do, in fact, get calls into and out of a Linux box.<br />
<br />
<img src="images/Example_Networks1.jpg" /><br />
<br />
One of the beauties of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is its
ability to scale from very small
architectures to very large
architectures relatively seamlessly.
An X100P from LSS, or a LineJack
from Quicknet can be used to bring
in an incoming phone line. A
telephone can be hooked up via an
S100U from LSS, or a Phonejack
from Quicknet. Or, alternatively, an
IP phone, such as the SNOM 100
could be used, or even a <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp">Cisco</a> ATA186 to bring out phone lines. In this way, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can
be used to create a very low-density PBX framework.<br />
<br />
<img src="images/Example_Networks2.jpg" /><br />
<br />
This diagram shows another
variation on the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> PBX
theme. Here, 8 incoming phone
lines, and 16 outgoing stations are
connected to a channel bank,
which is connected to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
using a T1 cross over cable
(remember, in this diagram T1 is just
a technology, not a service that
you have to pay for). A T100P from
LSS then performs all the PBX
functionality, using the channel
bank as a simple analog to digital converter. Channel banks are fairly commodity items in
the telcom industry, and just about any brand and vendor should be compatible with
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
<br />
<img src="images/Example_Networks3.jpg" /><br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can also be used in
high density, redundant
applications. In this example,
two rackmount servers could be
providing conferencing and/or
IVR, using a TDMoE span for
communication between the two
machines. Since <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> can
perform a graceful shutdown,
having redundancy in your
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> IVR <a href="../../a1/VoIP-Network-platform.asp">networks</a> means
you can take down one
machine at a time and upgrade
the software and then bring it back up, while you take down another machine to upgrade its
software.</p>

<p class="virtualpage3">
<b>The <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> Dialplan</b><br />
<br />
<img src="images/The_Asterisk_Dialplan.jpg" /><br />
<br />
It is <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>'s dialplan itself which provides much of its flexibility. The dialplan is what
determines how calls are routed through an <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> system. It is composed of a group of
(generally connected) extension contexts, which are simply collections of extensions. An
extension may appear in more than one context, and one context may include another (with
an optional date/time limitation). Contexts may also reference external switches (e.g. The IAX
switch), permitting the dialplan to be externally augmented.<br />
<br />
The above diagram represents a fictitious PBX for the GNU/Linux community. Each context
is in its own table, listing extensions and what those extensions represent. The fine, gray
lines represent Links from an extension in one context to another. The thick lines represent
the inclusion of one context in another (The arrow points from the context which is being
included).<br />
<br />
Creative use of contexts can be used to implement many features, some of which are
illustrated in the above diagram:<br />
<br />
Security - Dialplans can be segmented for allowing certain extensions to only be
accessible from certain interfaces or VoIP users. In the example dialplan, local phones
would be placed in the "dialout" context, permitting them to make outgoing calls, which
inbound callers would be placed in the "mainmenu" context, precluding them from accessing  the "9" extension for an outbound line.<br />
<br />
Voice Menus - Since extensions can be any length and can be reused in different
contexts, voice menus can be created. Notice how extensions 1,2, and 3 are used in the
"mainmenu" context for sales, support, and accounting, and then again in the "support"
menu for compiler, GUI, and kernel. By including the "default" context in both the mainmenu
and support menu contexts, the calling user can at any point enter the extension of the
person they are trying to reach.<br />
<br />
Authenticated Services - Contexts can be used to verify a password or passcode,
by permitting one valid extension to a given service and a pattern match going to failure.<br />
<br />
PBX Multihosting - Obviously multiple company PBX's could be hosted at one site,
with the DID (Direct Inward Dialing) number deciding which context the call would begin in.<br />
<br />
Callback Services - Extension contexts, external scripts, and app_qcall can be
combined to implement callback services.<br />
<br />
Day/night Modes - Since an include can be time/day dependent, behavior can be
modified for different times and days. For example, in the above diagram, the "daytime"
context might be included from 9 to 5 mon-fri, and then the "afterhours" included afterwards,
so that during the day the operator phone can ring, and in the evening an "after hours"
message played and immediately sent to enter an extension.<br />
<br />
Dialplan Centralization - Using the "IAX" internal switch, the dialplan of a remote
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> server can be polled, so that the dialplan for a complex system can be centralized
on a single server.<br />
<br />
<b><a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> Extensions</b><br />
Extension contexts are made of extensions. Extensions themselves consist of a set of
applications (optionally with arguments) and priorities. Beginning with priority one, the
applications are executed generally in order until the call is hung up. <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> applications
and AGI scripts may modify the call flow, however. Extensions may route based on both
called and caller number.<br />
<br />
Extension names may contain numbers, letters, *, and #. They can be numbers, or text
expressions (like "main"). If preceded by a "_", the number is considered a pattern match,
where 'N' represents any digit 2-9, 'X' represents any digit 0-9, and '.' represents a match
everything.<br />
<br />
Certain extension names are reserved for specialized purposes. Specifically:<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;s: The "start" extension. A call which does not have digits associated with it (for
example, a loopstart analog line) begins at the "s" extension.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;t: The "timeout" extension. When a caller is in a voice menu and does not enter the
proper number of digits (or enters no digits at all), the timeout extension is executed. If the
timeout extension is omitted, then the caller is disconnected.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o: The "operator" extension. When a caller zero-s out of voicemail to get an
operator, this extension is executed. If not present, the caller is not permitted to zero out of
voice mail.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;h: The "hangup" extension. If present, this extension is executed at the end of call
(i.e. when the caller hangs up or is hung up upon). Applications executed in this extension
should not try to do anything to the channel itself since it has been hung up already, but can
use it for logging, executing commands, etc.<br />
<br />
Extension logic is best illustrated by examples:<br />
<br />
<i>Simple extension with Voicemail</i><br />
<br />
<i>exten => 600, 1, Dial, Zap|15<br />
exten => 600, 2, Voicemail, u600<br />
exten => 600, 102, Voicemail, b600</i>
<br />
<br />
The above lines implement a simple extension with voicemail. When a caller was directed to
extension 600, first, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> would try dialing the Zap/9 interface (channel 9 of the zaptel
system) for up to 15 seconds. If there was no answer the user would be dumped to
unavailable voicemail. The "Dial" application is somewhat special in that it allows you to
optionally setup a separate rule for a "busy" (n + 101 instead of n+1), so in this case, if the
user gets a busy, they are sent to voicemail with a "busy" message played instead of
"unavailable".

</p>

<p class="virtualpage3">
<i>Extension with anti-ex-girlfriend</i><br />
<br />
extem => 600/2565551212,1,Congestion<br />
extem => 600, 1, Dial, Zap/9&amp;IAX/marko/s|15<br />
extem => 600,2,Voicemail, u600<br />
extem => 600,102,Vpicemail,b600<br />
<br />
Note: This example is derived from an actual extension used by one of my friends]
<br />
<br />
This example is similar to the previous one, except it exercises two additional features of
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> -- Caller*ID matching, and the ability to call multiple interfaces at the same time.<br />
<br />
If the incoming caller has the Caller*ID of 256-555-1212, then the caller is immediately given
"congestion tone", as if the number were invalid or having trouble. Otherwise, the Dial
application is used to try to call both Zap/9 and another IAX host, "marko", for up to 15
seconds. If neither answers, then the call is sent to voicemail, preceded by an "unavailable"
message. If the interfaces are busy, they are sent to the "busy" voicemail instead.<br />
<br />Simple Call Queue
exten=> 600,1,Dial,Zap/9|15<br />
exten=> 600,2, Voicemail,u600<br />
exten=> 600,102,WaitMusicOnHold,5<br />
exten=> 600,103,Goto,1<br />
<br />


<br />
This example demonstrates use of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> extension logic to create a simple call queue,
where the user is put on hold (with music in this case) until the called party becomes
available, or is unable to answer the phone.<br />
<br />
When a caller enters this extension, it immediately tries to dial on the Zap/9 interface for up
to 15 seconds. If there is no answer, the caller is sent to voicemail immediately. However, if
the interface is busy, they are played hold music for 5 seconds, and then sent back to the
first extension to try Zap/9 again. Note that this is not the same as true call queuing since
there is no prioritization of the calls (FIFO) implemented.<br />
<br />
<i>Operator Extension for a Small Business</i><br />
<br />
exten=> 0.1,Dial,Zap/9|15<br />
exten=> 0.2,Dial,Zap/10&amp;Zap/11&amp;Zap/12|15<br />
exten=> 0.3,Playback,companymailbox<br />
exten=> 0.4,Voicemail,600<br />
exten=> 0.5,Hangup<br />

<br />
This example implements an operator extension for a small business. Here, when the "0"
extension is executed, first the "real" operator is called on Zap/9. Then, if there is no
answer, or the person is busy, then three other people are dialed for up to 15 seconds. If
there is no answer or they are all busy, the user is asked to leave a message in the
company mailbox, and then dumped into the operator's voicemail, without their normal
announcement being played at all.<br />
<br />
<b>Outgoing Line with Least Cost Routing</b><br />
exten=> _9NXXXXXX,1,Dial,Zap/g2/BYEXTENSION<br />
exten=> _9NXXXXXX,2,Dial,IAX/oh/BYEXTENSION<br />
exten=> _9NXXXXXX,3,Congestion<br />

<br />
This example demonstrates the use of pattern matching and helps show how everything in
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is treated as an extension, be it an outgoing line, incoming interface, or voice menu.
Here, we first try using any interface in "group 2" first to connect the call. If that is
unavailable, we try calling through another IAX host called "oh". If that fails, the congestion
tone is executed.<br />
<br />
<i>Main Voice Menu</i><br />
<br />
exten=> s,1,Wait,1<br />
exten=> s,2,Answer<br />
exten=> s,3,DigitTimeout,5<br />
exten=> s,4,ResponseTimeout,10<br />
exten=> s,5,Background,intro<br />
exten=> s,6,Background,instructions<br />
<br />
exten=> 1,1,Goto,sales|s|1<br />
exten=> 2,1,Goto,support|s|1<br />
exten=> i,1,Playback,pbx-invalid<br />
exten=> i,2,Goto,s|6<br />
exten=> t,1,Goto,0|1
<br /><br />

This example implements a voice menu. When an incoming call is received, <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> waits
for one second (to give a little bit of ring to the caller), answers the line, sets the digit and
response timeouts to reasonable numbers, and then plays an introduction along the lines of
"Thank you for calling GNU/Linux headquarters." in the background, while waiting for the
user to enter an extension. After the introduction, instructions are played, for example "If you
know the extension of the party you wish to dial you may enter it at anytime, otherwise press
1 for sales or 2 for support." If the party does not enter any extension, they are sent to the
operator (whose extension is omitted for brevity). If they enter an invalid extension, they are
played a message to inform them the extension is invalid and then presented with the options
again.<br />
<br />

<i>AGI integration for routing</i><br />
<br />
exten=> s,1,AGI,agi-lookup.agi<br />
exten=> s,2,Background,intro<br /><br />

exten=> 100,1,AGI,agi-save.agi<br />
exten=> 100,2,Dial,Zap/9|15<br />
exten=> 100,3,Voicemail,u100<br />
...<br />
exten=> 120,1,AGI,agi-save.agi<br />
exten=> 120,2,Dial,Zap/24|15<br />
exten=> 120,3,Voicemail,u101<br />
<br />

<br />
This set of extensions is designed to allow a caller to speak with whomever the last person
was that they spoke with, without having to remember their extension. An incoming caller is
first sent to the agi-lookup script, which checks their Caller*ID against a database. If the
caller has already spoken with someone before, the "agi-lookup" script sends them to the
last extension they called. Otherwise, the call progresses normally. When the user
connects to someone's extension, the "agi-save" script saves the person they are about to
speak with, for future reference by the agi-lookup script the next time they call.<br />
<br />
<i>Ringback Example</i><br />
<br />
[ringback]<br />
exten=> s,1,Ringing<br />
exten=> s,2,Wait,1<br />
exten=> s,3,Congestion<br />
exten=> h,1,System,callback<br />
<br />
[default]<br />
exten=> 1234,1DISA,4321|trusted<br />
exten=> 1235,1,Goto,ringback|s|1<br />

<br />
[Note: This is the same format as what I use when I'm in France to call back to the U.S.]<br />
<br />
This example shows how to create a ringback system. When extension 1235 is dialed, the
caller is sent to the ringback extension, which plays ringtone for one second, followed by
congestion tone. Note that the call is *never* answered at this point. Finally, when the
caller hangs up, the "callback" script is executed, which would create an appropriate entry
in /var/spool/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>/qcall for the outbound call.<br />
<br />
Then, app_qcall would call a pre-specified number, and when the line was answered, would
dump the caller into extension 1234, which is the DISA (Direct Inward System Access). After
entering a password (4321), the called party could now make outbound calls through the
"trusted" extension context.

</p>



<p class="virtualpage3">
<b>Configuration Files</b><br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is configured through a number of configuration files located in /etc/<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. Config
files use a consitant format, which consists of one or more sections (delimited by the section
title in brackets, like "[section]"), and a number of "variable = value", or "object =>
parameters." Comments are delimited by ';' instead of '#' since # could often be used in
extensions. Here is a sample:<br />
<br />
[section1]<br />
variable1=value1 ; assign variable1 the value of value1<br />
variable2=value2 ; assign variable2 the value of value2<br /><br />

[section2] ; New section now<br />
variable3=value3 ; assign variable 3 the value of value3<br />
object=> param2 ; create an object1 type with param2 parameters<br />


<br />
When declaring objects or assigning variables, the "=" and "=>" may be used
interchangeably. They are parsed identically and their use is only designed to make
configuration files more readable. This format, coincidentally, fairly closely resembles the
"win.ini" format. It was designed to be easily machine and human parsable.<br />
<br />
The grammar of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> varies from one config file to another, however the configuration
files can generally be divided into three categories which will be detailed more closely in the
following sections:<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Interface Configuration - These types of files typically configure channel interfaces
which bind directly to physical hardware. Examples include adtranvofr.conf, alsa.conf,
modem.conf, oss.conf, phone.conf, and zapata.conf.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Simple Groups - These types of files define the existence of various simple entities,
like mailboxes, conference rooms, etc. Examples include extensions.conf, logger.conf,
meetme.conf, musiconhold.conf, parking.conf, and voicemail.conf.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp;Individual Entities - These configs detail a number of typically unrelated entities, like
clients and servers and are used most often for VoIP services. Examples include iax.conf,
oh323.conf, and sip.conf.<br />
<br />
An example of one of each of these types of config files can familiarize a user with their use,
and the details of what options and fields are configurable in each one is generally best
determined by looking at the sample configurations that are included with <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.<br />
<br />
<b>Interface Configuration Example: zapata.conf</b><br />
The zapata.conf file is one of the most adjustable interface configuration files. It controls all
zaptel based interfaces that <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> is to be aware of, setting up their protocol, security and
features.<br />
<br />
At the time of this writing, the zapata.conf file has only one section, the "channels" section.
Numerous options can be set for each channel to configure the caller-id, features, and
starting context of each channel. When a channel is instantiated with the "channel => <x>"
syntax, it is created with all the parameters specified above it. This allows you to setup
parameters which are global to many channels, and then tweak some of them individually as
you go through. For example:<br />
<br />
[channels]<br />
signalling = fxo_ks<br />
callwaiting = yes<br />
threewaycalling = yes<br />
callerid = "Linus Torvalds" <(256) 555 - 1000>
channel=> 1<br />
<br />
callerid = "Richard Stallman"> <(256) 555-2000>
callwaiting = 0<br />
channel=> 2<br />
<br />
callwaiting = yes<br />
callerid = "Eric Raymond" <(256) 555-3000><br />
channel=> 3<br />
<br />
signallimg = fxs_ks<br />
callerid = asreceived<br />
chanell=> 4-8
<br /><br />

In this example, Three FXO signalled channels and four FXS signalled channels are
created. The first three represent phones for Linus, Richard, and Eric respectively. We
enable callwaiting and threeway calling before instantiating Linus's phone on channel 1.
Then, we change the value of the callerid, and disable call waiting, then create Richard's
phone. Note that the "callwaiting = no" only affects Richard and not Linus because it is
specified after Linus's channel has been instantiated. After creating Richard's line, we
re-enable call waiting, change the Caller*ID to be Eric, and create the third channel.<br />
<br />
Next, we set the signalling to fxs_ks, set the callerid to be the value that is received from the
line, and create four new channels. Theoretically these channels have callwaiting and
threeway calling enabled, although those features only apply to station interfaces (that we
signal with FXO signalling).<br />
<br />
<b>Simple Group Configuration Example: extensions.conf</b><br />
In simple group configuration files, each section represents a group which contains
individual lines, each instantiating an object, completely independantly of the other objects
in the group. Sometimes the "general" section is reserved for global parameters that apply to
whatever it is configuring in general. The most widely used example of this sort of
configuration file is of course the extensions.conf file. In this file, the "general" section isn't
actually used at this time, but may be in the future. All other sections represent contexts,
and each line of each section either a) generates a single piece of an extension, b) declares
an external switch, or c) includes another context.<br />
<br />
[general]<br />
;<br />
; Rarely used, totally unimplemented general options done here.<br />
;<br />
[context1]<br />
;exten=> extension, priority, application ; [,args]<br />
exten=> s,1Wait,3<br />
exten=> s,2,Answer<br />
exten=> s,3,Voicemail, u600<br />
exten=> 100,1Dial,Zap/g2<br />
<br />
[context2]<br />
exten=> 9,1,Dial,Zap/g2/9<br />
include=> context1<br />
include=> context3<br />
<br />
[context3]<br />
switch=> IAX/myuser@otherhost/local
<br />

<br />
In the above example, we have three declared contexts. Context 1 has two extensions, "s"
which has three steps, and "100" which has only one step. Context 2 includes a "9"
extension as well as including everything in context1 and everything in context3 (note that
includes are processed in the order listed, so if an extension's step were in both context1
and context3, the one in context 1 would be executed).<br />
<br />
VoIP connections typically use the "individual entity" config grammar. The most commonly
used file of this type is the "iax" configuration file. It contains a "general" section, with global
parameters like the bind address and port, and then a number of entities, each of which has
a "type" (user, peer, or friend) and its own set of parameters. Each entity is entirely
independent of the other entities surrounding it.<br />
<br />
[general]<br />
type=friend<br />
secret=mypass<br />
callerid="Mark Spencer" <(256) 428-6000><br />
deny=0.0.0.0/0.0.0.0<br />
permit=216.207.245.0/255.255.255.0<br />
[greg]<br />
type=friend<br />
host=dynamic<br />

<br />
In this case, the binding port is set to 5036 (the default), and two entities are declared -- a
friend named "greg" and a friend named "mark". The "mark" user has a number of
parameters declared like callerid, a secret, and a permit/deny list. Note that with some
options, like "secret", there can be only one logical value, and so adding another simply
replaces the first. But with some options like "permit" and "deny", multiple ones can be
listed, each of which is applied in order.


</p>
<p class="virtualpage3">
<b>Programmability</b><br />
Although the details are beyond the scope of this document, it is important to at least give
brief mention to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>'s programmability. In general, there are three levels at which to
program <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>. From highest to lowest, they are:<br />
<br />
Extension logic: Use the dialplan in extensions.conf to create simple applications,
authorizations, etc. For more on extension logic, see the above examples.<br />
<br />
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> Gateway Interface (AGI): For more sophisticated or complex tasks, AGI
presents a way for a user to write scripts in their language of choice (i.e. Perl, PHP, etc). An
AGI script is launched from the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> dialplan. It receives an environment setup on stdin,
then can issue commands to stdout and read the results from stdin. In this way, no special
bindings are required and any language that can write to stdout and read from stdin (i.e. all
languages) will work.<br />
<br />
C-level API: For hard core applications, channel drivers, file formats, etc there is the
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> C API, which gives full access to all the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> internals.<br />
<br />
<b>Extra Resources</b><br />
The information in this white paper is designed to provide an overview and introduction to
the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> system. However, it does leave out a great deal of detail. Fortunately, there are
many resources available to the user to assist in planning, configuring, and developing with
the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> PBX. Details on <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> commands and applications (including the format of
their parameters) can be found at the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> command line, by typing the following
commands:<br />
<br />

help(to show a list of commands)<br />
help <command> (to explain how to use a particular command)<br />
show applications (to show a list of <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> applications)<br />
show application <foo> (to explain the purpose and arguments for a particular app)<br />

<br />
Also some URL's are helpful in finding <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> documentation, news, and hardware:<br />
<br />
http://www.<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.org - The main <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> web site contains links to documentation
and architecture specifications<br />
<br />
http://www.linux-support.net - Linux Support Services, Inc. is the primary sponsor of
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> and has documentation, provides commercial support/development services related
to <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>, and also sells low-cost zaptel compatible <a href="../../a1/VoIP-Network-platform.asp">telephony</a> hardware.<br />
<br />
http://<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>.drunkencoder.com - An independently maintained <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> FAQ<br />
<br />
http://www.markocam.com - My web cam, where you can often watch me code on
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> in real-time.<br />
<br />
Those interested in <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a> or its development may also wish to sign up on the <a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>
mailing list, asterisk@marko.net by sending the word "subscribe" in the body of a message to
<a href="http://www.asterisk-billing.net" target="_blank">Asterisk </a>-request@marko.net.
</p>
</div></td>
  </tr>
</table>
<script type="text/javascript">
var whatsnew=new virtualpaginate("virtualpage3", 1, "p") //Let script know you're using "p" tags as separator (instead of default "div")
whatsnew.buildpagination("listingpaginate")
</script></td>
          </tr>
          <tr>
            <td scope="col"></td>
          </tr>
      </table>
   <table width="776" border="0" cellspacing="0">
     <tr>
       <td height="132" scope="col"><table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
         <tr>
           <td width="255" align="left" valign="top" style="background-image: url(/imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
               <p align="right"><a href="../pdf/Introduction_to_Asterisk.pdf" target="_blank"><img src="/imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a> <a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a> <a href="../doc/Introduction_to_Asterisk.doc" target="_blank"><img src="/imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a>&nbsp;<a href="html/Introduction_to_the_Asterisk_Open.asp"></a>&nbsp;</p></td>
         </tr>
       </table></td>
     </tr>
   </table></td>
</tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--#include file="../../includes/footer.asp" -->
</body>
</html>
