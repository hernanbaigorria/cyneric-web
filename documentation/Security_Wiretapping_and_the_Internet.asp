<%@  language="VBSCRIPT" codepage="1252" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>VoIP Billing Documentation</title>
    <link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
    <script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
    <link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="simpletreemenu.js">

        /***********************************************
        * Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
        * This notice MUST stay intact for legal use
        * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
        ***********************************************/

    </script>
    <script type="text/javascript" src="../includes/chromejs/chrome.js"></script>
    <script>
        function detenerError() {
            return true
        }
        window.onerror = detenerError
    </script>
    <link rel="stylesheet" type="text/css" href="simpletree.css" />
</head>
<body onselectstart="return false">
    <table width="900" border="0" align="left" cellpadding="0" cellspacing="0" id="tope">
        <tr>
            <td width="777" height="79" align="right" valign="middle" background="imagenes/int-tope-fondo.gif">
                <h1>
                    Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1>
            </td>
        </tr>
    </table>
    <table width="900" border="0" align="left" cellpadding="0" cellspacing="0" class="ventral"
        id="menuhome">
        <tr>
            <td width="221" align="left" valign="top">
                <table width="201" border="0" align="left" cellspacing="0">
                    <tr>
                        <td width="199" scope="col">
                            <h1>
                                <span style="margin-bottom: 12px">Whitepapers Library</span></h1>
                            <div style="visibility: hidden; display: none">
                                <a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Expand All</a> |
                                <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Contact All</a></div>
                            <ul id="treemenu1" class="treeview">
                                <li><a href="asterisk-osp.asp">Asterisk OSP</a></li>
                                <li>VoIP Telephony
                                    <ul>
                                        <li><a href="3comsolutions.asp">Strategies for successful IP </a></li>
                                        <li><a href="IP_telephony_deployments.asp">The Five Considerations </a></li>
                                        <li><a href="increasing_business_value_with_voip.asp">Increasing business </a></li>
                                    </ul>
                                </li>
                                <li><a href="VoIP_addressing_QoS.asp">VoIP networking</a></li>
                                <li><a href="asterisk.asp">Asterisk Open Source</a></li>
                                <li><a href="quintum.asp">Life on the Edge</a></li>
                                <li><a href="Open_Source_Telephony.asp">Open Source Telephony</a></li>
                                <li><a href="Strategies_for_Migrating_Corporate.asp">Risks and Rewards</a></li>
                                <li><a href="Asterisk_billing.asp">Asterisk Billing</a>
                                    <ul>
                                        <li><a href="html/Pre-paid-ANI-recognition-PIN.doc.asp">Pre paid ANI </a></li>
                                        <li><a href="html/Post_paid_ANI_customers_with_credit_limit.asp">Post paid ANI</a></li>
                                        <li><a href="html/Calling_Card_Applications.asp">Calling Card </a></li>
                                        <li><a href="html/Call_Shop_Applications.asp">Call Shop </a></li>
                                        <li><a href="html/Web_Callback.asp">Web Callback</a></li>
                                        <li><a href="html/SMS_Callback.asp">SMS Callback </a></li>
                                        <li><a href="html/DID_forwarding_Follow_me.asp">DID forwarding </a></li>
                                        <li><a href="html/Web_Voicemail.asp">Web Voicemail </a></li>
                                        <li><a href="html/Automatic_Routing_LCR.asp">Automatic Routing </a></li>
                                        <li><a href="html/Automatic_Telephone_payment.asp">Automatic Telephone </a></li>
                                    </ul>
                                </li>
                                <li><a href="A_Proactive_Approach_to_VoIP_Security.asp">Approach to VoIP Security</a></li>
                                <li><a href="Secure_Multi-lateral_Peering.asp">Secure, Multi-lateral Peering</a></li>
                                <li><a href="Security_Wiretapping_and_the_Internet.asp">Security, Wiretapping</a></li>
                                <li><a href="Cisco_2007_AnnualSecurity_Report.asp">Cisco, Anual Security Report</a></li>
                                <li><a href="Compare_Top_PBX_Vendors.asp">Compare Top PBX Vendors</a></li>
                                <li><a href="hosted-pbx-comparison.asp">Hosted PBX Comparison</a></li>
                                <li>SIP
                                    <ul>
                                        <li><a href="sip.asp">What�s SIP got to do with it?</a></li>
                                        <li><a href="SIP_Security_and_the_IMS_Core.asp">SIP Security</a></li>
                                        <li><a href="SIP_Security_and_Session_Controllers.asp">SIP Security and Session </a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="SER_GettingStarted.asp">SER Getting Started</a>
                                    <ul>
                                        <li><a href="html/SER_GettingStarted.asp">Part 1</a></li>
                                        <li><a href="html/SER_GettingStarted-part2.asp">Part 2</a></li>
                                        <li><a href="html/SER_GettingStarted-part3.asp">Part 3</a></li>
                                        <li><a href="html/SER_GettingStarted-part4.asp">Part 4</a></li>
                                    </ul>
                                </li>
                                <li><a href="Telecom_Security_Threats.asp">Telecom Security Threats</a></li>
                                <li><a href="VoIP_Security.asp">VoIP Security</a></li>
                                <li><a href="What_is_a_Call_Center.asp">What Is a Call Center?</a></li>
                                <li><a href="VoIP_Bandwidth_Calculation.asp">VoIP Bandwidth Calculation</a></li>
                                <li><a href="The_Value_of_Open_Source_Software.asp">The Value of Open Source</a></li>
                                <li><a href="Understand_security_in_VoIP_networks.asp">Enhancing VoIP</a></li>
                            </ul>
                            <h4>
                                <script type="text/javascript">

                                    //ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

                                    ddtreemenu.createTree("treemenu1", true)
                                    ddtreemenu.createTree("treemenu2", false)

                                </script>
                            </h4>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
            <td width="556" align="center" valign="top">
                <table width="240" height="75" border="0" cellpadding="8" cellspacing="4" style="display: none;
                    visibility: hidden">
                    <tr>
                        <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif);
                            background-repeat: no-repeat; background-position: bottom">
                            <h1>
                                Download Information</h1>
                            <p align="right">
                                <a href="VOIP-Cyneric_en.pps" target="_blank"></a><a href="i-cr2.pdf" target="_blank">
                                    <img src="../imagenes/ico-d-pdf.gif" width="48" height="60" hspace="3" vspace="9"
                                        border="0" /></a></p>
                        </td>
                    </tr>
                </table>
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top">
                            <h2>
                                Security, Wiretapping,and the Internet</h2>
                            <div id="divfrase">
                            </div>
                            Susan Landau Sun Microsystems<br />
                            <br />
                            <h1>
                                Introduction</h1>
                            <p>
                                Wiretaps have been used since the invention of the telegraph and have been a legal
                                element of the US law enforcement arsenal for more than a quarter century. In keeping
                                with law enforcement�s efforts to keep laws current with changing technologies,
                                in 1994 the US Congress passed the Communications Assistance for Law Enforcement
                                Act (CALEA). The law proved to be controversial because it mandated that digitally
                                switched telephone<a href="../a1/VoIP-Network-platform.asp"> networks</a> must be
                                built wiretap enabled, with the US Department of Justice in charge of determining
                                the appropriate technology standards.
                            </p>
                            <div align="left">
                                <ul>
                                    The law provided a specific exclusion for �information services.� Despite that explicit
                                    exemption, in response to a request from the US Federal Bureau of Investigation
                                    (FBI), in August 2005, the Federal Communications Commission (FCC) ruled that broadband
                                    voice over IP (VoIP) must comply with CALEA. Civilliberties groups and the industry
                                    immediately objected, fearing the ruling�s impact on privacy and innovation. There
                                    is another community that should be very concerned. Applying CALEA to VoIP requires
                                    embedding surveillance technology deeply into the protocol stack. The FCC ruling
                                    undermines <a href="../a1/VoIP-Network-platform.asp">network</a> security and, because
                                    the Internet and private <a href="../a1/VoIP-Network-platform.asp">networks</a>
                                    using Internet protocols support critical as well as noncritical infrastructure,
                                    national security as well. The FCC ruling is a step backward in securing the Internet,
                                    a national� and international�priority. </li>
                                </ul>
                                <p align="center">
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                </p>
                            </div>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <a href="pdf/Introduction_to_Asterisk.pdf"></a>
                <table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
                    <tr>
                        <td width="255" align="left" valign="top" style="background-image: url(imagenes/fondo-celda-descarga2.gif);
                            background-repeat: no-repeat; background-position: bottom">
                            <h1>
                                &nbsp;Download Information</h1>
                            <p align="right">
                                <a href="html/Security_Wiretapping_and_the_Internet.asp">
                                    <img src="imagenes/ico-d-htm.gif" hspace="0" vspace="9" border="0" /></a><a href="pdf/Internet_Security_and_Wiretapping.pdf"
                                        target="_blank"><img src="imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0"
                                            vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a
                                                href="doc/Internet_Security_and_Wiretapping.doc" target="_blank"><img src="imagenes/ico-d-word.gif"
                                                    width="48" height="57" hspace="0" vspace="9" border="0" /></a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
