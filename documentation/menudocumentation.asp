<table width="201" border="0" align="center" cellspacing="0">
      <tr>
        <td width="199" scope="col"><h1><span style="margin-bottom:12px">Whitepapers Library</span></h1>
      <div style="visibility:hidden; display:none"><a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Expand All</a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Contact All</a></div>

<ul id="treemenu1" class="treeview">

<li><a href="/documentation/asterisk-osp.asp">Asterisk OSP</a></li>

<li>VoIP Telephony
	<ul>
	<li><a href="/documentation/3comsolutions.asp">Strategies for successful IP </a></li>
	<li><a href="/documentation/IP_telephony_deployments.asp">The Five Considerations </a></li>
	<li><a href="/documentation/increasing_business_value_with_voip.asp">Increasing business </a></li>
	</ul>
</li>
<li><a href="/documentation/VoIP_addressing_QoS.asp">VoIP networking</a></li>

	<li><a href="/documentation/asterisk.asp">Asterisk Open Source</a></li>
	<li><a href="/documentation/quintum.asp">Life on the Edge</a></li>
		<li><a href="/documentation/Open_Source_Telephony.asp">Open Source Telephony</a></li>
			<li><a href="/documentation/Strategies_for_Migrating_Corporate.asp">Risks and Rewards</a></li>
			<li><a href="/documentation/Asterisk_billing.asp">Asterisk Billing</a>
	<ul>
	<li><a href="/documentation/html/Pre-paid-ANI-recognition-PIN.doc.asp">Pre paid ANI </a></li>
	<li><a href="/documentation/html/Post_paid_ANI_customers_with_credit_limit.asp">Post paid ANI</a></li>
	<li><a href="/documentation/html/Calling_Card_Applications.asp">Calling Card </a></li>
		<li><a href="/documentation/html/Call_Shop_Applications.asp"> Call Shop </a></li>
			<li><a href="/documentation/html/Web_Callback.asp">Web Callback</a></li>
				<li><a href="/documentation/html/SMS_Callback.asp">SMS Callback </a></li>
				<li><a href="/documentation/html/DID_forwarding_Follow_me.asp">DID forwarding </a></li>
					<li><a href="/documentation/html/Web_Voicemail.asp">Web Voicemail </a></li>
						<li><a href="/documentation/html/Automatic_Routing_LCR.asp">Automatic Routing  </a></li>
							<li><a href="/documentation/html/Automatic_Telephone_payment.asp">Automatic Telephone </a></li>
                                                                                                                                                                                                                                                  
	</ul>
</li>
	<li><a href="/documentation/A_Proactive_Approach_to_VoIP_Security.asp">Approach to VoIP Security</a></li>
	<li><a href="/documentation/Secure_Multi-lateral_Peering.asp">Secure, Multi-lateral Peering</a></li>
		
	<li><a href="/documentation/Security_Wiretapping_and_the_Internet.asp">Security, Wiretapping</a></li>


<li><a href="/documentation/Cisco_2007_AnnualSecurity_Report.asp">Cisco, Anual Security Report</a></li>
<li><a href="/documentation/Compare_Top_PBX_Vendors.asp">Compare Top PBX Vendors</a></li>
<li><a href="/documentation/hosted-pbx-comparison.asp">Hosted PBX Comparison</a></li>
<li>SIP
	<ul>
	<li><a href="/documentation/sip.asp">What�s SIP got to do with it?</a></li>
<li><a href="/documentation/SIP_Security_and_the_IMS_Core.asp">SIP Security</a></li>
<li><a href="/documentation/SIP_Security_and_Session_Controllers.asp">SIP Security and Session </a></li>
	</ul>
</li>
<li><a href="/documentation/SER_GettingStarted.asp">SER Getting Started</a> 
	<ul>
	<li><a href="/documentation/html/SER_GettingStarted.asp">Part 1</a></li>
<li><a href="/documentation/html/SER_GettingStarted-part2.asp">Part 2</a></li>
<li><a href="/documentation/html/SER_GettingStarted-part3.asp">Part 3</a></li>
<li><a href="/documentation/html/SER_GettingStarted-part4.asp">Part 4</a></li>
	</ul>
</li>

<li><a href="/documentation/Telecom_Security_Threats.asp">Telecom Security Threats</a></li>
<li><a href="/documentation/VoIP_Security.asp">VoIP Security</a></li>
<li><a href="/documentation/What_is_a_Call_Center.asp">What Is a Call Center?</a></li>
<li><a href="/documentation/VoIP_Bandwidth_Calculation.asp">VoIP Bandwidth Calculation</a></li>
<li><a href="/documentation/The_Value_of_Open_Source_Software.asp">The Value of Open Source</a></li>
<li><a href="/documentation/Understand_security_in_VoIP_networks.asp">Enhancing VoIP</a></li>
		  </ul>
		  
		  
<h4>
  <script type="text/javascript">

//ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

ddtreemenu.createTree("treemenu1", true)
ddtreemenu.createTree("treemenu2", false)

  </script>
</h4></td>
      </tr>
    </table>