<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>VoIP Billing Documentation</title>
<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script src="../SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="simpletreemenu.js">

/***********************************************
* Simple Tree Menu- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="../includes/chromejs/chrome.js"></script>
<script>
function detenerError(){
return true
}
window.onerror=detenerError
</script>
<link rel="stylesheet" type="text/css" href="simpletree.css" />


</head>
<body onselectstart="return false">


<table width="100" border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho">
  <tr>
    <td align="left" valign="top"><!--#include file="../includes/menusuperior-contenidos.asp" --></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" id="tope">
  <tr>
    <td width="777" height="79" align="right" valign="middle" background="../imagenes/int-tope-fondo.gif"><h1>Free VoIP Documentation Area &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h1></td>
  </tr>
  <tr>
    <td height="66" align="left" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
            <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
            <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
            <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
            <!--<li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
    
            <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area </a></li>
            <li><a href="../a1/casos-de-exito.asp">Our Clients</a></li>        <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your VoIP Solution Partner
          <!--1st drop down menu -->
            <div id="dropmenu1" class="dropmenudiv"> <a href="../a1/VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution </a> <a href="../a1/VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a><a href="../a1/VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a>	 </div>
          <!--2nd drop down menu -->
            <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="../a1/productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="../a1/productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="../a1/productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="../a1/productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="../a1/VoIP-Network-Real-Time-alert-monitoring-cr2.asp" onfocus="if(this.blur)this.blur()">Cyner CR2</a>
			<a href="../a1/VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a>
			 <a href="../a1/VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
          <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="777" border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" ID="menuhome">
  <tr>
        <td width="221" align="left" valign="top"><br />
        <!--#include virtual="/includes/menudocumentation.asp" -->


        <br /></td><td width="556" align="center" valign="top">
      <table width="240" height="75" border="0" cellpadding="8" cellspacing="4" style="display:none; visibility:hidden">
              <tr>
                <td width="222" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>Download Information</h1>
                    <p align="right"><a href="VOIP-Cyneric_en.pps" target="_blank"></a><a href="i-cr2.pdf" target="_blank"><img src="../imagenes/ico-d-pdf.gif" width="48" height="60" hspace="3" vspace="9" border="0" /></a></p></td>
              </tr>
      </table>
      
            <table width="100%" border="0" cellspacing="0" cellpadding="11">
              <tr>
                <td height="371" align="left" valign="top"><h2>Comprehensive VoIP Security for the Enterprise: Not Just Encryption and Authentication</h2>
      
                  <div id="divfrase"></div>
       Roger Warner, Director, Squiz<br />
                      <br />
                      <br />
                
                                 <p>As enterprises and operators role out real-time Internet Protocol (IP) communications applications such as Voice-over IP (VoIP), instant messaging (IM), video and multimedia, the need to protect end-users and <a href="../a1/VoIP-Network-platform.asp">network</a> infrastructures from multiple catastrophic attacks, misuse, and abuse of session-based protocols is
becoming imperative.<br />
<br />

At the same time, the encryption and authentication that many advertise as VoIP security only scratches the surface of the required protection. In fact, there are many VoIP-specific vulnerabilities that have been discovered, along with thousands of threats that can be launched against SIP/UMA/IMS <a href="../a1/VoIP-Network-platform.asp">networks</a>, that encryption and authentication alone do not address.<br />
<br />
This white paper will look at a number of these threats that target the enterprise <a href="../a1/VoIP-Network-platform.asp">network</a> and users including reconnaissance, Denial of Service (DoS)/Distributed Denial of Service (DDoS), Stealth DoS/DDoS, Spoofing and VoIP spam in order to explore the unique methods and techniques to protect VoIP infrastructure as well as
end users from threats that endanger the continued exchange of time-critical, business-sensitive information.                  </p>
                                 
                </td>
              </tr>
            </table>
            <table width="279" height="75" border="0" align="right" cellpadding="8" cellspacing="4">
              <tr>
                <td width="255" align="left" valign="top" style="background-image: url(../imagenes/fondo-celda-descarga2.gif); background-repeat:no-repeat; background-position:bottom"><h1>&nbsp;Download Information</h1>
                <p align="right"><a href="html/Sipera_Enterprise_VoIP_Security_WP.asp"><img src="../imagenes/ico-d-htm.gif" hspace="0" vspace="9" border="0" /></a><a href="pdf/Sipera_Enterprise_VoIP_Security_WP.pdf" target="_blank"><img src="../imagenes/ico-d-pdf.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment.pdf"></a><a href="doc/Sipera_Enterprise_VoIP_Security_WP.doc" target="_blank"><img src="../imagenes/ico-d-word.gif" width="48" height="57" hspace="0" vspace="9" border="0" /></a></p></td>
              </tr>
          </table>
            <p align="right"><a href="pdf/Introduction_to_Asterisk.pdf"></a></p>            <p align="right"><a href="pdf/3Com_Strategies_for_Successful_IP_Telephony_Deployment%20.pdf"></a></p>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="top"><!--#include file="../includes/footer.asp" --></td>
  </tr>
</table>
</body>
</html>
