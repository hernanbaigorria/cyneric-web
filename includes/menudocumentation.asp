<table width="201" border="0" align="center" cellspacing="0">
    <tr>
        <td width="199" scope="col">
            <h1>
                <span style="margin-bottom: 12px">Whitepapers Library</span></h1>
            <div style="visibility: hidden; display: none">
                <a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Expand All</a> |
                <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Contact All</a></div>
            <ul id="treemenu1" class="treeview">
                <li><a href="asterisk-osp.asp">Asterisk OSP</a></li>
                <li>VoIP Telephony
                    <ul>
                        <li><a href="3comsolutions.asp">Strategies for successful IP </a></li>
                        <li><a href="IP_telephony_deployments.asp">The Five Considerations </a></li>
                        <li><a href="increasing_business_value_with_voip.asp">Increasing business </a></li>
                    </ul>
                </li>
                <li><a href="VoIP_addressing_QoS.asp">VoIP networking</a></li>
                <li><a href="asterisk.asp">Asterisk Open Source</a></li>
                <li><a href="quintum.asp">Life on the Edge</a></li>
                <li><a href="Open_Source_Telephony.asp">Open Source Telephony</a></li>
                <li><a href="Strategies_for_Migrating_Corporate.asp">Risks and Rewards</a></li>
                <li><a href="Asterisk_billing.asp">Asterisk Billing</a>
                    <ul>
                        <li><a href="html/Pre-paid-ANI-recognition-PIN.doc.asp">Pre paid ANI </a></li>
                        <li><a href="html/Post_paid_ANI_customers_with_credit_limit.asp">Post paid ANI</a></li>
                        <li><a href="html/Calling_Card_Applications.asp">Calling Card </a></li>
                        <li><a href="html/Call_Shop_Applications.asp">Call Shop </a></li>
                        <li><a href="html/Web_Callback.asp">Web Callback</a></li>
                        <li><a href="html/SMS_Callback.asp">SMS Callback </a></li>
                        <li><a href="html/DID_forwarding_Follow_me.asp">DID forwarding </a></li>
                        <li><a href="html/Web_Voicemail.asp">Web Voicemail </a></li>
                        <li><a href="html/Automatic_Routing_LCR.asp">Automatic Routing </a></li>
                        <li><a href="html/Automatic_Telephone_payment.asp">Automatic Telephone </a></li>
                    </ul>
                </li>
                <li><a href="A_Proactive_Approach_to_VoIP_Security.asp">Approach to VoIP Security</a></li>
                <li><a href="Secure_Multi-lateral_Peering.asp">Secure, Multi-lateral Peering</a></li>
                <li><a href="Security_Wiretapping_and_the_Internet.asp">Security, Wiretapping</a></li>
                <li><a href="Cisco_2007_AnnualSecurity_Report.asp">Cisco, Anual Security Report</a></li>
                <li><a href="Compare_Top_PBX_Vendors.asp">Compare Top PBX Vendors</a></li>
                <li><a href="hosted-pbx-comparison.asp">Hosted PBX Comparison</a></li>
                <li>SIP
                    <ul>
                        <li><a href="sip.asp">What�s SIP got to do with it?</a></li>
                        <li><a href="SIP_Security_and_the_IMS_Core.asp">SIP Security</a></li>
                        <li><a href="SIP_Security_and_Session_Controllers.asp">SIP Security and Session </a>
                        </li>
                    </ul>
                </li>
                <li><a href="SER_GettingStarted.asp">SER Getting Started</a>
                    <ul>
                        <li><a href="html/SER_GettingStarted.asp">Part 1</a></li>
                        <li><a href="html/SER_GettingStarted-part2.asp">Part 2</a></li>
                        <li><a href="html/SER_GettingStarted-part3.asp">Part 3</a></li>
                        <li><a href="html/SER_GettingStarted-part4.asp">Part 4</a></li>
                    </ul>
                </li>
                <li><a href="Telecom_Security_Threats.asp">Telecom Security Threats</a></li>
                <li><a href="VoIP_Security.asp">VoIP Security</a></li>
                <li><a href="What_is_a_Call_Center.asp">What Is a Call Center?</a></li>
                <li><a href="VoIP_Bandwidth_Calculation.asp">VoIP Bandwidth Calculation</a></li>
                <li><a href="The_Value_of_Open_Source_Software.asp">The Value of Open Source</a></li>
                <li><a href="Understand_security_in_VoIP_networks.asp">Enhancing VoIP</a></li>
            </ul>
            <h4>
                <script type="text/javascript">

                    //ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

                    ddtreemenu.createTree("treemenu1", true)
                    ddtreemenu.createTree("treemenu2", false)

                </script>
            </h4>
        </td>
    </tr>
</table>


<table width="201" border="0" align="center" cellspacing="0">
    <tr>
        <td width="199" scope="col">
            <h1>
                <span style="margin-bottom: 12px">Whitepapers Library</span></h1>
            <div style="visibility: hidden; display: none">
                <a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')">Expand All</a> |
                <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">Contact All</a></div>
            <ul id="Ul1" class="treeview">
                <li><a href="../asterisk-osp.asp">Asterisk OSP</a></li>
                <li>VoIP Telephony
                    <ul>
                        <li><a href="../3comsolutions.asp">Strategies for successful IP </a></li>
                        <li><a href="../IP_telephony_deployments.asp">The Five Considerations </a></li>
                        <li><a href="../increasing_business_value_with_voip.asp">Increasing business </a>
                        </li>
                    </ul>
                </li>
                <li><a href="../VoIP_addressing_QoS.asp">VoIP networking</a></li>
                <li><a href="../asterisk.asp">Asterisk Open Source</a></li>
                <li><a href="../quintum.asp">Life on the Edge</a></li>
                <li><a href="../Open_Source_Telephony.asp">Open Source Telephony</a></li>
                <li><a href="../Strategies_for_Migrating_Corporate.asp">Risks and Rewards</a></li>
                <li><a href="../Asterisk_billing.asp">Asterisk Billing</a>
                    <ul>
                        <li><a href="../html/Pre-paid-ANI-recognition-PIN.doc.asp">Pre paid ANI </a></li>
                        <li><a href="../html/Post_paid_ANI_customers_with_credit_limit.asp">Post paid ANI</a></li>
                        <li><a href="../html/Calling_Card_Applications.asp">Calling Card </a></li>
                        <li><a href="../html/Call_Shop_Applications.asp">Call Shop </a></li>
                        <li><a href="../html/Web_Callback.asp">Web Callback</a></li>
                        <li><a href="../html/SMS_Callback.asp">SMS Callback </a></li>
                        <li><a href="../html/DID_forwarding_Follow_me.asp">DID forwarding </a></li>
                        <li><a href="../html/Web_Voicemail.asp">Web Voicemail </a></li>
                        <li><a href="../html/Automatic_Routing_LCR.asp">Automatic Routing </a></li>
                        <li><a href="../html/Automatic_Telephone_payment.asp">Automatic Telephone </a></li>
                    </ul>
                </li>
                <li><a href="../A_Proactive_Approach_to_VoIP_Security.asp">Approach to VoIP Security</a></li>
                <li><a href="../Secure_Multi-lateral_Peering.asp">Secure, Multi-lateral Peering</a></li>
                <li><a href="../Security_Wiretapping_and_the_Internet.asp">Security, Wiretapping</a></li>
                <li><a href="../Cisco_2007_AnnualSecurity_Report.asp">Cisco, Anual Security Report</a></li>
                <li><a href="../Compare_Top_PBX_Vendors.asp">Compare Top PBX Vendors</a></li>
                <li><a href="../hosted-pbx-comparison.asp">Hosted PBX Comparison</a></li>
                <li>SIP
                    <ul>
                        <li><a href="../sip.asp">What�s SIP got to do with it?</a></li>
                        <li><a href="SIP_Security_and_the_IMS_Core.asp">SIP Security</a></li>
                        <li><a href="SIP_Security_and_Session_Controllers.asp">SIP Security and Session </a>
                        </li>
                    </ul>
                </li>
                <li><a href="SER_GettingStarted.asp">SER Getting Started</a>
                    <ul>
                        <li><a href="html/SER_GettingStarted.asp">Part 1</a></li>
                        <li><a href="html/SER_GettingStarted-part2.asp">Part 2</a></li>
                        <li><a href="html/SER_GettingStarted-part3.asp">Part 3</a></li>
                        <li><a href="html/SER_GettingStarted-part4.asp">Part 4</a></li>
                    </ul>
                </li>
                <li><a href="Telecom_Security_Threats.asp">Telecom Security Threats</a></li>
                <li><a href="VoIP_Security.asp">VoIP Security</a></li>
                <li><a href="What_is_a_Call_Center.asp">What Is a Call Center?</a></li>
                <li><a href="VoIP_Bandwidth_Calculation.asp">VoIP Bandwidth Calculation</a></li>
                <li><a href="The_Value_of_Open_Source_Software.asp">The Value of Open Source</a></li>
                <li><a href="Understand_security_in_VoIP_networks.asp">Enhancing VoIP</a></li>
            </ul>
            <h4>
                <script type="text/javascript">

                    //ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))

                    ddtreemenu.createTree("treemenu1", true)
                    ddtreemenu.createTree("treemenu2", false)

                </script>
            </h4>
        </td>
    </tr>
</table>


<video width="640" height="360" poster="__POSTER__.jpg" controls="controls">
    <source src="__VIDEO__.mp4" type="video/mp4" />
    <source src="__VIDEO__.webm" type="video/webm" />
    <source src="__VIDEO__.ogv" type="video/ogg" /><!--[if gt IE 6]>
    <object width="640" height="375" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B"><!
    [endif]--><!--[if !IE]><!-->
    <object width="640" height="375" type="video/quicktime" data="__VIDEO__.mp4"><!--<![endif]-->
    <param name="src" value="__VIDEO__.mp4" />
    <param name="autoplay" value="false" />
    <param name="showlogo" value="false" />
    <object width="640" height="380" type="application/x-shockwave-flash"
        data="__FLASH__.swf?image=__POSTER__.jpg&amp;file=__VIDEO__.mp4">
        <param name="movie" value="__FLASH__.swf?image=__POSTER__.jpg&amp;file=__VIDEO__.mp4" />
        <img src="__POSTER__.jpg" width="640" height="360" />
        <p>
            <strong>No video playback capabilities detected.</strong>
            Why not try to download the file instead?<br />
            <a href="__VIDEO__.mp4">MPEG4 / H.264 �.mp4� (Windows / Mac)</a> |
            <a href="__VIDEO__.ogv">Ogg Theora &amp; Vorbis �.ogv� (Linux)</a>
        </p>
    </object><!--[if gt IE 6]><!-->
    </object><!--<![endif]-->
</video>

<object id='mediaPlayer2' width="640" height="480" 
      classid='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' 
      codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701'
      standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>
      <param name='fileName' value="../images/IMG/Aion_2016.08.29.wmv">
      <param name='animationatStart' value='true'>
      <param name='transparentatStart' value='true'>
      <param name='autoStart' value="true">
      <param name='showControls' value="true">
      <param name='loop' value="true">
      <embed type='application/x-mplayer2'
        pluginspage='http://microsoft.com/windows/mediaplayer/en/download/'
        id='mediaPlayer3' name='mediaPlayer2' displaysize='4' autosize='-1' 
        bgcolor='darkblue' showcontrols="true" showtracker='-1' 
        showdisplay='0' showstatusbar='-1' videoborder3d='-1' width="640" height="480"
        src="../images/IMG/Aion_2016.08.29.wmv" autostart="true" designtimesp='5311' loop="true">
      </embed>
      </object>
