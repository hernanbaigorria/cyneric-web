<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--#include file="../includes/home-contenidos-random.asp" -->
<head>
<link rel="shortcut icon" href="/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!--#include file="../includes/fadeimagenhome.asp" -->
<link href="../includes/estilo.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../includes/chrometheme/chromestyle3.css" />
<script type="text/javascript" src="../includes/chromejs/chrome.js">
</script>
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--#include file="../includes/header.asp" -->
<title>Turnkey voip billing platform solutions and turnkey integration. Asterisk Cisco Mera SER Snom, QoS  quality of service</title>

</head>
<body>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho" id="menusup">
  <tr>
    <td align="center" valign="top"><!--#include file="../includes/menusuperior.asp" --></td>
  </tr>
</table>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho" id="contenedorsuperior">
  <tr>
    <td><!--#include file="../includes/homebanners.asp" --></td>
  </tr>
</table>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="bodyancho" id="contenedorsuperior">
  <tr>
    <td height="60" valign="top"><table width="777" height="60" border="0" cellpadding="0" cellspacing="0"  class="chromestyle" id="chromemenu">
      <tr>
        <td height="29" valign="bottom"><ul>
          <li><a href="../a1/home.asp" onfocus="if(this.blur)this.blur()">Home</a></li>
          <li><a href="#" rel="dropmenu1" onfocus="if(this.blur)this.blur()">Solutions</a></li>
          <li><a href="#" rel="dropmenu2" onfocus="if(this.blur)this.blur()">Products</a></li>
          <!--  <li><a href="../a1/cyneric-online-consultation.asp" target="_self" onfocus="if(this.blur)this.blur()">Online Request</a></li>-->
          <li><a href="http://upgrade.cyneric.com" target="_top" onfocus="if(this.blur)this.blur()">Member Area</a></li>
          <li><a href="casos-de-exito.asp">Our Clients</a></li>
          <li><a href="../a1/contact-cyneric.asp" onfocus="if(this.blur)this.blur()">Contact</a></li>
        </ul></td>
      </tr>
      <tr>
        <td align="left" valign="top" class="textoschicosnegro">&nbsp;&nbsp;Your voip billing solution provider          <!--1st drop down menu -->

              <div id="dropmenu1" class="dropmenudiv"> <a href="VoIP-Billing-Platform.asp" onfocus="if(this.blur)this.blur()">VoIP Billing solution</a> <a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()">VoIP Network solution</a> <a href="VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()">24/7 NOC Services</a> </div>
              <!--2nd drop down menu -->
              <div id="dropmenu2" class="dropmenudiv" style="width: 150px;"> <a href="productos-cdr.asp" onfocus="if(this.blur)this.blur()">Cyner CDR</a> <a href="productos-ua.asp" onfocus="if(this.blur)this.blur()">Cyner User Admin</a> <a href="productos-ram.asp" onfocus="if(this.blur)this.blur()">Cyner RAM</a> <a href="productos-route.asp" onfocus="if(this.blur)this.blur()">Cyner ROUTE</a> <a href="VoIP-Network-Real-Time-alert-monitoring-cr2.asp" target="_self" onfocus="if(this.blur)this.blur()">Cyner CR2</a> <a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Call Shop</a> <a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()">Check My Cisco </a> </div>
              <script type="text/javascript">
cssdropdown.startchrome("chromemenu")
      </script></td>
      </tr>
    </table></td>
  </tr>
</table>
<table border="0" align="center" cellpadding="0" cellspacing="0" class="ventral" id="menuhome">
  <tr >
    <td align="left" valign="top" class="ventral">
	<table width="777" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td width="33%" align="center" valign="top"><table width="240" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat">
          <tr>
            <td width="227" height="182" align="left" valign="top"><h1><%= Tcaja1 %></h1>
                    <p>What is your business model? </p>
                    <ul>
                      <li><a href="VoIP-Billing-Platform-WholeSaleVoip-Billing-Reporting.asp" target="_self" onfocus="if(this.blur)this.blur()">VoIP Wholesale, Terminator &amp; Originator</a>.</li>
                      <li><a href="VoIP-Billing-Platform-Retail-Voip-Billing.asp" onfocus="if(this.blur)this.blur()">Residential VoIP  Telephony</a>.</li>
                      <li><a href="VoIP-Billing-Platform-Calling-Card-Administration.asp" onfocus="if(this.blur)this.blur()">Calling Cards Administrator</a>.</li>
                      <li><a href="VoIP-Billing-Platform-Call-Shop.asp" onfocus="if(this.blur)this.blur()">Callshop, Cabin Booth Centers</a>.</li>
                  </ul></td>
          </tr>
        </table></td>
        <td width="33%" align="center" valign="top"><table width="245" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat">
          <tr>
            <td width="227" height="182" align="left" valign="top"><h1><%= Tcaja2 %></h1>
                    <p>Extensive traffic control and monitoring. Decrease the uncertainty in the quality of service  you provide.</p>
                    <ul>
                      <li> <a href="VoIP-Network-Real-Time-alert-monitoring.asp" onfocus="if(this.blur)this.blur()"> Real time traffic monitoring</a>.</li>
                      <li><a href="VoIP-Network-Real-Time-Cisco-monitoring.asp" onfocus="if(this.blur)this.blur()"> Cisco System monitoring &amp; alerts</a>.</li>
                      <li><a href="VoIP-Network-platform.asp" onfocus="if(this.blur)this.blur()"> Specialized technical support for<br />
                        &nbsp;&nbsp;&nbsp;                      &nbsp;Telephony&nbsp;Networks</a>.</li>
                      <li><a href="VoIP-NOC-Managment.asp" onfocus="if(this.blur)this.blur()"> 24/7 NOC Services</a>.</li>
                    </ul>
            </td>
          </tr>
        </table></td>
        <td width="33%" align="center" valign="top"><table width="240" height="182" border="0" cellpadding="9" cellspacing="0" id="tablaretail" style="background-image: url(../imagenes/fondo-celda.gif); background-repeat:no-repeat">
          <tr>
            <td width="227" height="182" align="left" valign="top"><h1> flexible &amp; smart billing integration</h1>
                    <p>
                      <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','219','height','114','hspace','3','vspace','4','src','../imagenes/marcas','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','wmode','transparent','movie','../imagenes/marcas' ); //end AC code
  </script>
                      <noscript>
                        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="219" height="114" hspace="3" vspace="4">
                        <param name="movie" value="../imagenes/marcas.swf" />
                        <param name="quality" value="high" />
                        <param name="wmode" value="transparent" />
                        <embed src="../imagenes/marcas.swf" width="219" height="114" hspace="3" vspace="4" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent"></embed>
                      </object>
                      </noscript>
                  </p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="4" colspan="3" align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="9">
          <tr>
            <td align="left" valign="top">Information  updated <%=fechadesde%><br />
                    <h3> Top Products &amp; Services: Improve your VoIP Business</h3>
              <%= strRandomTip %> </td>
            <td width="250" align="right"><!--#include file="../includes/es-ban-demo.asp" --></td>
          </tr>
        </table></td>
      </tr>
    </table>
	
        <p><img src="../imagenes/lineacorte.jpg" width="100%" height="2" vspace="0" /></p>
    </td>
  </tr>
</table>
<!--#include file="../includes/footer.asp" -->
</body>
</html>