<%
Dim res: Set res = Server.CreateObject("Scripting.Dictionary")
res("ASP_FAKERS_MISSING_FIELD") = "Fake Recordset Error: The field '%s' is not in the Fields Collection."
res("ASP_FAKERS_NO_RECORDS") = "Fake Recordset Error: No records for the current cursor position."
res("ASP_FAKERS_FIELD_EXISTS") = "Fake Recordset Error: You cannot append the same field twice."
res("ASP_FAKERS_NO_FIELDS") = "Fake Recordset Error: There are no fields."
%>