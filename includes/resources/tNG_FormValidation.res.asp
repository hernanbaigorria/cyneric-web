<%
Dim res: Set res = Server.CreateObject("Scripting.Dictionary")
res("FAILED") = "There were some errors in your form. Please make sure you filled in all required fields."
res("REQUIRED") = "The field is required."
res("TYPE") = "Please enter a valid type."
res("FORMAT") = "Please enter a valid %s."
res("TEXT_") = "Text"
res("TEXT_EMAIL") = "E-mail"
res("TEXT_CC_GENERIC") = "Credit Card Number"
res("TEXT_CC_VISA") = "Visa Credit Card Number"
res("TEXT_CC_MASTERCARD") = "Mastercard Credit Card Number"
res("TEXT_CC_AMERICANEXPRESS") = "American Express Credit Card Number"
res("TEXT_CC_DISCOVER") = "Discover Credit Card Number"
res("TEXT_CC_DINERSCLUB") = "Diners Club Credit Card Number"
res("TEXT_ZIP_GENERIC") = "ZIP Code"
res("TEXT_ZIP_US5") = "US ZIP Code (5 digits)"
res("TEXT_ZIP_US9") = "US ZIP Code (9 digits)"
res("TEXT_ZIP_CANADA") = "Canada Zip Code"
res("TEXT_ZIP_UK") = "UK Postcode"
res("TEXT_PHONE") = "Phone Number"
res("TEXT_SSN") = "Social Security Number"
res("TEXT_URL") = "URL"
res("TEXT_IP") = "IP Number"
res("TEXT_COLOR_HEX") = "Color (Hexadecimal format)"
res("TEXT_COLOR_GENERIC") = "Color Name (Web format)"
res("NUMERIC_") = "Number"
res("NUMERIC_INT") = "Integer"
res("NUMERIC_INT_POSITIVE") = "Positive Integer"
res("DOUBLE_") = "Float"
res("DOUBLE_FLOAT") = "Float"
res("DOUBLE_FLOAT_POSITIVE") = "Positive Float"
res("DATE_") = "Date"
res("DATE_DATE") = "Date"
res("DATE_TIME") = "Time"
res("DATE_DATETIME") = "Datetime"
res("MASK_") = "value"
res("REGEXP_") = "value"
res("REGEXP_FAILED") = "regular expression"
res("TEXT_MIN") = "Please enter at least %s characters."
res("TEXT_MAX") = "Please enter no more than %s characters."
res("TEXT_BETWEEN") = "Please enter between %s and %s characters."
res("OTHER_MIN") = "Please enter a value greater than %s."
res("OTHER_MAX") = "Please enter a value less than %s."
res("OTHER_BETWEEN") = "Please enter a value between %s and %s."
res("FORM_WAS_MODIFIED") = "You made some changes to the form. If you proceed, all changes will be lost."
%>