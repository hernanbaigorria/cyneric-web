<%
'
'	Copyright (c) InterAKT Online 2000-2005
'
Class KT_File
	Private userErrorMessage		'array		-	error message to be displayed as User Error
	Private develErrorMessage		'array		-	error message to be displayed as Developer Error
	
	Private fso
	Public inoutEncoding
	Private defaultEncoding
	
	Private Sub Class_Initialize()
		userErrorMessage	= Array()
		develErrorMessage	= Array()
		Set fso = Server.CreateObject("Scripting.FileSystemObject")
		
		defaultEncoding = "windows-1252"
		inoutEncoding = defaultEncoding
	End Sub

	Private Sub Class_terminate()
		Set fso = nothing
	End Sub

	Public Sub setEncoding(encoding)
		inoutEncoding = encoding
	End Sub
	
	Public Sub resetEncoding
		inoutEncoding = defaultEncoding
	End Sub	
	
	Public Function readFile(filePath)
		checkFolder filePath, "read", "Read content"
		If hasError() Then
			readFile = ""
			Exit Function
		End If
		
		
		Dim strContents: strContents = ""
		On Error Resume Next

		Set streamFile = Server.CreateObject("ADODB.Stream")
		streamFile.Type = 2
		If inoutEncoding <> "" Then
			streamFile.Charset = inoutEncoding
		End If	
		streamFile.Open
		streamFile.LoadFromFile filePath
		strContents = streamFile.readText(-1)
		streamFile.Close
		Set streamFile = nothing		

		If err.Number <>0 Then
			On Error GoTo 0
			setError "ASP_FILE_READ_ERROR", Array(), Array(filePath)
		End If
		On Error GoTo 0		
		
		readFile = strContents
     End Function	
	
	
	Public Sub writeFile(filePath, mode, content)
		checkFolder filePath, "write", "Write content"
		If hasError() Then
			Exit Sub
		End If
		
		Dim write_content: write_content = content
		
		On Error Resume Next
		Set streamFile = Server.CreateObject("ADODB.Stream")
		streamFile.Type = 2
		If inoutEncoding <> "" Then
			streamFile.Charset = inoutEncoding
		End If	
		streamFile.Open
		If Lcase(mode) = "append" Or Lcase(mode) = "prepend"  Then
			' read the content of the file
			streamFile.LoadFromFile filePath
			err.clear
			strContents = streamFile.readText(-1)
			streamFile.Close
			streamFile.Open
			If Lcase(mode) = "append" Then
				write_content = strContents  & 	content
			Else
				write_content = content  & strContents
			End If
		End If
		streamFile.WriteText write_content
		streamFile.SaveToFile filePath, 2
		streamFile.Close
		Set streamFile = nothing
		
		If err.Number <>0 Then
			On Error GoTo 0
			setError "ASP_FILE_WRITE_ERROR", Array(), Array(filePath)
		End If
		On Error GoTo 0	
	End Sub
	
	
	Public Sub createFile(filePath)
		checkFolder filePath, "write", "Create file"
		If hasError() Then
			Exit Sub
		End If
		
		Dim f
		Dim write_content: write_content = content
		
		On Error Resume Next
		Set f = fso.OpenTextFile(filePath, 2, True)
		If err.Number <>0 Then
			setError "ASP_FILE_CREATE_ERROR", Array(), Array(filePath)
		End If
		f.close()
		Set f = nothing
		On Error GoTo 0	
	End Sub	


	
	Public Sub renameFile(filePath, newFilePath)
		checkFolder filePath, "write", "Rename File"
		If hasError() Then
			Exit Sub
		End If
		checkFolder newFilePath, "write", "Rename File"
		If hasError() Then
			Exit Sub
		End If
		If Not fso.FileExists(filePath) Then
			setError "ASP_FILE_RENAME_NO_FILE", Array(), Array(filePath)
			Exit Sub
		End If
		If fso.FileExists(newFilePath) Then
			setError "ASP_FILE_RENAME_EXISTS", Array(), Array(newFilePath)
			Exit Sub
		End If
		On Error Resume Next
		fso.MoveFile filePath, newFilePath
		If err.Number <> 0 Then
			error = err.Description
			setError "ASP_FILE_RENAME", Array(), Array(filePath, newFilePath, error)
		End If
		On Error GoTo 0
	End Sub
	
	Public Sub copyFile(filePath, newFilePath)
		checkFolder filePath, "write", "Copy File"
		If hasError() Then
			Exit Sub
		End If
		checkFolder newFilePath, "write", "Copy File"
		If hasError() Then
			Exit Sub
		End If

		If Not fso.FileExists(filePath) Then
			setError "ASP_FILE_COPY_NO_FILE", Array(), Array(filePath)
			Exit Sub
		End If
		If fso.FileExists(newFilePath) Then
			setError "ASP_FILE_COPY_EXISTS", Array(), Array(newFilePath)
			Exit Sub
		End If
		
		On Error Resume Next
		fso.CopyFile filePath, newFilePath, False
		If err.Number <> 0 Then
			setError "ASP_FILE_COPY", Array(), Array(filePath, newFilePath, error)
		End If
		On Error GoTo 0		
	End Sub	
	
	Public Sub deleteFile(filePath)
		checkFolder filePath, "write", "Delete File"
		If hasError() Then
			Exit Sub
		End If
		
		On Error Resume Next
		fso.DeleteFile filePath
		If err.Number <> 0 Then
			setError "ASP_FILE_DELETE_ERROR", Array(), Array(filePath)
		End If
		On Error GoTo 0		
	End Sub	
	
	Public Sub checkFolder(filePath, mode, from)
	 	Dim folderName: folderName = getFolder(filePath)
		Dim folder: Set folder = new KT_Folder
		folder.createFolder folderName
		If folder.hasError() Then
			errors = folder.getError()
			setError "ASP_FILE_FOLDER_ERROR", Array(from, errors(0)), Array(from, errors(1))
		End If
		' do not check read / write for folder because it's not the case
	End Sub
	
    Public Function getFolder(filePath)
		getFolder = fso.GetParentFolderName(fso.GetAbsolutePathName(filePath))
	End Function
				
			
	Private Sub setError (errorCode, arrArgsUsr, arrArgsDev)
		errorCodeDev = errorCode
		If Not KT_in_array(errorCodeDev, array("", "%s"), false) Then
			errorCodeDev = errorCodeDev & "_D"
		End If
		If errorCode <> "" Then
			userErrorMessage = KT_array_push (userErrorMessage, KT_getResource(errorCode, "File", arrArgsUsr))
		Else
			userErrorMessage = array()
		End If
		
		If errorCodeDev <> "" Then
			develErrorMessage = KT_array_push (develErrorMessage, KT_getResource(errorCodeDev, "File", arrArgsDev))
		Else
			develErrorMessage = array()
		End If			
	End Sub

	Public Function hasError
		If ubound(userErrorMessage) > -1 Then
			hasError = True
		Else
			hasError = False	
		End If
	End Function
	

	Public Function getError
		getError = Array ( join(userErrorMessage,"<br />"), join (develErrorMessage, "<br />"))
	End Function

	Public Function clearError
		userErrorMessage = Array()
		develErrorMessage = Array()
	End Function
End Class
%>