<%
' Array definitions
  Set tNG_login_config = Server.CreateObject("Scripting.Dictionary")
  Set tNG_login_config_session = Server.CreateObject("Scripting.Dictionary")
  Set tNG_login_config_redirect_success = Server.CreateObject("Scripting.Dictionary")
  Set tNG_login_config_redirect_failed = Server.CreateObject("Scripting.Dictionary")

' Start Variable definitions
  tNG_debug_mode = "DEVELOPMENT"
  tNG_debug_log_type = ""
  tNG_debug_email_to = "you@yoursite.com"
  tNG_debug_email_subject = "(BUG) The site went down"
  tNG_debug_email_from = "webserver@yoursite.com"
  tNG_email_host = ""
  tNG_email_user = ""
  tNG_email_port = "25"
  tNG_email_password = ""
  tNG_email_defaultFrom = "nobody@nobody.com"
  tNG_login_config("connection") = "mahendra"
  tNG_login_config("table") = "usuarios"
  tNG_login_config("pk_field") = "id"
  tNG_login_config("pk_type") = "NUMERIC_TYPE"
  tNG_login_config("email_field") = "email"
  tNG_login_config("user_field") = "user"
  tNG_login_config("password_field") = "pass"
  tNG_login_config("level_field") = "acceso"
  tNG_login_config("level_type") = "NUMERIC_TYPE"
  tNG_login_config("randomkey_field") = ""
  tNG_login_config("activation_field") = "activo"
  tNG_login_config("password_encrypt") = "false"
  tNG_login_config("autologin_expires") = "30"
  tNG_login_config("redirect_failed") = "admin/index.asp"
  tNG_login_config("redirect_success") = "admin/consoila.asp"
  tNG_login_config("login_page") = "admin/index.asp"
  tNG_login_config_redirect_success("1") = "admin/consoila.asp"
  tNG_login_config_redirect_failed("1") = "admin/index.asp"
  tNG_login_config_session("kt_login_id") = "id"
  tNG_login_config_session("kt_login_user") = "user"
  tNG_login_config_session("kt_login_level") = "acceso"
  tNG_login_config_session("kt_nombre") = "nombre"
' End Variable definitions
%>