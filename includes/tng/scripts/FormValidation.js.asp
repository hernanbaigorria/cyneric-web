<%
'
'	Copyright (c) InterAKT Online 2000-2005
'
If isEmpty(KT_CoreFunctions__ALREADYLOADED) Then
	KT_CoreFunctions__ALREADYLOADED = True
	KT_uploadErrorMsg = "<strong>File not found:</strong> <br />###<br /><strong>Please upload the includes/ folder to the testing server.</strong> <br /><a href=""http://www.interaktonline.com/error/?error=upload_includes"" onclick=""return confirm('Some data will be submitted to InterAKT. Do you want to continue?');"" target=""KTDebugger_0"">Online troubleshooter</a>"
	Sub KT_createGlobalFSO()
		If Not isObject(KT_FSO) Then
			ExecuteGlobal "Set KT_FSO = Server.CreateObject(""Scripting.FileSystemObject"")"	
		End If	
	End Sub
	Function KT_SetPathSessions()
		KT_createGlobalFSO
		' sets 2 sessions: KT_AbsolutePathToRootFolder and KT_SiteURL
		
		' In order to know which one is the root folder, must check for (a) particular folder(s) 
		' that we know for sure that is(are) located in the root folder of the site
		Dim SearchForFolderName: SearchForFolderName = "includes\common" 

		Dim url: url = Request.ServerVariables("URL")
		' cut the trailing /
		LastSeparator = InStrRev(url, "/")
		If LastSeparator > 0 Then
			url = left(url, LastSeparator-1)
		End If

		Dim path: path = Server.MapPath(".") & "\"
		' cut the trailing \
		LastSeparator = InStrRev(path, "\")
		If LastSeparator > 0 Then
			path = left(path, LastSeparator-1)
		End If
		
		Dim prefix: prefix = ""
		Dim found: found = False
		Do while not Found
			If KT_FSO.FolderExists (path &  "\" & SearchForFolderName) Then
				Found = true
				Exit do
			Else
				' remove one folder lever both from path and url
				LastSeparator = InStrRev(url, "/")
				If LastSeparator > 0 Then
					url = left(url, LastSeparator-1)
				Else
					found = true ' force the exit from loop	
				End If
		
				LastSeparator = InStrRev(path, "\")
				If LastSeparator > 0 Then
					path = left(path, LastSeparator-1)
					prefix = prefix & "..\"
				Else
					found = true ' force the exit from loop	
				End If				 	
			End If						
		Loop

		If prefix = "" Then
			prefix = "."
		End If
		If found Then
			Session("KT_SitePath") = url
			Session("KT_AbsolutePathToRootFolder") = KT_FSO.GetAbsolutePathName(Server.MapPath(".") & "\" & prefix) & "\"
		Else
			KT_GetAbsolutePathToRootFolder = ""
		End If	
	End Function

	' retrieves the path on disk to the site root (eg C:\www\sites\MYSITE\)
	Function KT_GetAbsolutePathToRootFolder()
		If Session("KT_AbsolutePathToRootFolder") = ""  Then
			KT_SetPathSessions
		End If
		KT_GetAbsolutePathToRootFolder	= Session("KT_AbsolutePathToRootFolder")
	End Function
	

	Sub KT_LoadASPFiles (arrPathsRelativeToRoot)
		KT_createGlobalFSO
		absolutePathToRootFolder = KT_GetAbsolutePathToRootFolder()
		
		On Error Resume Next	
		Dim i
		For i=0 to ubound(arrPathsRelativeToRoot)		
			pathRelativeToRootFolder = arrPathsRelativeToRoot(i)
			absolutePathToFile = absolutePathToRootFolder & replace(pathRelativeToRootFolder, "/", "\")
			If KT_FSO.FileExists(absolutePathToFile) Then
				' read the file content
				Dim f: Set f = KT_FSO.OpenTextFile(absolutePathToFile, 1, False)
				content = f.ReadAll
				f.Close
				Set f = nothing
				
				If instr(pathRelativeToRootFolder, "KT_common.asp") <> 0 Then
					content = mid(content, 1, instr(content, "<S" & "CRIPT language=""jscript"" runat=""server")-1)
				End If
				If instr(pathRelativeToRootFolder, "tNG.inc.asp") <> 0 Then
					content = mid(content, 1, instr(content, "<S" & "CRIPT language=""jscript"" runat=""server")-1)
				End If								
				' replace ASP tags 
				execcontent = replace (content, "<" & "%", "")
				execcontent = replace (execcontent, "%" & ">", "")
				ExecuteGlobal(execcontent)
			Else
				Session.Contents.RemoveAll
				Response.write replace(KT_uploadErrorMsg, "###", pathRelativeToRootFolder)
				Response.End()
			End If
			If err.number<>0 Then
				Response.write "<br><span style=""color:red"">Error loading file '" & pathRelativeToRootFolder & "'<br>" & err.description & "</font>"
				Response.End()
			End If
		Next
		On Error GoTo 0
	End Sub

End If

	KT_LoadASPFiles Array("includes/common/lib/resources/KT_Resources.asp")
%>
<%
'
'	Copyright (c) InterAKT Online 2000-2005
'
	d = "tNG_FormValidation"
%>
//Javascript UniVAL Resources
UNI_Messages = {};
UNI_Messages['required'] 				= '<%= KT_escapeJS(KT_getResource("REQUIRED", d, null))%>';
UNI_Messages['type'] 					= '<%= KT_escapeJS(KT_getResource("TYPE", d, null))%>';
UNI_Messages['format'] 					= '<%= KT_escapeJS(KT_getResource("FORMAT", d, null))%>';
UNI_Messages['text_'] 					= '<%= KT_escapeJS(KT_getResource("TEXT_", d, null))%>';
UNI_Messages['text_email'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_EMAIL", d, null))%>';
UNI_Messages['text_cc_generic']			= '<%= KT_escapeJS(KT_getResource("TEXT_CC_GENERIC", d, null))%>';
UNI_Messages['text_cc_visa'] 			= '<%= KT_escapeJS(KT_getResource("TEXT_CC_VISA", d, null))%>';
UNI_Messages['text_cc_mastercard'] 		= '<%= KT_escapeJS(KT_getResource("TEXT_CC_MASTERCARD", d, null))%>';
UNI_Messages['text_cc_americanexpress'] = '<%= KT_escapeJS(KT_getResource("TEXT_CC_AMERICANEXPRESS", d, null))%>';
UNI_Messages['text_cc_discover'] 		= '<%= KT_escapeJS(KT_getResource("TEXT_CC_DISCOVER", d, null))%>';
UNI_Messages['text_cc_dinersclub'] 		= '<%= KT_escapeJS(KT_getResource("TEXT_CC_DINERSCLUB", d, null))%>';
UNI_Messages['text_zip_generic'] 		= '<%= KT_escapeJS(KT_getResource("TEXT_ZIP_GENERIC", d, null))%>';
UNI_Messages['text_zip_us5'] 			= '<%= KT_escapeJS(KT_getResource("TEXT_ZIP_US5", d, null))%>';
UNI_Messages['text_zip_us9'] 			= '<%= KT_escapeJS(KT_getResource("TEXT_ZIP_US9", d, null))%>';
UNI_Messages['text_zip_canada'] 		= '<%= KT_escapeJS(KT_getResource("TEXT_ZIP_CANADA", d, null))%>';
UNI_Messages['text_zip_uk'] 			= '<%= KT_escapeJS(KT_getResource("TEXT_ZIP_UK", d, null))%>';
UNI_Messages['text_phone'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_PHONE", d, null))%>';
UNI_Messages['text_ssn'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_SSN", d, null))%>';
UNI_Messages['text_url'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_URL", d, null))%>';
UNI_Messages['text_ip'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_IP", d, null))%>';
UNI_Messages['text_color_hex'] 			= '<%= KT_escapeJS(KT_getResource("TEXT_COLOR_HEX", d, null))%>';
UNI_Messages['text_color_generic'] 		= '<%= KT_escapeJS(KT_getResource("TEXT_COLOR_GENERIC", d, null))%>';
UNI_Messages['numeric_'] 				= '<%= KT_escapeJS(KT_getResource("NUMERIC_", d, null))%>';
UNI_Messages['numeric_int'] 			= '<%= KT_escapeJS(KT_getResource("NUMERIC_INT", d, null))%>';
UNI_Messages['numeric_int_positive'] 	= '<%= KT_escapeJS(KT_getResource("NUMERIC_INT_POSITIVE", d, null))%>';
UNI_Messages['numeric_zip_generic'] 	= '<%= KT_escapeJS(KT_getResource("TEXT_ZIP_GENERIC", d, null))%>';
UNI_Messages['double_float'] 			= '<%= KT_escapeJS(KT_getResource("DOUBLE_FLOAT", d, null))%>';
UNI_Messages['double_float_positive'] 	= '<%= KT_escapeJS(KT_getResource("DOUBLE_FLOAT_POSITIVE", d, null))%>';
UNI_Messages['date_'] 					= '<%= KT_escapeJS(KT_getResource("DATE_", d, null))%>';
UNI_Messages['date_date'] 				= '<%= KT_escapeJS(KT_getResource("DATE_DATE", d, null))%>';
UNI_Messages['date_time'] 				= '<%= KT_escapeJS(KT_getResource("DATE_TIME", d, null))%>';
UNI_Messages['date_datetime'] 			= '<%= KT_escapeJS(KT_getResource("DATE_DATETIME", d, null))%>';
UNI_Messages['mask_'] 					= '<%= KT_escapeJS(KT_getResource("MASK_", d, null))%>';
UNI_Messages['regexp_'] 				= '<%= KT_escapeJS(KT_getResource("REGEXP_", d, null))%>';
UNI_Messages['text_min'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_MIN", d, null))%>';
UNI_Messages['text_max'] 				= '<%= KT_escapeJS(KT_getResource("TEXT_MAX", d, null))%>';
UNI_Messages['text_between'] 			= '<%= KT_escapeJS(KT_getResource("TEXT_BETWEEN", d, null))%>';
UNI_Messages['other_min'] 				= '<%= KT_escapeJS(KT_getResource("OTHER_MIN", d, null))%>';
UNI_Messages['other_max'] 				= '<%= KT_escapeJS(KT_getResource("OTHER_MAX", d, null))%>';
UNI_Messages['other_between'] 			= '<%= KT_escapeJS(KT_getResource("OTHER_BETWEEN", d, null))%>';
UNI_Messages['form_was_modified'] 		= '<%= KT_escapeJS(KT_getResource("FORM_WAS_MODIFIED", d, null))%>';