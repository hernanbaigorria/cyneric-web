<%
'
'	Copyright (c) InterAKT Online 2000-2005
'
Class tNG_ManyToMany
	Public tNG  ' object
	Public table
	Public pkName
	Public fkName
	Public fkReference
	Public errorObj
	
	Private Sub Class_Initialize()
		Set this = Me
	End Sub
	
	Private Sub Class_Terminate()
	End Sub

	Public Sub Init (ByRef tNG__param)
		Set tNG = tNG__param
		table = ""
		pkName = ""
		fkName = ""
		fkReference = ""
		Set errorObj = nothing
	End Sub

	'===========================================
	' Inheritance
	'===========================================
	Public this
	
	Public Sub SetContextObject(ByRef objContext)
		Set this = objContext
	End Sub
	'===========================================
	' End Inheritance
	'===========================================
	
	Public Sub setTable (table__param)
		table = table__param
	End Sub

	Public Sub setPkName (pkName__param)
		pkName = pkName__param
	End Sub

	Public Sub setFkName (fkName__param)
		fkName = fkName__param
	End Sub
	
	Public Sub setFkReference (fkReference__param)
		fkReference = fkReference__param
	End Sub
	
	
	Public Function getValues()
		values = array()
		
		fkRef = fkReference
		idxRef = ""
		If KT_isSet(tNG.multipleIdx) Then
			idxRef = "_" & tNG.multipleIdx
			'idxRef = KT_preg_quote(idxRef)
		End If
		'fkRef = KT_preg_quote (fkRef)
		
		
		For each key in Request.Form
			If KT_preg_test("^" & fkRef & "_(\d+)" & idxRef & "$", key) Then
				'value = KT_getRealValue("POST", key)
				match1 = KT_preg_replace("^" & fkRef & "_(\d+)" & idxRef & "$", "$1", key)
				values = KT_array_push (values, match1)		
			End If
		Next
		getValues = values
	End Function
	
	Public Function getOldValues()
		Dim ret: ret = array()
		pk_value = tNG.getPrimaryKeyValue()
		pk_type = tNG.getColumnType (tNG.getPrimaryKey())
		pk_value = KT_escapeForSql (pk_value, pk_type)
		
		strSQL = "SELECT " & KT_escapeFieldName(fkName) & " FROM " &  table & " WHERE " & KT_escapeFieldName(pkName) & " = " & pk_value
		On Error Resume Next
		Set rs = tNG.connection.Execute (strSQL)
		If Err.number <> 0 Then
			Set errorObj = new tNG_Error
			errorObj.Init "TRIGGER_MESSAGE__MTM_SQL_ERROR", array(), array(err.Description, strSQL)
			getOldValues = null
			On Error GoTo 0
			Exit Function
		End If
		On Error GoTo 0
		While Not rs.EOF
			ret = KT_array_push(ret, rs.Fields.Item(fkName).Value)			
			rs.MoveNext()
		Wend
		getOldValues = ret
	End Function
	

	Public Function Execute()
		If fkReference = "" Then
			Set myErr = new tNG_Error
			myErr.Init "TRIGGER_MESSAGE__MTM_NO_REFERENCE", array(), array()
			Set Execute = myErr
			Exit Function
		End If
		pk_value = tNG.getPrimaryKeyValue()
		pk_type = tNG.getColumnType (tNG.getPrimaryKey())
		pk_value = KT_escapeForSql (pk_value, pk_type)

		values = this.getValues()
		oldValues = getOldValues()
		If KT_isSet(errorObj) Then
			Set Execute = errorObj
			Exit Function
		End If
		
		
		Dim i
		If ubound(oldValues) > -1 Then
			deleteValues = KT_array_diff(oldValues, values)
			If ubound(deleteValues) > -1 Then
				in_sql = ""
				For i = 0 to ubound(deleteValues)
					If i > 0 Then
						in_sql = in_sql & ","
					End If
					in_sql = in_sql & KT_escapeForSql(deleteValues(i), pk_type)
				Next
				strSQL = "DELETE FROM " & table & " WHERE " & KT_escapeFieldName(pkName) & " = " & pk_value & " AND " & KT_escapeFieldName(fkName) & " IN (" & in_sql & ")"
				On Error Resume Next
				tNG.connection.Execute (strSQL)
				On Error GoTo 0
			End If
		End If
		
		If ubound(values) > -1 Then	
			insertValues = KT_array_diff(values, oldValues)
			For i=0 to ubound(insertValues)
				value = KT_escapeForSql(insertValues(i), pk_type)
				strSQL = "INSERT INTO " & table & " ( " & KT_escapeFieldName(pkName) & " , " & KT_escapeFieldName(fkName) & " ) VALUES (" & pk_value & " , " & value & ")"
				On Error Resume Next
				tNG.connection.Execute (strSQL)
				If Err.number <> 0 Then
					Set myErr = new tNG_Error
					myErr.Init "TRIGGER_MESSAGE__MTM_SQL_ERROR", array(), array(err.Description, strSQL)
					Set Execute = myErr
					On Error GoTo 0
					Exit Function
				End If
				On Error GoTo 0
			Next
		End If
		
		Set Execute = nothing
	End Function
End Class	
	
%>