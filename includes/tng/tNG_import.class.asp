<%
'
'	Copyright (c) InterAKT Online 2000-2005
'
Class tNG_import
	Public multipleIdx
	Public executeSubSets
	Public KT_RESERVED 	' dictionary for dynamic fields
	
	Public hasHeader
	Public handleDuplicates
	Public source
	Public data
	Public headers
	Public uniqueKey
	Public importType
	Public importReference
	Public lineStart
	
	Public noSkip
	Public uploadObj

		
	Private Sub Class_Initialize()
		Set this = Me
		Set parent = new tNG_multiple
		parent.SetContextObject Me
	End Sub

	Private Sub Class_Terminate()
		
	End Sub

	Public Sub Init(ByRef connection__par)
		parent.Init  connection__par
		executeSubSets = True
		
		transactionType = "_import"
		exportRecordset = false
		multipleIdx = null		
		
		Set source = Server.CreateObject("Scripting.Dictionary")
		Set headers = Server.CreateObject("Scripting.Dictionary")
		Set data = Server.CreateObject("Scripting.Dictionary")
		hasHeader = false
		lineStart = 0
		
		noSkip = 0
		Set uploadObj = nothing
	End Sub


	Public Sub setSource(type__param, reference__param)
		source("type") = UCase(Trim(type__param))
		source("reference") = Trim(reference__param)
	End Sub
	
	Public Sub setUniqueKey(uniqueKey__param)
		If uniqueKey__param <> "" Then
			If Not columns.Exists(uniqueKey__param) Then
				Response.write "tNG_Import.setUniqueKey:<br /> Unique Key Column " & uniqueKey__param & " is not part of the current transaction."
				Response.End()
			End If
		End If	
		uniqueKey = uniqueKey__param
	End Sub
	
	Public Sub setHandleDuplicates(	handleDuplicates__param)
		handleDuplicates = UCase(Trim(handleDuplicates__param))
	End Sub

	Public Sub addColumn (colName__param, type__param, method__param, reference__param, defaultValue__param)
		parent.addColumn colName__param, type__param, method__param, reference__param
		Set cols = columns
		If method__param = "VALUE" Then
			cols(colName__param)("default") = reference__param
		Else
			If method__param = importType Then
				Set localHeaders = headers
				localHeaders(reference__param) = reference__param
			End If	
			cols(colName__param)("default") = defaultValue__param
		End If
	End Sub

	Public Function prepareData()
		Response.write "tNG_import.prepareData:<br />Method must be implemented in inherited class."
		Response.end()
	End Function

	Public Function prepareSQL()
		tNG_log__log "tNG_Import", "prepareSQL", "begin"
		Dim ret: Set ret = nothing
		
		Set ret = this.prepareData()
		If Not KT_isSet(ret) Then
			noSuccess = 0
			noSkip = 0
			
			Dim failed: failed = false
			Dim line: line = lineStart

			Dim continue
			Dim isInsert
			Dim tNGi
			Dim tNGindex: tNGindex = 1
		
			Dim i, j, k

			Set multTNGsObj = multTNGs 
			For each k in data
				Set dataarr = data(k)
				
				continue = True
				skipped = false
				line = line + 1

				If continue Then
					' export this values line to be available for KT_getRealValue and KT_DynamicData
					ExecuteGlobal "Set KT_IMPORT = nothing"
					Set KT_IMPORT = dataarr
					ExecuteGlobal "Set "  & importReference  & " = KT_IMPORT"
					ExecuteGlobal importReference & "_LINE = " & line
									
					
					continue = True
					isInsert = True
					uniqueColName = uniqueKey
					If uniqueColName <> "" Then
						Set cols = columns
						Set uniqueColDetails = this.computeMultipleValues(cols(uniqueColName), tNGindex)
						If uniqueColDetails("value") <> "" Then
						
							sqlString = "SELECT " & KT_escapeFieldName(uniqueColName) & " FROM " & this.getTable() & " WHERE " & KT_escapeFieldName(uniqueColName) & "=" & KT_escapeForSql(uniqueColDetails("value"), uniqueColDetails("type"))
							On Error Resume Next
							Set rs = connection.Execute(sqlString)
							If err.Number <> 0 Then
								failed = true
								Set ret = new tNG_error
								ret.Init "IMPORT_SQL_ERROR", array(), array(sqlString, err.Description)
								tNG_log__log "KT_ERROR", null, null
								On Error GoTo 0
								Exit For
							End If
							On Error GoTo 0
								
							If Not rs.EOF Then
								' duplicates found
								If handleDuplicates = "SKIP" Then
									isInsert = false
									noSkip = noSkip + 1
									continue = False
								ElseIf handleDuplicates = "UPDATE" Then
									' update case
									isInsert = false
									Set tNGi = new tNG_update
									tNGi.Init connectionString
								ElseIf handleDuplicates = "SKIPWITHERROR" Then
									' throw error case
									isInsert = false
									skipped = true
									noSkip = noSkip + 1
									Set tNGi = new tNG_insert
									tNGi.Init connectionString
									Set insErr = new tNG_error
									insErr.Init importType & "_IMPORT_DUPLICATE_ERROR", array(line, uniqueColDetails("value"), uniqueColName), array()
									tNGi.setError insErr
								End If
							End If	
						End If
					End If		
					
					If continue Then
						If isInsert Then
							Set tNGi = new tNG_insert
							tNGi.Init connectionString						
						End If
						
						Set disp = this.getDispatcher()
						tNGi.setDispatcher disp
						tNGi.multipleIdx = tNGindex
						
						' add the current transaction
						Set multTNGsObj(tNGindex-1) = tNGi							
						
						' register triggers
						Set mT = multTriggers
						For each j in mT
							currParams = mT(j)
							tNGi.registerConditionalTrigger currParams(0), currParams(1)
						Next
						
						' add columns
						tNGi.setTable table
						Set cols = columns
						For each colName in cols
							Set colDetails = cols(colName)

							Set colDetails = this.computeMultipleValues(colDetails, tNGindex)
							cols(colName)("value") = colDetails("value")
							If tNGi.transactionType = "_update" Then
								If colName <> uniqueColName Then
									tNGi.addColumn colName, colDetails("type"), colDetails("method"), colDetails("reference")
								End If	
							Else
								tNGi.addColumn colName, colDetails("type"), colDetails("method"), colDetails("reference"), colDetails("default")
							End If	
						Next
						
						Set pk = primaryKeyColumn
						If tNGi.transactionType = "_update" Then
							tNGi.setPrimaryKey uniqueColName, uniqueColDetails("type"), "VALUE", uniqueColDetails("value")
						Else
							tNGi.setPrimaryKey primaryKey, pk("type"), "", ""
						End If
						
						tNGi.compileColumnsValues()
						
						If KT_isSet(this.getError()) Then
							tNGi.setError(this.getError())
						End If
						
						
						tNGi.setStarted True
						tNGi.doTransaction()

						If Not skipped Then
							If KT_isSet(tNGi.getError()) Then
								Set myErr = tNGi.getError()
								Set fieldErrors = myErr.fieldErrors
								tmp_all_errmsg = ""
								tmp_unique_details = ""
								
								If uniqueColName <> "" Then
									If uniqueColDetails("value") <> "" Then
										tmp_unique_details = " (" & uniqueColName & " = " & uniqueColDetails("value") & ")"
									End If
								End If
								For each tmp_col in fieldErrors
									tmp_errmsg = fieldErrors(tmp_col)
									tmp_all_errmsg = tmp_all_errmsg & vbNewLine & "<br />&nbsp;&nbsp;&nbsp;- " & tmp_col & " : " & tmp_errmsg
								Next
								If tmp_all_errmsg = "" Then
									tmp_all_errmsg = myErr.getDetails()
								End If
								lineErr = line & tmp_unique_details
								Set newErr = new tNG_error
								newErr.Init importType & "_IMPORT_LINE_ERROR", array(lineErr, tmp_all_errmsg), array()
								tNGi.setError newErr
								failed = True
							Else
								noSuccess = noSuccess + 1
								If this.getPrimaryKey() = tNGi.getPrimaryKey() Then
									Set pk = primaryKeyColumn
									pk("value") = tNGi.getPrimaryKeyValue()
								End If	
							End If
						End If
						
						tNGindex = tNGindex + 1
					End If ' If continue 2
				End If ' if continue
			Next
			
			If Not failed Then
				For each i in multTNGsObj
					Set tNGi = multTNGsObj(i)
					If KT_isSet(tNGi.getError()) Then
						failed = True
						Set ret = new tNG_error
						ret.Init "IMPORT_SKIPPED", array(), array()
						tNG_log__log "KT_ERROR", null, null
						Exit For
					End If	
				Next				
			End If
			
			If failed Then
				If Not KT_isSet(ret) Then
					Set ret = new tNG_error
					ret.Init "IMPORT_ERROR", array(), array()
					tNG_log__log "KT_ERROR", null, null
				End If
				If executeSubSets = false Then
					For each i in multTNGsObj
						Set tNGi = multTNGsObj(i)
						If Not KT_isSet(tNGi.getError()) Then
							tNGi.setError ret
							tNGi.executeTriggers "ERROR"
						End If	
					Next
				End If				
			End If
			
			If executeSubSets = false Then
				noSuccess = 0
			End If
		Else
			tNG_log__log "KT_ERROR", null, null	
		End If
			
		tNG_log__log "tNG_import", "prepareSQL", "end"
		Set prepareSQL = ret
	End Function
	
	
	
	
	Public Function computeMultipleValues ( ByRef colDetails, i)
		Set colDet = KT_cloneObject(colDetails)
		If colDet("method") = "VALUE" Then
			reference = colDet("reference")
			value = KT_getRealValue(colDet("method"), colDet("reference"))
		ElseIf colDet("method") = importType Then
			Set localHeaders = headers
			value = KT_getRealValue(colDetails("method"), localHeaders(colDetails("reference")))
			reference = localHeaders(colDetails("reference"))
		Else
			reference = colDet("reference") & "_" & i
			value = KT_getRealValue(colDet("method"), colDet("reference")  & "_" & i)
			If Not KT_isSet(value) Then
				reference = colDet("reference")
				value = KT_getRealValue(colDet("method"), colDet("reference"))
			End If
		End If
		colDet("value") = value
		colDet("reference") = reference
		Set computeMultipleValues = colDet
	End Function	
	
	
	Public Function getErrorMsg()
		ret_warning = ""
		If noSkip <> 0 Then
			ret_warning = " " & KT_getResource("IMPORT_OPERATIONS_SKIPPED", "tNG", array(noSkip))
		End If
		
		Dim ret
		ret = parent.getErrorMsg()
		ret(0) = ret(0) & ret_warning
		ret(0) = trim(ret(0))
		
		getErrorMsg = ret
	End Function
	
	Public Function saveData()
		Set saveData = nothing
	End Function

	Public Function getLocalRecordset()
		Set getLocalRecordset = parent.getLocalRecordset()
	End Function	
	
	
	
	'===========================================
	' Inheritance
	'===========================================
	Public this
	Public parent

	Public Sub SetContextObject(ByRef objContext)
		Set this = objContext
		parent.SetContextObject objContext
	End Sub

	'  Inherited properties from tNG_multiple
	'-------------------------------------------
	' multTNGs
	Public Property Get multTNGs
		Set multTNGs = parent.multTNGs
	End Property
	Public Property Set multTNGs(ByRef multTNGs__par)
		Set parent.multTNGs = multTNGs__par
	End Property

	' multTriggers
	Public Property Get multTriggers
		Set multTriggers = parent.multTriggers
	End Property
	Public Property Set multTriggers(ByRef multTriggers__par)
		Set parent.multTriggers = multTriggers__par
	End Property

	' noSuccess
	Public Property Get noSuccess
		noSuccess = parent.noSuccess
	End Property
	Public Property Let noSuccess(noSuccess__par)
		parent.noSuccess = noSuccess__par
	End Property

	' errorWasCompiled
	Public Property Get errorWasCompiled
		errorWasCompiled = parent.errorWasCompiled
	End Property
	Public Property Let errorWasCompiled(errorWasCompiled__par)
		parent.errorWasCompiled = errorWasCompiled__par
	End Property

	'  Inherited properties as Property
	'-------------------------------------------
	Public Property Get columns
		Set columns = parent.columns
	End Property
	Public Property Set columns(ByRef columns__par)
		Set parent.columns = columns__par
	End Property
	Public Property Get primaryKey
		primaryKey = parent.primaryKey
	End Property
	Public Property Let primaryKey(primaryKey__par)
		parent.primaryKey = primaryKey__par
	End Property
	Public Property Get primaryKeyColumn
		Set primaryKeyColumn = parent.primaryKeyColumn
	End Property
	Public Property Set primaryKeyColumn(ByRef primaryKeyColumn__par)
		Set parent.primaryKeyColumn = primaryKeyColumn__par
	End Property
	Public Property Get pkName
		pkName = parent.pkName
	End Property
	Public Property Let pkName(pkName__par)
		parent.pkName = pkName__par
	End Property
	Public Property Get savedData
		Set savedData = parent.savedData
	End Property
	Public Property Set savedData(ByRef savedData__par)
		Set parent.savedData = savedData__par
	End Property
	Public Property Get table
		table = parent.table
	End Property
	Public Property Let table(table__par)
		parent.table = table__par
	End Property
	Public Property Get connection
		Set connection = parent.connection
	End Property
	Public Property Set connection(ByRef connection__par)
		Set parent.connection = connection__par
	End Property
	' connectionString
	Public Property Get connectionString
		connectionString = parent.connectionString
	End Property
	Public Property Let connectionString(connectionString__par)
		parent.connectionString = connectionString__par
	End Property	
	Public Property Get sql
		sql = parent.sql
	End Property
	Public Property Let sql(sql__par)
		parent.sql = sql__par
	End Property
	Public Property Get triggers
		Set triggers = parent.triggers
	End Property
	Public Property Set triggers(ByRef triggers__par)
		Set parent.triggers = triggers__par
	End Property
	Public Property Get started
		started = parent.started
	End Property
	Public Property Let started(started__par)
		parent.started = started__par
	End Property
	Public Property Get transactionType
		transactionType = parent.transactionType
	End Property
	Public Property Let transactionType(transactionType__par)
		parent.transactionType = transactionType__par
	End Property
	Public Property Get exportRecordset
		exportRecordset = parent.exportRecordset
	End Property
	Public Property Let exportRecordset(exportRecordset__par)
		parent.exportRecordset = exportRecordset__par
	End Property
	Public Property Get transactionResult
		Set transactionResult = parent.transactionResult
	End Property
	Public Property Set transactionResult(ByRef transactionResult__par)
		Set parent.transactionResult = transactionResult__par
	End Property
	Public Property Get errorObj
		Set errorObj = parent.errorObj
	End Property
	Public Property Set errorObj(ByRef errorObj__par)
		Set parent.errorObj = errorObj__par
	End Property
	Public Property Get dispatcher
		Set dispatcher = parent.dispatcher
	End Property
	Public Property Set dispatcher(ByRef dispatcher__par)
		Set parent.dispatcher = dispatcher__par
	End Property

	'  Inherited methods
	'-------------------------------------------
	Public Function registerTrigger(params)
		registerTrigger = parent.registerTrigger(params)
	End Function

	Public Function registerConditionalTrigger(condition, params)
		registerConditionalTrigger = parent.registerConditionalTrigger(condition, params)
	End Function

	Public Sub compileError()
		parent.compileError 
	End Sub
    
    ' breaking signature for this method
	Public Function getFieldError(fName)
		getFieldError = parent.parent.getFieldError(fName)
	End Function

	Public Function getFakeRecordset(ByRef fakeArr)
		Set getFakeRecordset = parent.getFakeRecordset( fakeArr)
	End Function

	Public Function getRecordset()
		Set getRecordset = parent.getRecordset()
	End Function

	Public Function getSavedValue(colName)
		getSavedValue = parent.getSavedValue(colName)
	End Function


	Public Function executeTransaction()
		executeTransaction = parent.executeTransaction()
	End Function


	Public Function evaluateNumeric(expr)
		evaluateNumeric = parent.evaluateNumeric(expr)
	End Function

	Public Sub setColumnValue(colName, colValue)
		parent.setColumnValue colName, colValue
	End Sub

	Public Sub setRawColumnValue(colName, colValue)
		parent.setRawColumnValue colName, colValue
	End Sub

	Public Function getColumnValue(colName)
		getColumnValue = parent.getColumnValue(colName)
	End Function

	Public Function getColumnReference(colName)
		getColumnReference = parent.getColumnReference(colName)
	End Function

	Public Function getColumnType(colName)
		getColumnType = parent.getColumnType(colName)
	End Function

	Public Sub setTable(tableName)
		parent.setTable tableName
	End Sub

	Public Function getTable()
		getTable = parent.getTable()
	End Function

	Public Sub setPrimaryKey (colName__param, type__param, method__param, reference__param)
		parent.setPrimaryKey colName__param, type__param, method__param, reference__param
	End Sub

	Public Function getPrimaryKey()
		getPrimaryKey = parent.getPrimaryKey()
	End Function

	Public Function getPrimaryKeyValue()
		getPrimaryKeyValue = parent.getPrimaryKeyValue()
	End Function

	Public Sub compileColumnsValues()
		parent.compileColumnsValues 
	End Sub


	Public Function getFakeRsArr()
		Set getFakeRsArr = parent.getFakeRsArr()
	End Function

	Public Function parseSQLError (sql, error)
		Set parseSQLError = parent.parseSQLError(sql, error)
	End Function

	Public Function afterUpdateField (fieldName, fieldValue)
		Set afterUpdateField = parent.afterUpdateField(fieldName, fieldValue)
	End Function

	Public Sub setDispatcher(ByRef dispatcher__par)
		parent.setDispatcher  dispatcher__par
	End Sub

	Public Function getDispatcher()
		Set getDispatcher = parent.getDispatcher()
	End Function

	Public Function exportsRecordset()
		exportsRecordset = parent.exportsRecordset()
	End Function

	Public Function executeTriggers (i_triggerType)
		Set executeTriggers = parent.executeTriggers(i_triggerType)
	End Function


	Public Function getTransactionType()
		getTransactionType = parent.getTransactionType()
	End Function

	Public Function isStarted()
		isStarted = parent.isStarted()
	End Function

	Public Sub setStarted (started__param)
		parent.setStarted started__param
	End Sub

	Public Sub setSQL (sql__param)
		parent.setSQL sql__param
	End Sub

	Public Function postExecuteSql()
		Set postExecuteSql = parent.postExecuteSql()
	End Function

	Public Sub setError (ByRef errorObj__param)
		parent.setError  errorObj__param
	End Sub

	Public Function getError()
		Set getError = parent.getError()
	End Function

	Public Function doTransaction()
		doTransaction = parent.doTransaction()
	End Function

	Public Sub rollBackTransaction(ByRef errorObj__param)
		parent.rollBackTransaction  errorObj__param
	End Sub

End Class
%>